<?php

namespace Fwok\Routes;

use Phalcon\Mvc\Micro\Collection;

class RoutesBase extends Collection
{
    /**
     * Internal function to add a handler to the group
     *
     * @param string|array $method
     * @param string $routePattern
     * @param mixed $handler
     * @param string $name
     * @param array $allowedRoles
     * @param array $deniedRoles
     * @return $this|void
     */
    protected function _addMap($method, $routePattern, $handler, $name, $allowedRoles = [ ], $deniedRoles = [ ])
    {
        $this->_handlers[] = [ $method, $routePattern, $handler, $name, $allowedRoles, $deniedRoles ];

        return $this;
    }

    /**
     * Maps a route to a handler that only matches if the HTTP method is GET
     *
     * @param string $routePattern
     * @param callable $handler
     * @param string $name
     * @param array $allowedRoles
     * @param array $deniedRoles
     * @return \Fwok\Routes\RoutesBase
     */
    public function get($routePattern, $handler, $name = null, $allowedRoles = [ ], $deniedRoles = [ ])
    {
        $this->_addMap("GET", $routePattern, $handler, $name, $allowedRoles, $deniedRoles);
    }

    /**
     * Maps a route to a handler that only matches if the HTTP method is POST
     *
     * @param string $routePattern
     * @param callable $handler
     * @param string $name
     * @param array $allowedRoles
     * @param array $deniedRoles
     * @return \Fwok\Routes\RoutesBase
     */
    public function post($routePattern, $handler, $name = null, $allowedRoles = [ ], $deniedRoles = [ ])
    {
        $this->_addMap("POST", $routePattern, $handler, $name, $allowedRoles, $deniedRoles);
    }

    /**
     * Maps a route to a handler that only matches if the HTTP method is PUT
     *
     * @param string $routePattern
     * @param callable $handler
     * @param string $name
     * @param array $allowedRoles
     * @param array $deniedRoles
     * @return \Fwok\Routes\RoutesBase
     */
    public function put($routePattern, $handler, $name = null, $allowedRoles = [ ], $deniedRoles = [ ])
    {
        $this->_addMap("PUT", $routePattern, $handler, $name, $allowedRoles, $deniedRoles);
    }

    /**
     * Maps a route to a handler that only matches if the HTTP method is PATCH
     *
     * @param string $routePattern
     * @param callable $handler
     * @param string $name
     * @param array $allowedRoles
     * @param array $deniedRoles
     * @internal param array $allowed_Roles
     * @return \Fwok\Routes\RoutesBase
     */
    public function patch($routePattern, $handler, $name = null, $allowedRoles = [ ], $deniedRoles = [ ])
    {
        $this->_addMap("PATCH", $routePattern, $handler, $name, $allowedRoles, $deniedRoles);
    }

    /**
     * Maps a route to a handler that only matches if the HTTP method is HEAD
     *
     * @param string $routePattern
     * @param callable $handler
     * @param string $name
     * @param array $allowedRoles
     * @param array $deniedRoles
     * @internal param array $allowed_Roles
     * @return \Fwok\Routes\RoutesBase
     */
    public function head($routePattern, $handler, $name = null, $allowedRoles = [ ], $deniedRoles = [ ])
    {
        $this->_addMap("HEAD", $routePattern, $handler, $name, $allowedRoles, $deniedRoles);
    }

    /**
     * Maps a route to a handler that only matches if the HTTP method is DELETE
     *
     * @param string $routePattern
     * @param callable $handler
     * @param string $name
     * @param array $allowedRoles
     * @param array $deniedRoles
     * @return \Fwok\Routes\RoutesBase
     */
    public function delete($routePattern, $handler, $name = null, $allowedRoles = [ ], $deniedRoles = [ ])
    {
        $this->_addMap("DELETE", $routePattern, $handler, $name, $allowedRoles, $deniedRoles);
    }

    /**
     * Maps a route to a handler that only matches if the HTTP method is OPTIONS
     *
     * @param string $routePattern
     * @param callable $handler
     * @param string $name
     * @param array $allowedRoles
     * @param array $deniedRoles
     * @internal param array $allowed_Roles
     * @return \Fwok\Routes\RoutesBase
     */
    public function options($routePattern, $handler, $name = null, $allowedRoles = [ ], $deniedRoles = [ ])
    {
        $this->_addMap("OPTIONS", $routePattern, $handler, $name, $allowedRoles, $deniedRoles);
    }


}
