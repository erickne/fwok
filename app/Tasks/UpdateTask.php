<?php

use Fwok\Tasks\TaskBase;

class UpdateTask extends TaskBase
{
    public function mainAction()
    {
        echo "Main Backup Task";
    }

    public function composerAppAction()
    {
        echo "Update Composer";
        $commands[] = sprintf(
            'composer --no-ansi --no-interaction --no-progress --working-dir=%s install %s'
            , TMP_DIR
            , (defined('COMPOSER_OPTIONS')) ? COMPOSER_OPTIONS : ''
        );
        if (defined('COMPOSER_HOME') && is_dir(COMPOSER_HOME)) {
            putenv('COMPOSER_HOME=' . COMPOSER_HOME);
        }
    }

    public function composerFwokClientAction()
    {
        echo "Update Composer Ultra Fwok Client";
    }

    public function clearCacheAction()
    {
        $i = 0;
        echo "Removing models data cache... ";
        $files = glob(APPLICATION_PATH . '/cache/Data/*');
        ob_flush();
        foreach ($files as $file) {
            if (is_file($file)) {
                if (unlink($file)) {
                    $i++;
                };
            }
        }
        echo "$i files removed.\n";
        ob_flush();
        
        $i = 0;
        echo "Removing models metadata cache...";
        $files = glob(APPLICATION_PATH . '/cache/Metadata/*');
        ob_flush();
        foreach ($files as $file) {
            if (is_file($file)) {
                if (unlink($file)) {
                    $i++;
                };
            }
        }
        echo "$i files removed.\n";
        ob_flush();

    }


}