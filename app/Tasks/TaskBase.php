<?php
namespace Fwok\Tasks;

use Phalcon\Di;

class TaskBase extends \Phalcon\CLI\Task
{
    public function initialize()
    {
        echo "\nInitializing CLI Task";
        echo "\n------------------------------------";
        echo "\nCalled Class: \t" . get_called_class();
        echo "\nTask Name: \t" . $this->dispatcher->getActiveMethod();
        echo "\nDate Started: \t" . date('Y-m-d H:i:s');
        echo "\n====================================\n\n";
    }

    public function mainAction()
    {
        echo "\nThis is the default task\n";
    }
    /**
     * Get the service via static call
     * @param $service
     * @return mixed
     */
    public function getService($service)
    {
        return $this->getDI()->get($service);
    }

    public static function getStaticService($service)
    {
        return Di::getDefault()->get($service);
    }

    public static function getStaticDI()
    {
        return Di::getDefault();
    }
}
