<?php

//namespace RDN\Tasks;

use Fwok\Plugins\Backup;
use Fwok\Tasks\TaskBase;

class BackupTask extends TaskBase
{
    public function mainAction()
    {
        echo 'Main Backup Task';
    }

    public function applicationAction()
    {
        echo "Starting application backup...\n";

        $destination_file = Backup::dumpAppFiles(APPLICATION_PATH.'/storage/files/');

        if($destination_file ) {
            echo 'Application backup successfull! Stored at '.$destination_file."\n";

            echo "Starting upload to Google Drive...\n";
            if(Backup::copyToGoogleDrive($destination_file,null,null)){
                echo 'Application backup file uploaded to GoogleDrive'."\n\n";
                if(unlink($destination_file)) {
                    echo 'File removed from server'."\n\n";
                }else{
                    echo 'Error removing file from server '.$destination_file."\n\n";
                }
            };
        }

        echo "Finished application backup.\n";

        return $destination_file;
    }

    /**
     * @return string
     * @throws \Fwok\Exceptions\SystemException
     */
    public function databaseAction()
    {
        echo "Starting database backup...\n";
        $destination_file = Backup::dumpDatabaseFile([
            'add-drop-table' => true,
            'compress' => 'Gzip'
        ]);

        if($destination_file ) {
            echo 'Database backup successfull! Stored at '.$destination_file."\n";

            echo "Starting upload to Google Drive...\n";
            if(Backup::copyToGoogleDrive($destination_file,null,null)){
                echo 'Database backup file uploaded to GoogleDrive'."\n";
                if(unlink($destination_file)) {
                    echo 'File removed from server'."\n\n";
                }else{
                    echo 'Error removing file from server '.$destination_file."\n\n";
                }
            };
        }

        echo "Finished database backup.\n";

        return $destination_file ;
    }

}