<?php

namespace Fwok\Routers;

class Route extends \Phalcon\Mvc\Router\Route
{
    protected $_allowedRoles;
    protected $_deniedRoles;

    public function setAllowedRoles($roles)
    {
        $this->_allowedRoles = $roles;
		return $this;
    }

    public function setDeniedRoles($roles)
    {
        $this->_deniedRoles = $roles;
        return $this;
    }

    public function getAllowedRoles()
    {
        return $this->_allowedRoles;
    }

    public function getDeniedRoles()
    {
        return $this->_deniedRoles;
    }
}