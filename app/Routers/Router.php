<?php

namespace Fwok\Routers;

use Fwok\Exceptions\Exception;
use Phalcon\Mvc\RouterInterface;

class Router extends \Phalcon\Mvc\Router implements RouterInterface
{
    /**
     * Adds a route to the router without any HTTP constraint
     *
     *<code>
     * use Phalcon\Mvc\Router;
     *
     * $router->add('/about', 'About::index');
     * $router->add('/about', 'About::index', ['GET', 'POST']);
     * $router->add('/about', 'About::index', ['GET', 'POST'], Router::POSITION_FIRST);
     *</code>
     * @param string $pattern
     * @param null $paths
     * @param null $httpMethods
     * @param int|mixed $position
     * @throws Exception
     * @return \Fwok\Routers\Route|\Phalcon\Mvc\Router\RouteInterface
     */
    public function add($pattern, $paths = null, $httpMethods = null, $position = Router::POSITION_LAST)
    {
        //var route;

        /**
         * Every route is internally stored as a Phalcon\Mvc\Router\Route
         */
        $route = new Route($pattern, $paths, $httpMethods);

        switch ($position) {

            case self::POSITION_LAST:
                $this->_routes[] = $route;
                break;

            case self::POSITION_FIRST:
                $this->_routes = array_merge([ $route ], $this->_routes);
                break;

            default:
                throw new Exception("Invalid route position");
        }

        return $route;
    }
}