<?php
/**
 * Created by PhpStorm.
 * User: erick
 * Date: 14/03/2017
 * Time: 16:42
 */
namespace Fwok\Validation;

use Fwok\Application\App;
use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;

class ValidatorBase extends Validator
{
    /**
     * Executes the validation
     *
     * @param Validation $validator
     * @param string $attribute
     * @return bool
     * @internal param Validation $validator
     */
    public function validate(Validation $validator, $attribute)
    {
        $this->validator = $validator;
        return true;
    }

    /**
     * Add an error message to validation process
     *
     * @param $message
     * @param Validation $validator
     * @param null $field
     * @param null $type
     * @return $this
     */
    public function appendStringMessage($message, Validation $validator, $field = null, $type = null)
    {
        $message = App::getTranslatedKey($message);

        $validator->appendMessage(new Message($message, $field, $type));

        return $this;
    }
}