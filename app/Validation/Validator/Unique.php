<?php

namespace Fwok\Validation\Validator;


use Fwok\Application\App;
use Fwok\Models\Criteria\CriteriaBase;
use Fwok\Models\ModelBase;
use Phalcon\Validation;
use Phalcon\Validation\Message;
use Fwok\Validation\ValidatorBase;
use RDN\Models\Round;

class Unique extends ValidatorBase
{
    CONST REQUIRED = 'required';
    CONST ACCEPT_DELETED = 'accept_deleted';
    CONST SEARCH_IN_BUSINESS = 'search_in_business';
    CONST FIELD = 'field';
    CONST MODEL = 'model';
    CONST ID = 'id';

    /**
     * Executes the validation
     *
     * @param Validation $validator
     * @param string|array $attribute
     * @return bool
     * @internal param Validation $validator
     */
    public function validate(Validation $validator, $attribute)
    {
        /** @var ModelBase $object */
        $object = $validator->getEntity();
        /**
         * A OPÇÃO DE ENVIAR O MODELO DIFERENTE NÃO ESTÁ IMPLEMENTADA!!!!!
         */
        if ($this->getOption(self::MODEL)) {
            $object_class_name = $this->getOption(self::MODEL);
            $object = new $object_class_name;
        } else {
            $object_class_name = $object::getClassName();
            $object = $validator->getEntity();
        }

        $search_in_business = $this->getOption(self::SEARCH_IN_BUSINESS) ? $this->getOption(self::SEARCH_IN_BUSINESS) : false;
        $object_name_up = strtoupper(str_replace('-', '_', constant($object_class_name . '::OBJECT_NAME')));


        /** @var CriteriaBase $query */
        $query = call_user_func($object_class_name . '::query');

        if ($search_in_business) {
            $query->searchInBusiness();
        }

        foreach (explode(',', $attribute) as $field) {
            $query->addWhereBinded($field . ' = ', $object->$field);
        }

        $object_query = $query->executeFirst();

        if ($object_query) { # Se encontrar um objeto com campos e valores iguais
            if ($object->id !== $object_query->id) {
                $this->appendStringMessage('VALIDATION_' . $object_name_up . '_ALREADY_EXISTS', $validator);
            }
        }

        if ($validator->getMessages()) {
            return false;
        }

        return true;
    }
}