<?php

namespace Fwok\Validation\Validator;


use Fwok\Application\App;
use Fwok\Models\Criteria\CriteriaBase;
use Fwok\Models\ModelBase;
use Phalcon\Validation;
use Phalcon\Validation\Message;
use Fwok\Validation\ValidatorBase;
use RDN\Models\Round;

class Existence extends ValidatorBase
{
    CONST REQUIRED = 'required';
    CONST ACCEPT_DELETED = 'accept_deleted';
    CONST SEARCH_IN_BUSINESS = 'search_in_business';
    CONST FIELD = 'field';
    CONST MODEL = 'model';
    CONST ID = 'id';

    /**
     * Executes the validation
     * 
     * 
     *
     *  $validator->add('product_id', new Existence([
     *      Existence::ID => $this->getProductId(),
     *      Existence::MODEL => Product::getClassName()
     *  ]));
     * 
     *
     * @param Validation $validator
     * @param string $attribute
     * @return bool
     * @internal param Validation $validator
     */
    public function validate(Validation $validator, $attribute)
    {
        $id = $this->getOption(self::ID)? $this->getOption(self::ID) : false;
        $required = $this->getOption(self::REQUIRED)? $this->getOption(self::REQUIRED) : true;
        $field = $this->getOption(self::FIELD)? $this->getOption(self::FIELD) : 'id';
        $accept_deleted = $this->getOption(self::ACCEPT_DELETED)? $this->getOption(self::ACCEPT_DELETED) : true;
        $search_in_business = $this->getOption(self::SEARCH_IN_BUSINESS)? $this->getOption(self::SEARCH_IN_BUSINESS) : false;

        $object_class_name = $this->getOption(self::MODEL);

        $object_name_up = strtoupper(str_replace('-','_',constant($object_class_name.'::OBJECT_NAME')));

        if (!$id and $required) {
            $this->appendStringMessage('VALIDATION_'.$object_name_up.'_IS_MANDATORY',$validator);
        }

        if ($id) {
            /** @var CriteriaBase $query */
            $query = call_user_func($object_class_name.'::query');

            if($search_in_business) {
                $query->searchInBusiness();
            }

            $object = $query->addWhereBinded($field.' = ',$id)->executeFirst();

            if (!$object) {
                $this->appendStringMessage('VALIDATION_'.$object_name_up.'_NOT_FOUND',$validator);
            } else {
                if ($object->getDeleted() and !$accept_deleted) {
                    $this->appendStringMessage('VALIDATION_'.$object_name_up.'_DELETED',$validator);
                }
            }
        }

        if ($validator->getMessages()) {
            return false;
        }

        return true;
    }
}