<?php
namespace Fwok\Exceptions;

class HTTPException extends BaseException
{
    const REDIRECT_TO_DEFAULT = 1;
    const REDIRECT_TO_MODULE = 2;
    public $devMessage;
    public $errorCode;
    public $response;
    public $additionalInfo;
    public $redirectUrl;

    public function __construct(
        $message,
        $redirect_to = null,
        $errorCode = null,
        $devMessage = null,
        $additionalInfo = null
    ) {
        $this->message = $message;
        $this->errorCode = $errorCode;
        $this->devMessage = $devMessage;
        $this->additionalInfo = $additionalInfo;

        if ($redirect_to == HTTPException::REDIRECT_TO_DEFAULT) {
            $this->setRedirectionToDefault();
        } elseif ($redirect_to == HTTPException::REDIRECT_TO_MODULE) {
            $this->setRedirectionToModule();
        } else {
            $this->setRedirectionToUrl($redirect_to);
        }

        return $this;
    }

    private function setRedirectionToDefault()
    {
        $this->redirectUrl = $this->getService('config')->redirection->default;
    }

    /*
     * If module = admin go to config->reditection->admin
     */

    private function setRedirectionToModule()
    {
        $module = $this->getService('config')->application->module;
        if (!$this->getService('config')->redirection->$module) {
            console('Could not identify config->redirection->' . $module . ' .Redirection to default.');
            $url = $this->getService('config')->redirection->default;
        } else {
            $url = $this->getService('config')->application->full_host . '/' . $this->getService(
                    'config'
                )->redirection->$module . '/' . $this->getMessage();
        }
        $this->redirectUrl = $url;
    }

    private function setRedirectionToUrl($url)
    {
        $this->redirectUrl = $url;
    }

    public function send()
    {
        echo($this->message);
        if ($this->getService('config')->debug->vardumpErrors) {
            var_dump($this);
        }

        return true;
    }
}