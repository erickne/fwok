<?php

namespace Fwok\Exceptions;

use Fwok\Application\ApplicationBase;
use Fwok\Plugins\Logger;
use Phalcon\DI;
use Phalcon\Exception;

class BaseException extends Exception
{
    const GENERIC_MESSAGE = 'Sorry but we found an unexpected condition. Ours developers have already been notified. ';
    public $e;
    public $message;
    public $dev_message;

    public function getService($service)
    {
        return $this->getDI()->get($service);
    }

    public static function getStaticService($service)
    {
        return Di::getDefault()->get($service);
    }

    public static function getStaticDI()
    {
        return Di::getDefault();
    }

    public function getDI()
    {
        return Di::getDefault();
    }

    public function __toString()
    {
        return get_class($this) . " '{$this->message}' in {$this->file}({$this->line})\n" . "{$this->getTraceAsString(
        )}";
    }

    /**
     * @param Exception|\Fwok\Exceptions\BaseException|\Exception $e
     */
    public static function runLog($e)
    {
        $type = get_class($e);

        $message_array = [
            "[$type] ===============================================================================",
            "[$type] Exception: " . $type,
            "[$type] Message: " . $e->getMessage(),
            "[$type] Developer Message: " . $e->dev_message,
            "[$type] Code: " . $e->getCode(),
            "[$type] File: " . $e->getFile(),
            "[$type] Line: " . $e->getLine(),
            "[$type] Trace: \r\n" . self::getExceptionTraceAsString($e),
        ];

        # Registra detalhes da exception
        //$logger = new Logger(APPLICATION_MODULE, Logger::TYPE_EXCEPTION);
        foreach ($message_array as $message) {
            Logger::shout($message, Logger::TYPE_EXCEPTION, Logger::CRITICAL, APPLICATION_MODULE);
        }

//        # Registra no log de alerta
//        $message = $type . ' [module=' . APPLICATION_MODULE . ']: ' . $e->getFile() . '(' . $e->getLine() . '):' . $e->getMessage();
//        Logger::shoutInAlert($message, Logger::CRITICAL);

    }

    /**
     * @param self|Exception|\PDOException $e
     */
    public static function dispatchException($e)
    {
        if (!defined('APPLICATION_PATH')) {
            die('Invalid APPLICATION PATH');
        }

        switch (get_class($e)) {
            case 'Fwok\Exceptions\BaseException':
            case 'Fwok\Exceptions\Exception':
                $e->send();
                break;
            case 'Phalcon\Exception':
                self::redirect($e, self::GENERIC_MESSAGE);
                break;
            case 'PDOException':
                self::redirect($e, self::GENERIC_MESSAGE);
                break;
            default:
                self::redirect($e, self::GENERIC_MESSAGE);
                break;
        }
    }

    /**
     * @param BaseException|Exception $exception
     * @return string
     */
    public static function getExceptionTraceAsString($exception)
    {
        $rtn = '';
        $count = 0;
        foreach ($exception->getTrace() as $frame) {
            $args = '';
            if (array_key_exists('args', $frame)) {
                $args = [];
                foreach ($frame['args'] as $arg) {
                    if (is_string($arg)) {
                        $args[] = "'" . $arg . "'";
                    } elseif (is_array($arg)) {
                        $args[] = 'Array';
                    } elseif ($arg === null) {
                        $args[] = 'NULL';
                    } elseif (is_bool($arg)) {
                        $args[] = $arg ? 'true' : 'false';
                    } elseif (is_object($arg)) {
                        $args[] = get_class($arg);
                    } elseif (is_resource($arg)) {
                        $args[] = get_resource_type($arg);
                    } else {
                        $args[] = $arg;
                    }
                }
                $args = implode(', ', $args);
            }
            $rtn .= sprintf(
                "#%s %s %s(%s): %s(%s)\n",
                $count,
                $count,
                $frame['file'],
                $frame['line'],
                $frame['function'],
                $args
            );
            $count++;
        }

        return $rtn;
    }

    public static function redirect($e, $custom_message = null)
    {
        if (self::getStaticService('config')->debug->vardumpErrors) {
            /** @noinspection ForgottenDebugOutputInspection */
            var_dump($e);
        }
    }

    public function send()
    {
        echo $this;

        echo $this->message;
        if ($this->getService('config')->debug->vardumpErrors) {
            /** @noinspection ForgottenDebugOutputInspection */
            var_dump($this);
        }
    }


}