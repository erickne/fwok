<?php
/**
 * Set a Top Level Exception Handler
 * The set_exception_handler() function sets a user-defined function to handle all uncaught exceptions.
 *
 * <?php
 * function myException($exception) {
 * echo "<b>Exception:</b> " . $exception->getMessage();
 * }
 *
 * set_exception_handler('myException');
 *
 * throw new Exception('Uncaught Exception occurred');
 * ?>
 */
namespace Fwok\Exceptions;

use Fwok\Application\App;
use Fwok\Application\ApplicationBase;
use Fwok\Plugins\ChromePhp;
use Fwok\Plugins\Logger;
use Fwok\Plugins\Translator;
use Fwok\Services\EmailsService;
use Phalcon\Di;
use Phalcon\Http\Request;

class Exception extends BaseException
{
    //public $message;                      // Exception message
    public $code_message;                // User-defined exception message
//    public $commands;                // Commands
    public $details;                // Extra details in envelope
    public $dev_message;                 // User-defined exception developer message
    public $code = 0;                  // User-defined exception code
    public $previous_exception;                  // Last Exception
    private $title;                       // User-defined exception title
    private $http_response_code;          // HTTP Response Code
    private $http_response_message;       // HTTP Response Message
    public $validation_messages;       // HTTP Response Message
    public $payload;                     // Payload
    protected $file;                      // Source filename of exception
    protected $line;                      // Source line of exception

    /**
     * @param string $code
     * @param null $message
     * @param null $title
     * @param null $dev_message
     * @param int $http_response_code
     * @param array|\stdClass $validation_messages
     * @param null $previous_exception
     * @param null $payload
     * @param array $commands
     * @param array $details
     * @throws \Fwok\Exceptions\Exception
     */
    public function __construct(
        $code,
        $message = null,
        $title = null,
        $dev_message = null,
        $http_response_code = 500,
        $validation_messages = [],
        $previous_exception = null,
        $payload = null
//        $commands = [],
//        $details = []
    ) {
        // Por algum motivo tive que redeclarar o translator
        $translator = new Translator();
        App::getStaticDI()->setShared('translation', $translator);

        if (!$message) {
            $this->message = $this->getCodeDescription($code);
        } else {
            $this->message = ApplicationBase::getTranslatedKey($message);
        }

        $this->title = $title;
        $this->dev_message = $dev_message;

        $this->code = $code;
//        $this->code_message = $this->getCodeDescription($code);
        $this->http_response_code = $http_response_code;
        $this->http_response_message = $this->getResponseDescription($http_response_code);
        $this->validation_messages = $validation_messages;
        $this->previous_exception = $previous_exception;
        $this->payload = $payload;
//        $this->commands = $commands;
//        $this->details = $details;

        $config_debug = App::getConfig('debug');
        if($config_debug->saveExceptionsInDatabase) {
            if (App::hasStaticService('db')) {
                /** @var \Phalcon\Db\AdapterInterface $db */
                $db = App::getStaticService('db');
                $exception = new \Fwok\Models\Exception();
                $exception->setMessage($this->message);
                $exception->setTitle($this->title);
                $exception->setDevMessage($this->dev_message);
                $exception->setCode($this->code);
//                $exception->setCodeMessage($this->code_message);
                $exception->setHttpResponseCode($this->http_response_code);
                $exception->setHttpResponseMessage($this->http_response_message);
                $exception->setValidationMessages($this->validation_messages);
                $exception->setPreviousException($this->previous_exception);
                $exception->setPayload($this->payload);
                // errors ignored
                @$db->begin();
                @$exception->saveOrException();
                @$db->commit();
            }
        }

        parent::__construct($this->message, $previous_exception = null);
    }

    /**
     * @param Exception $e
     * @throws \Fwok\Exceptions\Exception
     */
    public static function runLog($e = null)
    {
        if ($e->previous_exception) {
            self::runLog($e->previous_exception);
        }

        $req = App::getRequest();
        $endpoint = '';
        if ($req) {
            $endpoint = $req->getMethod() . ' ' . $req->getURI();
        }
        $trace_id = App::hasStaticService('trace') ? App::getTrace()->getId() : null;

        $module = APPLICATION_MODULE;
        $type = get_class($e);
        $message_array = [
            "\n".
            '===============================================================================',
            'Exception: [' .$module.'] '. $type .'::' . $e->getCode() . '',
            'Endpoint: ' . $endpoint,
            'Message User: ' . $e->getMessage(),
            'Message Dev: ' . $e->dev_message,
            'Exception Fired At File: ' . $e->getFile() . '(' . $e->getLine() . ')',
        ];

        if($trace_id) {
            $message_array[] = 'Trace ID: ' . $trace_id;
        }
        if(count($e->validation_messages)) {
            $message_array[] = 'Validation Message: ' . print_r($e->validation_messages, 1);
        }
        if(count($e->payload)) {
            $message_array[] ='Payload: ' . json_encode($e->payload);
        }
        if(count($e->getTrace())>1) {
            $message_array[] = 'Trace: ' . "\r\n" . self::getExceptionTraceAsString($e);
        }
//        $message_array[] = "\n";

        # Registra detalhes da exception
        $logger_excep = new Logger($module, Logger::TYPE_EXCEPTION);
        $logger_excep->critical(implode("\n", $message_array));

        # Registra no log de alerta
        $message = sprintf('[%s]%s DEV: %s | USER: %s | AT %s | FIRED AT %s(%s)',
            $module,
            $type,
            $e->dev_message,
            $e->getMessage(),
            $e->getTrace()[0]['file'] . '(' . $e->getTrace()[0]['line'] . ')',
            $e->getFile(),
            $e->getLine()
        );
        Logger::shoutInAlert($message, Logger::CRITICAL);
    }

    private function getCodeDescription($code_value)
    {

        if (ApplicationBase::hasStaticService('translation')) {
            return ApplicationBase::getTranslatedKey($code_value);
        }

        # TODO: adicionar translation aqui não sei pq não encontra o serviço
        return App::t($code_value);
    }

    public static function runEmail($e)
    {
        $configEmail = App::getConfig('email');
        $configDebug = App::getConfig('debug');

        if(!$configDebug->emailException) {
            return;
        }

        if($configEmail->dontsend) {
            return;
        }
        
        if ($e->previous_exception) {
            self::runEmail($e->previous_exception);
        }

        $emails = explode(';', $configDebug->sendExceptionTo);
        if (!count($emails)) {
            $msg = 'There are no emails configured to receive exceptions. Please configure debug->sendExceptionTo'; 
            Logger::shoutInAlert($msg);
            return false;
        }
        
        $emailService = new EmailsService();
        $emailService->phpmailer->isSMTP(); // telling the class to use SMTP
        $emailService->phpmailer->SMTPDebug = $configEmail->SMTPDebug; // 1 = errors and messages 2 = messages only
        $emailService->phpmailer->SMTPAuth = $configEmail->SMTPAuth; // enable SMTP authentication
        $emailService->phpmailer->Host = $configEmail->host;
        $emailService->phpmailer->Port = $configEmail->port;
        $emailService->phpmailer->CharSet = $configEmail->charSet;
        $emailService->phpmailer->SMTPSecure = $configEmail->SMTPSecure;
        $emailService->phpmailer->Username = $configEmail->username;
        $emailService->phpmailer->Password = $configEmail->password;
        $emailService->phpmailer->setFrom($configDebug->sendExceptionFromEmail, $configDebug->sendExceptionFromName);
        $emailService->phpmailer->isHTML(true);

        foreach ($emails as $email) {
            $emailService->addAddress($email);
        }
        $emailService->setHeader('');
        $emailService->setFooter('');

        return $emailService->templateException($e, get_class($e));
    }

    /**
     * @return bool
     */
    public function sendApi()
    {
        $req = $this->getService('request');
        $res = $this->getService('response');
        $config_debug = App::getConfig('debug');
        $trace_id = null;

        if (Di::getDefault()->has('trace')) {
            $trace = $this->getService('trace');
            $trace_id = $trace->getId() + 0;
        }

        $error = [
            'method' => $req->getMethod(),
            'uri' => $req->getURI(),
            'message' => $this->message,
            'dev_message' => $this->dev_message,
            'code' => $this->code,
            'code_message' => $this->code_message,
            'http_response_code' => $this->http_response_code,
            'http_response_message' => $this->http_response_message,
            'trace_id' => $trace_id,
            'validation_messages' => $this->validation_messages,
            'payload' => $this->payload,
//            'commands' => $this->commands,
//            'details' => $this->details
        ];

        ChromePhp::error($error);

        $res->setStatusCode($this->http_response_code, $this->http_response_message)->sendHeaders();

        if (Di::getDefault()->has('trace')) {
            $trace = $this->getService('trace');
            $this->getService('db')->begin();
            $trace->setException($error)->save();
            $trace->setResponseCode($res->getStatusCode())->save();
            $this->getService('db')->commit();
        }

        if(!$config_debug->showDevMessage) {
            unset($error['dev_message']);
        }

        ApplicationBase::getStaticService('apiresponse')->send($error, true);

        return false;
    }


    public function sendCli()
    {
        echo $this->getMessage();
        exit(255);
    }

    /**
     * @param $code
     * @return mixed|string
     */
    private function getResponseDescription($code)
    {
        $codes = [
            // Informational 1xx
            100 => 'Continue',
            101 => 'Switching Protocols',
            // Success 2xx
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            // Redirection 3xx
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            // 1.1
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            // 306 is deprecated but reserved
            307 => 'Temporary Redirect',
            // Client Error 4xx
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            // Server Error 5xx
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported',
            509 => 'Bandwidth Limit Exceeded'
        ];
        if (@$codes[$code]) {
            return $codes[$code];
        } else {
            return 'Unknown Status Code';
        }
    }
}
