<?php

namespace Fwok\Exceptions;

class SystemException extends Exception
{
    // Não encontrar valores da DB que deveria ter encontrado
    const DATABASE_CONSISTENCY = 'SYS-DB-001';
    const SERVICE_INVALID = 'SYS-SERV-001';
    const CONFIGURATION_INVALID = 'SYS-CONF-001';
    const REFERENCE_INVALID = 'SYS-REFE-001';
    const LOGIC_INVALID = 'SYS-LOGI-001';
    const FILE_INVALID = 'SYS-FILE-001';
    const ACTION_INVALID = 'SYS-ACTI-001';
    const SYSTEM_ERROR = 'SYS-ERRO-001';
    const FATAL_ERROR = 'SYS-FATL-001';
    const BACKUP_ERROR = 'SYS-BKUP-001';
    const MAINTENANCE = 'SYS-MAIN-001';

    /**
     * @param string $code
     * @param null $message
     * @param null $title
     * @param null $dev_message
     * @param int $http_response_code
     * @param array|\stdClass $validation_messages
     * @param null $previous_exception
     * @param null $payload
     * @param array $commands
     * @param array $details
     */
    public function __construct(
        $code,
        $message = null,
        $dev_message = null,
        $http_response_code = 500,
        $validation_messages = [ ],
        $previous_exception = null,
        $payload = null,
        $commands = [ ],
        $details = [ ]
    ) {
        parent::__construct(
            $code,
            $message,
            '',
            $dev_message,
            $http_response_code,
            $validation_messages,
            $previous_exception,
            $payload,
            $commands,
            $details
        );
    }
}
