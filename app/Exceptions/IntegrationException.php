<?php

namespace Fwok\Exceptions;

class IntegrationException extends Exception
{
    const INVALID_RESPONSE = 'INT-RESP-001';
    const INVALID_REQUEST = 'INT-REQU-001';
    const INVALID_VALUE = 'INT-VALU-001';
    /**
     * @param string $code
     * @param null $message
     * @param null $title
     * @param null $dev_message
     * @param int $http_response_code
     * @param array|\stdClass $validation_messages
     * @param null $previous_exception
     * @param null $payload
     * @param array $commands
     * @param array $details
     */
    public function __construct(
        $code,
        $message = null,
        $dev_message = null,
        $http_response_code = 500,
        $validation_messages = [ ],
        $previous_exception = null,
        $payload = null,
        $commands = [ ],
        $details = [ ]
    ) {
        parent::__construct(
            $code,
            $message,
            '',
            $dev_message,
            $http_response_code,
            $validation_messages,
            $previous_exception,
            $payload,
            $commands,
            $details
        );
    }
}
