<?php

namespace Fwok\Exceptions;

class InputException extends Exception
{
    const INVALID_AUTHENTICATION = 'INP-AUTH-001';
    const UNAVAILABLE_FEATURE = 'INP-UNFE-001';
    const PARAMETER_INVALID = 'INP-PARA-001';
    const VALUE_INVALID = 'INP-VALU-001';
    const PERMISSION_INVALID = 'INP-PERM-001';
    const NOT_FOUND = 'INP-NOTF-001';
    const DELETED = 'INP-DELE-002';
    const ACTION_INVALID = 'INP-ACTI-001';

    /**
     * @param string $code
     * @param null $message
     * @param null $title
     * @param null $dev_message
     * @param int $http_response_code
     * @param array|\stdClass $validation_messages
     * @param null $previous_exception
     * @param null $payload
     * @param array $commands
     * @param array $details
     */
    public function __construct(
        $code,
        $message = null,
        $dev_message = null,
        $http_response_code = 400,
        $validation_messages = [ ],
        $previous_exception = null,
        $payload = null,
        $commands = [ ],
        $details = [ ]
    ) {
        parent::__construct(
            $code,
            $message,
            '',
            $dev_message,
            $http_response_code,
            $validation_messages,
            $previous_exception,
            $payload,
            $commands,
            $details
        );
    }
}
