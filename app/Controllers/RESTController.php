<?php
namespace Fwok\Controllers;

use Fwok\Controllers;
use Fwok\Exceptions\Exception;
use Fwok\Exceptions\InputException;
use Fwok\Exceptions\SystemException;
use Phalcon\Http\Response;

class RESTController extends ControllerBase
{

    /**
     * @param $var
     * @param null $field
     * @param null $filters
     * @param null $user_message
     * @return mixed
     * @throws InputException
     */
    protected function validateGetVar($get_var, $filters = null, $user_message = null)
    {
        $return =$this->request->get($get_var, $filters);
        if (!$return) {
            if ($user_message) {
                $msg = $user_message;
            } else {
                $msg = 'Empty Variable: ' . $get_var;
            }
            throw new InputException(InputException::VALUE_INVALID, $msg, null, 400);
        }
        return $return;

    }
    /**
     * @param $var
     * @param null $field
     * @param null $filters
     * @param null $user_message
     * @return mixed
     * @throws InputException
     */
    protected function validateVar($var, $field = null, $filters = null, $user_message = null)
    {
        if (@!isset($var) or @$var == '') {
            if ($user_message) {
                $msg = $user_message;
            } else {
                $msg = 'Empty Variable: ' . $field;
            }
            throw new InputException(InputException::VALUE_INVALID, $msg, null, 400);
        } else {
            if ($filters) {
                return $this->filter->sanitize($var, $filters);
            } else {
                return $var;
            }
        }
    }

    /**
     * If our request contains a body, it has to be valid JSON.  This parses the
     * body into a standard Object and makes that vailable from the DI.  If this service
     * is called from a function, and the request body is nto valid JSON or is empty,
     * the program will throw an Exception.
     * @throws \Fwok\Exceptions\InputException
     * @return \stdClass
     */
    protected function bodyContent()
    {
        $data = json_decode($this->request->getRawBody(), false);

        // JSON body could not be parsed, throw exception
        if ($data === null) {
            $msg = 'The JSON body sent to the server was unable to be parsed. Probably Empty.';
            throw new InputException(InputException::VALUE_INVALID, null, $msg, 409);
        }

        return $data;
    }

    /**
     * Should be called by methods in the controllers that need to output results to the HTTP Response.
     * Ensures that arrays conform to the patterns required by the Response objects.
     *
     * @param  array $recordsArray Array of records to format as return output
     *
     * @param int $code
     * @throws Exception
     * @return array               Output array.  If there are records (even 1), every record will be an array ex: array(array('id'=>1),array('id'=>2))
     */
    protected function respond($recordsArray, $code = 200)
    {

        if (!is_array($recordsArray)) {
            // This is bad.  Throw a 500.  Responses should always be arrays.
            $msg = 'The records returned were malformed.';
            throw new SystemException(SystemException::LOGIC_INVALID, null, $msg);
        }
        // No records returned, so return an empty array
        if (count($recordsArray) < 1) {
            return [ ];
        }


        $response = new Response();

        $response->setStatusCode($code, $this->getResponseDescription($code));
        $response->send();

        return $recordsArray;
    }

    protected function getResponseDescription($code)
    {
        $codes = [
            // Informational 1xx
            100 => 'Continue',
            101 => 'Switching Protocols',
            // Success 2xx
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            // Redirection 3xx
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',  // 1.1
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            // 306 is deprecated but reserved
            307 => 'Temporary Redirect',
            // Client Error 4xx
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            // Server Error 5xx
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported',
            509 => 'Bandwidth Limit Exceeded'
        ];
        $result = (isset($codes[$code])) ?
            $codes[$code] :
            'Unknown Status Code';

        return $result;
    }
}