<?php

namespace Fwok\Controllers;

use Phalcon\Flash as Flash;
use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\View;

class MvcControllerBase extends ControllerBase
{
    /**
     * Using output buffering for more targetted partial view
     * rendering.
     * @param $viewPath
     * @param $data
     * @return
     */
    public function renderPartial($viewPath, $data)
    {
        ob_start();
        $this->view->partial($viewPath, $data);
        $contents = ob_get_contents();
        ob_end_clean();

        return $contents;
    }

    public function disableLayout()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    /**
     * Does a forward to another place
     *
     * @param string $uri
     * @param int|string $id
     *
     * @param null $params
     * @return mixed
     */
    public function forward($uri, $id = '', $params = null)
    {
        # remove first char if '/'
        $uri = ltrim($uri, '/');
        $uriParts = explode('/', $uri);
        $return = [
            'controller' => $uriParts[0],
            'action' => $uriParts[1]
        ];

        if ($params) {
            $return['params'] = $params;
        }
        if ($id) {
            $return['params']['id'] = $id;
        }

        return $this->dispatcher->forward($return);
    }

    /**
     * HMVC Request
     *
     * @param string $uri string with controller and action
     * @param mixed $params mixed
     *
     *
     * Examples:
     *
     * $this->HMVCrequest('invoices/close');
     * $this->HMVCrequest('invoices/close',44);
     * $this->HMVCrequest('invoices/close',['id'=>22,'status'=>'OPEN');
     * @return string
     */
    public function HMVCRequest($uri, $params = null)
    {
        $dispatcher = clone $this->getDI()->get('dispatcher');

        $uriParts = explode('/', $uri);

        $dispatcher->setControllerName($uriParts[0]);

        if (isset($uriParts[1])) {
            $dispatcher->setActionName($uriParts[1]);
        } else {
            $dispatcher->setActionName('index');
        }

        if (!is_null($params)) {
            if (is_array($params)) {
                $dispatcher->setParams($params);
            } else {
                $dispatcher->setParams([$params]);
            }
        } else {
            $dispatcher->setParams([]);
        }

        $dispatcher->dispatch();

        $response = $dispatcher->getReturnedValue();
        if ($response instanceof ResponseInterface) {
            return $response->getContent();
        }

        return $response;
    }

    protected function initialize()
    {
        $config = \Phalcon\Di::getDefault()->get('config');

        $this->view->setTemplateAfter('normal');

        \Phalcon\Tag::prependTitle($config->admin->prependTitle);

        $this->assets->collection('style')
            ->addCss('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css', false)
            ->addCss('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css', false)
            ->addCss($config->admin->customCss);

        $this->assets->collection('js-header')
            ->addJs('https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js', false)
            ->addJs('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js', false)
            ->addJs($config->admin->customJs);

        $this->assets->collection('js-footer');
    }

    /**
     * Go back to last page
     *
     * @return void
     */
    protected function rewind()
    {
        header('Location: ' . $_SERVER['HTTP_REFERER']);
        die();
    }
}
