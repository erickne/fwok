<?php

namespace Fwok\Controllers;

class ErrorsController extends ControllerBase
{

    public function initialize()
    {

        $config = $this->getService('config');
        \Phalcon\Tag::prependTitle($config->admin->prependTitle);

        $footer = '
        <p>
          Mas fique tranquilo, nossos técnicos já foram notificados deste incidente. Caso ele se repita, entre em contato com o nosso suporte.
        </p>
        <p>
          <a href="/">Clique aqui para retornar</a> ou aguarde alguns segundos para retornar automaticamente.
        </p>
      ';

        $this->view->setVar('redirect', $this->getService('config')->errors->redirect);
        $this->view->setVar('redirect_to', $this->getService('config')->errors->redirectTo);
        $this->view->setVar('redirect_timeout', $this->getService('config')->errors->redirectTimeout);

        $this->view->setVar('footer', $footer);
    }

    public function showAction($error = '')
    {
        \Fwok\Plugins\Logger::shoutInAlert('Redirection to errors/show ' . $error);
        $this->view->setVar('error', $error);
        $this->view->setVar('header', 'Infelizmente um erro aconteceu.');
    }

    public function notfoundAction()
    {
        $this->view->setVar('header', 'Página não encontrada');
        $this->view->setVar('error', 'A página solicitada não foi encontrada em nossos servidores.');
    }

    public function securityAction()
    {
        $path = implode(
            '/',
            [
                $this->dispatcher->getParam('controller'),
                $this->dispatcher->getParam('action')
            ]
        );
        $this->view->setVar('header', 'Erro de Segurança');
        $this->view->setVar('error', 'Você não possui permissão para acessar esta página (' . $path . ').');
    }


}
