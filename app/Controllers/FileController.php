<?php
namespace Fwok\Controllers;

use Fwok\Models\Files;
use Fwok\Plugins\Logger;
use Phalcon\Mvc\View;

class FileController extends MvcControllerBase
{

    public function initialize()
    {
        parent::initialize();
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
    }

    /**
     * @return Files $this
     * @throws \Fwok\Exceptions\SystemException
     */
    private function getFileFromDispatcher()
    {
        $name_unique = $this->dispatcher->getParam('name_unique');
        $file = Files::findFirst(['name_unique = :name_unique:', 'bind' => ['name_unique' => $name_unique]]);

        if (!$file) {
            Logger::shoutInAlert('File Object not found. name_unique=' . $name_unique);
            $this->throwFileNotFound();
        }

        if (!$file->readContents()) {
            Logger::shoutInAlert('File Object found but empty. name_unique=' . $name_unique);
            $this->throwFileNotFound();
        }

        return $file;
    }

    public function viewAction()
    {
        $file = $this->getFileFromDispatcher();

        $this->fileRenderImage($file);

        return true;
    }

    public function downloadAction()
    {
        $file = $this->getFileFromDispatcher();

        $this->fileDownload($file);

        return true;
    }

    public function streamAction()
    {
        $file = $this->getFileFromDispatcher();

        $this->fileDownload($file, true, 1000);

        return true;
    }

    private function cachingHeaders($file, $datetime)
    {
        $timestamp = strtotime($datetime);
        $gmt_m_time = gmdate('r', $timestamp);
        header('ETag: "' . md5($timestamp . $file) . '"');
        header('Last-Modified: ' . $gmt_m_time);
        header('Cache-Control: public');

        if (array_key_exists('HTTP_IF_MODIFIED_SINCE', $_SERVER) || array_key_exists('HTTP_IF_NONE_MATCH', $_SERVER)) {
            $c1 = $_SERVER['HTTP_IF_MODIFIED_SINCE'] === $gmt_m_time;
            $c2 = str_replace('"', '', stripslashes($_SERVER['HTTP_IF_NONE_MATCH'])) === md5($timestamp . $file);
            if ($c1 || $c2) {
                header($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified');
                exit();
            }
        }

    }

    /**
     * @param Files $file
     */
    private function fileRenderImage(Files $file)
    {
        $content = $file->readContents();

        $this->cachingHeaders($file->getPathSystem(), $file->getCreatedAt());

        $expires = '1 year';
        switch ($file->getExtension()) {
            case 'gif':
                $content_type = 'image/gif';
                $expires = '1 week';
                break;
            case 'png':
                $content_type = 'image/png';
                $expires = '1 week';
                break;
            case 'jpeg':
            case 'jpg':
                $content_type = 'image/jpeg';
                $expires = '1 week';
                break;
            case 'pdf':
                $content_type = 'application/pdf';
                break;
            default:
                $content_type = 'image/jpeg';
        }

        header('Content-type: ' . $content_type);
        header('Expires: ' . date(DATE_RFC822, strtotime($expires)));
        echo $content;

    }

    private function throwFileNotFound()
    {
        http_response_code(404);
        die('Image not found!');
    }

    /**
     * Download with resume/speed/stream options
     * If streaming - movies will show as movies, images as images instead of download prompt
     *
     * @param Files $file
     * @param bool $doStream
     * @param int $maxSpeed
     * @return bool
     * @internal param $fileLocation
     * @internal param $fileName
     */
    private function fileDownload(Files $file, $doStream = false, $maxSpeed = 1000)
    {
        if (connection_status() !== 0) {
            return false;
        }
        $extension = strtolower(end(explode('.', $file->getName())));

        /* List of File Types */
        $fileTypes['swf'] = 'application/x-shockwave-flash';
        $fileTypes['pdf'] = 'application/pdf';
        $fileTypes['exe'] = 'application/octet-stream';
        $fileTypes['zip'] = 'application/zip';
        $fileTypes['doc'] = 'application/msword';
        $fileTypes['xls'] = 'application/vnd.ms-excel';
        $fileTypes['ppt'] = 'application/vnd.ms-powerpoint';
        $fileTypes['gif'] = 'image/gif';
        $fileTypes['png'] = 'image/png';
        $fileTypes['jpeg'] = 'image/jpg';
        $fileTypes['jpg'] = 'image/jpg';
        $fileTypes['rar'] = 'application/rar';

        $fileTypes['ra'] = 'audio/x-pn-realaudio';
        $fileTypes['ram'] = 'audio/x-pn-realaudio';
        $fileTypes['ogg'] = 'audio/x-pn-realaudio';

        $fileTypes['wav'] = 'video/x-msvideo';
        $fileTypes['wmv'] = 'video/x-msvideo';
        $fileTypes['avi'] = 'video/x-msvideo';
        $fileTypes['asf'] = 'video/x-msvideo';
        $fileTypes['divx'] = 'video/x-msvideo';

        $fileTypes['mp3'] = 'audio/mpeg';
        $fileTypes['mp4'] = 'audio/mpeg';
        $fileTypes['mpeg'] = 'video/mpeg';
        $fileTypes['mpg'] = 'video/mpeg';
        $fileTypes['mpe'] = 'video/mpeg';
        $fileTypes['mov'] = 'video/quicktime';
        $fileTypes['swf'] = 'video/quicktime';
        $fileTypes['3gp'] = 'video/quicktime';
        $fileTypes['m4a'] = 'video/quicktime';
        $fileTypes['aac'] = 'video/quicktime';
        $fileTypes['m3u'] = 'video/quicktime';

        $contentType = $fileTypes[$extension];

        header('Cache-Control: public');
        header("Content-Transfer-Encoding: binary\n");
        header('Content-Type: ' . $contentType);

        $contentDisposition = 'attachment';

        if ($doStream === true) {
            /* extensions to stream */
            $array_listen = [
                'mp3',
                'm3u',
                'm4a',
                'mid',
                'ogg',
                'ra',
                'ram',
                'wm',
                'wav',
                'wma',
                'aac',
                '3gp',
                'avi',
                'mov',
                'mp4',
                'mpeg',
                'mpg',
                'swf',
                'wmv',
                'divx',
                'asf'
            ];
            if (in_array($extension, $array_listen, false)) {
                $contentDisposition = 'inline';
            }
        }

        if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false) {
            $fileName = preg_replace('/\./', '%2e', $file->getName(), substr_count($file->getName(), '.') - 1);
        } else {
            $fileName = $file->getName();
        }
        header("Content-Disposition: $contentDisposition;filename=\"$fileName\"");
        header('Accept-Ranges: bytes');

        $range = 0;
        $size = filesize($file->getPathSystem());

        if ($size === 0) {
            die('Zero byte file! Aborting download');
        }

        if (array_key_exists('HTTP_RANGE', $_SERVER)) {
            $range_arr = explode('=', $_SERVER['HTTP_RANGE']);
            $range = $range_arr[1];
            str_replace($range, '-', $range);
            $size2 = $size - 1;
            $new_length = $size - $range;
            header('HTTP/1.1 206 Partial Content');
            header("Content-Length: $new_length");
            header("Content-Range: bytes $range$size2/$size");
        } else {
            $size2 = $size - 1;
            header("Content-Range: bytes 0-$size2/$size");
            header('Content-Length: ' . $size);
        }

        $fp = fopen($file->getPathSystem(), 'rb');

        fseek($fp, $range);

        while (!feof($fp) and (connection_status() === 0)) {
            set_time_limit(0);
            print(fread($fp, 1024 * $maxSpeed));
            flush();
            ob_flush();
            sleep(1);
        }
        fclose($fp);
        if ($doStream) {
            return ((connection_status() === 0) and !connection_aborted());
        } else {
            return true;
        }
    }
}
