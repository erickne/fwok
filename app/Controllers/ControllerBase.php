<?php

namespace Fwok\Controllers;

use Phalcon\DI;
use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{

    /**
     * Get the service via static call
     * @param $service
     * @return object
     */
    function getService($service)
    {
        return $this->getDI()->get($service);
    }

    static function getStaticService($service)
    {
        return DI::getDefault()->get($service);
    }

    static function getStaticDI()
    {
        return DI::getDefault();
    }

}