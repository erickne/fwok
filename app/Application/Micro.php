<?php

namespace Fwok\Application;

use Fwok\Exceptions\InputException;
use Fwok\Models\Roles;
use Phalcon\Exception;
use Phalcon\Mvc\Micro\CollectionInterface;
use Phalcon\Mvc\Micro\LazyLoader;

class Micro extends \Phalcon\Mvc\Micro
{

    /**
     * Returns the internal router used by the application
     */
    public function getRouter()
    {
        //var router;

        $router = $this->_router;
        if (!is_object($router)) {

            $router = $this->getSharedService("router");

            /**
             * Clear the set routes if any
             */
            $router->clear();

            /**
             * Automatically remove extra slashes
             */
            $router->removeExtraSlashes(true);

            /**
             * Update the internal router
             */
            $this->_router = $router;
        }

        return $router;
    }

    /**
     * Maps a route to a handler without any HTTP method constraint
     *
     * @param string $routePattern
     * @param callable $handler
     * @return \Fwok\Routers\Route|\Phalcon\Mvc\Router\RouteInterface
     */
    public function map($routePattern, $handler)
    {
        //var router, route;

        /**
         * We create a router even if there is no one in the DI
         */
        $router = $this->getRouter();

        /**
         * Routes are added to the router
         */
        $route = $router->add($routePattern);

        /**
         * Using the id produced by the router we store the handler
         */
        $this->_handlers[$route->getRouteId()] = $handler;

        /**
         * The route is returned, the developer can add more things on it
         */

        return $route;
    }

    /**
     * Mounts a collection of $handles
     * @param CollectionInterface $collection
     * @throws Exception
     * @return $this|\Phalcon\Mvc\Micro
     */
    public function mount(CollectionInterface $collection)
    {
        /*
            var mainHandler, $handles, lazyHandler, prefix, methods, pattern,
                subHandler, realHandler, prefixedPattern, route, $handle, name;
    */
        /**
         * Get the main $handle
         */
        $mainHandler = $collection->getHandler();
        if (!$mainHandler) {
            throw new Exception("Collection requires a main handle");
        }

        $handlers = $collection->getHandlers();
        if (!count($handlers)) {
            throw new Exception("There are no handles to mount");
        }

        if (is_array($handlers)) {

            /**
             * Check if $handle is lazy
             */
            if ($collection->isLazy()) {
                $lazyHandler = new LazyLoader($mainHandler);
            } else {
                $lazyHandler = $mainHandler;
            }

            /**
             * Get the main prefix for the collection
             */
            $prefix = $collection->getPrefix();

            foreach ($handlers as $handle) {

                if (!is_array($handle)) {
                    throw new Exception("One of the registered handles is invalid");
                }

                $methods = $handle[0];
                $pattern = $handle[1];
                $subHandler = $handle[2];
                $name = $handle[3];
                $allowedRoles = $handle[4];
                $deniedRoles = $handle[5];

                /**
                 * Create a real $handle
                 */
                $realHandler = [ $lazyHandler, $subHandler ];

                if ($prefix) {
                    if ($pattern == "/") {
                        $prefixedPattern = $prefix;
                    } else {
                        $prefixedPattern = $prefix . $pattern;
                    }
                } else {
                    $prefixedPattern = $pattern;
                }

                /**
                 * Map the route manually
                 */
                $route = $this->map($prefixedPattern, $realHandler);

                if ($methods != "" || (is_array($methods))) {
                    $route->via($methods);
                }

                if ($name) {
                    $route->setName($name);
                }
                if($allowedRoles){
                    (!in_array(Roles::ULTRA,$allowedRoles)) ? $allowedRoles[] = Roles::ULTRA : null;
                    $route->setAllowedRoles($allowedRoles);
                }
                if($deniedRoles){
                    $route->setDeniedRoles($deniedRoles);
                }
            }
        }

        return $this;
    }

    /**
     * The notFound service is the default handler function that runs when no route was matched.
     * We set a 404 here unless there's a suppress error codes.
     * @throws \Fwok\Exceptions\InputException
     */
    public function processNotFound()
    {
        if ($this->request->getMethod() === 'OPTIONS') {
            $this->response->setStatusCode(200);
            return true;
        }

        $dev_msg = 'The route is not implemented or configured - ' . $this->getRouter()->getRewriteUri();
        throw new InputException(InputException::VALUE_INVALID, 'ROUTE_NOT_FOUND', $dev_msg, 404);
    }

    /**
     * After a route is run, usually when its Controller returns a final value,
     * the application runs the following function which actually sends the response to the client.
     *
     * The default behavior is to send the Controller's returned value to the client as JSON.
     * However, by parsing the request querystring's 'type' parameter, it is easy to install
     * different response type handlers.  Below is an alternate csv handler.
     */
    public function processAfter()
    {
        $records = $this->getReturnedValue();

        App::getTrace()
            ->setResponse($records)
            ->setResponseCode(App::getApiResponse()->getStatusCode())
            ->saveOrExceptionNewTransaction();

        App::getApiResponse()
            ->send($records);
    }
}