<?php
namespace Fwok\Application;

class ApiApplicationBase extends ApplicationBase
{

    public function run()
    {
        echo $this->handle()->getContent();
    }
}
