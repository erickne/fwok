<?php
namespace Fwok\Application;

use Fwok\Exceptions\Exception;
use Fwok\Exceptions\SystemException;
use Fwok\Models\Trace;
use Fwok\Plugins\AndroidPay;
use Fwok\Plugins\Pagseguro;
use Fwok\Plugins\Timezone;
use Fwok\Plugins\Translator;
use Phalcon\Config;
use Phalcon\Crypt;
use Phalcon\DI;
use Phalcon\Http\Request;
use Phalcon\Http\Response;
use Phalcon\Logger\Adapter\Firephp;
use Phalcon\Mvc\Application;
use Phalcon\Security;

class ApplicationBase extends Application
{
    const MODULE_CLI = 'CLI';
    const MODULE_ADMIN = 'ADMIN';
    const MODULE_API = 'API';
    const MODULE_WEBAPP = 'WEBAPP';
    const MODULE_TEST = 'TEST';
    const MODULE_CDN = 'CDN';

    const ENV_DEVELOPMENT = 'development';
    const ENV_PRODUCTION = 'production';
    const ENV_STAGING = 'staging';

    const PERMISSION_MODE_ONLY_ROLES = 'ONLY_ROLES';
    const PERMISSION_MODE_ROLES_AND_PERMISSIONS = 'ROLES_AND_PERMISSIONS';

    /**
     * Get the service via static call
     * @param $service
     * @return object
     */
    public function getService($service)
    {
        return Di::getDefault()->get($service);
    }

    /**
     * @param $service
     * @return mixed
     */
    public static function getStaticService($service)
    {
        return Di::getDefault()->get($service);
    }

    /**
     * @param $service
     * @param bool $check_is_null
     * @return bool
     */
    public function hasService($service, $check_is_null = false)
    {
        return self::hasStaticService($service, $check_is_null);
    }

    /**
     * @param $service
     * @param bool $check_is_null
     * @return bool
     */
    public static function hasStaticService($service, $check_is_null = false)
    {
        if ($check_is_null) {
            return (Di::getDefault()->has($service) and Di::getDefault()->$service) ? true : false;
        } else {
            return Di::getDefault()->has($service);
        }
    }

    /**
     * @return \Phalcon\DiInterface
     */
    public static function getStaticDI()
    {
        return DI::getDefault();
    }

    public function onConstruct()
    {
        $this->di = DI::getDefault();
    }

    /**
     * @return \Fwok\Models\Users
     * @throws Exception
     */
    public static function getUser()
    {
        $user = self::getStaticService('user');
        if (!$user) {
            throw new SystemException(SystemException::SERVICE_INVALID, 'Service user does not exist', null, 500);
        }

        return $user;

    }

    /**
     * @return Trace
     * @throws Exception     *
     */
    public static function getTrace()
    {
        return self::getStaticService('trace');
    }

    /**
     * @return \Google_Client()
     * @throws Exception     
     */
    public static function getGoogleClient()
    {
        return self::getStaticService('googleclient');
    }

    /**
     * @return Response
     */
    public static function getResponse()
    {
        return self::getStaticService('response');
    }
    /**
     * @return Request
     */
    public static function getRequest()
    {
        return self::getStaticService('request');
    }

    /**
     * @return \Fwok\Responses\ApiResponse|\Fwok\Responses\JSONResponse|\Fwok\Responses\CSVResponse
     * @throws Exception     *
     */
    public static function getApiResponse()
    {
        return self::getStaticService('apiresponse');
    }

    /**
     * @return \Fwok\Responses\EnvelopeMeta
     * @throws Exception     *
     */
    public static function getApiResponseEnvelopeMeta()
    {
        return self::getStaticService('apiresponseenvelopemeta');
    }

    /**
     * @return \Phalcon\Events\Manager
     * @throws Exception     *
     */
    public static function getMainEventsManager()
    {
        return self::getStaticService('eventsManager');
    }

    /**
     * @return \Fwok\Plugins\GCM
     * @throws Exception
     */
    public static function getGCM()
    {
        return self::getStaticService('gcm');
    }

    /**
     * @return \Fwok\Plugins\Financial\Iugu
     * @throws Exception
     */
    public static function getIugu()
    {
        return self::getStaticService('iugu');
    }

    /**
     * @return \Fwok\Plugins\Security\SecurityApi
     * @throws Exception
     */
    public static function getSecurityApi()
    {
        return self::getStaticService('securityapi');
    }

    /**
     * @return Translator
     * @throws Exception
     */
    public static function getTranslation()
    {
        return self::getStaticService('translation');
    }

    /**
     * @param null $parameter
     * @return Config|\stdClass
     */
    public static function getConfig($parameter = null)
    {
        $config = self::getStaticService('config');
        if ($parameter) {
            return $config->$parameter;
        }

        return $config;
    }

    /**
     * @return Crypt
     * @throws Exception
     */
    public static function getCrypt()
    {
        return self::getStaticService('crypt');
    }
    /**
     * @return Security
     * @throws Exception
     */
    public static function getSecurity()
    {
        return self::getStaticService('security');
    }

    /**
     * @return Pagseguro
     * @throws Exception
     */
    public static function getPagSeguro()
    {
        return self::getStaticService('pagseguro');
    }

    /**
     * @return AndroidPay
     * @throws Exception
     */
    public static function getAndroidPay()
    {
        return self::getStaticService('androidpay');
    }

    /**
     * @return Timezone
     * @throws Exception
     */
    public static function getTimezone()
    {
        return self::getStaticService('timezone');
    }

    /**
     * @param int $exception_on_not_found
     * @throws SystemException
     * @return \Fwok\Models\Business
     * @throws \Fwok\Exceptions\Exception
     */
    public static function getBusiness($exception_on_not_found = true)
    {
        if (self::hasStaticService('business')) {
            return self::getStaticService('business');
        };
        $user = App::getUser();
        $business = $user->getBusiness();

        if (!$business and $exception_on_not_found) {
            $msg = 'Could not find business';
            throw new SystemException(SystemException::REFERENCE_INVALID, null, $msg);
        }

        return $business;
    }

    /**
     * @param string $key
     * @return string
     */
    public static function getTranslatedKey($key)
    {
        $translation = self::getStaticService('translation');

        if ($translation) {
            $t = $translation->getMessages();

            return $t->_($key);
        }
    }

    public static function set($name, $definition, $shared = false)
    {
        return self::getStaticDI()->set($name, $definition, $shared);
    }

    /**
     * @param string $key
     * @return string
     */
    public static function t($key)
    {
        return self::getTranslatedKey($key);
    }

    /**
     * @return \Phalcon\Db\AdapterInterface
     */
    public static function getDb()
    {
        return self::getStaticService('db');
    }

    /**
     * @return \Phalcon\Cache\Backend\Memory|\Phalcon\Cache\Backend\File
     */
    public static function getModelsCache()
    {
        return self::getStaticService('modelsCache');
    }

    public static function envIsStaging()
    {
        return (App::getConfig('application')->environment === self::ENV_STAGING);
    }
    public static function envIsDevelopment()
    {
        return (App::getConfig('application')->environment === self::ENV_DEVELOPMENT);
    }
    public static function envIsProduction()
    {
        return (App::getConfig('application')->environment === self::ENV_PRODUCTION);
    }

}