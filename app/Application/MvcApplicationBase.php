<?php
namespace Fwok\Application;

class MvcApplicationBase extends \Fwok\Application\ApplicationBase
{

    public function run()
    {
        echo $this->handle()->getContent();
    }
}
