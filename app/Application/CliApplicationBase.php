<?php
namespace Fwok\Application;

use Phalcon\Cli\Console;

class CliApplicationBase extends Console
{
    public function run($argv)
    {
        /**
         * Process the console arguments
         */
        $arguments = [ ];
        foreach ($argv as $k => $arg) {
            if ($k == 1) {
                $arguments['task'] = $arg;
            } elseif ($k == 2) {
                $arguments['action'] = $arg;
            } elseif ($k >= 3) {
                $arguments['params'][] = $arg;
            }
        }

        // define global constants for the current task and action
        define('CURRENT_TASK', (isset($argv[1]) ? $argv[1] : null));
        define('CURRENT_ACTION', (isset($argv[2]) ? $argv[2] : null));

        $this->handle($arguments);
    }
}
