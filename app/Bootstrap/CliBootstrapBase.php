<?php

use Phalcon\Cli\Console;
use Phalcon\DI;

class CliBootstrapBase extends \Fwok\Bootstrap\BootstrapBase
{
    /**
     * $default_namespace = 'Website\Cli\Tasks'
     *
     * @param $default_namespace
     */
    public function initCliDispatcher($default_namespace)
    {
        $dispatcher = new \Phalcon\Cli\Dispatcher();
        $dispatcher->setDefaultNamespace($default_namespace);

        $this->di->setShared('dispatcher', $dispatcher);
    }

    /**
     * @param Console $console
     */
    public function initCliConsole($console = null)
    {
        $this->di = new \Phalcon\Di\FactoryDefault\CLI();

        if (!$console) {
            $console = new Console();
        }
        $console->setDI($this->di);
        
        $this->di->setShared('console', $console);
    }
}
