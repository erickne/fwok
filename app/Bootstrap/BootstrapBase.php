<?php

namespace Fwok\Bootstrap;

use Fwok\Application\App;
use Fwok\Application\ApplicationBase;
use Fwok\Exceptions\InputException;
use Fwok\Exceptions\SystemException;
use Fwok\Models\ColorSet;
use Fwok\Models\Trace;
use Fwok\Models\Users;
use Fwok\Plugins\AndroidPay;
use Fwok\Plugins\ChromePhp;
use Fwok\Plugins\Financial\Iugu;
use Fwok\Plugins\GCM;
use Fwok\Plugins\Logger;
use Fwok\Plugins\Pagseguro;
use Fwok\Plugins\PiwikClient;
use Fwok\Plugins\Timezone;
use Fwok\Plugins\Translator;
use Fwok\Responses\CSVResponse;
use Fwok\Responses\EnvelopeMeta;
use Fwok\Responses\JSONResponse;
use Fwok\Services\EmailsService;
use Phalcon\Config;
use Phalcon\Config\Adapter\Php;
use Phalcon\Crypt;
use Phalcon\Di;
use Phalcon\Di\FactoryDefault;
use Fwok\Exceptions\Exception;
use Phalcon\Events\Event;
use Phalcon\Http\Request;
use Phalcon\Loader;
use Phalcon\Mvc\Application;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Router;
use Phalcon\Mvc\Url;
use Phalcon\Mvc\View;
use Phalcon\Security;

/**
 * This is the default Dependency Injection bootstrap class. It
 * contains service definitions for each of the default registered
 * services, loads the application config files, and sets up the
 * the application namespaces.
 *
 * Other classes in \Lib\Bootstrap extend this and are responsible
 * for creating the DI object and overwriting/instantiating any
 * dependencies.
 */
class BootstrapBase
{
    public $di;
    protected $services;

    /**
     * Get the service via static call
     * @param $service string
     * @return mixed|void
     */
    public function getService($service)
    {
        return $this->getDI()->get($service);
    }

    public static function getStaticService($service)
    {
        return Di::getDefault()->get($service);
    }

    public static function getStaticDI()
    {
        return Di::getDefault();
    }

    public function initFwok()
    {
        if (!defined('APPLICATION_MODULE')) {
            $msg = 'Constant APPLICATION_MODULE is not defined.';
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $msg);
        }
        if (!defined('APPLICATION_PATH')) {
            $msg = 'Constant APPLICATION_PATH is not defined.';
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $msg);
        }
        $this->initConfig();
        $this->initModelsManager();
        $this->initModelsCache();
        $this->initModelsMetadata();
        $this->initSecurity();
        $this->initTimezone();
        $this->initTranslation();
        $this->initColorSets();
    }

    public function initGCM()
    {
        $api = $this->di['config']->google->developerKey;
        $gcm = new GCM($api);

        $this->di['gcm'] = $gcm;
    }

    public function initIugu()
    {
        $iugu = new Iugu();

        $this->di['iugu'] = $iugu;
    }

    /**
     * Define translator engine language
     *
     * @param $language String
     * @throws Exception
     */
    public function initTranslation($language = null)
    {
        if (!$language) {
            $language = App::getConfig('application')->defaultLanguage;
        }
        if (!$language) {
            $language = 'pt';
        }

        $translator = new Translator($language);

        $this->di->setShared('translation', $translator);
    }

    /**
     * @param \Phalcon\Mvc\Micro|\Phalcon\Mvc\Application $app
     * @throws \Fwok\Exceptions\InputException
     */
    public function initTrace($app)
    {
        $headers = $app->request->getHeaders();

        //Removed larger files and password
        unset($_POST['password'], $_POST['uploadFiles'], $_POST['uploadFile']);
        $body = json_decode($app->request->getRawBody());
        unset($body->password, $body->uploadFiles, $body->uploadFile);

        /** @var Router $router */
        $router = $app->getRouter();
        $trace = new Trace();

        if (App::getConfig('debug')->saveTraces) {
            $trace->setEndpoint($router->getRewriteUri());
            $trace->setMethod($app->request->getMethod());
            $trace->setVarGet($_GET);
            $trace->setVarPost($_POST);
            $trace->setVarBody($app->request->getRawBody());
            $trace->setProxyIp($_SERVER['HTTP_X_FORWARDED_FOR']);
            $trace->setClientIp($_SERVER['REMOTE_ADDR']);
            $trace->setAgent($_SERVER['HTTP_USER_AGENT']);
            $trace->setVersion($headers['fw-version']);
            $trace->saveOrExceptionNewTransaction();
        }

        $this->di->setShared('trace', $trace);
    }

    public function initUser()
    {
        $user = new Users();

        $this->di->setShared('user', $user);
    }

    public static function getRealClientIp()
    {
        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        } else {
            if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                if (isset($_SERVER['HTTP_X_FORWARDED'])) {
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
                } else {
                    if (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
                        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
                    } else {
                        if (isset($_SERVER['HTTP_FORWARDED'])) {
                            $ipaddress = $_SERVER['HTTP_FORWARDED'];
                        } else {
                            if (isset($_SERVER['REMOTE_ADDR'])) {
                                $ipaddress = $_SERVER['REMOTE_ADDR'];
                            } else {
                                $ipaddress = 'UNKNOWN';
                            }
                        }
                    }
                }
            }
        }

        return $ipaddress;
    }

    public static function isSSL()
    {
        if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
            return true;
        }

        return false;
    }

    public static function isMobile($useragent = null)
    {
        if (!$useragent) {
            $useragent = $_SERVER['HTTP_USER_AGENT'];
        }

        if (preg_match(
                '/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',
                $useragent
            ) || preg_match(
                '/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',
                substr($useragent, 0, 4)
            )
        ) {
            return true;
        } else {
            return false;
        }
    }

    public static function fullHost()
    {
        return ((self::isSSL()) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];
    }


    /**
     * Default constructor. Services contains a list of the
     * services to attach to the DI container.
     *
     * @param array $services services to run
     * @return \Fwok\Bootstrap\BootstrapBase default DI.
     */
    public function __construct($services = [])
    {
        $this->di = new FactoryDefault();
        $this->services = $services;
    }

    /**
     * Get the internal DI object
     */
    public function getDI()
    {
        return $this->di;
    }

    public function initPaypal()
    {
        $paypal = new \Fwok\Plugins\PayPal(
            $this->getService('config')->paypal->username,
            $this->getService('config')->paypal->password,
            $this->getService('config')->paypal->signature,
            $this->getService('config')->paypal->sandbox
        );
        $this->di->setShared('paypal', $paypal);
    }

    public function initPagseguro()
    {
        $pagseguro = new Pagseguro();
        $this->di->setShared('pagseguro', $pagseguro);
    }

    public function initAndroidPay()
    {
        $androidpay = new AndroidPay();
        $this->di->setShared('androidpay', $androidpay);
    }

    public function initGoogleClient()
    {
        $developerKey = $this->getService('config')->google->developerKey;

        $client = new \Google_Client();
        $client->setDeveloperKey($developerKey);
        $client->setAuthConfig(
            APPLICATION_PATH . '/app/Config/' . $this->getService('config')->google->serviceAuthFile
        );
        $client->setRedirectUri($this->getService('config')->google->redirect_uri);
        $client->setScopes(explode(" ", $this->getService('config')->google->scopes));

        $this->di->setShared('googleclient', $client);
    }

    public function initPiwikClient()
    {
        if (!$this->di['config']) {
            http_response_code(500);
            die('Please initConfig before initPiwikClient');
        }
        if (!$this->di['config']->piwik->host or !$this->di['config']->piwik->tokenAuth) {
            http_response_code(500);
            die('Invalid Piwik Config');
        }

        $piwik_client = new PiwikClient();
        $piwik_client->setServer($this->di['config']->piwik->host);
        $piwik_client->setDebug($this->di['config']->debug->piwikRequest);
        $piwik_client->setTokenAuth($this->di['config']->piwik->tokenAuth);

        $this->di->setShared('piwik_client', $piwik_client);
    }

    public function initErrorHandler()
    {
        $config = App::getConfig();

        error_reporting(E_ALL ^ E_NOTICE ^ E_STRICT ^ E_DEPRECATED ^ E_WARNING);

        if ($config->debug->showPhpErrors) {
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
//            error_reporting(-1);
        } else {
            ini_set('display_errors', 0);
            ini_set('display_startup_errors', 0);
        }

        set_error_handler(
            function ($errNo, $errStr, $errFile, $errLine) {

                if (0 === $errNo & 0 === error_reporting()) {
                    return;
                }
                switch ($errNo) {
                    case E_ERROR:
                    case E_USER_ERROR:
                    case E_USER_WARNING:
                    case E_USER_NOTICE:
                    case E_STRICT:
                    case E_CORE_ERROR:
                    case E_CORE_WARNING:
                    case E_COMPILE_ERROR:
                    case E_COMPILE_WARNING:
                        $error_msg = sprintf('%s - %s, %s:%s', $errNo, $errStr, $errFile, $errLine);
                        \Fwok\Plugins\Logger::shoutInAlert($error_msg);
                        break;
                    case E_ALL:
                    default:
                        break;
                }
//                $msg = sprintf('[%s] [%s] %s - %s', $errorNumber, $errorLine, $errorString, $errorFile);
                // Don't execute PHP internal error handler
                return true;
            }
        );

        set_exception_handler(
            function ($exception) {
                http_response_code(500);
                $dev_msg = 'Uncaught System Exception. Please check log.';
                $e = new SystemException(SystemException::SYSTEM_ERROR, SystemException::SYSTEM_ERROR . '-' . $exception->getMessage(), $dev_msg, 500, null, $exception);
                SystemException::runLog($exception);
                SystemException::runEmail($exception);
                if (APPLICATION_MODULE === App::MODULE_API) {
                    $e->sendApi();
                }
            }
        );


        register_shutdown_function(function () {
            $error = error_get_last();

            if ($error === null) {
                return;
            }
            if (in_array($error['type'], [E_ERROR, E_COMPILE_ERROR, E_CORE_ERROR, E_PARSE], true)) { // Fatal
                $errNo = $error["type"];
                $errFile = $error["file"];
                $errLine = $error["line"];
                $errStr = $error["message"];
//                Registra no log
                $dev_msg = sprintf('%s - %s, %s:%s', $errNo, $errStr, $errFile, $errLine);
                $exception = new SystemException(SystemException::SYSTEM_ERROR, null, $dev_msg, 500);
                http_response_code(500);
                $exception::runLog($exception);
                $exception::runEmail($exception);

                // Dispara para o usuário. Neste caso, a mensagem de erro não contém informações sensíveis
//                $dev_msg = 'Fatal PHP Error. Check Log.';
                $exception = new SystemException(SystemException::SYSTEM_ERROR, null, $dev_msg, 500);
                if (!App::getResponse()->isSent()) {
                    $exception->sendApi();
                }
            }
        });
    }

    /**
     * Load configuration files
     * @throws \Fwok\Exceptions\SystemException
     */
    public function initConfig()
    {
        $path_log = APPLICATION_PATH . '/storage/logs/';
        if (!file_exists($path_log) OR !is_writable($path_log)) {
            http_response_code(500);
            die('Log path is not writable');
        }
        $path_log = APPLICATION_PATH . '/storage/config/';
        if (!file_exists($path_log) OR !is_writable($path_log)) {
            http_response_code(500);
            die('Config path is not writable.');
        }
        $config_file = APPLICATION_PATH . '/storage/config/config.php';
        if (!file_exists($config_file)) {
            http_response_code(500);
            die('Please configure application first.');
        }

        /** @var \stdClass|Php $config */
        $config = new Php($config_file);

        ini_set('memory_limit', '96M');
        ini_set('post_max_size', '64M');
        ini_set('upload_max_filesize', '64M');

        ini_set('xdebug.var_display_max_depth', 5);
        ini_set('xdebug.var_display_max_children', 256);
        ini_set('xdebug.var_display_max_data', 1024);

        $config->application->ssl = BootstrapBase::isSSL();
        $config->application->module = APPLICATION_MODULE;

        $this->di->setShared('config', $config);

        if ($config->application->maintenance) {
            $message = 'Our server is under maintenance. Please come back later.';
            throw new SystemException(SystemException::MAINTENANCE, $message);
        }

        if (!$config->files->storagePath) {
            $dev = 'Invalid Files->storagePath configuration';
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $dev);
        }

        if (!file_exists($config->files->storagePath) OR !is_writable($config->files->storagePath)) {
            $dev = 'File Storage Path is not writable';
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $dev);
        }

        if(in_array($config->enviroment,['production','staging','development'])) {
            $dev = 'Enviroment not set: roduction,staging,development';
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $dev);
        }
    }

    /**
     * Register the namespaces, classes and directories
     *
     * Examples:
     *
     * $register_dirs = [
     *    APPLICATION_PATH . '/app/modules/admin/controllers',
     *    APPLICATION_PATH . '/app/modules/sites/controllers'
     * ];
     *
     * $register_namespaces = [
     *    'Pornficator\Admin\Plugin' => APPLICATION_PATH . '/app/modules/admin/plugins/' ,
     * ];
     *
     * $requires = [
     *    VENDOR_PATH . '/autoload.php',
     * ];
     *
     * @param array $register_dirs
     * @param array $register_namespaces
     * @param array $requires
     * @throws Exception
     */
    public function initLoader(array $register_dirs = array(), array $register_namespaces = array(), array $requires = array())
    {
        $error_msg = false;
        if (!is_array($register_namespaces)) {
            $error_msg = 'Invalid namespaces in initLoader';
        }
        if (!is_array($register_dirs)) {
            $error_msg = 'Invalid dirs in initLoader';
        }
        if (!is_array($requires)) {
            $error_msg = 'Invalid requires in initLoader';
        }

        if ($error_msg) {
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $error_msg);
        }

        $register_namespaces['Fwok'] = APPLICATION_PATH . '/app/Vendor/erickne/fwok/app';

        $loader = new Loader();

        $register_dirs[] = APPLICATION_PATH . '/app/Library/';
        $register_dirs[] = APPLICATION_PATH . '/app/Bootstrap/';
        $register_dirs[] = APPLICATION_PATH . '/app/Models/';
        $register_dirs[] = APPLICATION_PATH . '/app/Models/Behaviors/';
        $register_dirs[] = APPLICATION_PATH . '/app/Application/';
        $register_dirs[] = APPLICATION_PATH . '/app/Services/';
        $register_dirs[] = APPLICATION_PATH . '/app/Responses/';
        $register_dirs[] = APPLICATION_PATH . '/app/Validation/';
        $register_dirs[] = APPLICATION_PATH . '/app/Tasks/';

        $loader->registerDirs($register_dirs)->register();
        $loader->registerNamespaces($register_namespaces)->register();

        # autoload vendor dependencies
        foreach ($requires as $require) {
            require $require;
        }

        $this->di->setShared('loader', $loader);
    }

    public function initDb()
    {
        $config = $this->di['config'];
        $eventsManager = App::getMainEventsManager();

        $connection = new \Phalcon\Db\Adapter\Pdo\Mysql(
            [
                'host' => $config->database->host,
                'username' => $config->database->username,
                'password' => $config->database->password,
                'dbname' => $config->database->dbname,
                'persistent' => !$config->database->notPersistent,
                'options' => [\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8']
            ]
        );

        if ($config->debug->logQueries === 1) {
            $eventsManager->attach('db', function ($event, $connection) {
                /** @var Event $event */
                if ($event->getType() === 'beforeQuery') {
                    /** @var $connection \Phalcon\Db\Adapter\Pdo\Mysql */
                    $variables = $connection->getSqlVariables();
                    $string = $connection->getSQLStatement();
                    if ($variables) {
                        $string .= ' [' . implode(',', $variables) . ']';
                    }

                    Logger::shoutInSql($string);
                    //Logger::shoutInSql(json_encode($event->getSource()));

                    ChromePhp::log([
                        'query' => $string
                        //'sqlStatement'     => $sqlStatement,
                        //'sqlRealStatement' => $sqlRealStatement,
                        //'sqlVariables'     => $sqlVariables
                    ]);
                }
            }
            );
        }

        $connection->setEventsManager($eventsManager);
        $this->di->setShared('db', $connection);
    }

    public function initEventsManager()
    {
        $em = new \Phalcon\Events\Manager();
        $this->di->setShared('eventsManager', $em);
    }

    public function initModelsManager()
    {
        $modelsManager = new \Phalcon\Mvc\Model\Manager();
        $modelsManager->setEventsManager(App::getMainEventsManager());

        $this->di->setShared('modelsManager', $modelsManager);
    }

    public function initModelsMetadata()
    {
        $metaDataConfig = ApplicationBase::getConfig()->metadata;

        # todo habilitar o Redis/Memcache
        if ($config->application->environment === App::ENV_DEVELOPMENT) {
            $modelsMetadata = new \Phalcon\Mvc\Model\Metadata\Memory();
        } elseif ($metaDataConfig->adapter === 'Files') {

            if (!file_exists(APPLICATION_PATH . '/cache/Metadata/')) {
                mkdir(APPLICATION_PATH . '/cache/Metadata/', 0775, true);
            }
            if (!is_writable(APPLICATION_PATH . "/cache/Metadata/")) {
                $dev = "Invalid Models Metadata Directory";
                throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $dev);
            }
            $modelsMetadata = new \Phalcon\Mvc\Model\Metadata\Files([
                'metaDataDir' => APPLICATION_PATH . "/cache/Metadata/",
                "lifetime" => 8640000,
                'prefix' => 'tMeta'
            ]);
        } else {
            $modelsMetadata = new \Phalcon\Mvc\Model\Metadata\Memory();
        }
        $this->di->setShared('modelsMetadata', $modelsMetadata);
    }

    public function initModelsCache()
    {
        $config = ApplicationBase::getConfig();
        if ($config->application->environment === App::ENV_DEVELOPMENT) {
            $frontCache = new \Phalcon\Cache\Frontend\None();
            $modelsCache = new \Phalcon\Cache\Backend\Memory($frontCache);
        } else {
            //Cache data for one day by default
            $frontCache = new \Phalcon\Cache\Frontend\Data(
                [
                    "lifetime" => $config->metadata->lifetime * 30
                ]
            );

            if (!file_exists(APPLICATION_PATH . '/cache/Data/')) {
                mkdir(APPLICATION_PATH . '/cache/Data/', 0775, true);
            }
            if (!is_writable(APPLICATION_PATH . "/cache/Data/")) {
                $dev = "Invalid Models Cache Directory";
                throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $dev);
            }

            $modelsCache = new \Phalcon\Cache\Backend\File(
                $frontCache,
                [
                    "cacheDir" => APPLICATION_PATH . "/cache/Data/",
                    "prefix" => $config->metadata->prefix
                ]
            );
        }
        $this->di->setShared('modelsCache', $modelsCache);

    }

    public function initLogger()
    {
        if (!file_exists(APPLICATION_PATH . '/app/Logs/')) {
            mkdir(APPLICATION_PATH . '/app/Logs/', 0775, true);
        }

        $this->di->setShared('logger', new \Fwok\Plugins\Logger());
    }

    public function initSecurityApi()
    {
        $security = new \Fwok\Plugins\Security\SecurityApi();
        $this->di->setShared('securityapi', $security);
    }

    /**
     * Load color set data
     *
     * @param null $path
     * @throws SystemException
     */
    public function initColorSets($path = null)
    {
        if (!$path) {
            $path = APPLICATION_PATH . '/app/Vendor/erickne/fwok/app/Data/ColorSet.php';
        }

        if (!file_exists($path)) {
            $dev_msg = 'Invalid colorSet configuration path';
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $dev_msg);
        }
        ColorSet::$color_sets = new Php($path);
    }

    public function initEmail()
    {
        $email = new EmailsService();

        $email->setFrom($this->di['config']->email->fromname, $this->di['config']->email->fromemail);
        $email->setDebug($this->di['config']->debug->emailPhpMailer);
        $email->setWordWrap($this->di['config']->email->wordwrap);
        $email->isHtml(true);

        $this->di->setShared('email', $email);
    }


    /**
     * Api Response service
     *
     * @usage $this->getService('apiresponse')->setDetails(['teste'=>'abc','teste2'=>'123'])->setCommands(['logout']);
     *
     * @throws \Fwok\Exceptions\Exception
     */
    public function initApiResponse()
    {

        // Respond by default as JSON
        if (!self::getService('request')->get('response-type') || self::getService('request')->get('response-type') === 'json') {
            // Results returned from the route's controller.  All Controllers should return an array
            $response = new JSONResponse();
            $this->initApiResponseEnvelopeMeta();
        } elseif (self::getService('request')->get('response-type') === 'csv') {
            $response = new CSVResponse();
            $this->initApiResponseEnvelopeMeta();
        } else {
            $msg = 'Could not understand type specified by type parameter in query string.';
            throw new InputException(InputException::PARAMETER_INVALID, 'INVALID_PARAMETER', null, $msg);
        }
        $this->di->setShared('apiresponse', $response);
    }


    /**
     * Api Meta Envelope service
     *
     * @usage $this->getService('envelope')->setDetails(['teste'=>'abc','teste2'=>'123'])->setCommands(['logout']);
     *
     * @throws \Fwok\Exceptions\Exception
     */
    public function initApiResponseEnvelopeMeta()
    {
        $meta = new EnvelopeMeta();

        $this->di->setShared('apiresponseenvelopemeta', $meta);
    }


    /**
     * Security for Password
     *
     * @throws SystemException
     */
    public function initSecurity()
    {
        $security = new Security();

        // Set the password hashing factor to 12 rounds
        $security->setWorkFactor(12);

        $this->di->setShared('security', $security);
    }

    /**
     * Encrypt and Decrypt strings
     *
     * @usage $this->crypt->encrypt($text);
     * @usage $this->crypt->encryptBase64($text);
     *
     * @throws SystemException
     */
    public function initCrypt()
    {
        $crypt = new Crypt();
        $config = App::getConfig('application');

        if ($config->encryptKey) {
            $crypt->setKey($config->encryptKey);
        } else {
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, 'Invalid Application Encrypt Key');
        }

        if ($config->encryptCipher) {

            if (!in_array($config->encryptCipher, $crypt->getAvailableCiphers(), true)) {
                $dev = sprintf('Cipher [%s] is not available in system', App::getConfig('application')->encryptCipher);
                throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $dev);
            };

            $crypt->setCipher($config->application->encryptCipher);
        }
        $this->di->setShared('crypt', $crypt);
    }

    /**
     *
     */
    public function initRouter()
    {
        $router = new \Fwok\Routers\Router();
        $this->di->setShared('router', $router);
    }

    public function initTimezone()
    {
        $timezone = new Timezone();
        $timezone->setServerTimezone(App::getConfig('application')->timezone);
        // This value must be overwritten in identifing user's timezone
        $timezone->setUserTimezone(App::getConfig('application')->defaultUserTimezone);

        $this->di->setShared('timezone', $timezone);
    }
}
