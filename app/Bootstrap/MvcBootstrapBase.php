<?php
namespace Fwok\Bootstrap;

use Fwok\Application\ApplicationBase;
use Fwok\Exceptions\Exception;
use Fwok\Models\Users;
use Phalcon\Http\Request;
use Phalcon\Http\Response;
use Phalcon\Mvc\Dispatcher;

class MvcBootstrapBase extends BootstrapBase
{

    public function initFwokRoutes($routes = [ ])
    {
        $router = $this->di['router'];

        foreach ($routes as $route) {

            if ($route === 'file') {
                $router->add(
                    "/file/showbyname/:params",
                    [
                        'namespace'  => 'Fwok\Controllers',
                        'controller' => 'file',
                        'action'     => 'showbyname',
                        'params'     => 1,
                    ]
                );
                $router->add(
                    "/file/showbyid/:params",
                    [
                        'namespace'  => 'Fwok\Controllers',
                        'controller' => 'file',
                        'action'     => 'showbyid',
                        'params'     => 1,
                    ]
                );
                $router->add(
                    "/file/show/:params",
                    [
                        'namespace'  => 'Fwok\\Controllers',
                        'controller' => 'file',
                        'action'     => 'show',
                        'params'     => 1,
                    ]
                );
                $router->add(
                    "/file/view/{name_unique}/{name}",
                    [
                        'namespace'  => 'Fwok\\Controllers',
                        'controller' => 'file',
                        'action'     => 'view'
                    ]
                );
                $router->add(
                    "/file/download/{name_unique}/{name}",
                    [
                        'namespace'  => 'Fwok\Controllers',
                        'controller' => 'file',
                        'action'     => 'download'
                    ]
                );
                $router->add(
                    "/file/stream/{name_unique}/{name}",
                    [
                        'namespace'  => 'Fwok\Controllers',
                        'controller' => 'file',
                        'action'     => 'stream'
                    ]
                );
            }

            if ($route == 'errors') {
                $router->add(
                    "/errors/show/:params",
                    [
                        'namespace'  => 'Fwok\Controllers',
                        'controller' => 'errors',
                        'action'     => 'show',
                        'params'     => 1,
                    ]
                );
                $router->add(
                    "/errors/notfound",
                    [
                        'namespace'  => 'Fwok\Controllers',
                        'controller' => 'errors',
                        'action'     => 'notfound',
                    ]
                );
                $router->add(
                    "/errors/security",
                    [
                        'namespace'  => 'Fwok\Controllers',
                        'controller' => 'errors',
                        'action'     => 'security',
                    ]
                );
            }

        }
    }

    public function initDispatcher()
    {
        $eventsManager = $this->di->getShared('eventsManager');

        $eventsManager->attach("dispatch:beforeException",
            function ($event, $dispatcher, $exception) {
                /** @var Dispatcher $dispatcher */
                /** @var Exception $exception */
                switch ($exception->getCode()) {
                    case Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                    case Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                        $dispatcher->forward([
                            'namespace'  => 'Fwok\Controllers',
                            'controller' => 'errors',
                            'action'     => 'notfound',
                        ]);
                        return false;
                        break;
                    /* Não testei com as opções abaixo */
                    case Dispatcher::EXCEPTION_CYCLIC_ROUTING:
                    case Dispatcher::EXCEPTION_INVALID_HANDLER:
                        $dispatcher->forward([
                            'namespace'  => 'Fwok\Controllers',
                            'controller' => 'errors',
                            'action'     => 'notfound', // # TODO alterar página de erro ciclico
                        ]);
                        return false;
                        break;
                }
            }
        );

        $dispatcher = new Dispatcher();
        $dispatcher->setDefaultNamespace('Fwok\Controllers\\');
        $dispatcher->setEventsManager($eventsManager);

        $this->di['dispatcher'] = $dispatcher;
    }

    public function initSecurity()
    {
        $security = new \Phalcon\Security();

        //Set the password hashing factor to 12 rounds
        $security->setWorkFactor(12);

        $this->di['security'] = $security;
    }

    public function initUrl()
    {
        $url = new \Phalcon\Mvc\Url();
        $base_uri = $this->di['config']->application->baseUri ? $this->di['config']->application->baseUri : '/';
        $url->setBaseUri();

        $this->di['url'] = $url;
    }

    public function initResponse()
    {
        return new Response();
    }
    public function initRequest()
    {
        return new Request();
    }
    public function initRouter()
    {
        $router = new \Phalcon\Mvc\Router();
        // Define a route
        $router->add(
            "/:controller/:action/:params",
            array(
                "controller" => 1,
                "action"     => 2,
                "params"     => 3
            )
        );

        $this->di['router'] = $router;

    }

    public function initUser()
    {
        $user = new Users();
        if ($this->di['session']->user) {
            $user->fillFromArray($this->di['session']->user);
            $this->di['user'] = $user;
        }
        $this->di['user'] = $user;
    }

    public function initDb()
    {
        $config = $this->di['config'];

        $connection = new \Phalcon\Db\Adapter\Pdo\Mysql(
            [
                "host"       => $config->database->host,
                "username"   => $config->database->username,
                "password"   => $config->database->password,
                "dbname"     => $config->database->dbname,
                "persistent" => 1,
                "options"    => [ \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8' ]
            ]
        );

        $connection->setEventsManager($this->di->getShared('eventsManager'));
        $this->di['db'] = $connection;

    }

    public function initSession()
    {
        $session = new \Phalcon\Session\Adapter\Files();
        $session->start();

        $this->di['session'] = $session;
    }

    public function initFacebook()
    {
        $app_id = $this->getService('config')->facebook->appid;
        $app_secret = $this->getService('config')->facebook->secret;

        // Make sure to load the Facebook SDK for PHP via composer or manually

        \Facebook\FacebookSession::setDefaultApplication($app_id, $app_secret);
    }

    public function initGoogle()
    {
        $client_id = $this->getService('config')->google->client_id;
        $client_secret = $this->getService('config')->google->client_secret;
        $redirect_uri = $this->getService('config')->google->redirect_uri;
        $scope = $this->getService('config')->google->scope;

        $client = new \Google_Client();
        $client->setClientId($client_id);
        $client->setClientSecret($client_secret);
        $client->addScope('email');
        $client->setRedirectUri($redirect_uri);
        $client->addScope($scope);
        $this->di['google'] = $client;
    }

}
