<?php

namespace Fwok\UI;

Class Bootstrap
{
    public static function checkIcon($value)
    {
        if ($value) {
            $type = 'success';
            $icon = 'fa fa-check';
        } else {
            $type = 'danger';
            $icon = 'fa fa-remove';
        }

        return self::BsLabel(self::iconWrapper($icon), $type);
    }

    public static function BsLabel($value, $type = 'default')
    {
        return '<span class="label label-' . $type . '">' . $value . '</span>';
    }

    public static function requiredTick()
    {
        return '<span class="text-danger">*</span>';
    }

    public static function iconWrapper($icon)
    {
        return "<i class='" . $icon . "'></i>";
    }

    public static function starIcon($value, $show_empty = 1, $show_label = 1, $label_color = 'default')
    {
        $type = $label_color;

        if ($value) {
            $icon = 'fa fa-star';
        } else {
            $icon = 'fa fa-star-o';
        }

        if ($show_empty or $value) {
            if ($show_label) {
                return self::BsLabel(self::iconWrapper($icon), $type);
            } else {
                return self::iconWrapper($icon);
            }

        }
    }

    public static function trafficLightIcon($value = 0)
    {
        switch ($value) {
            case 0:
                $color = "danger";
                break;
            case 1:
                $color = "success";
                break;
            case 3:
                $color = "default";
                break;
            case 2:
                $color = "warning";
                break;
            case 4:
            default:
                $color = "muted";
        }

        return '<span class="text-' . $color . '"><i class="fa fa-circle"></i></span>';
    }

    public static function iconTooltip($title, $icon = 'fa-info-circle')
    {
        return '<i class="fa ' . $icon . '" data-toggle="tooltip" title="' . $title . '"></i>';
    }

    public static function helpBlock($text)
    {
        return "<span class=\"help-block\">$text</span>";
    }

    public static function popover($code, $icon = null)
    {
        if (!$icon) {
            $icon = 'fa-question-circle';
        }

        return '<a class="popup-ajax" href="#" data-popover="/uihelper/popover/' . $code . '"><i class="fa ' . $icon . '"></i></a>';
    }
}