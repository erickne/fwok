<?php

namespace Fwok\UI;

use Phalcon\Mvc\User\Component;

Class Navbar extends Component
{
    var $options;
    var $inverse;
    var $items;
    var $brand;
    var $brand_link;

    public function __construct($options = null)
    {
        $this->inverse = $options['inverse'];
        $this->fixed_top = $options['fixed_top'];
        $this->brand = $options['brand'];
        $this->brand_link = $options['brand_link'];

        return $this;
    }

    public function addItem($caption, $link, $position = 'left', $icon = null, $subitems = null, $class = null)
    {
        $this->items[] = $this->buildItem($caption, $link, $position, $icon, $subitems, $class);

        return $this;
    }

    public function buildItem($caption, $link, $position = 'left', $icon = null, $subitems = null, $class = null)
    {
        return [
            'caption'  => $caption,
            'link'     => $link,
            'position' => $position,
            'icon'     => $icon,
            'class'    => $class,
            'subitems' => $subitems,
        ];
    }

    public function buildHeader($caption)
    {
        return [
            'caption'  => $caption,
            'link'     => null,
            'position' => null,
            'icon'     => null,
            'class'    => 'dropdown-header',
            'subitems' => null,
        ];
    }

    public function buildDivider()
    {
        return [
            'caption'  => null,
            'link'     => null,
            'position' => null,
            'icon'     => null,
            'class'    => 'divider',
            'subitems' => null,
        ];
    }

    private function renderItem($item)
    {
        if (count($item['subitems'])) {
            $father_item[] = '
        <li class="dropdown">
          <a href="' . $item['link'] . '" class="dropdown-toggle ' . $item['class'] . '" data-toggle="dropdown">' . $item['caption'] . ' <b class="caret"></b></a>
          <ul class="dropdown-menu">';
            foreach ($item['subitems'] as $subitem) {
                $father_item[] = $this->renderItem($subitem);
            }
            $father_item[] = '
          </ul>
        </li>
';

            return implode("\n", $father_item);
        } else {
            if ($item['class'] == 'divider' or $item['class'] == 'dropdown-header') {
                return '            <li class="' . $item['class'] . '">' . $item['caption'] . '</li>';
            } else {
                return '            <li><a href="' . $item['link'] . '" class="' . $item['class'] . '">' . $item['caption'] . '</a></li>';
            }
        }
    }

    private function getPositionItems($position)
    {
        $return = [ ];
        foreach ($this->items as $item) {
            if ($item['position'] == $position) {
                $return[] = $item;
            }
        }

        return $return;

    }

    public function setBrand($html, $link = '/')
    {
        $this->brand = $html;
        $this->brand_link = $link;
    }

    public function render()
    {
        $render = [ ];
        $render[] = '
<div class="navbar navbar-default ' . (($this->fixed_top) ? 'navbar-fixed-top' : ' ') . ' ' . (($this->inverse) ? 'navbar-inverse' : '') . '">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="' . $this->brand_link . '">
' . $this->brand . '
      </a>
    </div>
    <div class="navbar-collapse collapse navbar-responsive-collapse">
      <ul class="nav navbar-nav">
';
        foreach ($this->getPositionItems('left') as $item) {
            $render[] = $this->renderItem($item);
        }

        $render[] = '
      </ul>

      <ul class="nav navbar-nav navbar-right">
';
        foreach ($this->getPositionItems('right') as $item) {
            $render[] = $this->renderItem($item);
        }

        $render[] = '
    </div>
  </div>
</div>
';
        var_dump($render);

        return implode("\n", $render);
    }
}