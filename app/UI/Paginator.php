<?php

namespace Fwok\UI;

use Fwok\Plugins\Logger;
use Phalcon\Mvc\User\Component;

Class Paginator extends Component
{
    var $page_current;
    var $items_per_page;
    var $items_count;
    var $qty_links = 9;
    var $uri;
    var $pages = [ ];
    var $options = [ ];
    var $messages = [ ];
    private $page_last;

    /**
     * Final function the return array with href and caption
     *
     * @param null $opt
     *
     * @return array
     */
    public function getPagination($opt = null)
    {

        if (!$this->checkVariables()) {
            return [ ];
        }

        $this->generatePages($opt);

        return $this->pages;
    }

    private function checkVariables()
    {
        $success = true;
        if (!$this->items_per_page) {
            Logger::shoutInAlert("Paginator items_per_page not set");
            $success = false;
        }
        if (!$this->items_count) {
            $this->options['only_first_and_last'] = true;
        }

        return $success;
    }

    private function generatePages($opt = null)
    {
        $this->options = $opt;

        $this->uri = str_replace('&page=' . $_GET['page'], '', $this->uri);

        if ($opt['show_text']) {
            $symbol_previous = 'Previous Page';
            $symbol_next = 'Next Page';
        } else {
            $symbol_previous = '&laquo;';
            $symbol_next = '&raquo;';
        }

        if ($opt['only_first_and_last']) {
            $this->page_last = $_GET['page'] + 1;
            $this->addItem(($_GET['page'] <= 1) ? 1 : ($_GET['page'] - 1), $symbol_previous, 0);
        } else {
            $this->page_last = (int) ceil($this->items_count / $this->items_per_page);
            $this->addItem(1, $symbol_previous, 0);

            if (!$this->page_current) {
                $this->page_current = 1;
            }

            $start = (($this->page_current - $this->qty_links) > 0) ? $this->page_current - $this->qty_links : 1;
            $end = (($this->page_current + $this->qty_links) < $this->page_last) ? $this->page_current + $this->qty_links : $this->page_last;

            for ($i = $start; $i <= $end; $i++) {
                $this->addItem($i, $i, ($this->page_current == $i) ? 1 : 0);
            }

        }

        $this->addItem($this->page_last, $symbol_next, 0);
    }

    /**
     * Internal function to add item to final array
     *
     * @param      $page_number
     * @param null $caption
     *
     * @param      $selected
     *
     * @return array
     */
    private function addItem($page_number, $caption, $selected)
    {
        if (is_null($caption)) {
            $caption = $page_number;
        }
        $page = new \stdClass();
        $page->href = $this->uri . '&page=' . $page_number;
        $page->caption = $caption;
        $page->class = ($selected) ? 'active' : '';

        $this->pages[] = $page;
    }
}
