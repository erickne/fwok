<?php

namespace Fwok\UI;

use Phalcon\Mvc\User\Component;

Class KpiDataset extends Component
{

    var $id;
    var $title;
    var $data;
    var $goal;
    var $average;

    const DEFAULT_COLOR_DATA = '#a6a6a6';
    const DEFAULT_COLOR_GOAL = '#777777';
    const DEFAULT_COLOR_AVERAGE = '#2f2f2f';

    public function __construct($title)
    {
        $this->id = md5(microtime(false));
        $this->setTitle($title);
        $this->setData();
        $this->setGoal();

        return $this;
    }

    public function check()
    {
        if (!$this->goal->values) {
            unset ($this->goal);
        }
        if (!$this->average->title) {
            unset ($this->average);
        }
    }

    public function debug()
    {
        var_dump($this->average);
    }

    private function setColor($color)
    {
        return '#' . str_replace('#', '', $color);
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function setData($values = null, $title = null, $color = null)
    {
        $this->data = new \stdClass();
        $this->data->title = $title;
        $this->data->color = $this->setColor((is_null($color) ? \Fwok\UI\KpiDataset::DEFAULT_COLOR_DATA : $color));
        $this->data->values = $values;

        return $this;
    }


    public function setGoal($values = null, $title = null, $color = null)
    {
        $this->goal = new \stdClass();
        $this->goal->title = $title;
        $this->goal->color = $this->setColor((is_null($color) ? \Fwok\UI\KpiDataset::DEFAULT_COLOR_GOAL : $color));
        $this->goal->values = $values;

        return $this;
    }

    public function setAverage($title = null, $color = null, $field = 'qty', $total_items = null)
    {
        $this->average = new \stdClass();
        $this->average->title = $title;
        $this->average->color = $this->setColor(
            (is_null($color) ? \Fwok\UI\KpiDataset::DEFAULT_COLOR_AVERAGE : $color)
        );
        $this->average->field = $field;
        $this->average->total_items = $total_items;
        $this->average->values = $this->calcAverage();

        return $this;

    }

    private function calcAverage()
    {

        $i = 0;
        $total = 0;
        foreach ($this->data->values as $value) {
            $i++;
            $total = $total + $value[$this->average->field];
        }
        if (!$this->average->total_items) {
            $$this->average->total_items = $i;
        }

        return $total / $this->average->total_items;

    }
}