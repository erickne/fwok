<?php


namespace Fwok\Modules\Api\Controllers;

use Fwok\Application\App;
use Fwok\Controllers\RESTController;
use Fwok\Models\Business;

class BusinessesController extends RESTController
{

    public function getAll()
    {
        $return = [ ];

        $businesses = Business::findByMainParameters($this->request->get(), true);

        foreach ($businesses as $business) {
            $return[] = $business->export();
        }

        return $return;
    }

    public function get($id)
    {
        $business = Business::findFirstById($id);

        return $business->export(
            [
                Business::EXPORT_CONTACT_USER  => true,
                Business::EXPORT_SUBSCRIPTIONS => true,
            ]
        );
    }

    public function getByHost($host)
    {
        $config = BusinessConfigs::queryFirstBy('host=',$host,true);
        $business = $config->getBusiness();
        Business::throwModelCheck($business);

        return $business->export();
    }
}
