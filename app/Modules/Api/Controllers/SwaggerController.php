<?php

namespace Fwok\Modules\Api\Controllers;

use Fwok\Controllers\RESTController;
use Fwok\Exceptions\InputException;
use Fwok\Exceptions\SystemException;
use Fwok\Models\Roles;
use Phalcon\Flash as Flash;
use Phalcon\Session as Session;
use Phalcon\Tag as Tag;

class SwaggerController extends RESTController
{
    public function dist()
    {
        if(file_exists('docs/dist.json')) {
            readfile('docs/dist.json'); // Read and print file
            die();
        }
        $msg = 'Swagger configuration file not found';
        throw new SystemException(SystemException::CONFIGURATION_INVALID,$msg);
    }
}
