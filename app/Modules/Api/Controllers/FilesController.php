<?php

namespace Fwok\Modules\Api\Controllers;

use Fwok\Models\Files;
use Fwok\Models\ModelBase;
use Fwok\Exceptions\InputException;
use Fwok\Controllers\RESTController;

class FilesController extends RESTController
{
    public function create($data = null)
    {
        $object = $this->saveData($data, ModelBase::ACTION_CREATE);

        return $this->respond($object->export([]), 201);
    }

    /**
     * @param null $data
     * @param null $action
     * @return $this|Files
     * @throws \Fwok\Exceptions\Exception
     * @throws InputException
     */
    private function saveData($data = null, $action = null)
    {
        if ($data === null) {
            $data = $this->bodyContent();
        }

        $this->db->begin();

        if ($action === Files::ACTION_CREATE) {
            $file = new File();
            $file->setDeleted(0);
        } else {
            $file = Files::findFirstValidById($data->id);
        }
        isset($data->status) ? $file->setStatusId($data->status->id) : null;
        isset($data->category) ? $file->setCategoryId($data->category->id) : null;
        isset($data->name) ? $file->setName($data->name) : null;
        isset($data->description) ? $file->setDescription($data->description) : null;
        isset($data->headline) ? $file->setHeadline($data->headline) : null;
        isset($data->cost) ? $file->setCost($data->cost) : null;
        isset($data->mainImageId) ? $file->setMainImageId($data->mainImageId) : null;

        $file->saveOrException();

        foreach ($data->uploadFiles as $uploadFile) {
            $file = new Files();
            //$file->setMime($data->mime);
            //$file->setName(($data->name) ? $data->name : $data->name);
            //$file->setCaption(($data->caption) ? $data->caption : $data->caption);
            //$file->setDescription(($data->description) ? $data->description : $data->description);
            $file->writeBase64UrlToFolder($uploadFile);
            $file->setObjectType(Files::OBJECT_NAME);
            $file->setObjectId($file->getId());
            $file->saveOrException();
        }

        $this->db->commit();

        return $file;
    }

    public function update($data = null)
    {
        $object = $this->saveData($data, ModelBase::ACTION_UPDATE);

        return $this->respond($object->export(), 200);
    }

    public function get($id)
    {
        $file = Files::query()
            ->addWhereBinded('id = ', $this->validateVar($id, 'id', 'int'))
            ->executeFirst();

        return $file->export();
    }

    public function getAll()
    {
        $return = [];
        $parameters = [
            'where' => json_decode($this->request->get('where')),
            'order' => json_decode($this->request->get('order'))
        ];
        $files = Files::findByMainParameters($parameters);

        foreach ($files as $file) {
            $return[] = $file->export([]);
        }

        return $return;
    }

    public function getAllFromBusinessId($business_id)
    {
        $return = [];
        $files = Files::query()
            ->searchInBusiness($business_id)
            ->execute();

        foreach ($files as $file) {
            $return[] = $file->export([]);
        }

        return $return;
    }

    public function delete($id)
    {
        $file = Files::findFirstValidById($id);

        return $file->delete();
    }
}
