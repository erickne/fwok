<?php


namespace Fwok\Modules\Api\Controllers;

use Fwok\Application\App;
use Fwok\Controllers\RESTController;
use Fwok\Models\BusinessSubscriptions;

class BusinessSubscriptionsController extends RESTController
{

    public function getAll()
    {
        $return = [ ];

        $businessSubscriptions = BusinessSubscriptions::findByMainParameters($this->request->get(), true);

        foreach ($businessSubscriptions as $businessSubscription) {
            $return[] = $businessSubscription->export();
        }

        return $return;
    }

    public function get($id)
    {
        $businessSubscription = BusinessSubscriptions::findFirstById($id);

        return $businessSubscription->export([
            BusinessSubscriptions::EXPORT_PAYMENTS => true
        ]);

    }

    public function ultraRefreshStatus($id)
    {
        $busSub = BusinessSubscriptions::findFirstById($id);

        return $busSub->retrieveExternalStatus();
    }
}
