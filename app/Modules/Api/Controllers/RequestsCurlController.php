<?php


namespace Fwok\Modules\Api\Controllers;

use Fwok\Application\App;
use Fwok\Controllers\RESTController;
use Fwok\Models\RequestsCurl;

class RequestsCurlController extends RESTController
{

    public function getAll()
    {
        $return = [ ];

        $curl_requests = RequestsCurl::findByMainParameters($this->request->get(), true);

        foreach ($curl_requests as $curl_request) {
            $return[] = $curl_request->export();
        }

        return $return;
    }

    public function get($id)
    {
        $curl_request = RequestsCurl::findFirstValidById($id);

        return $curl_request->export();

    }
}
