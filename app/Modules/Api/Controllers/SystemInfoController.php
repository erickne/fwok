<?php


namespace Fwok\Modules\Api\Controllers;

use Fwok\Application\App;
use Fwok\Controllers\RESTController;

class SystemInfoController extends RESTController
{

    public function getAll()
    {
        $return = [
            'user date' => App::getTimezone()->convertToUserDate(), // = date('Y-m-d H:i:s')
            'user timezone' => App::getTimezone()->getUserTimezone(),
            'date default timezone' => date_default_timezone_get(),
            'server date' => App::getTimezone()->convertToServerDate(),
            'server timezone' => App::getTimezone()->getServerTimezone()
        ];
        
        return $return;
    }
    
    public function getStatus()
    {
        die('OK');
    }

}
