<?php


namespace Fwok\Modules\Api\Controllers;

use Fwok\Controllers\RESTController;
use Fwok\Models\Discounts;

class DiscountsController extends RESTController
{

    public function getAll()
    {
        $return = [ ];

        $discounts = Discounts::find();

        foreach ($discounts as $discount) {
            $return[] = $discount->export();
        }

        return $return;
    }

    public function get($id)
    {
        $discount = Discounts::findFirstValidById($id);

        return $discount->export();

    }
}
