<?php


namespace Fwok\Modules\Api\Controllers;

use Fwok\Application\App;
use Fwok\Controllers\RESTController;
use Fwok\Models\Trace;

class TracesController extends RESTController
{

    public function getAll()
    {
        $return = [ ];

        $traces = Trace::findByMainParameters($this->request->get(), true);

        foreach ($traces as $trace) {
            $return[] = $trace->export([ ]);
        }

        return $return;
    }

    public function get($id)
    {
        $trace = Trace::findFirstValidById($id);

        return $trace->export([ ]);

    }
}
