<?php


namespace Fwok\Modules\Api\Controllers;

use Fwok\Application\App;
use Fwok\Controllers\RESTController;
use Fwok\Models\NotificationsCloud;

class CloudNotificationsController extends RESTController
{

    public function getAll()
    {
        $return = [ ];

        $notifications = NotificationsCloud::findByMainParameters($this->request->get(), true);

        foreach ($notifications as $notification) {
            $return[] = $notification->export([]);
        }

        return $return;
    }

    public function get($id)
    {
        $notification = NotificationsCloud::findFirstValidById($id);

        return $notification->export([]);

    }
}
