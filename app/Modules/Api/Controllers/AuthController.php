<?php

namespace Fwok\Modules\Api\Controllers;

use Fwok\Application\App;
use Fwok\Controllers\RESTController;
use Fwok\Exceptions\InputException;
use Fwok\Models\Business;
use Fwok\Models\Roles;
use Fwok\Models\Users;
use Fwok\Services\EmailsService;

class AuthController extends RESTController
{

    public function login()
    {
        $data = $this->bodyContent();

        $this->validateVar($data->email, null, null, App::getTranslatedKey('INVALID_EMAIL'));
        $this->validateVar($data->password, null, null, App::getTranslatedKey('INVALID_PASSWORD'));

        $user = Users::loginEmail($data->email, $data->password);

        if (!$user) {
            throw new InputException(InputException::INVALID_AUTHENTICATION, null, null, 403);
        }

        if($user->getSuspended()) {
            throw new InputException(InputException::PERMISSION_INVALID, 'USER_IS_SUSPENDED', null, 403);
        }

        if(!$user->hasRole(Roles::ADMIN)) {
            throw new InputException(InputException::PERMISSION_INVALID, 'INVALID_PERMISSION', null, 403);
        }
        $user->createNewToken();

        return $user->export([]);
    }

    public function changePassword()
    {
        $data = $this->bodyContent();
        if (!$data->codeResetPassword) {
            throw new InputException(InputException::VALUE_INVALID, 'INVALID_CODE');
        }

        $user = Users::findFirst(['code_reset_password = :code:', 'bind' => ['code' => $data->codeResetPassword]]);
        if (!$user or !$data->codeResetPassword) {
            $dev = 'User not found with given reset code';
            throw new InputException(InputException::NOT_FOUND, 'INVALID_CODE', $dev);
        }

        if ($data->passwordNew !== $data->passwordConfirm) {
            throw new InputException(InputException::VALUE_INVALID, 'PASSWORD_MISMATCH');
        }

        $user->setPassword($data->passwordNew);
        $user->setCodeResetPassword('');
        $user->saveOrException();

        return $this->respond($user->export(), 200);
    }

    /**
     * @throws Exception
     */
    public function lostPassword()
    {
        $data = $this->bodyContent();

        $user = Users::queryFirstBy('email=',$data->email,true);

        $this->db->begin();

        $user->setCodeResetPassword(Users::generateRandomCode());
        $user->saveOrException();

        $e = new EmailsService(Business::queryFirstBy('id=',BUSINESS_ID));

        $name = ($user->getName()) ? $user->getName() : '';
        $link = App::getConfig('application')->baseHost . '#!/auth/change-password/' . $user->getCodeResetPassword();

        $body = sprintf(
            "<p>Olá %s,</p>
<p>Clique no link abaixo para redefinir sua senha.</p>
<p><a href='%s'>Redefinir Senha</a><br>%s</p>",
            $name,
            $link,
            $link
        );

        $e->appendSubject('Reiniciar a senha');
        $e->addAddress($user->getEmail(), $name);
        $e->setBody($body);

        if($e->saveToPool()) {
            $this->db->commit();
            App::getApiResponseEnvelopeMeta()->addMessage('Não esqueça de verificar sua caixa de SPAM.','toaster','Email Enviado!','info');
            return true;
        }
        return false;
    }


    public function loginUltra()
    {
        $data = $this->bodyContent();

        $user = Users::loginEmail($data->email, $data->password);

        if ($user) {
            $user->hasRole(Roles::ULTRA, true);
            $user->createNewToken();

            return $user->export([ ]);
        } else {
            throw new InputException(InputException::INVALID_AUTHENTICATION, null, null, 403);
        }

    }
}
