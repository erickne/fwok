<?php


namespace Fwok\Modules\Api\Controllers;

use Fwok\Application\App;
use Fwok\Controllers\RESTController;
use Fwok\Exceptions\InputException;
use Fwok\Exceptions\IntegrationException;
use Fwok\Models\BusinessSubscriptions;
use Fwok\Models\Discounts;
use Fwok\Models\Payments;
use Fwok\Models\Subscriptions;

class PaymentsController extends RESTController
{

    public function ultraGetAll()
    {
        $return = [];

        $payments = Payments::findByMainParameters($this->request->get(), true);

        foreach ($payments as $payment) {
            $return[] = $payment->export();
        }

        return $return;
    }

    public function ultraGet($id)
    {
        $payment = Payments::findFirstValidById($id);

        return $payment->export([
            Payments::EXPORT_BUSINESS_SUBSCRIPTION => true
        ]);

    }

    public function ultraRefreshStatus($id)
    {
        $payment = Payments::findFirstValidById($id);

        return $payment->retrieveExternalStatus();
    }

    /**
     * @return array
     * @throws \Fwok\Exceptions\Exception
     * @throws InputException
     * @throws IntegrationException
     * @throws \Exception
     * @throws \Fwok\Exceptions\SystemException
     */
    public function pagseguroLink()
    {
        $data = $this->bodyContent();
        $this->db->begin();

        $subscription = Subscriptions::findFirstValidById($data->subscription->id, 1, 1);
        $discount = Discounts::findFirstValidById($data->discount->id, 1, 0);
        $config = $this->getService('config');

        if ($subscription->getFinalDatePagseguro()) {
            $deadline = $subscription->getFinalDatePagseguro();
        } else {
            $deadline = '+ 1 year';
        }

        $business_subscription = new BusinessSubscriptions();
        $business_subscription->setSubscriptionId($subscription->getId());
        $business_subscription->setBusinessId(App::getUser()->getBusiness()->getId());
        $business_subscription->setDiscountId(($discount) ? $discount->getId() : null);
        $business_subscription->setStatus(BusinessSubscriptions::STATUS_UNPAID);
        $business_subscription->setPaymentMethod(BusinessSubscriptions::PAYMENT_METHOD_PAGSEGURO);
        $business_subscription->setSuccess(0);
        $business_subscription->setCost($subscription->getCost());
        $business_subscription->saveOrException();

        // Instantiate a new pre-approval request
        $amount = number_format($business_subscription->getCalculatedPrice(), 2);
        $date = date('Y-m-d H:i:s');
        $new_date = strtotime($deadline, strtotime($date));

        $query_data = [
            'auth_token=' . App::getUser()->getLastToken()->getValue(),
            'business_subscription_id=' . $business_subscription->getId()
        ];
        $url_redirect = $config->application->baseHost . '#/payments/pagseguroreturn?' . implode('&', $query_data);
        //$url_notification = $config->application->baseHost . 'hooks/pagseguro';
        $url_review = $config->application->baseHost;


        $preApprovalRequest = new \PagSeguroPreApprovalRequest();
        $preApprovalRequest->setCurrency(strtoupper($subscription->getCurrencyCode()));
        $preApprovalRequest->setReference($business_subscription->getId()); # Account Subscription ID
        $preApprovalRequest->setPreApprovalCharge('auto');
        $preApprovalRequest->setPreApprovalName($subscription->getName());
        $preApprovalRequest->setPreApprovalDetails($subscription->getDescription());
        $preApprovalRequest->setPreApprovalAmountPerPayment($amount);
        $preApprovalRequest->setPreApprovalPeriod($subscription->getBillingPeriod('ly'));
        $preApprovalRequest->setPreApprovalMaxTotalAmount($amount);
        $preApprovalRequest->setPreApprovalFinalDate(date('c', $new_date));
        //$preApprovalRequest->setNotificationURL($url_notification);
        //$preApprovalRequest->setRedirectURL($url_redirect);
        //$preApprovalRequest->setReviewURL($url_review);
        try {
            $response = $preApprovalRequest->register(App::getPagSeguro()->getCredentials());
        } catch (\PagSeguroServiceException $e) {
            $this->db->rollback();
            throw new IntegrationException(
                IntegrationException::INVALID_RESPONSE,
                'Resposta inválida do Pagseguro.',
                'Pagseguro returned an unexpected response. Please check Pagseguro Log.',
                500,
                [],
                $e
            );
        }

        /** @noinspection OffsetOperationsInspection */
        $business_subscription->setPaymentUrl($response['checkoutUrl']);
        $business_subscription->saveOrException();

        $this->db->commit();

        return $business_subscription->export();

    }

    public function freeSubscription()
    {
        $this->db->begin();
        $subscription = Subscriptions::findFirstFree();
        $business = App::getBusiness();
        if (App::getBusiness()->getActiveBusinessSubscription()->getSubscriptionId() == $subscription->getId()) {
            throw new InputException(InputException::VALUE_INVALID, 'SUBSCRIPTION_ALREADY_ATTACHED_TO_BUSINESS', null);
        }

        $business->deactivateAllSubscription();

        $result = $business->createSubscription($subscription, true);
        $this->db->commit();

        return $result;
    }
}
