<?php


namespace Fwok\Modules\Api\Controllers;

use Fwok\Controllers\RESTController;
use Fwok\Models\Config;

class ConfigurationsController extends RESTController
{

    public function getAll()
    {
        $return = [ ];

        $configurations = Config::find();

        foreach ($configurations as $configuration) {
            $return[] = $configuration->export();
        }

        return $return;
    }

    public function get($id)
    {
        $configuration = Config::findFirstValidById($id);

        return $configuration->export();

    }
}
