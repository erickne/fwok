<?php


namespace Fwok\Modules\Api\Controllers;

use Fwok\Controllers\RESTController;
use Fwok\Models\Logs;

class LogFilesController extends RESTController
{

    public function getAll()
    {
        $return = [ ];

        $logs = Logs::find();

        foreach ($logs as $log) {
            $return[] = $log->export();
        }

        return $return;
    }

    public function getFile($filename)
    {
        $log = new Logs(APPLICATION_PATH.Logs::$LOG_DIR . $this->filter->sanitize($filename, 'string'));

        return $log->getContent(true);
    }

    public function deleteFile($filename)
    {
        $log = new Logs(APPLICATION_PATH.Logs::$LOG_DIR . $this->filter->sanitize($filename, 'string'));

        return $log->delete();
    }
}
