<?php

namespace Fwok\Modules\Api\Controllers;

use Fwok\Controllers\RESTController;
use Fwok\Application\App;
use Fwok\Models\UsersSettings;

class UsersSettingsController extends RESTController
{
    public function create($data)
    {
        $object = $this->saveData($data, UsersSettings::ACTION_CREATE);
        return $this->respond($object->export(), 201);
    }

    private function saveData($data = null, $action)
    {
        if (null === $data) {
            $data = $this->bodyContent();
        }

        $this->db->begin();

        if ($action === UsersSettings::ACTION_CREATE) {
            $fwok_users_settings = new UsersSettings();
            $fwok_users_settings->setDeleted(0);
            $fwok_users_settings->setUserId(App::getUser()->getId());
        } else {//if ($action == UsersSettings::ACTION_UPDATE) {
            $fwok_users_settings = UsersSettings::query()
                ->addWhereBinded('user_id = ',App::getUser()->getId())
                ->addWhereBinded('id = ', $data->id)
                ->executeFirst();
            UsersSettings::throwModelCheck($fwok_users_settings, true, false, $data->id);
        }

		isset($data->type) ? $fwok_users_settings->setType($data->type) : null;
		isset($data->key) ? $fwok_users_settings->setKey($data->key) : null;
		isset($data->value) ? $fwok_users_settings->setValue($data->value) : null;
		isset($data->name) ? $fwok_users_settings->setName($data->name) : null;

        $fwok_users_settings->saveOrException();

        $this->db->commit();

        return $fwok_users_settings;
    }

    public function update($data = null)
    {
        $object = $this->saveData($data, UsersSettings::ACTION_UPDATE);

        return $this->respond($object->export(), 200);
    }

    public function get($id)
    {
        $fwok_users_settings = UsersSettings::query()
            ->addWhereBinded('user_id = ',App::getUser()->getId())
            ->addWhereBinded('id = ', $id)
            ->executeFirst();

        return $fwok_users_settings->export();
    }
    public function getKey($key)
    {
        $fwok_users_settings = UsersSettings::query()
            ->addWhereBinded('user_id = ',App::getUser()->getId())
            ->addWhereBinded('key = ', $key)
            ->executeFirst();
        UsersSettings::throwModelCheck($fwok_users_settings);

        return $fwok_users_settings->export();
    }

    public function getAll()
    {
        $return = [];

        $results = UsersSettings::query()
            ->addWhereBinded('user_id = ',App::getUser()->getId());

        $type = $this->request->get('type', 'string');
        if ($type) {
            $results->addWhereBinded('type = ', $type);;
        }
        $key = $this->request->get('key', 'string');
        if ($key) {
            $results->addWhereBinded('key = ', $key);;
        }
        $name = $this->request->get('name', 'string');
        if ($name) {
            $results->addWhereBinded('name = ', $name);;
        }

        foreach ($results->execute() as $fwok_users_settings) {
            $return[] = $fwok_users_settings->export();
        }

        return $return;

    }

    public function delete($id)
    {
        $fwok_users_settings = UsersSettings::query()
            ->addWhereBinded('user_id = ',App::getUser()->getId())
            ->addWhereBinded('id = ', $id)
            ->executeFirst();

        return $fwok_users_settings->deleteOrException();
    }
}
