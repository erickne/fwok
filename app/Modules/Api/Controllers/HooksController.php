<?php


namespace Fwok\Modules\Api\Controllers;

use Fwok\Application\App;
use Fwok\Controllers\RESTController;
use Fwok\Exceptions\InputException;
use Fwok\Exceptions\IntegrationException;
use Fwok\Helpers\Format;
use Fwok\Models\BusinessSubscriptions;
use Fwok\Models\Hooks;
use Fwok\Models\Payments;
use Fwok\Models\Subscriptions;

class HooksController extends RESTController
{

    public function getAll()
    {
        $return = [];

        $hooks = Hooks::findByMainParameters($this->request->get(), true);

        App::getApiResponseEnvelopeMeta()->setPageValue('title', 'WebHooks');
        App::getApiResponseEnvelopeMeta()->setPageValue('subTitle', 'Received notification hooks from other servers');

        foreach ($hooks as $hook) {
            $return[] = $hook->export();
        }

        return $return;
    }

    public function get($id)
    {
        $hook = Hooks::findFirstValidById($id);

        return $hook->export();

    }

    public function pagseguro()
    {
        $pagseguro = App::getPagSeguro();
        $code = $this->request->getPost('notificationCode', 'string');
        $type = $this->request->getPost('notificationType', 'string');

        if (!$code or !$type) {
            $msg = sprintf('Could not understand hook from pagseguro. Type [%s] Code [%s]', $type, $code);
            throw new IntegrationException(IntegrationException::INVALID_REQUEST, null, $msg);
        }

        $this->db->begin();

        $hook = new Hooks();
        $hook->setMethod(Hooks::METHOD_PAGSEGURO)
            ->setGetData(App::getStaticService('request')->get())
            ->setPostData(App::getStaticService('request')->getPost())
            ->setCode($code)
            ->setType($type)
            ->saveOrExceptionNewTransaction();

        $notificationType = new \PagSeguroNotificationType($type);
        $strType = $notificationType->getTypeFromValue();
        switch ($strType) {
            case 'TRANSACTION':
                $processed = $pagseguro->notificationTransaction($code, $hook);
                break;
            case 'APPLICATION_AUTHORIZATION':
                $processed = $pagseguro->notificationAuthorization($code);
                break;
            case 'PRE_APPROVAL':
                $processed = $pagseguro->notificationPreApproval($code, $hook);
                break;
            default:
                $msg = sprintf('Unknown pagseguro notification type [%s]', $notificationType->getValue());
                throw new IntegrationException(IntegrationException::INVALID_REQUEST, null, $msg);
        }

        $hook->setProcessed($processed)
            ->saveOrExceptionNewTransaction();

        $this->db->commit();

        return 'OK';
    }

    public function androidpay()
    {
        /*
        $data = '{
            "orderId":"",
   "productId":"plan_professional",
   "purchaseInfo":{
      "responseData":"{\"packageName\":\"com.rockapps.businessmanagement\",\"productId\":\"plan_professional\",
        \"purchaseTime\":1452286828155,\"purchaseState\":0,\"developerPayload\":\"subs:plan_professional\",
        \"purchaseToken\":\"akoacoldgoeniegfblkjkmmd.AO-J1Oz4pqTa3eRheBIDTJg_Kjka_k3dO_XUKNPqggxZXBzRZlJ4d4zgTrb7qY7_7
        VAh4iuRMUml2P_eFzUwlD54oykzMswgNB_2tyvKnRyv7HK9vnbLCG7puOqdf633ku_cqMQww1rQycKnPMciTK4s7SvE6WgWvg\",
        \"autoRenewing\":true}",
      "signature":"IhCgXm+kJN9UUKSpcJWQMsbRsR0b7GgceMJST+EB8qFW+gwq2N3oBao4wAvaWIstBhFNJlHcErhgqQZNT7vspW0qCWGxJL6/o0
        eneuEm0/av2OkElMLdmGfa3MeXCeIPsRVz4CYF76BMRMkacAkquGNSCRbXtO+WLjtqflmTJ6HKahyysTK54hCJL65Ql9SeJciWTBBHmpTO
        DZTw5QUH6o6qiQJCol3dFKwAPTVQfvt02BOdzKeBPSReg5Sd1x0gnHSZqwrDDZymPc/b3DgSz3+BbfUJQzBW2EAyCkH/qHg92G2OrIv/sdh6
        GJBKBw3mXiQbYXGyTH/Hkt1RVbPw7Q\u003d\u003d"
   },
   "purchaseTime":"Jan 8, 2016 7:00:28 PM",
   "purchaseToken":"akoacoldgoeniegfblkjkmmd.AO-J1Oz4pqTa3eRheBIDTJg_Kjka_k3dO_XUKNPqggxZXBzRZlJ4d4zgTrb7qY7_7VAh4iu
        RMUml2P_eFzUwlD54oykzMswgNB_2tyvKnRyv7HK9vnbLCG7puOqdf633ku_cqMQww1rQycKnPMciTK4s7SvE6WgWvg"
}';
        $data = json_decode($data);
        /***** REMOVE ABOVE HERE *****/

        $this->db->begin();
        $data = $this->bodyContent()->androidPay;

        $hook = new Hooks();
        $hook->setMethod(Hooks::METHOD_ANDROIDPAY)
            ->setGetData(App::getStaticService('request')->get())
            ->setPostData(App::getStaticService('request')->getPost())
            ->saveOrExceptionNewTransaction();

        $google_subscription = App::getAndroidPay()->getSubscriptionPurchase($data->productId, $data->purchaseToken);

        $existing_payment = Payments::findFirstByExternalToken($data->purchaseToken);
        if ($existing_payment) {
            /*
             * Inicialmente, imaginei que o token era único. Mas após alguns testes
             * o google retornou o mesmo token. Dessa forma, reescrevi o external_token
             * e vou criar outro pagamento no final do script.
             *
             * $dev = 'Google token already in payments database';
             * throw new InputException(InputException::VALUE_INVALID, 'PAYMENT_ALREADY_EXISTS', $dev);
             */
            $existing_payment->setExternalToken('REPLACED_' . $existing_payment->getExternalToken());
            $existing_payment->saveOrException();
        }

        $subscription = Subscriptions::findFirstByCode($data->productId);
        if (!$subscription) {
            $msg = 'Subscription code not found in database';
            throw new InputException(InputException::VALUE_INVALID, 'INVALID_SUBSCRIPTION', $msg);
        }
        // Verifiquei novamente para aproveitar o método com as validações
        $subscription = Subscriptions::findFirstValidById($subscription->getId());

        if (!$subscription->getActive()) {
            throw new InputException(InputException::VALUE_INVALID, 'INVALID_SUBSCRIPTION', 'Subscription inactive');
        }
        
        $expireDatetime = date('Y-m-d H:i:s', $google_subscription->expiryTimeMillis / 1000);
        $startDatetime = date('Y-m-d H:i:s', $google_subscription->startTimeMillis / 1000);

        if (!Format::isFuture($expireDatetime)) {
            $msg = 'Subscription expire date is past';
            throw new InputException(
                InputException::VALUE_INVALID,
                'INVALID_SUBSCRIPTION',
                $msg,
                400,
                [],
                null,
                $google_subscription
            );
        }
        $business = App::getBusiness();
        $business->deactivateAllSubscription();

        $business_subscription = new BusinessSubscriptions();
        $business_subscription->setSubscriptionId($subscription->getId());
        $business_subscription->setBusinessId($business->getId());
        $business_subscription->setPaymentMethod(BusinessSubscriptions::PAYMENT_METHOD_ANDROID_PAY);
        $business_subscription->setCost($subscription->getCost());
        $business_subscription->setSuccess(1);
        $business_subscription->setStatus(BusinessSubscriptions::STATUS_ACTIVE);
        $business_subscription->setStartDate($startDatetime);
        $business_subscription->setEndDate($expireDatetime);
        $business_subscription->saveOrException();

        $payment = new Payments();
        $payment->setMethod(Payments::METHOD_ANDROID_PAY);
        $payment->setStatus(Payments::STATUS_PAID);
        $payment->setExternalToken($data->purchaseToken);
        $payment->setBusinessSubscriptionId($business_subscription->getId());
        $payment->setValue($subscription->getCost());
        $payment->setCurrencyCode($google_subscription->getPriceCurrencyCode());
        $payment->saveOrException();

        $business_subscription->setPaymentId($payment->getId());
        $business_subscription->saveOrException();
        $business_subscription->activate();

        $hook->setProcessed(1)
            ->saveOrExceptionNewTransaction();
        
        $this->db->commit();

        return true;
    }
}
