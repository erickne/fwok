<?php


namespace Fwok\Modules\Api\Controllers;

use Fwok\Application\App;
use Fwok\Controllers\RESTController;
use Fwok\Models\Users;

class UsersController extends RESTController
{

    public function getAll()
    {
        $return = [];

        $users = Users::findByMainParameters($this->request->get(), true);

        foreach ($users as $user) {
            $return[] = $user->export();
        }

        return $return;
    }

    public function get($id)
    {
        $user = Users::findFirstValidById($id);

        return $user->export([
            Users::EXPORT_ACCOUNT_INFO => true,
            Users::EXPORT_PERMISSIONS => true,
            Users::EXPORT_TEAMS => true,
        ]);
    }

    public function getMe()
    {
        $user = App::getUser();

        return $user->export();

    }
}
