<?php


namespace Fwok\Modules\Api\Controllers;

use Fwok\Controllers\RESTController;
use Fwok\Models\Subscriptions;

class SubscriptionsController extends RESTController
{

    public function getAll()
    {
        $return = [ ];

        $subscriptions = Subscriptions::find();

        foreach ($subscriptions as $subscription) {
            $return[] = $subscription->export();
        }

        return $return;
    }

    public function get($id)
    {
        $subscription = Subscriptions::findFirstValidById($id);

        return $subscription->export();

    }
}
