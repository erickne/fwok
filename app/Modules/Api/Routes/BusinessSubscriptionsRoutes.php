<?php

namespace Fwok\Modules\Api\Routes;

use Fwok\Models\Roles;
use Fwok\Modules\Api\Controllers\BusinessesController;
use Fwok\Modules\Api\Controllers\BusinessSubscriptionsController;

class BusinessSubscriptionsRoutes extends FwokRoutesBase
{
    public function __construct()
    {

        $this->setHandler(new BusinessSubscriptionsController());

        $this->setPrefix('/business-subscriptions');

        $this->get('/', 'getAll', null, [ Roles::ULTRA ]);
        $this->get('/{id}', 'get', null, [ Roles::ULTRA ]);
        $this->get('/refresh/{id}', 'ultraRefreshStatus', null, [Roles::ULTRA]);


    }
}
