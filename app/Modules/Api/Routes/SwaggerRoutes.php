<?php

namespace Fwok\Modules\Api\Routes;

use Fwok\Routes\RoutesBase;

class SwaggerRoutes extends RoutesBase
{
    public function __construct()
    {
        /**
         * It's mandatory to have one controller. Using a dummy.
         */
        $this->setHandler(new \Fwok\Modules\Api\Controllers\SwaggerController());

        $this->setPrefix('/swagger');

        $this->get('/dist.json', 'dist','Swagger Documentation',[]);
    }
}
