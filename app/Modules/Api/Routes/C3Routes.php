<?php

namespace Fwok\Modules\Api\Routes;

use Fwok\Controllers\RESTController;
use Phalcon\Mvc\Micro\Collection;

class C3Routes extends Collection
{
    public function __construct()
    {
        /**
         * It's mandatory to have one controller. Using a dummy.
         */
        $this->setHandler(new RESTController());

        $this->setPrefix('/c3');

        $this->get(
            '/report/clover',
            function () {
            }
        );
        $this->get(
            '/report/serialized',
            function () {
            }
        );
        $this->get(
            '/report/html',
            function () {
            }
        );
        $this->get(
            '/report/clean',
            function () {
            }
        );
        $this->get(
            '/report/clear',
            function () {
            }
        );
        $this->get(
            '/c3.php',
            function () {
            }
        );
    }
}
