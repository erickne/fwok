<?php

namespace Fwok\Modules\Api\Routes;

use Fwok\Models\Roles;
use Fwok\Modules\Api\Controllers\PaymentsController;

class PaymentsRoutes extends FwokRoutesBase
{
    public function __construct()
    {

        $this->setHandler(new PaymentsController());

        $this->setPrefix('/payments');

        $this->get('/', 'ultraGetAll', null, [Roles::ULTRA]);
        $this->get('/{id}', 'ultraGet', null, [Roles::ULTRA]);
        $this->get('/refresh/{id}', 'ultraRefreshStatus', null, [Roles::ULTRA]);

    }
}
