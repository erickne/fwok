<?php

namespace Fwok\Modules\Api\Routes;

use Fwok\Models\Roles;
use Fwok\Modules\Api\Controllers\SubscriptionsController;

class SubscriptionsRoutes extends FwokRoutesBase
{
    public function __construct()
    {

        $this->setHandler(new SubscriptionsController());

        $this->setPrefix('/subscriptions');

        $this->get('/', 'getAll',null,[Roles::ULTRA]);
        $this->get('/{id}', 'get', null, [ Roles::ULTRA ]);


    }
}
