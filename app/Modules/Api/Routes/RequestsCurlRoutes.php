<?php

namespace Fwok\Modules\Api\Routes;

use Fwok\Models\Roles;
use Fwok\Modules\Api\Controllers\RequestsCurlController;

class RequestsCurlRoutes extends FwokRoutesBase
{
    public function __construct()
    {

        $this->setHandler(new RequestsCurlController());

        $this->setPrefix('/curlrequests');

        $this->get('/', 'getAll',null,[Roles::ULTRA]);
        $this->get('/{id}', 'get', null, [ Roles::ULTRA ]);


    }
}
