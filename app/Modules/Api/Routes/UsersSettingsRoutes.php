<?php

namespace Fwok\Modules\Api\Routes;

use Fwok\Routes\RoutesBase;
use Fwok\Models\Roles;
use Fwok\Modules\Api\Controllers\UsersSettingsController;

class UsersSettingsRoutes extends RoutesBase
{
    public function __construct()
    {

        $this->setHandler(new UsersSettingsController());

        $this->setPrefix('/users-settings');
        $this->get('/', 'getAll', 'Get All UsersSettings');
        $this->get('/{id}', 'get', 'Get UsersSettings');
        $this->get('/key/{key}', 'getKey', 'Get UsersSettings By Key');
        $this->post('/', 'create', 'Create New UsersSettings', [Roles::ADMIN]);
        $this->delete('/{id}', 'delete', 'Delete UsersSettings', [Roles::ADMIN]);
        $this->put('/', 'update', 'Update UsersSettings', [Roles::ADMIN]);
    }
}
