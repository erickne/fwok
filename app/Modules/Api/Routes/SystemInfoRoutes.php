<?php

namespace Fwok\Modules\Api\Routes;

use Fwok\Models\Roles;
use Fwok\Modules\Api\Controllers\SystemInfoController;

class SystemInfoRoutes extends FwokRoutesBase
{
    public function __construct()
    {

        $this->setHandler(new SystemInfoController());

        $this->setPrefix('/system-info');

        $this->get('/', 'getAll',null,[Roles::ULTRA]);
        $this->get('/status', 'getStatus',null,[],[]);

    }
}
