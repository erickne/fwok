<?php

namespace Fwok\Modules\Api\Routes;

use Fwok\Models\Roles;
use Fwok\Modules\Api\Controllers\DiscountsController;

class DiscountsRoutes extends FwokRoutesBase
{
    public function __construct()
    {

        $this->setHandler(new DiscountsController());

        $this->setPrefix('/discounts');

        $this->get('/', 'getAll',null,[Roles::ULTRA]);
        $this->get('/{id}', 'get', null, [ Roles::ULTRA ]);


    }
}
