<?php

namespace Fwok\Modules\Api\Routes;

use Fwok\Models\Roles;
use Fwok\Modules\Api\Controllers\LogFilesController;

class LogFilesRoutes extends FwokRoutesBase
{
    public function __construct()
    {

        $this->setHandler(new LogFilesController());

        $this->setPrefix('/log-files');

        $this->get('/', 'getAll',null,[Roles::ULTRA]);
        $this->get('/{filename}', 'getFile',null,[Roles::ULTRA]);
        $this->delete('/{filename}', 'deleteFile',null,[Roles::ULTRA]);

    }
}
