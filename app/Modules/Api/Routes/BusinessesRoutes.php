<?php

namespace Fwok\Modules\Api\Routes;

use Fwok\Models\Roles;
use Fwok\Modules\Api\Controllers\BusinessesController;

class BusinessesRoutes extends FwokRoutesBase
{
    public function __construct()
    {

        $this->setHandler(new BusinessesController());

        $this->setPrefix('/businesses');

        $this->get('/', 'getAll', null, [ Roles::ULTRA ]);
        $this->get('/{id}', 'get', null, [ Roles::ULTRA ]);
        $this->get('/host/{host}', 'getByHost', null);


    }
}
