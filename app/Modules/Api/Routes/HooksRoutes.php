<?php

namespace Fwok\Modules\Api\Routes;

use Fwok\Models\Roles;
use Fwok\Modules\Api\Controllers\HooksController;

class HooksRoutes extends FwokRoutesBase
{
    public function __construct()
    {
        $this->setHandler(new HooksController());

        $this->setPrefix('/hooks');

        $this->get('/', 'getAll', null, [ Roles::ULTRA ]);
        $this->get('/{id}', 'get', null, [ Roles::ULTRA ]);
        $this->post('/pagseguro', 'pagseguro');
        $this->post('/androidpay', 'androidpay');

    }
}
