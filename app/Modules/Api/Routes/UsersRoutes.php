<?php

namespace Fwok\Modules\Api\Routes;

use Fwok\Models\Roles;
use Fwok\Modules\Api\Controllers\UsersController;

class UsersRoutes extends FwokRoutesBase
{
    public function __construct()
    {

        $this->setHandler(new UsersController());

        $this->setPrefix('/users');

        $this->get('/', 'getAll',null,[Roles::ULTRA]);
        $this->get('/{id}', 'get', null, [ Roles::ULTRA ]);
        $this->get('/me', 'getMe', null, [ Roles::ULTRA ]);

    }
}
