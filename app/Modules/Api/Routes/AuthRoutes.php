<?php

namespace Fwok\Modules\Api\Routes;

use Fwok\Modules\Api\Controllers\AuthController;

class AuthRoutes extends \Fwok\Routes\RoutesBase
{
    /**
     * StatusRoutes constructor.
     */
    public function __construct()
    {
        $this->setHandler(new AuthController());

        $this->setPrefix('/auth');
        $this->post('/login', 'login', 'Login de Usuário', [], []);
        $this->put('/change-password', 'changePassword');
        $this->put('/lost-password', 'lostPassword');

        $this->post('/ultra/login', 'loginUltra', null);

    }
}
