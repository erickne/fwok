<?php

namespace Fwok\Modules\Api\Routes;

use Fwok\Models\Roles;
use Fwok\Modules\Api\Controllers\TracesController;

class TracesRoutes extends FwokRoutesBase
{
    public function __construct()
    {

        $this->setHandler(new TracesController());

        $this->setPrefix('/traces');

        $this->get('/', 'getAll',null,[Roles::ULTRA]);
        $this->get('/{id}', 'get', null, [ Roles::ULTRA ]);


    }
}
