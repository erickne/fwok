<?php

namespace Fwok\Modules\Api\Routes;

use Fwok\Models\Roles;
use Fwok\Modules\Api\Controllers\ConfigurationsController;

class ConfigurationsRoutes extends FwokRoutesBase
{
    public function __construct()
    {

        $this->setHandler(new ConfigurationsController());

        $this->setPrefix('/configurations');

        $this->get('/', 'getAll',null,[Roles::ULTRA]);
        $this->get('/{id}', 'get', null, [ Roles::ULTRA ]);


    }
}
