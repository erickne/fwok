<?php

namespace Fwok\Modules\Api\Routes;

use Fwok\Models\Roles;
use Fwok\Modules\Api\Controllers\CloudNotificationsController;

class CloudNotificationsRoutes extends FwokRoutesBase
{
    public function __construct()
    {

        $this->setHandler(new CloudNotificationsController());

        $this->setPrefix('/cloud-notifications');

        $this->get('/', 'getAll', null, [ Roles::ULTRA ]);
        $this->get('/{id}', 'get', null, [ Roles::ULTRA ]);


    }
}
