<?php

namespace Fwok\Modules\Api\Routes;

use Fwok\Application\Micro;
use Fwok\Modules\Api\Controllers\AuthController;
use Fwok\Routes\RoutesBase;

class FwokRoutesBase extends RoutesBase
{

    public function setPrefix($prefix)
    {
        return parent::setPrefix(str_replace('//','/','/fwok'.$prefix));
    }

    /**
     * @param $app Micro
     */
    public static function mountFwokRoutes($app)
    {
        $app->mount(new \Fwok\Modules\Api\Routes\AuthRoutes());
        $app->mount(new \Fwok\Modules\Api\Routes\BusinessesRoutes());
        $app->mount(new \Fwok\Modules\Api\Routes\C3Routes());
        $app->mount(new \Fwok\Modules\Api\Routes\CloudNotificationsRoutes());
        $app->mount(new \Fwok\Modules\Api\Routes\ConfigurationsRoutes());
        $app->mount(new \Fwok\Modules\Api\Routes\DiscountsRoutes());
        $app->mount(new \Fwok\Modules\Api\Routes\HooksRoutes());
        $app->mount(new \Fwok\Modules\Api\Routes\LogFilesRoutes());
        $app->mount(new \Fwok\Modules\Api\Routes\PaymentsRoutes());
        $app->mount(new \Fwok\Modules\Api\Routes\RequestsCurlRoutes());
        $app->mount(new \Fwok\Modules\Api\Routes\SubscriptionsRoutes());
        $app->mount(new \Fwok\Modules\Api\Routes\SystemInfoRoutes());
        $app->mount(new \Fwok\Modules\Api\Routes\SwaggerRoutes());
        $app->mount(new \Fwok\Modules\Api\Routes\TracesRoutes());
        $app->mount(new \Fwok\Modules\Api\Routes\UsersRoutes());
    }
}
