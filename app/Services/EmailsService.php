<?php

namespace Fwok\Services;

use Fwok\Application\App;
use Fwok\Bootstrap\BootstrapBase;
use Fwok\Exceptions\Exception;
use Fwok\Exceptions\SystemException;
use Fwok\Models\Business;
use Fwok\Models\BusinessConfigs;
use Fwok\Models\BusinessSubscriptions;
use Fwok\Models\EmailPool;
use Fwok\Models\RequestsCurl;
use Fwok\Plugins\Logger;
use Phalcon\Mvc\Model\Resultset;

class EmailsService extends ServicesBase
{
    public $phpmailer;
    public $body;
    public $content;
    public $business;
    public $subject;
    public $recipients = [];

    /**
     * EmailsService constructor.
     * @param Business $business
     * @throws SystemException
     */
    public function __construct($business)
    {
        if(!$business) {
            $business = App::getBusiness();
        }
        if(!$business) {
            $msg = 'Business not set in EmailService';
            throw new SystemException(SystemException::LOGIC_INVALID,null,$msg);
        }

        include_once APPLICATION_PATH . '/app/Vendor/phpmailer/phpmailer/PHPMailerAutoload.php';

        $email = new \PHPMailer;
        $this->phpmailer = $email;

        $this->business = $business;

        return $this;
    }

    public function clearContent()
    {
        $this->phpmailer->clearAllRecipients();
        $this->phpmailer->clearAddresses();
        $this->phpmailer->clearCCs();
        $this->phpmailer->clearBCCs();
        $this->phpmailer->clearAttachments();
        $this->setBody('');
        $this->setSubject('');
    }

    /**
     * @return $this
     */
    public function mergeContent()
    {
        $email_header = '';
        $email_footer = '';
        $email_wrapper = '';

        if ($this->business) {
            /** @var BusinessConfigs $config_email_footer */
            $config_email_footer = BusinessConfigs::query()
                ->addWhereBinded('business_id=', $this->business->getId())
                ->addWhereBinded('code=', 'email_footer')
                ->executeFirst(false, false, 'email_footer not found');
            if ($config_email_footer) {
                $email_footer = $config_email_footer->getValue();
            }

            /** @var BusinessConfigs $config_email_header */
            $config_email_header = BusinessConfigs::query()
                ->addWhereBinded('business_id=', $this->business->getId())
                ->addWhereBinded('code=', 'email_header')
                ->executeFirst(false, false, 'email_header not found');
            if ($config_email_header) {
                $email_header = $config_email_header->getValue();
            }

            /** @var BusinessConfigs $config_email_wrapper */
            $config_email_wrapper = BusinessConfigs::query()
                ->addWhereBinded('business_id=', $this->business->getId())
                ->addWhereBinded('code=', 'email_wrapper')
                ->executeFirst(false, false, 'email_wrapper not found');
            if ($config_email_wrapper) {
                $email_wrapper = $config_email_wrapper->getValue();
            }
        }

        $this->body = $email_header . $this->body . $email_footer;

        if (strpos($email_wrapper, '[[body]]') !== false) {
            $this->body = str_replace('[[body]]', $this->body, $email_wrapper);
        }

        $this->phpmailer->Body = $this->body;
        $this->content = $this->body;

        return $this;
    }

    public function premailerDialect($html)
    {
        $url = 'http://premailer.dialect.ca/api/0.1/documents';
        $request_model = new \Fwok\Models\RequestsCurl();
        $request_model->setUrl($url);
        $request_model->setPostData($html);

        //iniate an api request pass in $body returns a json object
        $request = $this->curlContents($url, ['html' => $html], 'POST');

        //collect the result and convert into array
        $result = json_decode($request, true);

        //return html contents
        $html = file_get_contents($result['documents']['html']);

        $request_model->setResponse($html);
        $request_model->setServer(RequestsCurl::SERVER_PREMAILER_DIALECT);
        $request_model->saveOrException();

        return $html;
    }

    private function curlContents($url, $data = false, $method = 'GET', $headers = false, $returnInfo = false)
    {
        $ch = curl_init();

        if ($method == 'POST') {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            if ($data !== false) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            }
        } else {
            if ($data !== false) {
                if (is_array($data)) {
                    $dataTokens = [];
                    foreach ($data as $key => $value) {
                        array_push($dataTokens, urlencode($key) . '=' . urlencode($value));
                    }
                    $data = implode('&', $dataTokens);
                }
                curl_setopt($ch, CURLOPT_URL, $url . '?' . $data);
            } else {
                curl_setopt($ch, CURLOPT_URL, $url);
            }
        }
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        if ($headers !== false) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        $contents = curl_exec($ch);

        if ($returnInfo) {
            $info = curl_getinfo($ch);
        }

        curl_close($ch);

        if ($returnInfo) {
            return [
                'contents' => $contents,
                'info' => $info
            ];
        } else {
            return $contents;
        }
    }

    public function send()
    {
        $this->buildConfiguration();
        $this->mergeContent();
        $config = App::getConfig('email');

        if ($config->dontsend) {
            Logger::shoutInAlert('Email ignored. To send emails, please change config->email->dontsend to false');
            return true;
        }

        if ($config->premailer == 'dialect') {
            $body = $this->premailerDialect($this->phpmailer->Body);
            if ($body) {
                $this->phpmailer->Body = $body;
            }
        }

        if (!$this->phpmailer->send()) {
            $msg = 'Error sending email : [' . $this->subject . '] PHPMailer error : ' . $this->getSendError();
            Logger::shoutInAlert($msg);
            return false;
        };

        return true;
    }

    public function getSendError()
    {
        return $this->phpmailer->ErrorInfo;
    }

    public function setFrom($from_name, $from_email)
    {
        $this->phpmailer->FromName = $from_name;
        $this->phpmailer->From = $from_email;

        return $this;
    }

    public function setDebug($bool)
    {
        $this->phpmailer->SMTPDebug = $bool;

        return $this;
    }

    public function isHtml($bool)
    {
        $this->phpmailer->isHTML($bool);

        return $this;
    }

    public function setSubject($subject)
    {
        $this->phpmailer->Subject = $subject;

        return $this;
    }

    public function appendSubject($text)
    {
        $this->phpmailer->Subject = $this->phpmailer->Subject . $text;

        return $this;
    }

    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    public function setWordWrap($word_wrap)
    {
        $this->phpmailer->WordWrap = $word_wrap;
    }

    public function addAddress($email, $name = null)
    {
        $filter = new \Phalcon\Filter();

        $rec_name = ($name) ? $name : $email;
        $this->phpmailer->addAddress($filter->sanitize($email, "email"), $rec_name);
        $this->recipients[] = [
            $email,
            $rec_name
        ];

        return $this;
    }
    public function addCcAddress($email, $name = null)
    {
        $filter = new \Phalcon\Filter();

        $rec_name = ($name) ? $name : $email;
        $this->phpmailer->addCC($filter->sanitize($email, "email"), $rec_name);
        $this->recipients[] = [
            $email,
            $rec_name
        ];

        return $this;
    }

    public function addBccAddress($email, $name = null)
    {
        $filter = new \Phalcon\Filter();

        $rec_name = ($name) ? $name : $email;
        $this->phpmailer->addBCC($filter->sanitize($email, "email"), $rec_name);
        $this->recipients[] = [
            $email,
            $rec_name
        ];

        return $this;
    }

    public function templateException($e, $type)
    {
        $module = $this->getService('config')->application->module;

        $this->setSubject("[$module-$type] " . $e->getMessage());
        $url = BootstrapBase::fullHost() . $_GET['_url'];
        $body_arr = [];
        $body_arr[] = "<b>Module:</b> " . $module;
        $body_arr[] = "<b>Type:</b> " . $type;
        $body_arr[] = "<b>Message:</b> " . $e->getMessage();
        $body_arr[] = "<b>DocRoot:</b> " . $_SERVER['DOCUMENT_ROOT'];
        $body_arr[] = "<b>Hostname:</b> " . $_SERVER['HTTP_HOST'];
        $body_arr[] = "<b>Full Path:</b> <a href='.$url.'>$url</a>";
        $body_arr[] = "<b>Code:</b> " . $e->getCode();
        $body_arr[] = "<b>File:</b> " . $e->getFile();
        $body_arr[] = "<b>Line:</b> " . $e->getLine();
        $body_arr[] = "<b>Get:</b> " . print_r($_GET, 1);
        $body_arr[] = "<b>Post:</b> " . print_r($_POST, 1);
        $body_arr[] = "<b>Redirection Url:</b> " . $e->redirectUrl;
        $body_arr[] = "<b>Client IP:</b> " . BootstrapBase::getRealClientIp();
        $body_arr[] = "<br>" . "<b>Trace:</b> <br>" . preg_replace(
                '/\#\d/',
                "<hr>",
                Exception::getExceptionTraceAsString($e)
            );

        $req = App::getRequest();
        if ($req) {
            $body_arr[] = "<b>Method:</b> " . $req->getMethod();
            $body_arr[] = "<b>URI:</b> " . $req->getURI();
        }

        $this->setBody(implode("<br>", $body_arr), 1);

        return $this->send();
    }


    /**
     * @param BusinessSubscriptions $business_subscription
     * @return bool
     */
    public function templateSubscriptionActivated($business_subscription)
    {
        $user = $business_subscription->getBusiness()->getContactUser();

        $body = sprintf(
            '<p>Olá%s,</p>
<p>Sua assinatura foi ativada com sucesso. Agora você já pode usufruir de todos os benefícios do plano %s.</p>
<p>Caso encontre algum problema, entre em contato com nosso suporte!</p>',
            ($user->getName()) ? ' ' . $user->getName() : '',
            $business_subscription->getSubscription()->getName()
        );

        $this->appendSubject(' Assinatura ativada!');
        $this->addAddress($user->getEmail(), $user->getName());
        $this->setBody($body);

        return $this->send();
    }

    /**
     * @return $this
     */
    public function buildConfiguration($keep_alive)
    {
        $configEmail = $this->getService('config')->email;

        $configs_emails_query = BusinessConfigs::query()
            ->addWhereBinded('business_id=', BUSINESS_ID);

        $host_q = clone $configs_emails_query;
        $host = $host_q->addWhereBinded('code=', 'email_config_host')->executeFirst();
        $port_q = clone $configs_emails_query;
        $port = $port_q->addWhereBinded('code=', 'email_config_port')->executeFirst();
        $secure_q = clone $configs_emails_query;
        $secure = $secure_q->addWhereBinded('code=', 'email_config_secure')->executeFirst();
        $user_q = clone $configs_emails_query;
        $user = $user_q->addWhereBinded('code=', 'email_config_user')->executeFirst();
        $password_q = clone $configs_emails_query;
        $password = $password_q->addWhereBinded('code=', 'email_config_password')->executeFirst();
        $from_name_q = clone $configs_emails_query;
        $from_name = $from_name_q->addWhereBinded('code=', 'email_config_from_name')->executeFirst();
        $from_email_q = clone $configs_emails_query;
        $from_email = $from_email_q->addWhereBinded('code=', 'email_config_from_email')->executeFirst();

        if ($host AND $port AND $secure AND $user AND $password AND $from_name AND $from_email) {

            $this->phpmailer->IsSMTP(); // telling the class to use SMTP
            $this->phpmailer->SMTPDebug = $configEmail->SMTPDebug; // 1 = errors and messages 2 = messages only
            $this->phpmailer->SMTPAuth = $configEmail->SMTPAuth; // enable SMTP authentication
            $this->phpmailer->Host = $host->getValue();
            $this->phpmailer->Port = $port->getValue();
            $this->phpmailer->CharSet = $configEmail->charSet;
            $this->phpmailer->SMTPSecure = $secure->getValue();
            $this->phpmailer->Username = $user->getValue();
            $this->phpmailer->Password = $password->getValue();
            $this->setFrom($from_name->getValue(), $from_email->getValue());

        } else {
            $this->phpmailer->IsSMTP(); // telling the class to use SMTP
            $this->phpmailer->SMTPDebug = $configEmail->SMTPDebug; // 1 = errors and messages 2 = messages only
            $this->phpmailer->SMTPAuth = $configEmail->SMTPAuth; // enable SMTP authentication
            $this->phpmailer->Host = $configEmail->host;
            $this->phpmailer->Port = $configEmail->port;
            $this->phpmailer->CharSet = $configEmail->charSet;
            $this->phpmailer->SMTPSecure = $configEmail->SMTPSecure;
            $this->phpmailer->Username = $configEmail->username;
            $this->phpmailer->Password = $configEmail->password;
            $this->setFrom($configEmail->fromname, $configEmail->from);
        }
        $this->phpmailer->SMTPKeepAlive = $keep_alive;

        $this->setWordWrap($configEmail->wordwrap);
        $this->isHtml(true); // Set email format to HTML
        $this->addBccAddress('serversmonitor@rockapps.com.br', 'Email Replay');

        return $this;
    }

    /**
     * @param EmailPool[]|Resultset $emails_pool
     * @param bool $keep_alive
     * @return bool
     */
    public static function sendEmailPool($emails_pool, $keep_alive)
    {
        if(count($emails_pool) === 0) return true;

        $business = Business::queryFirstBy('id=',$emails_pool[0]->getBusinessId());
        $config = App::getConfig('email');
        $count = 0;
        $emailService = new EmailsService($business);

        $emailService->buildConfiguration($keep_alive);

        foreach ($emails_pool as $email_pool) {
            $emailService->clearContent();
            $emailService->setBody($email_pool->getContentRaw());
            $emailService->setSubject($email_pool->getSubject());
            foreach ($email_pool->getTo() as $recipient) {
                $emailService->addAddress($recipient[0],$recipient[1]);
            }
            $emailService->mergeContent();
            $email_pool->setContent($emailService->content)->saveOrException();

            if ($config->dontsend) {
                Logger::shoutInAlert('Email ignored. To send emails, please change config->email->dontsend to false');
                $email_pool->setSent(1)
                    ->setSentAt(date('Y-m-d H:i:s'))
                    ->saveOrExceptionNewTransaction();
                $count++;
                continue;
            }

            if ($config->premailer == 'dialect') {
                $body = $emailService->premailerDialect($emailService->phpmailer->Body);
                if ($body) {
                    $emailService->phpmailer->Body = $body;
                }
            }
            if (!$emailService->phpmailer->send()) {
                $msg = 'Error sending email : [' . $emailService->subject . '] PHPMailer error : ' . $emailService->getSendError();
                Logger::shoutInAlert($msg);
                continue;
            };
            $email_pool->setSent(1)
                ->setSentAt(date('Y-m-d H:i:s'))
                ->saveOrExceptionNewTransaction();
            $count++;
            continue;
        }

        $emailService->phpmailer->SmtpClose();

        return $count;
    }

    public function saveToPool($object_type = null,$object_id = null)
    {
        $email_pool = new EmailPool();
        $email_pool->setBusinessId($this->business->getId());
        $email_pool->setContentRaw($this->body);
        $email_pool->setSubject($this->phpmailer->Subject);
        $email_pool->setDebug($this->phpmailer->SMTPDebug);
        $email_pool->setObjectId($object_id);
        $email_pool->setObjectType($object_type);
        $email_pool->setTo($this->phpmailer->getToAddresses());
        return $email_pool->saveOrException();
    }

}