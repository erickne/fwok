<?php


namespace Fwok\Services;

use Phalcon\DI as DI;
use Phalcon\Mvc\User\Component;

class ServicesBase extends Component
{

    /**
     * Get the service via static call
     * @param $service
     * @return object
     */
    function getService($service)
    {
        return $this->getDI()->get($service);
    }

    static function getStaticService($service)
    {
        return DI::getDefault()->get($service);
    }

    static function getStaticDI()
    {
        return DI::getDefault();
    }
}