<?php
function e($var)
{
    echo $var;
}
function vd($var)
{
    /** @noinspection ForgottenDebugOutputInspection */
    var_dump($var);
}
function vdd($var)
{
    /** @noinspection ForgottenDebugOutputInspection */
    var_dump($var);
    die();
}
function issetTrue($var)
{
    return (isset($var) and $var === true);
}

function fatal_handler()
{
    $error = error_get_last();
    if ($error !== null) {
        $errno = $error["type"];
        $errfile = $error["file"];
        $errline = $error["line"];
        $errstr = $error["message"];
        http_response_code(503);
        \Fwok\Plugins\Logger::shoutInException(sprintf('%t - %s, %s:%s', $errno, $errstr, $errfile, $errline));
        die('Fatal Error. Please check log.');
    }
}

function error_handler($errno, $errstr, $errfile, $errline)
{
    switch ($errno) {
        case E_USER_ERROR:
        case E_USER_WARNING:
        case E_USER_NOTICE:
        case E_STRICT:
        case E_ALL:
        default:
            //\Fwok\Plugins\Logger::shoutInException(sprintf('%t - %s, %s:%s',$errno,$errstr,$errfile,$errline));
            break;
    }

    /* Don't execute PHP internal error handler */

    return true;
}