<?php
namespace Fwok\Responses;

use Fwok\Exceptions\SystemException;
use Fwok\Plugins\PluginBase;

/**
 * @SWG\Definition(id="EnvelopeMeta")
 */
class EnvelopeMeta extends PluginBase
{

    const KEY_PAGE_TITLE = 'title';
    const KEY_PAGE_SUB_TITLE = 'sub_title';
    const MSG_MODE_CONSOLE = 'console';
    const MSG_MODE_DEBUG = 'debug';
    const MSG_MODE_TOASTER = 'toaster';
    const MSG_MODE_ALERT = 'alert';

    private $error = false;
    private $status = false;
    private $records = [];
    private $count = null;
    private $count_max = null;
    private $custom = [];
    private $page = [
        self::KEY_PAGE_TITLE => null,
        self::KEY_PAGE_SUB_TITLE => null
    ];
    private $commands = [];
    private $details = [];
    private $messages = [];
    private $query_strings = [];
    private $hide_errors = false;

    public function __construct($records = [], $error = false)
    {
        $this->setError($error);
        $this->setRecords($records);

        $this->setQueryStrings();
    }

    public function setHideErrors($val)
    {
        $this->hide_errors = $val;
    }

    public function setQueryStrings($values = null)
    {
        if ($values) {
            $this->query_strings = $values;
        } else {
            $this->query_strings = $this->request->get();
        }

        return $this;
    }

    public function getQueryStrings()
    {
        return $this->query_strings;
    }

    public function addMessage($message, $mode, $title = '', $type = '')
    {
        if (!in_array(
            $mode,
            [self::MSG_MODE_ALERT, self::MSG_MODE_CONSOLE, self::MSG_MODE_DEBUG, self::MSG_MODE_TOASTER]
        )
        ) {
            $msg = sprintf('Invalid message mode: %s', $mode);
            throw new SystemException(SystemException::LOGIC_INVALID, null, $msg);
        }
        $this->messages[] = [
            'message' => $message,
            'title' => $title,
            'mode' => $mode, //flash, toast, console, etc
            'type' => $type, //'success','error','warning'
        ];

        return $this;
    }

//    public function setPageValue($key, $value)
//    {
//        $this->page[$key] = $value;
//
//        return $this;
//    }

    public function setRecords($records)
    {
        $this->records = $records;
    }

    public function getErrors()
    {
        if ($this->getError()) {
            return $this->records;
        } else {
            return false;
        }
    }

    public function setCustomProperty($property, $value)
    {
        $this->custom[$property] = $value;

        return $this;
    }

//    public function getCountMax()
//    {
//        return $this->count_max;
//    }

    public function getCount()
    {
        if ($this->getError()) {
            return 0;
        } else {
            return count($this->records);
        }
    }

    public function setError($error)
    {
        $this->error = !!$error;
        $this->setStatus(($this->error));

        return $this;
    }

    public function getError()
    {
        return $this->error;
    }

    public function setStatus($error)
    {
        $this->status = ($error) ? 'ERROR' : 'SUCCESS';
    }

    public function getStatus()
    {
        return $this->status;
    }

//    public function getCommands()
//    {
//        return $this->commands;
//    }
//
//    public function getDetails()
//    {
//        return $this->details;
//    }

    public function getMessages()
    {
        return $this->messages;
    }

//    public function getPage()
//    {
//        return $this->page;
//    }

    public function export()
    {
        $return = [
            'status' => $this->getStatus(),
//            'count_max' => $this->getCountMax(),
//            'commands' => $this->getCommands(),
//            'details' => $this->getDetails(),
            'messages' => $this->getMessages(),
//            'page' => $this->getPage(),
            'query_strings' => $this->getQueryStrings(),
            'hide_errors' => $this->hide_errors
        ];

        if ($this->getError()) {
            $return['errors'] = $this->getErrors();
        }else{
            $return['count'] = $this->getCount();
        }

        foreach ($this->custom as $key => $value) {
            $return[$key] = $value;
        }

        return $return;
    }
}
