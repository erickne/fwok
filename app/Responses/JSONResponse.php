<?php
namespace Fwok\Responses;

use Fwok\Application\App;

class JSONResponse extends ApiResponse
{
    protected $snake = true;
    protected $envelope = true;

    /** @noinspection PhpHierarchyChecksInspection
     * @param \Fwok\Exceptions\Exception|\stdClass|array $records
     * @param bool $error
     * @return $this
     * @param $records
     * @param bool $error
     * @return $this|\Phalcon\Http\Response|void
     * @throws \Fwok\Exceptions\Exception
     */
    public function send($records, $error = false)
    {
        // Error's come from Exception.  This helps set the proper envelope data
        $response = $this->getDI()->get('response');
        $request = $this->getDI()->get('request');
        $status = $error ? 'ERROR' : 'SUCCESS';

        $response->setHeader('E-Tag', md5(serialize($records)));
        $response->setContentType('application/json');
        
        // If the query string 'envelope' is set to false, do not use the envelope.
        // Instead, return headers.
        if ($request->get('envelope', null, null) === 'false') {
            $this->envelope = false;
        }
        // Most devs prefer camelCase to snake_Case in JSON, but this can be overriden here
        if ($this->snake) {
            $records = $this->camelCaseKeys($records);
        }
        
        if ($this->envelope) {
            $envelope = [];
            $meta = App::getApiResponseEnvelopeMeta();
            $meta->setRecords($records);
            $meta->setError($error);

            if (!$error) {
                $envelope['records'] = $records;
            }
            $envelope['meta'] = $meta->export();
            $final_response = $envelope;
        } else {
            $response->setHeader('X-Record-Count', count($records));
            $response->setHeader('X-Status', $status);
            $final_response = $records;
        }
        $final_response = \Fwok\Helpers\Format::arrayRemoveNull($final_response);

        //var_dump($final_response);
        // HEAD requests are detected in the parent constructor. HEAD does everything exactly the
        // same as GET, but contains no body.
        if (@ !$this->head) {
            $response->setJsonContent($final_response);
        }


        /* BYPASS para alguma modificação do PHP7
        //        json_encode(unserialize(str_replace(array('NAN;','INF;'),'0;',serialize($reply))));
        */
        $final_response = unserialize(str_replace(array('NAN;','INF;'),'0;',serialize($final_response)));
        $response->setJsonContent($final_response);
        
        $response->send();

        return $this;
    }
}
