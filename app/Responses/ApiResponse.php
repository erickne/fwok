<?php
namespace Fwok\Responses;

use Fwok\Exceptions\Exception;
use Fwok\Exceptions\SystemException;

/**
 * @SWG\Definition(id="ApiResponse")
 */
class ApiResponse extends \Phalcon\Http\Response
{
    public $details;
    public $commands;
    public $di;
    protected $head = false;

    public function __construct()
    {
        parent::__construct();
        $this->setStatusCode(200, 'OK');
        $this->di = \Phalcon\Di::getDefault();
        if (strtolower($this->di->get('request')->getMethod()) === 'head') {
            $this->head = true;
        }
    }

    /**
     * Add details do response envelope
     *
     * @param mixed $details
     * @return $this
     */
    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }

    /**
     * Add commands to response envelope
     *
     * @param mixed $commands
     * @return $this
     */
    public function setCommands($commands)
    {
        $this->commands = $commands;

        return $this;
    }

    /**
     * In-Place, recursive conversion of array keys in snake_Case to camelCase
     * @param  array $snakeArray Array with snake_keys
     * @return array return value, array is edited in place
     */
    protected function arrayKeysToSnake($snakeArray)
    {
        // DEPRECATED: não estava mais funcionando...
        return $this->camelCaseKeys($snakeArray);
    }

    /**
     * Convert under_score type array's keys to camelCase type array's keys
     * @param   array   $array          array to convert
     * @param   array   $arrayHolder    parent array holder for recursive array
     * @return  array   camelCase array
     */
    public function camelCaseKeys($array, $arrayHolder = array()) {
        $camelCaseArray = !empty($arrayHolder) ? $arrayHolder : array();
        foreach ($array as $key => $val) {
            $newKey = @explode('_', $key);
            array_walk($newKey, create_function('&$v', '$v = ucwords($v);'));
            $newKey = @implode('', $newKey);
            $newKey{0} = strtolower($newKey{0});
            if (!is_array($val)) {
                $camelCaseArray[$newKey] = $val;
            } else {
                $camelCaseArray[$newKey] = $this->camelCaseKeys($val, $camelCaseArray[$newKey]);
            }
        }
        return $camelCaseArray;
    }

    /**
     * Throw an exception because method send must be called from JSON or CSV
     *
     * @return \Phalcon\Http\Response|void
     * @throws Exception
     */
    public function send()
    {
        throw new SystemException(SystemException::CONFIGURATION_INVALID, null, 'Api response type is not defined');
    }
}
