<?php

namespace Fwok\Plugins;

use Fwok\Application\App;
use Fwok\Application\ApplicationBase;
use Phalcon\DI;
use Phalcon\Logger\Adapter\File;

class Logger extends File
{
    const TYPE_SQL = 'SQL';
    const TYPE_DEBUG = 'DEBUG';
    const TYPE_EXCEPTION = 'EXCEPTION';
    const TYPE_ALERT = 'ALERT';
    const TYPE_FIREPHP = 'FIREPHP';

    public static $LOG_DIR = '/storage/logs';

    const SPECIAL = 9;
    const CUSTOM = 8;
    const DEBUG = 7;
    const INFO = 6;
    const NOTICE = 5;
    const WARNING = 4;
    const ERROR = 3;
    const ALERT = 2;
    const CRITICAL = 1;
    const EMERGENCE = 0;
    const EMERGENCY = 0;

    public function __construct($module = null, $type = Logger::TYPE_ALERT, $path = null)
    {
        if (!$module and defined('APPLICATION_MODULE')) {
            $module = APPLICATION_MODULE;
        } elseif (!$module and !defined('APPLICATION_MODULE')) {
            die('Invalid APPLICATION MODULE');
        }

        if (!$path) {
            $path = APPLICATION_PATH . self::$LOG_DIR;
        }

        $filename = str_replace('//', '/', $path . DIRECTORY_SEPARATOR) . implode(
                "_",
                [
                    date('Y-m-d'),
                    $module,
                    $type,
                ]
            );

        parent::__construct($filename . '.log');

    }

    public static function shout($message, $type = Logger::TYPE_DEBUG, $msg_type = \Phalcon\Logger::DEBUG, $module = null)
    {
        switch ($type) {
            case Logger::TYPE_ALERT:
            case Logger::TYPE_DEBUG:
            case Logger::TYPE_SQL:
            case Logger::TYPE_EXCEPTION:
                $logger = new Logger($module, $type);
                $logger->log($message, $msg_type);
                break;
            case Logger::TYPE_FIREPHP:
                exit;
                break;
            default:
                die('Invalid Logger Type');
                break;
        }

        if (App::getConfig('debug')->firephp) {
            self::shoutInFirebug($message, $msg_type);
        }
    }

    public
    static function shoutInFirephp($message, $msg_type = Logger::DEBUG)
    {
        $log = new \Phalcon\Logger\Adapter\Firephp();
        $log->log($msg_type, print_r($message, 1));
    }

    public
    static function shoutInAlert($message, $msg_type = Logger::WARNING)
    {
        Logger::shout($message, Logger::TYPE_ALERT, $msg_type);
    }

    public
    static function shoutInDebug($message)
    {
        Logger::shout($message, Logger::TYPE_DEBUG, Logger::DEBUG);
    }

    public
    static function shoutInSql($message)
    {
        Logger::shout($message, Logger::TYPE_SQL, Logger::DEBUG);
    }

    public
    static function shoutInException($message)
    {
        Logger::shout($message, Logger::TYPE_ALERT, Logger::CRITICAL);
    }


}