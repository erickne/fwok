<?php


namespace Fwok\Plugins;

use Phalcon\DI as Di;

class PluginBase extends \Phalcon\Mvc\User\Component
{
    /**
     * Get the service via static call
     * @param $service
     * @return mixed
     */
    public function getService($service)
    {
        return $this->getDI()->get($service);
    }

    public static function getStaticService($service)
    {
        return Di::getDefault()->get($service);
    }

    public static function getStaticDI()
    {
        return Di::getDefault();
    }
}