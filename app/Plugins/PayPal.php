<?php

namespace Fwok\Plugins;

use Fwok\Exceptions\Exception;
use Fwok\Exceptions\IntegrationException;
use Fwok\Exceptions\SystemException;
use Fwok\Models\RequestsCurl;

class PayPal extends PluginBase
{
    const OBJECT_NAME = 'paypal';
    const ICON = 'fa fa-paypal fa-fw';

    const BILLING_PERIOD_MONTH = 'Month';
    const BILLING_PERIOD_DAY = 'Day';
    const BILLING_PERIOD_YEAR = 'Year';

    const ACK_SUCCESS = 'Success';

    const RECURRING_PROFILE_STATUS_ACTIVE = 'Active'; //First status of recurringProfile
    const RECURRING_PROFILE_STATUS_CANCEL = 'Cancel'; //Only profiles in Active or Suspended state can be canceled.
    const RECURRING_PROFILE_STATUS_SUSPEND = 'Suspend'; //Only profiles in Active state can be suspended.
    const RECURRING_PROFILE_STATUS_REACTIVATE = 'Reactivate'; //Only profiles in a suspended state can be reactivated.

    const RECURRING_PROFILE_STATUS_CANCELLED_PROFILE = 'Cancelled'; //Only profiles in Active or Suspended state can be canceled.

    var $url = '';

    var $credentials;
    var $sandbox = 0;

    public function __construct($username, $password, $signature, $sandbox = 0)
    {
        if (!$username) {
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, 'Invalid username');
        }
        if (!$password) {
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, 'Invalid password');
        }
        if (!$signature) {
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, 'Invalid signature');
        }

        $this->sandbox = $sandbox;
        if ($sandbox) {
            $this->url = 'https://api-3t.sandbox.paypal.com/nvp';
        } else {
            $this->url = 'https://api-3t.paypal.com/nvp';
        }

        $this->credentials = [
            'USER'      => $username,
            'PWD'       => $password,
            'SIGNATURE' => $signature,
        ];

    }

    public function getNvpUrl()
    {
        if ($this->sandbox) {
            return 'https://www.sandbox.paypal.com/cgi-bin/webscr?%s';
        } else {
            return 'https://www.paypal.com/cgi-bin/webscr?%s';
        }
    }


    /**
     * @param $custom_data
     * @return array
     *
     * $custom_data = [
     * 'PAYMENTREQUEST_0_AMT' => 100,
     *  'PAYMENTREQUEST_0_CURRENCYCODE' => 'BRL',
     *  'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
     *  'PAYMENTREQUEST_0_ITEMAMT' => 100,
     *  'L_PAYMENTREQUEST_0_NAME0' => 'Exemplo',
     *  'L_PAYMENTREQUEST_0_DESC0' => 'Assinatura de exemplo',
     *  'L_PAYMENTREQUEST_0_QTY0' => 1,
     *  'L_PAYMENTREQUEST_0_AMT0' => 100,
     *  'L_PAYMENTREQUEST_0_ITEMCATEGORY0' => 'Digital',
     *  'L_BILLINGTYPE0' => 'RecurringPayments',
     *  'L_BILLINGAGREEMENTDESCRIPTION0' => 'Exemplo',
     *  'CANCELURL' => 'http://app.gerenciarnegocio.xom.br/payments/paypalBack',
     *  *'RETURNURL' => 'http://app.gerenciarnegocio.xom.br/payments/paypalBack'
     * ];
     *
     */
    public function setExpressCheckout($custom_data)
    {
        $data = [
            'METHOD'     => 'SetExpressCheckout',
            'VERSION'    => '108',
            'LOCALECODE' => 'pt_BR',
        ];

        return $this->_makeRequest(array_merge($data, $custom_data));
    }

    /**
     * @param $token
     * @return array
     *
     */
    public function getExpressCheckoutDetails($token)
    {
        $data = [
            'METHOD'  => 'GetExpressCheckoutDetails',
            'VERSION' => '108',
            'TOKEN'   => $token
        ];

        return $this->_makeRequest($data);
    }


    /**
     * @param $profile_id
     * @param $status
     * @param null $note
     * @throws IntegrationException
     * @internal param $token
     * @return array
     */
    public function setRecurringPaymentsProfileStatus($profile_id, $status, $note = null)
    {
        $data = [
            'METHOD'    => 'ManageRecurringPaymentsProfileStatus',
            'VERSION'   => '108',
            'PROFILEID' => $profile_id,
            'ACTION'    => $status
        ];

        if ($note) {
            $data['NOTE'] = $note;
        }

        return $this->_makeRequest($data);
    }

    /**
     * @param $profile_id
     * @return array
     *
     */
    public function getRecurringPaymentsProfileDetails($profile_id)
    {
        $data = [
            'METHOD'    => 'GetRecurringPaymentsProfileDetails',
            'VERSION'   => '108',
            'PROFILEID' => $profile_id
        ];

        return $this->_makeRequest($data);
    }

    /**
     * @param $custom_data
     * @return array
     *
     * $custom_data = [
     *  'TOKEN' => $token,
     *  'PayerID' => $payer_id,
     *  'PROFILESTARTDATE' => date('Y-m-d H:i:s'), //'2012-10-08T16:00:00Z',
     *  'DESC' => 'Exemplo',
     *  'BILLINGPERIOD' => 'Month',
     *  'BILLINGFREQUENCY' => '1',
     *  'AMT' => 100,
     *  'CURRENCYCODE' => 'BRL',
     *  'COUNTRYCODE' => 'BR',
     *  'MAXFAILEDPAYMENTS' => 3
     * ];
     *
     */
    public function createRecurringPaymentsProfile($custom_data)
    {

        $data = [
            'METHOD'     => 'CreateRecurringPaymentsProfile',
            'VERSION'    => '108',
            'LOCALECODE' => 'pt_BR',
        ];

        return $this->_makeRequest(array_merge($data, $custom_data));
    }

    public function cancelRecurringPaymentProfile($profile_id)
    {
        // Busca o recurringProfile
        try {
            $nvp = $this->getRecurringPaymentsProfileDetails($profile_id);
        } catch (IntegrationException $e) {
            Exception::runLog($e);
            throw new IntegrationException(IntegrationException::INVALID_REQUEST, $e->getMessage());
        }

        if ($nvp['STATUS'] == PayPal::RECURRING_PROFILE_STATUS_ACTIVE) {
            try {
                $nvp_cancel = $this->setRecurringPaymentsProfileStatus(
                    $profile_id,
                    PayPal::RECURRING_PROFILE_STATUS_CANCEL
                );

                if ($nvp_cancel['ACK'] == PayPal::ACK_SUCCESS) {
                    return true;
                } else {
                    throw new IntegrationException(
                        IntegrationException::INVALID_RESPONSE,
                        'INVALID_PAYPAL_RESPONSE',
                        'Invalid Paypal ACK',
                        500,
                        [ ],
                        null,
                        $nvp_cancel
                    );
                }

            } catch (IntegrationException $e) {
                Exception::runLog($e);
                throw new IntegrationException(IntegrationException::INVALID_REQUEST, $e->getMessage());
            }
        } elseif ($nvp['STATUS'] == PayPal::RECURRING_PROFILE_STATUS_CANCELLED_PROFILE) {
            # Já Cancelado (provavelmente por IPN)
            return true;
        } else {
            $msg = 'Paypal retornou um resultado inesperado';
            throw new IntegrationException(IntegrationException::INVALID_RESPONSE, null, $msg, 500, [ ], null, $nvp);
        }
    }

    /**
     * @param $custom_data
     * @return array
     *
     * $custom_data = [
     *   'NOTE' => 'Uma nota opcional, explicando o motivo da mudança',
     *   'AMT' => 120,
     *   'CURRENCYCODE' => 'BRL'
     * ];
     */
    public function updateRecurringPaymentsProfile($custom_data)
    {
        $data = [
            'METHOD'    => 'UpdateRecurringPaymentsProfile',
            'VERSION'   => '108',
            'PROFILEID' => 'I-FYYMDB55ADSH',
        ];

        return $this->_makeRequest(array_merge($data, $custom_data));
    }

    private function _makeRequest(array $data)
    {
        $request_curl = new RequestsCurl();
        $request_curl->setServer(RequestsCurl::SERVER_PAYPAL);
        $request_curl->setUrl($this->url);

        $curl = curl_init();
        $post_data = http_build_query(array_merge($this->credentials, $data));
        $request_curl->setPostData($post_data);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_URL, $this->url);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);

        $response = curl_exec($curl);

        curl_close($curl);

        $request_curl->save();

        $nvp = [ ];

        if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
            foreach ($matches['name'] as $offset => $name) {
                $nvp[$name] = urldecode($matches['value'][$offset]);
            }
        }

        if (!$nvp) {
            throw new IntegrationException(
                IntegrationException::INVALID_REQUEST,
                'Ocorreu um erro ao contactar o servidor do PayPal.',
                'Empty response on Paypal connection',
                500,
                null,
                null,
                $post_data
            );
        }
        $request_curl->setResponse(json_encode($nvp));
        $request_curl->save();

        if ($nvp['ACK'] == 'Failure') {
            //var_dump($nvp);die();
            $message = implode(" - ", array_filter([ $nvp['L_SHORTMESSAGE0'], $nvp['L_LONGMESSAGE0'] ]));
            throw new IntegrationException(
                IntegrationException::PAYPAL_REQUEST_ERROR,
                'Ocorreu um erro ao contactar o servidor do PayPal.',
                $message,
                500,
                null,
                null,
                $post_data
            );
        }

        return $nvp;

    }

}