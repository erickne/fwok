<?php

namespace Fwok\Plugins;

use Fwok\Application\ApplicationBase;
use Fwok\Exceptions\Exception;
use Fwok\Plugins\Logger;
use Phalcon\Http\Request;
use Phalcon\Translate\Adapter\NativeArray;

class FilterItem extends PluginBase
{
    const OBJECT_NAME = 'filteritem';
    const ICON = 'fa fa-cog fa-fw';
    const TYPE_STRING = 'string';
    const TYPE_NUMBER = 'number';
    const TYPE_DATE = 'date';

    private $name;
    private $property_name;
    private $type;
    private $required = false;
    private $filtrable;
    private $description;

    public function __construct()
    {
        return $this;
    }

    private function setPropertyName($property_name)
    {
        $this->property_name = $property_name;
    }

    private function setName($name)
    {
        $this->name = $name;
    }

    private function setDescription($description)
    {
        $this->description = $description;
    }

    private function setType($type)
    {
        $this->type = $type;
    }

    private function setRequired($required)
    {
        $this->required = $required;
    }

    private function setFiltrable($filtrable)
    {
        $this->filtrable = $filtrable;
    }

    public static function addString($property_name, $name, $required = false, $filtrable = true, $description = '')
    {
        $filter = new FilterItem();
        $filter->setPropertyName($property_name);
        $filter->setName($name);
        $filter->setFiltrable($filtrable);
        $filter->setDescription($description);
        $filter->setType(self::TYPE_STRING);
        $filter->setRequired($required);

        return $filter;
    }

    public static function addDate($property_name, $name, $required = false, $filtrable = true, $description = '')
    {
        $filter = new FilterItem();
        $filter->setPropertyName($property_name);
        $filter->setName($name);
        $filter->setFiltrable($filtrable);
        $filter->setDescription($description);
        $filter->setType(self::TYPE_DATE);
        $filter->setRequired($required);

        return $filter;
    }

    public static function addNumber(
        $property_name,
        $name,
        $decimals = 2,
        $required = false,
        $filtrable = true,
        $description = ''
    ) {
        $filter = new FilterItem();
        $filter->setPropertyName($property_name);
        $filter->setName($name);
        $filter->setFiltrable($filtrable);
        $filter->setDescription($description);
        $filter->setType(self::TYPE_NUMBER);
        $filter->setRequired($required);
        $filter->decimals = $decimals;

        return $filter;
    }

    private function arrayRemoveNull($array)
    {
        foreach ($array as $key => $value) {
            if (is_null($value)) {
                unset($array[$key]);
            }
            if (is_array($value)) {
                $array[$key] = arrayRemoveNull($value);
            }
        }

        return $array;
    }

    /**
     * @param $bag FilterBag
     * @return array
     */
    public function toArray($bag)
    {
        $return = [
            'name'               => $this->name,
            'property_name'      => $this->property_name,
            'property_full_name' => implode('.', [ $bag->name, $this->property_name ]),
            'description'        => $this->description,
            'type'               => $this->type,
            'filtrable'          => $this->filtrable,
            'required'           => $this->required,
        ];

        if (isset($this->decimals)) {
            $return['decimals'] = $this->decimals;
        }

        return $this->arrayRemoveNull($return);
    }

}