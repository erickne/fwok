<?php

namespace Fwok\Plugins;

use Fwok\Application\App;
use Fwok\Exceptions\IntegrationException;
use Fwok\Exceptions\SystemException;

class AndroidPay extends PluginBase
{
    const OBJECT_NAME = 'androidpay';
    const ICON = 'fa fa-dollar fa-fw';

    const PREAPPROVAL_STATUS_PENDING = 'PENDING';
    const PREAPPROVAL_STATUS_ACTIVE = 'ACTIVE';

    const NOTIFICATION_TYPE_PREAPPROVAL = 'preApproval';
    const NOTIFICATION_TYPE_TRANSACTION = 'transaction';

    public $package_name;

    public function __construct()
    {
        $this->package_name = App::getConfig('google')->packageName;
        if (!$this->package_name) {
            $msg = 'Empty config->google->packageName';
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $msg);
        }
    }

    /**
     * @param $product_id
     * @param $purchase_token
     * @return \Google_Service_AndroidPublisher_SubscriptionPurchase
     * @throws IntegrationException
     */
    public function getSubscriptionPurchase($product_id, $purchase_token)
    {
        $service = new \Google_Service_AndroidPublisher(App::getConfig('googleclient'));
        try {
            $google = $service->purchases_subscriptions->get(
                $this->package_name,
                $product_id,
                $purchase_token
            );
        } catch (\Google_Service_Exception $e) {
            throw new IntegrationException(
                IntegrationException::INVALID_RESPONSE,
                'GOOGLE_COMMUNICATION_ERROR',
                json_decode($e->getMessage()),
                500,
                [],
                $e
            );
        }

        if ($google->kind !== 'androidpublisher#subscriptionPurchase') {
            $msg = sprintf(
                'Invalid response object. Expected (%s) got (%s)',
                'androidpublisher#subscriptionPurchase',
                $google->kind
            );
            throw new IntegrationException(
                IntegrationException::INVALID_RESPONSE,
                'GOOGLE_COMMUNICATION_ERROR',
                $msg,
                500,
                [],
                null,
                $google
            );
        }
        return $google;
    }

    /**
     * https://developers.google.com/android-publisher/api-ref/purchases/subscriptions/cancel#request-body
     *
     * @param $product_id
     * @param $purchase_token
     * @return void
     * @throws IntegrationException
     */
    public function cancelSubscriptionPurchase($product_id, $purchase_token)
    {
        $service = new \Google_Service_AndroidPublisher(App::getConfig('googleclient'));
        try {
            $google = $service->purchases_subscriptions->cancel(
                $this->package_name,
                $product_id,
                $purchase_token
            );
        } catch (\Google_Service_Exception $e) {
            throw new IntegrationException(
                IntegrationException::INVALID_RESPONSE,
                'GOOGLE_COMMUNICATION_ERROR',
                json_decode($e->getMessage()),
                500,
                [],
                $e
            );
        }
        // Return empty!!!
        //https://developers.google.com/android-publisher/api-ref/purchases/subscriptions/cancel#request-body
        return $google;
    }
}
