<?php

namespace Fwok\Plugins;

use Fwok\Application\App;
use Fwok\Application\ApplicationBase;
use Fwok\Exceptions\Exception;
use Fwok\Exceptions\SystemException;
use Fwok\Plugins\Logger;
use Phalcon\Http\Request;
use Phalcon\Translate\Adapter\NativeArray;

class Translator extends PluginBase
{
    const OBJECT_NAME = 'translator';
    const ICON = 'fa fa-cog fa-fw';

    public $language;
    public $messages;

    public function __construct($language = null)
    {
        if ($language) {
            $this->setLanguage($language);
        } else {
            $this->setLanguage($this->discoverLanguage());
        }

        return $this;

    }

    /**
     * Set language and Remove language variation
     * Ex: pt_BR = pt
     *
     * @param $language string
     * @return $this
     */
    public function setLanguage($language = null)
    {
        if (!$language) {
            $language = App::getConfig('application')->defaultLanguage;
        }

        $language = explode("_", $language)[0];
        $language = explode("-", $language)[0];
        $language = strtolower(explode("-", $language)[0]);
        $this->language = $language;

        $this->includeMessagesFromFile();

        return $this;

    }

    private function discoverLanguage()
    {
        $request = new Request();

        if ($request->get('lang', 'string')) {
            // Get language form get url
            $language = $request->get('lang', 'string');
        } elseif ($this->di->user) {
            // Get language from user preference
            $language = ApplicationBase::getUser()->getLocale();
        } else {
            // Ask browser what is the best language
            $language = $request->getBestLanguage();
        }

        return $language;
    }

    private function includeMessagesFromFile()
    {
        $config = $this->getService('config');

        // Check if we have a translation file for that lang
        $language_path_selected = APPLICATION_PATH . '/app/Messages/' . $this->language . '.php';
        $language_path_default = APPLICATION_PATH . '/app/Messages/' . $config->application->defaultLanguage . '.php';
        if (file_exists($language_path_selected)) {
            $messages = require $language_path_selected;
        } elseif (file_exists($language_path_default)) {
            // Fallback to some default
            $messages = require $language_path_default;
        } else {
            $msg = 'Could not identify translation message file';
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $msg);
        }

        $language_path_fwok = APPLICATION_PATH . '/app/Vendor/erickne/fwok/app/Messages/' . $this->language . '.php';
        if (file_exists($language_path_fwok)) {
            $messages_fwok = require $language_path_fwok;
        } else {
            $msg = 'Could not identify Vendor default translation message file';
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $msg);
        }

        $messages = array_merge($messages, $messages_fwok);
        // Save a translation object
        /** @var Array $messages */
        $this->messages = new NativeArray(
            [
                "content" => $messages
            ]
        );

        return $this;
    }

    /**
     * @return NativeArray
     */
    public function getMessages()
    {
        return $this->messages;
    }

}