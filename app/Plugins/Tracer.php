<?php

namespace Fwok\Plugins;

class Tracer extends PluginBase
{
    var $_trace;

    public function __construct($app = null)
    {
        $trace = new \Fwok\Models\Trace();

        //$trace->setEndpoint($app->getRouter()->getRewriteUri());
        //$trace->setMethod($app->request->getMethod());
        $trace->setVarGet($_GET);
        $trace->setVarPost($_POST);
        //$trace->setVarBody($app->request->getRawBody());
        $trace->setProxyIp($_SERVER['HTTP_X_FORWARDED_FOR']);
        $trace->setClientIp($_SERVER['REMOTE_ADDR']);
        $trace->setAgent($_SERVER['HTTP_USER_AGENT']);
        //$request->setResponse($app->getReturnedValue());

        if ($trace->save()) {
            $this->_trace = $trace;
        };

    }

    public function save()
    {
        return $this->_trace->save();
    }
}