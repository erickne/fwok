<?php

namespace Fwok\Plugins;

/**
 * Class NodeQueryClient
 * @package Fwok\Plugins
 *
 * Usage:
 * https://nodequery.com/api/account
 * https://nodequery.com/api/servers
 * https://nodequery.com/api/servers/763
 * https://nodequery.com/api/loads/hourly/763
 *
 */

class NodeQueryClient extends PluginBase
{
    const SERVICE_NAME = 'node_query_client';

    const LOADS_PERIOD_HOURLY = 'hourly';
    const LOADS_PERIOD_DAILY = 'daily ';
    const LOADS_PERIOD_MONTHLY = 'monthly';
    const LOADS_PERIOD_YEARLY = 'yearly ';

    var $url;
    var $api_key;
    var $server;
    var $content;

    function __construct($o = null)
    {
        if (!$o['server']) {
            $this->setServer('https://nodequery.com/api/');
        } else {
            $this->setServer($o['server']);
        }
    }

    public function setApiKey($api_key)
    {
        $this->api_key = $api_key;

        return $this;
    }

    public function getApiKey()
    {
        return '?api_key=' . $this->api_key;
    }

    public function setServer($server)
    {
        $this->server = $server . "?";

        return $this;
    }

    private function setEndpoint($end_point)
    {
        $this->url = implode('/', [ $this->server, $end_point ]);
        $this->url = str_replace('//', '/', $this->url);

        return $this;
    }

    public function send()
    {
        $this->url = $this->url . $this->getApiKey();

        Logger::shoutInDebug("Node Query: " . $this->url);

        $fetched = file_get_contents($this->url);
        $this->content = json_decode($fetched);

        if ($this->content->status != 'OK') {
            Logger::shoutInAlert("NodeQuery Client Result Error: " . $this->content . " | URL = " . $this->url);
        }

        return $this->content;
    }

    public static function account()
    {
        $service = self::getStaticService(self::SERVICE_NAME);
        $service->setEndpoint('account');

        return $service->send();
    }

    public static function servers()
    {
        $service = self::getStaticService(self::SERVICE_NAME);
        $service->setEndpoint('servers');

        return $service->send();
    }

    public static function server($server_id)
    {
        $service = self::getStaticService(self::SERVICE_NAME);
        $service->setEndpoint('servers/' . $server_id);

        return $service->send();
    }

    public static function loads($period, $server_id)
    {
        $service = self::getStaticService(self::SERVICE_NAME);
        $service->setEndpoint(implode('/', [ 'loads', $period, $server_id ]));

        return $service->send();
    }

}