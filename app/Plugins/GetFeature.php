<?php

namespace Fwok\Plugins;

use Fwok\Application\App;
use Fwok\Exceptions\Exception;
use Fwok\Exceptions\InputException;
use Fwok\Models\Subscriptions;
use Fwok\Models\SubscriptionsFeatures;

class GetFeature extends PluginBase
{
    const KEY_MAX_USERS = 'MAX_USERS';
    const KEY_SUPPORT = 'SUPPORT';
    const KEY_PERMISSION_CONTROL = 'PERMISSION_CONTROL';
    const KEY_TEAMS_CONTROL = 'TEAMS_CONTROL';

    /**
     * @param $key
     * @param int $throw_exception
     * @param Subscriptions $subscription
     * @throws \Fwok\Exceptions\Exception
     * @return SubscriptionsFeatures
     */
    public static function byKey($key, $throw_exception = 1, Subscriptions $subscription = null)
    {
        if (!$subscription) {
            $subscription = App::getBusiness()->getActiveBusinessSubscription()->getSubscription();
        }

        $feature = SubscriptionsFeatures::findFirstCacheBackend(
            [
                'key = :key: and subscription_id = :subscription_id:',
                'bind'  => [
                    'key'             => $key,
                    'subscription_id' => $subscription->getId()
                ]
            ],
            false,
            'user-cached-' . $key . '-' . $subscription->getId(),
            500
        );

        if ($throw_exception and !$feature) {
            $msg = sprintf('The feature %s is not available to users\' subscription', $key);
            throw new InputException(InputException::UNAVAILABLE_FEATURE, null, null, $msg);
        }

        return $feature;
    }

    /**
     * Check if validation with int
     *
     * Usage:
     *
     * # Verifica se é criação de usuário e compara com a assinatura
     * if(!$this->getId()) {
     *   $users = \Application::getUser()->getBusiness()->getBusinessUsers(['active = 1']);
     *   if (GetFeature::checkValidityInt(GetFeature::KEY_MAX_USERS , count($users))) {
     *     $this->appendErrorMessage("Máwximo de usuários atingido. Faça um upgrade no seu plano para
     * adicionar mais usuários.");
     *   }
     * }
     *
     * @param $key
     * @param $compared_value
     * @param $throw_exception
     * @param Subscriptions $subscription
     * @return bool
     * @throws Exception
     */
    public static function checkValidityInt(
        $key,
        $compared_value,
        $throw_exception = 1,
        Subscriptions $subscription = null
    ) {
        $feature = self::byKey($key, $throw_exception, $subscription);

        # Feature does not exist (and throw_exception = 0)
        if (!$feature) {
            return false;
        }

        # Cast value to int
        $value = (int) $feature->getValue();
        if (!is_int($value)) {
            $msg = sprintf('Compared value (%s) is not an int.', $feature->getValue());
            throw new InputException(InputException::UNAVAILABLE_FEATURE, null, null, $msg);
        }

        # -1 equals to infinite
        if ($value === -1) {
            return false;
        }
//echo sprintf("encontrou a feature %s|valor:%s|compared_value:%s",$feature,$value,$compared_value);
        if ($value >= $compared_value) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * @param $key
     * @param Subscriptions $subscription
     * @return string
     * @throws \Fwok\Exceptions\Exception
     */
    public static function byKeyAsInt($key, Subscriptions $subscription = null)
    {
        /** @noinspection PhpWrongStringConcatenationInspection */
        return self::byKey($key, $subscription)->getValue() + 0;
    }
}
