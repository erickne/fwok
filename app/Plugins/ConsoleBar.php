<?php

namespace Fwok\Plugins;

use Fwok\Application\Application;

class ConsoleBar extends PluginBase
{

    public $messages = [ ];
    public $routes = [ ];
    public $query_count;
    public $queries = [ ];
    public $page_load_time;
    public $parameters = [ ];

    public static function add($message, $index = null)
    {
        $value = ($index) ? [
            $index,
            $message
        ] : $message;
        self::getStaticService('console')->messages[] = $value;
    }

    public static function addRoute($route)
    {
        self::getStaticService('console')->routes[] = $route;
    }

    public static function addQuery($query)
    {
        self::getStaticService('console')->queries[] = $query;
    }

    public function setParameter($key, $value)
    {
        $this->parameters[$key] = $value;

        return $this;
    }

    public function getData()
    {
        $this->calculate();

        $return = [
            'page_load_time' => 'missing start_time',
            'queries_count'  => count($this->queries),
            'messages'       => $this->messages,
            '$_GET'          => $_GET,
            '$_POST'         => $_POST,
            '$_FILES'        => $_FILES,
            'queries'        => $this->queries,
        ];

        return $return;
    }

    private function calculate()
    {
        if ($this->parameters['start_time']) {
            $this->page_load_time = (number_format(microtime(true) - $this->parameters['start_time'], 3));
        } else {
            $this->page_load_time = 'Unkown';
        }
    }


    public function renderBootstrapBar()
    {
        $this->calculate();

        $vars = [
            [
                'id'      => '',
                'icon'    => 'fa-clock-o',
                'caption' => $this->page_load_time,
            ],
            [
                'id'      => 'Queries',
                'icon'    => 'fa-database',
                'caption' => 'Queries',
                'data'    => $this->queries
            ],
            [
                'id'      => 'Messages',
                'icon'    => 'fa-envelope',
                'caption' => 'Messages',
                'data'    => $this->messages
            ],
            [
                'id'      => 'Routes',
                'icon'    => 'fa-exchange',
                'caption' => 'Routes',
                'data'    => $this->routes
            ],
            [
                'id'      => 'Get',
                'icon'    => 'fa-link',
                'caption' => 'Get',
                'data'    => $_GET
            ],
            [
                'id'      => 'Post',
                'icon'    => 'fa-file-text-o',
                'caption' => 'Post',
                'data'    => $_POST
            ],
            [
                'id'      => 'Files',
                'icon'    => 'fa-file',
                'caption' => 'Files',
                'data'    => $_FILES
            ],
            [
                'id'         => 'Configuration',
                'icon'       => 'fa-cog',
                'caption'    => 'Configuration',
                'data'       => \Fwok\Application\ApplicationBase::di('config'),
                'hide_count' => 1
            ],
        ];
        $li = [ ];
        foreach ($vars as $var) {
            $li[] = $this->createLink($var['id'], $var['icon'], $var['caption'], $var['data'], $var['hide_count']);
        }

        $li[] = '<a href="#" onclick="$(\'#console-bar\').hide()"><i class="fa fa-close fa-fw"></i></a>';

        $return = [ ];
        $return[] = '
<div id="console-bar">
  <ul class="list-inline pull-right">
    ' . implode('', $li) . '
  </ul>
</div>';

        foreach ($vars as $var) {
            $return[] = $this->renderModal($this->beautifyResults($var['data']), ucwords($var['caption']), $var['id']);
        }
        echo implode("", $return);
    }

    private function createLink($id, $icon, $caption, $data, $hide_count = 0)
    {
        $a = [ ];
        $a[] = '<li>';
        $a[] = ($data) ? '<a href="#consoleModal' . $id . '" data-toggle="modal">' : '';
        $a[] = ($icon) ? '<i class="fa ' . $icon . ' fa-fw"></i> ' : '';
        $a[] = $caption;
        $a[] = ($hide_count) ? '' : ($data) ? '(' . count($data) . ')' : '';
        $a[] = ($data) ? '</a>' : '';
        $a[] = '</li>';

        return implode('', $a);

    }

    private function renderModal($body, $title, $id)
    {
        return '
      <div class="modal fade console-modal" id="consoleModal' . $id . '" tabindex="-1" role="dialog"
  aria-labelledby="modal' . $id . 'Label" aria-hidden="true">
  <div class="modal-dialog">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
            class="sr-only">Close</span></button>
        <h4 class="modal-title" id="modal' . $id . 'Label">' . $title . '</h4>
      </div>
      <div class="modal-body" id="modalL' . $id . 'Body">
        <div class="row">
          <div class="col-md-12">
            ' . $body . '
          </div>
        </div>
      </div>
      <div class="modal-footer"><a type="button" class="btn btn-default" data-dismiss="modal">Close</a></div>
    </div>
  </div>
</div>

';
    }

    /**
     * Verifies if input is array, multidimensional array or other thing
     *
     * @param $data
     *
     * @return array|string
     */
    private function beautifyResults($data)
    {

        if (is_array($data)) {
            # Verifica se o array é multi dimensional
            if ((count($data) == count($data, COUNT_RECURSIVE))) {
                $return = [ ];
                foreach ($data as $key => $value) {
                    $return[] = $key . '<pre>' . $value . '</pre>';
                }

                return implode('', $return);
            } else {
                ob_start();
                var_dump($data);
                $return = ob_get_clean();

                return $return;
            }

        } else {
            ob_start();
            var_dump($data);
            $return = ob_get_clean();

            return $return;
        }
    }

}
