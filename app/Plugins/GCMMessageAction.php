<?php

namespace Fwok\Plugins;

class GCMMessageAction extends PluginBase
{
    const TYPE_SWIPE_LEFT = 'swipe_left';
    const TYPE_SWIPE_RIGHT = 'swipe_right';
    const TYPE_BUTTON = 'button';
    const TYPE_MULTI_CHOICE = 'multi_choice';
    
    public function __construct()
    {
    }

    public function export()
    {
        $ret = [
            'object_id' => $this->getObjectId(),
            'object_type' => $this->getObjectType(),
            'title' => $this->getTitle(),
            'command' => $this->getCommand(),
            'type' => $this->getType(),
            'icon' => $this->icon
        ];

        return $ret;

    }

    /**
     *
     * @var string
     * @SWG\Property(name="command",type="string",description="")
     */
    protected $command;

    /**
     * Method to set the value of field command
     *
     * @param string $command
     * @return $this
     */
    public function setCommand($command)
    {
        $this->command = $command;

        return $this;
    }

    /**
     * Returns the value of field command
     *
     * @return string
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="object_type",type="string",description="")
     */
    protected $object_type;

    /**
     * Method to set the value of field object_type
     *
     * @param string $object_type
     * @return $this
     */
    public function setObjectType($object_type)
    {
        $this->object_type = $object_type;

        return $this;
    }

    /**
     * Returns the value of field object_type
     *
     * @return string
     */
    public function getObjectType()
    {
        return $this->object_type;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="icon",type="string",description="")
     */
    protected $icon;

    /**
     * Method to set the value of field icon
     *
     * @param string $icon
     * @return $this
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Returns the value of field icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }
    /**
     *
     * @var integer
     * @SWG\Property(name="object_id",type="integer",description="")
     */
    protected $object_id;

    /**
     * Method to set the value of field object_id
     *
     * @param integer $object_id
     * @return $this
     */
    public function setObjectId($object_id)
    {
        $this->object_id = $object_id;

        return $this;
    }

    /**
     * Returns the value of field object_id
     *
     * @return integer
     */
    public function getObjectId()
    {
        return (int) $this->object_id;
    }
    /**
     *
     * @var string
     * @SWG\Property(name="title",type="string",description="")
     */
    protected $title;

    /**
     * Method to set the value of field title
     *
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Returns the value of field title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="type",type="string",description="")
     */
    protected $type;

    /**
     * Method to set the value of field type
     *
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Returns the value of field type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
