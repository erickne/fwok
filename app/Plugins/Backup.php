<?php

namespace Fwok\Plugins;

use Fwok\Application\App;
use Fwok\Exceptions\SystemException;
use Ifsnop\Mysqldump\Mysqldump;
use Phalcon\Http\Request;

class Backup extends PluginBase
{
    const OBJECT_NAME = 'backup';

    public function __construct()
    {
    }

    public static function importDatabaseLines($sql_lines)
    {
        $temp_line = '';
        $errors = [];

        App::getDb()->begin();
        foreach (preg_split("/((\r?\n)|(\r\n?))/", $sql_lines) as $line) {

            // Remove non printable char from base 64 decode
            $line = preg_replace('/[\x00-\x1F\x7F]/u', '', $line);

            // Skip it if it's a comment
            if ($line == '' || substr($line, 0, 2) == '--') {
                $temp_line = '';
                continue;
                }

            // Add this line to the current segment
            $temp_line .= $line;

            // If it has a semicolon at the end, it's the end of the query
            if (substr(trim($line), -1, 1) == ';') {

                Logger::shoutInAlert($temp_line);
                $q = App::getDb()->execute($temp_line);
                if (!$q) {
                    $errors[] = sprintf('Error performing query %s : %s', $temp_line, mysql_error());
                }
                // Reset temp variable to empty
                $temp_line = '';
            }
        }
        if (!count($errors)) {
            App::getDb()->commit();
            return true;
        } else {
            foreach ($errors as $error) {
                Logger::shoutInAlert($error);
            }
            App::getDb()->rollback();
            return false;
        }
    }

    public static function importDatabaseFile($backup_file_path)
    {
        $lines = \file($backup_file_path);

        return self::importDatabaseLines($lines);
    }

    /**
     * @param array $dumpSettings
     * @param array $pdoSettings
     * @return string
     * @throws SystemException
     */
    public static function dumpDatabaseFile(Array $dumpSettings = array(), Array $pdoSettings = array())
    {
        $configBackup = App::getConfig('backup');
        $configDb = App::getConfig('database');
        $backupDir = $configBackup->destination . DIRECTORY_SEPARATOR;

        if (!file_exists($backupDir)) {
            mkdir($backupDir, 0775);
        }

        $backupFileName = $configBackup->filePrefix . $configDb->dbname . '-' . date('Ymd-His') . '.sql';

        if ($dumpSettings['compress'] === 'Gzip') {
            $backupFileName .= '.gz';
        } else if ($dumpSettings['compress'] === 'Bzip2') {
            $backupFileName .= '.bz2';
        }

        try {
            if (!is_writable($backupDir)) {
                $dev = "Invalid Backup Directory";
                throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $dev);
            }
            $dsn = 'mysql:host=' . $configDb->host . ';dbname=' . $configDb->dbname;
            $dump = new Mysqldump($dsn, $configDb->username, $configDb->password, $dumpSettings, $pdoSettings);
            $dump->start($backupDir . $backupFileName);
        } catch (\Exception $e) {
            Logger::shoutInException($e->getMessage());
            throw new SystemException(SystemException::BACKUP_ERROR, null, 'Backup error. Please check log', 500, null, $e);
        }

        return $backupDir . $backupFileName;
    }

    /**
     *
     * \Fwok\Plugins\Backup::dumpAppFiles('../../public',null,'public');
     *
     * @param $source
     * @param null|string $filename
     * @param null $parent_dir
     * @return bool|string
     * @throws SystemException
     */
    public static function dumpAppFiles($source, $filename = null, $parent_dir = null)
    {
        // Make sure the script can handle large folders/files
        ini_set('max_execution_time', 600);
        ini_set('memory_limit', '1024M');

        $configBackup = App::getConfig('backup');
        if (!$configBackup->destination) {
            $msg = 'Backup destination not configured';
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $msg);
        }

        if (!file_exists($configBackup->destination . DIRECTORY_SEPARATOR)) {
            mkdir($configBackup->destination . DIRECTORY_SEPARATOR, 0775);
        }

        if (!$filename) {
            $filename = 'files-' . date('Ymd-His') . '.zip';
        }

        $filename = $configBackup->filePrefix . $filename;

        $destination = $configBackup->destination . DIRECTORY_SEPARATOR . $filename;

        if (!extension_loaded('zip')) {
            $msg = 'Extension ZIP not available';
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $msg);
        }
        if (!file_exists($source)) {
            $msg = 'Invalid Backup Source';
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $msg);
        }
        if (file_exists($destination)) {
            $msg = 'Invalid Backup Destination File. File already exists.';
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $msg);
        }

        $zip = new \ZipArchive();
        if (!$zip->open($destination, \ZipArchive::CREATE)) {
            $msg = 'ZipArchive::open() failed to open path';
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $msg);
        }

        $source = str_replace('\\', '/', realpath($source));
        if (is_dir($source) !== true) {
            $msg = 'Specified path is not a directory';
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $msg);
        }

        $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($source), \RecursiveIteratorIterator::SELF_FIRST);
        foreach ($files as $file) {
            // Skip parent folder construction
            if ($file->getFilename() === '..') {
                continue;
            }
            $file = str_replace('\\', '/', realpath($file));
            if (is_dir($file) === true) {
                $dir_name = ($parent_dir ? $parent_dir . '/' : '') . str_replace($source . '/', '', $file . '/');
                $zip->addEmptyDir($dir_name);
            } else if (is_file($file) === true) {
                $dir_name = ($parent_dir ? $parent_dir . '/' : '') . str_replace($source . '/', '', $file);
                $zip->addFromString($dir_name, file_get_contents($file));
            }
        }
        if ($zip->close()) {
            return $destination;
        } else {
            return false;
        }

    }

    /**
     *
     * \Fwok\Plugins\Backup::importAppFiles(APPLICATION_PATH.'/backup/files-20161224-191132.zip',APPLICATION_PATH.'/backup/restore');
     *
     * @param $archive
     * @param $extract_to
     * @return bool
     * @throws SystemException
     */
    public
    static function importAppFiles($archive, $extract_to)
    {
        if (!extension_loaded('zip')) {
            $msg = 'Extension ZIP not available';
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $msg);
        }
        if (!file_exists($archive)) {
            $msg = 'Invalid file archive: ' . $archive;
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $msg);
        }

        if (!file_exists($extract_to)) {
            /** @noinspection MkdirRaceConditionInspection */
            mkdir($extract_to, 0775, true);
        }
//        $msg = 'Invalid extract to directory: ' . $extract_to;
//        throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $msg);

        $zip = new \ZipArchive();
        if ($zip->open($archive) !== TRUE) {
            $msg = 'ZipArchive::open() failed to open path';
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $msg);
        }

        if (!$zip->extractTo($extract_to)) {
            $msg = 'Error extracting file to destination: ' . $extract_to;
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $msg);
        }

        return $zip->close();
    }

    public static function copyToGoogleDrive($file, $folderId, $description)
    {
        $configBackup = App::getConfig('backup');
        if(!class_exists('\Google_Client')){
            $msg = 'Cannot load Google_Client. Check if Composer has installed library.';
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $msg);
        }
        $client = new \Google_Client();
        $client->setScopes(array('https://www.googleapis.com/auth/drive'));
        $client->setClientId($configBackup->google->client_id);
        $client->setClientSecret($configBackup->google->client_secret);
        $client->setRedirectUri('urn:ietf:wg:oauth:2.0:oob');
        $client->setAccessType('offline');

        if (!$configBackup->google->refresh_token) {
            Logger::shoutInAlert('Refresh Token is not set');
            $refresh_token = GoogleAPI::getRefreshTokenCli($client);
            echo 'Gravar backup->google->refresh_token no arquivo config: ' . $refresh_token;
            die();
        }

        $client->refreshToken($configBackup->google->refresh_token);

        $service = new \Google_Service_Drive($client);

        if(!$folderId) {
            if($configBackup->google->folder_id) {
                $folderId = $configBackup->google->folder_id;
            }else{
                $msg = 'Could not find Google Drive backup folder';
                throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $msg);
            }
        }
        
        $createdFile = GoogleAPI::driveInsertFile($service, $file, $description, $folderId);
        if ($createdFile) {
            return $createdFile;
        } else {
            $msg = sprintf('Error uploading file %s to Google Drive', $file);
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $msg);
        }
    }

}