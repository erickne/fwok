<?php

namespace Fwok\Plugins;

use Fwok\Application\App;

class Debugger extends PluginBase
{
    public static function modelParameters($class_name, $method, $parameters, $query = null)
    {
        if (App::getConfig('debug')->modelParameters) {
            $msg = sprintf('%s -> (%s)', $class_name, $method);
            if($parameters) {
                $msg .= ' -> '.json_encode($parameters);
            }
            if($query) {
                $msg .= ' '.json_encode($query);
            }
            Logger::shoutInDebug($msg);
        }
    }

}
