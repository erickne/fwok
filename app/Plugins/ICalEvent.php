<?php

namespace Fwok\Plugins;

class ICalEvent extends PluginBase
{
    public $summary;
    public $id;
    public $status;
    public $starts;
    public $ends;
    public $last_update;
    public $location;
    private $date_format;

    const STATUS_CONFIRMED = 'CONFIRMED';
    const STATUS_TENTATIVE = 'TENTATIVE';
    const STATUS_CANCELLED = 'CANCELLED';

    public function __construct($date_format = '')
    {
        // the iCal date format. Note the Z on the end indicates a UTC timestamp.
        $this->date_format = ($date_format) ? $date_format : 'Ymd\THis\Z';
    }

    public function getEvent()
    {
        $event = new \stdClass();
        $event->summary = $this->summary;
        $event->id = $this->id;
        $event->status = $this->setStatus($this->status);
        $event->starts = $this->starts;
        $event->ends = $this->ends;
        $event->last_update = $$this->last_update;
        $event->location_name = $$this->location_name;

        return $event;
    }

    private function setStatus($status = '')
    {
        switch ($status) {
            case ICalEvent::STATUS_CONFIRMED:
            case ICalEvent::STATUS_TENTATIVE:
            case ICalEvent::STATUS_CANCELLED:
                return $status;
            default:
                return ICalEvent::STATUS_CONFIRMED;
        }
    }

    public function buildEvent()
    {
        $output = "BEGIN:VEVENT
SUMMARY:$this->summary
UID:$this->id
STATUS:" . strtoupper($this->status) . "
DTSTART:" . date($this->date_format, strtotime($this->starts)) . "
DTEND:" . date($this->date_format, strtotime($this->ends)) . "
LAST-MODIFIED:" . date($this->date_format, strtotime($this->last_update)) . "
LOCATION:$this->location
END:VEVENT\n";

        return $output;
    }
}

