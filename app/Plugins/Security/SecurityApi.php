<?php

namespace Fwok\Plugins\Security;

use Fwok\Application\App;
use Fwok\Application\ApplicationBase;
use Fwok\Exceptions\Exception;
use Fwok\Exceptions\InputException;
use Fwok\Exceptions\SystemException;
use Fwok\Models\Roles;
use Fwok\Models\RolesUsers;
use Fwok\Models\Token;
use Fwok\Models\Users;
use Fwok\Routers\Route;
use Phalcon\Di;
use Phalcon\Filter;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Router\RouteInterface;

/**
 * Security
 *
 * This is the security plugin which controls that users only have access to the modules they're assigned to
 */
class SecurityApi extends SecurityBase
{

    /**
     * @var Route
     */
    private $_route;

    /**
     * @var bool
     */
    public $isPublicRoute = false;

    /**
     * @var bool
     */
    public $hasToken = false;

    /**
     * @var Token
     */
    private $_token;

    /**
     * @param Route|RouteInterface $route
     * @return $this
     */
    public function setRoute(Route $route)
    {
        $this->_route = $route;
        return $this;
    }

    /**
     * Before every request, make sure user is authenticated.
     * Returning true in this function resumes normal routing.
     * Returning false stops any route from executing
     * 
     * 
     * @param $app Micro
     * @return bool
     * @throws InputException
     * @throws SystemException
     */
    public function processRoutesAuthorization($app)
    {
        # Check if matched route is public
        $this->setRoute($app->getRouter()->getMatchedRoute());
        $this->checkIsPublicRoute();
        $this->checkHasAuthToken();

        if ($this->isPublicRoute AND !$this->hasToken) {
            return true;
        } elseif (
            $this->hasToken OR
            ($this->isPublicRoute AND $this->hasToken)
        ) {
            $this->authorizeToken();
            # Set user locale
            if (App::getUser()->getLocale()) {
                App::getTranslation()->setLanguage(App::getUser()->getLocale());
            }

            # Return if user is authorized
            return $this->checkRouteAndRoles();
        }

        /**
         * If we made it this far, we have no valid auth method, throw a 401.
         */
        $dev_msg = 'Please provide credentials passing in a token via cookie or providing BEARER authentication.';
        throw new InputException(InputException::INVALID_AUTHENTICATION, null, $dev_msg, 401);
    }

    public function checkHasAuthToken()
    {
        $headers = getallheaders();
        $authorization = [];

        if ($headers['Authorization']) {
            $authorization = explode(' ', $headers['Authorization']);
        } elseif ($headers['authorization']) {
            $authorization = explode(' ', $headers['authorization']);
        }

        if (count($authorization) !== 2) {
            return false;
        }

        if ($authorization[0] !== 'Bearer') {
            $dev = 'Token is not in Bearer mode. Should be [Authorization: Bearer XXXXXXXXX]';
            throw new InputException(InputException::INVALID_AUTHENTICATION, null, $dev, 401);
        }

        $filter = new Filter();
        $token = Token::findFirstByValue($filter->sanitize($authorization[1], 'string'));
        $this->_token = $token;
        if ($token) {
            App::getTrace()
                ->setTokenId($this->_token->getId())
                ->saveOrExceptionNewTransaction();
            $this->_token->setRequests($this->_token->getRequests() + 1)->saveOrException();
            $this->_token->saveOrException();
            $this->hasToken = true;
            return true;
        }

        return false;
    }

    public function authorizeToken()
    {

        if (!$this->_token) {
            $dev = 'Token was not found in database';
            throw new InputException(InputException::INVALID_AUTHENTICATION, null, $dev, 401);
        }

        if ($this->_token->getDeleted()) {
            $dev = 'Inactive token';
            throw new InputException(InputException::INVALID_AUTHENTICATION, null, $dev, 401);
        }

        $user = Users::findFirstCacheBackend([
                'id = :id: and "cached" = "cached"',
                'bind' => [ 'id' => $this->_token->getUserId() ]
            ],
            'user-cached-' . $this->_token->getUserId(),
            500
        );

        App::getTrace()
            ->setUserId($user->getId())
            ->saveOrExceptionNewTransaction();

        Di::getDefault()->setShared('user', $user);

        return true;
    }

    /**
     * @return bool
     */
    public function checkIsPublicRoute()
    {
        /*
         * If no roles are found at's an unprotected route
         */
        if (!count($this->_route->getAllowedRoles()) and !count($this->_route->getDeniedRoles())) {
            $this->isPublicRoute = true;

            return true;
        }
        $this->isPublicRoute = false;

        return false;

    }

    /**
     * @return bool
     * @throws \Fwok\Exceptions\Exception
     * @throws InputException
     * @throws SystemException
     */
    public function checkRouteAndRoles()
    {
        if ($this->checkIsPublicRoute()) {
            return true;
        }
        $user = ApplicationBase::getUser();

        /*
         * Search if allowable roles are attached to user
         */
        foreach ($this->_route->getAllowedRoles() as $allowedRole) {
            $role = Roles::findFirstByCode($allowedRole, true);

            if (RolesUsers::findFirstByUserIdAndRoleId($user, $role)) {
                return true;
            };
        }

        /*
         * Search if deniable roles are attached to user
         */
        foreach ($this->_route->getDeniedRoles() as $deniedRole) {
            $role = Roles::findFirstByCode($deniedRole, true);

            if (RolesUsers::findFirstByUserIdAndRoleId($user, $role, !$role->getIsPublic())) {
                $dev = sprintf('User [%s] role [%s] is not allowed to access this route', $user->getId(),
                    $role->getCode());
                throw new InputException(InputException::INVALID_AUTHENTICATION, 'INVALID_ROUTE_PERMISSION', $dev, 403);
            }
        }

        /*
         * Return false if user has a role but does not applies to allowed ones
         * and allowed/denied roles are not empty
         */

        $dev = 'User does not have permission to access this route';
        throw new InputException(InputException::INVALID_AUTHENTICATION, 'INVALID_ROUTE_PERMISSION', $dev, 403);
    }

    /**
     * Esta função funciona apenas se a base trace tiver poucos registros
     *
     * @param int $limit
     * @param int $seconds
     * @return bool
     * @throws Exception
     */
    /*
    public function checkIpRequestThreshold($limit = 5, $seconds = 1)
    {
        $created_at = new DateTime(date("Y-m-d H:i:s"));
        $created_at->modify('-' . $seconds . ' seconds');

        $traces = Trace::count(
            [
                'client_ip = :client_ip: and created_at >= :created_at:',
                'bind'   => [
                    'client_ip'  => $_SERVER['REMOTE_ADDR'],
                    'created_at' => $created_at->format('Y-m-d H:i:s')
                ],
                'offset' => 1000
            ]
        );

        if ($traces >= 10) {
            throw new SystemException(
                SystemException::IP_REQUEST_LIMIT,
                'You have reached maximum requests. Please wait a few moments before trying again.',
                null,
                sprintf('IP request limitation(%s of %s)', $traces, $limit),
                429
            );
        }

        return true;
    }
    */
}
