<?php

namespace Fwok\Plugins;

class ICalCalendar extends PluginBase
{
    private $events = [ ];
    public $prodid;

    public function addEvent(ICalEvent $event)
    {
        $this->events[] = $event;
    }

    public function setProdid($prodid)
    {
        $this->prodid = $prodid;
    }

    public function buildCalendar()
    {
        // max line length is 75 chars. New line is \\n

        $output = "BEGIN:VCALENDAR
VERSION:2.0
METHOD:PUBLISH
PRODID:" . $this->prodid . "\n";

        // loop over events
        foreach ($this->events as $event):
            $output .= $event->buildEvent();
        endforeach;

        // close calendar
        $output .= "END:VCALENDAR";

        return $output;
    }

}

