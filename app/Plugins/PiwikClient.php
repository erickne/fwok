<?php

namespace Fwok\Plugins;


class PiwikClient extends PluginBase
{
    var $url;
    var $token_auth;
    var $server;
    var $content;
    var $format = 'PHP';
    var $module = 'API';
    var $method;
    var $debug = 0;
    var $queries = [ ];

    function __construct($o = null)
    {
        $this->setServer($o['server']);
        $this->debug = $o['debug'];
    }

    public function setDebug($bool)
    {
        $this->debug = $bool;

        return $this;
    }

    public function setTokenAuth($token)
    {
        $this->token_auth = $token;

        return $this;
    }

    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }

    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    public function setModule($module)
    {
        $this->module = $module;

        return $this;
    }

    public function setServer($server)
    {
        $this->server = $server . "?";

        return $this;
    }

    public function setApiModule($module)
    {
        $this->addQuery('apiModule', $module);

        return $this;
    }

    public function setApiAction($action)
    {
        $this->addQuery('apiAction', $action);

        return $this;
    }

    public function addQuery($key, $value)
    {
        $this->queries[$key] = urlencode($value);

        return $this;
    }

    public function resetQuery()
    {
        $this->queries = [ ];

        return $this;
    }

    public function send()
    {
        $this->url = $this->server;

        $this->addQuery('method', $this->method);
        $this->addQuery('module', $this->module);
        $this->addQuery('token_auth', $this->token_auth);
        $this->addQuery('format', $this->format);

        foreach ($this->queries as $k => $v) {
            $this->url .= "&" . $k . "=" . $v;
        }

        Logger::shoutInDebug("Piwik Query: " . $this->url);

        $fetched = file_get_contents($this->url);
        $this->content = unserialize($fetched);

        if ($this->content['result'] == 'error') {
            Logger::shoutInAlert("Piwik Query Result Error: " . $this->content['message'] . " | URL = " . $this->url);
        }
        $this->printDebug();

        $this->resetQuery();

        return $this->content;
    }

    private function printDebug()
    {
        if ($this->debug) {
            echo "\n" . "========================== PIWIK DEBUG QUERIES   =======================================" . "\n";
            print_r($this->queries);
            echo "========================== PIWIK DEBUG REQUEEST   ======================================" . "\n";
            echo $this->url . "\n";
            echo "========================== PIWIK DEBUG RESULT ==========================================" . "\n";
            print_r($this->content);
            echo "\n" . "========================== PIWIK DEBUG END  ============================================" . "\n";
        }
    }


    public static function siteAdd($site_name, $site_url)
    {
        $service = self::getStaticService('piwik_client');
        $service->setMethod('SitesManager.addSite');
        $service->addQuery('siteName', $site_name);
        if (is_array($site_url)) {
            $i = 0;
            foreach ($site_url as $url) {
                $service->addQuery('urls[' . $i . ']', $url);
                $i++;
            }
        } else {
            $service->addQuery('urls', $site_url);
        }

        return $service->send();
    }

    public static function siteUpdate($piwik_site_id, $site_name, $site_url)
    {
        $service = self::getStaticService('piwik_client');
        $service->setMethod('SitesManager.updateSite');
        $service->addQuery('siteName', $site_name);
        $service->addQuery('idSite', $piwik_site_id);
        if (is_array($site_url)) {
            $i = 0;
            foreach ($site_url as $url) {
                $service->addQuery('urls[' . $i . ']', $url);
                $i++;
            }
        } else {
            $service->addQuery('urls', $site_url);
        }

        return $service->send();
    }

    public static function siteGet($piwik_site_id)
    {
        $service = self::getStaticService('piwik_client');
        $service->setMethod('SitesManager.getSiteFromId');
        $service->addQuery('idSite', $piwik_site_id);

        return $service->send();
    }

    public static function imageGraph($piwik_site_id)
    {
        $service = self::getStaticService('piwik_client');
        $service->setMethod('ImageGraph.get');
        $service->addQuery('idSite', $piwik_site_id);
        $service->setApiModule('VisitsSummary');
        $service->setApiAction('get');
        $service->addQuery('graphType', 'evolution');
        $service->addQuery('period', 'day');
        $service->addQuery('date', 'previous30');
        $service->addQuery('width', '500');
        $service->addQuery('height', '250');

        return $service->send();
    }
}