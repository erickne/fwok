<?php

namespace Fwok\Plugins;

use Fwok\Application\ApplicationBase;
use Fwok\Exceptions\Exception;
use Fwok\Plugins\Logger;
use Phalcon\Http\Request;
use Phalcon\Translate\Adapter\NativeArray;

class FilterBag extends PluginBase
{
    const OBJECT_NAME = 'filterbag';
    const ICON = 'fa fa-cog fa-fw';

    public $bag = [];
    public $name;

    public function __construct($object)
    {
        $this->name = $object;
        return $this;
    }

    /**
     * @param $filter_item FilterItem
     * @return $this
     */
    public function addFilter($filter_item)
    {
        $this->bag[] = $filter_item;
        return $this;
    }

    public function extract()
    {
        $bag_array = [];
        foreach ($this->bag as $item) {
            /**@var \Fwok\Plugins\FilterItem $item **/
            $bag_array[] = $item->toArray($this);
        }
        return $bag_array;
    }
}