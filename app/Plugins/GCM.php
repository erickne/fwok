<?php

namespace Fwok\Plugins;

use Fwok\Exceptions\IntegrationException;
use Fwok\Exceptions\SystemException;
use Fwok\Models\GCMMessage;
use Fwok\Models\NotificationsCloud;
use Fwok\Models\RequestsCurl;

class GCM extends PluginBase
{
    const URL = 'https://android.googleapis.com/gcm/send';
    public $api_key = '';
    public $devices = [];

    const ERROR_UNAVAILABLE = 'Unavailable'; //Server not available, resend the message
    const ERROR_INVALID_REGISTRATION = 'InvalidRegistration'; //Invalid device registration Id
    const ERROR_NOT_REGISTERED = 'NotRegistered'; //Application was uninstalled from the device

    /**
     * GCM constructor.
     * @param $api_key
     * @param array $devices
     * @throws SystemException
     */
    public function __construct($api_key, array $devices = array())
    {
        $this->api_key = $api_key;
        $this->setDevices($devices);

        if (strlen($this->api_key) < 8) {
            $msg = 'GCM Invalid API key';
            throw new SystemException(SystemException::CONFIGURATION_INVALID, 'System Configuration Error', $msg);
        }
    }

    public function setDevices($deviceIds = null)
    {
        if (is_array($deviceIds)) {
            $this->devices = $deviceIds;
        } else {
            $this->devices = [$deviceIds];
        }

    }

    /**
     * @param GCMMessage $message
     * @param array $devices
     * @return bool
     * @throws \Fwok\Exceptions\Exception
     * @throws IntegrationException
     * @throws SystemException
     * @throws \Fwok\Exceptions\InputException
     */
    public function send(GCMMessage $message, $devices = [])
    {
        //$this->setDevices(); //Reload devices from database
        $this->devices = $devices;

        if (!is_array($this->devices)) {
            $msg = 'GCM requires devicesIDs as an array';
            throw new SystemException(SystemException::LOGIC_INVALID, null, $msg);
        }

        if (count($this->devices) === 0) {
            return false;
        }

        $fields = [
            'registration_ids' => $this->devices,
            'data' => ["message" => $message->exportToSend()],
        ];

        $headers = [
            'Authorization: key=' . $this->api_key,
            'Content-Type: application/json'
        ];

        $request = new RequestsCurl();
        //$request->setUserId(ApplicationBase::getUser()->getId());
        $request->setServer(RequestsCurl::SERVER_GCM);
        $request->setPostData(json_encode($fields));
        $request->setUrl(self::URL);
        $request->saveOrException();
        // Open connection
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, self::URL);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        // Avoids problem with https certificate
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // Execute post
        $curl_result = curl_exec($ch);
        $decoded_result = json_decode($curl_result);

        $request->setResponse($decoded_result);
        $request->saveOrException();

        if (!json_decode($curl_result)) {
            throw new IntegrationException(IntegrationException::INVALID_RESPONSE, 'INVALID_RESPONSE_FROM_GCM');
        }
        /*
         * Response:
         *
            { "multicast_id": 216,
              "success": 2,
              "failure": 3,
              "canonical_ids": 1,
              "results": [
                { "message_id": "XXXX" } - success
                { "message_id": "XXXX", "registration_id": "XXXX" } - success, device
                    registration id has been changed mainly due to app re-install
                { "error": "Unavailable" } - Server not available, resend the message
                { "error": "InvalidRegistration" } - Invalid device registration Id
                { "error": "NotRegistered"} - Application was uninstalled from the device
              ]
            }
         */

        /*
         * https://developers.google.com/cloud-messaging/http#request
         */
        //var_dump($decoded_result->results);
        foreach ($this->devices as $key => $device_id) {
            /**@var \Fwok\Models\UsersDevices $device_obj * */
            /** @noinspection PhpUndefinedMethodInspection */
            $device_obj = \Fwok\Models\UsersDevices::findFirstByDeviceId($device_id);
            $notification = new NotificationsCloud();
            $notification->setMessage($message->exportToSend());
            $notification->setUsersDeviceId($device_obj->getId());
            $notification->setRequestCurlId($request->getId());
            if ($decoded_result->results[$key]->message_id) {
                $notification->setReturnedMessage($decoded_result->results[$key]->message_id);
            }
            if ($decoded_result->results[$key]->error) {
                $notification->setReturnedError($decoded_result->results[$key]->error);
                $device_obj->deleteOrException();
            }
            //Update device id
            if ($decoded_result->results[$key]->registration_id) {
                $device_obj->setDeviceId($decoded_result->results[$key]->registration_id);
                $device_obj->saveOrException();
            }
            $notification->saveOrException();
        }

        // Close connection
        curl_close($ch);

        return ($decoded_result->success > 0) ? true : false;
    }
}
