<?php

namespace Fwok\Plugins;

use Fwok\Application\ApplicationBase;
use Fwok\Exceptions\Exception;
use Fwok\Exceptions\SystemException;

/**
 * Batch Mockup
 *
 * I placed this in my BaseController.php file
 *
 * @usage
 *     $batch = new Batch('stats');
 *     $batch->columns = ['score', 'name'];
 *     $batch->data = [
 *         [1, 'john'],
 *         [4, 'fred'],
 *         [1, 'mickey'],
 *     ];
 *     $batch->insert();
 *
 */
class BatchInsert extends PluginBase
{
    public $table = null;
    public $columns = [ ];
    public $data = [ ];
    public $insert_string = '';

    public $created_at = null;
    public $updated_at = null;
    public $business_id = null;
    public $deleted = null;
    public $user_id = null;

    public $default_values = [ ];

    // --------------------------------------------------------------
    public function __construct($table = false)
    {
        if ($table) {
            $this->table = $table;
        }
        $di = \Phalcon\DI::getDefault();
        $this->db = $di->get('db');
    }

    public function setDefaultValue($column, $value = null)
    {
        $this->default_values[] = [
            'column' => $column,
            'value'  => $value
        ];
    }

    // --------------------------------------------------------------
    public function insert()
    {
        $this->validate();
        $this->stringify();
        $result = null;

        $this->db->begin();

        $result = $this->db->execute($this->insert_string);

        $this->insert_string = '';

        $this->db->commit();

        if (count($this->data) == $result) {
            return $result;
        } else {
            return false;
        }
    }

    // --------------------------------------------------------------
    private function stringify()
    {
        foreach ($this->default_values as $default_value) {
            $this->columns[] = $default_value['column'];
        }


        $columns = sprintf('`%s`', implode('`,`', $this->columns));
        $str = '';
        foreach ($this->data as $values) {

            foreach ($this->default_values as $default_value) {
                $values[] = $default_value['value'];
            }

            foreach ($values as &$val) {
                if (is_null($val)) {
                    $val = 'NULL';
                    continue;
                }
                if (is_string($val)) {
                    $val = "'" . $val . "'";
                }
            }
            $str .= sprintf('(%s),', implode(',', $values));
        }
        $str = rtrim($str, ',');
        $query = sprintf(
            "INSERT INTO `%s` (%s) VALUES %s",
            $this->table,
            $columns,
            $str
        );
        $this->insert_string = $query;
    }

    private function makeValues($item)
    {
        print_r($item);
    }

    // --------------------------------------------------------------
    private function validate()
    {
        if ($this->table == null) {
            throw new SystemException(SystemException::LOGIC_INVALID, null, 'Batch Table must be defined');
        }
        if (count($this->columns) == 0) {
            throw new SystemException(SystemException::LOGIC_INVALID, null, 'Batch Columns cannot be empty');
        }
        if (count($this->data) == 0) {
            throw new SystemException(SystemException::LOGIC_INVALID, null, 'Batch Data array cannot be empty');
        }
        $required_count = count($this->columns);
        foreach ($this->data as $value) {
            if (count($value) !== $required_count) {
                $msg = 'Batch Data must match the same column count of ' . $required_count;
                throw new SystemException(SystemException::LOGIC_INVALID, null, $msg, 500, null, null, $value);
            }
        }
    }
}
