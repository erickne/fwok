<?php

namespace Fwok\Plugins;

use Fwok\Application\App;
use Fwok\Exceptions\IntegrationException;
use Fwok\Exceptions\SystemException;
use Fwok\Models\BusinessSubscriptions;
use Fwok\Models\Hooks;
use Fwok\Models\Payments;
use Fwok\Models\Subscriptions;

class Pagseguro extends PluginBase
{
    const OBJECT_NAME = 'pagseguro';
    const ICON = 'fa fa-dollar fa-fw';

    const PREAPPROVAL_STATUS_PENDING = 'PENDING';
    const PREAPPROVAL_STATUS_ACTIVE = 'ACTIVE';

    const NOTIFICATION_TYPE_PREAPPROVAL = 'preApproval';
    const NOTIFICATION_TYPE_TRANSACTION = 'transaction';

    public $url = '';

    public $credentials;
    public $sandbox = 0;

    public function __construct($url_suffix = '/v2')
    {
        $config = App::getConfig('pagseguro');

        putenv(sprintf('PAGSEGURO_ENV=%s', $config->environment));
        putenv(sprintf('PAGSEGURO_EMAIL=%s', $config->email));
        putenv(sprintf('PAGSEGURO_LOG_ACTIVE=%s', $config->logActive));
        putenv(sprintf('PAGSEGURO_LOG_LOCATION=%s', $config->logLocation));
        putenv(sprintf('PAGSEGURO_CHARSET=%s', $config->charSet));

        putenv(sprintf('PAGSEGURO_TOKEN_PRODUCTION=%s', $config->production->token));
        putenv(sprintf('PAGSEGURO_TOKEN_SANDBOX=%s', $config->sandbox->token));
        putenv(sprintf('PAGSEGURO_APP_ID_PRODUCTION=%s', $config->production->appid));
        putenv(sprintf('PAGSEGURO_APP_ID_SANDBOX=%s', $config->sandbox->appid));
        putenv(sprintf('PAGSEGURO_APP_KEY_PRODUCTION=%s', $config->production->appkey));
        putenv(sprintf('PAGSEGURO_APP_KEY_SANDBOX=%s', $config->sandbox->appkey));

        $this->credentials = new \stdClass();

        $this->credentials->email = $config->email;
        if ($config->environment === 'sandbox') {
            $this->sandbox = true;
            $this->url = 'https://ws.sandbox.pagseguro.uol.com.br' . $url_suffix;
            $this->credentials->token = $config->sandbox->token;
            $this->credentials->appid = $config->sandbox->appid;
            $this->credentials->appkey = $config->sandbox->appkey;
        } elseif ($config->environment === 'production') {
            $this->sandbox = false;
            $this->url = 'https://ws.pagseguro.uol.com.br' . $url_suffix;
            $this->credentials->token = $config->production->token;
            $this->credentials->appid = $config->production->appid;
            $this->credentials->appkey = $config->production->appkey;
        } else {
            $msg = 'Invalid Pagseguro environment configuration';
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $msg);
        }

        // Iniciar configurações
        \PagSeguroLibrary::init();
        \PagSeguroConfig::init();

    }

    /**
     * @param $notificationCode
     * @param Hooks $hook
     * @return bool
     * @throws \Fwok\Exceptions\Exception
     * @throws \Exception
     * @throws \Fwok\Exceptions\InputException
     * @throws \PagSeguroServiceException
     */
    public function notificationTransaction($notificationCode, $hook)
    {
        $transaction = \PagSeguroNotificationService::checkTransaction($this->getCredentials(), $notificationCode);

        $hook->setResponse($transaction->toString())->saveOrExceptionNewTransaction();

        $payment = Payments::findFirstByExternalToken($transaction->getCode());
        if (!$payment) {
            $payment = new Payments();
            $payment->setMethod(Payments::METHOD_PAGSEGURO);
            $payment->setExternalToken($transaction->getCode());
            $payment->setBusinessSubscriptionId($transaction->getReference());
            $payment->setCurrencyCode('BRL');
        }

        $payment->setValue($transaction->getGrossAmount());
        $payment->setStatus(App::getPagSeguro()->convertTransactionStatus($transaction->getStatus()->getValue()));
        return $payment->saveOrException();

    }

    /**
     * @param $notificationCode
     * @return bool
     */
    public function notificationAuthorization($notificationCode)
    {
        $msg = sprintf('Received pagseguro Autorization notification [%s]. Nothing to do.', $notificationCode);
        Logger::shoutInAlert($msg);
        return true;
    }

    /**
     * @param $preApprovalCode
     * @param Hooks $hook
     * @return bool
     * @throws \Fwok\Exceptions\Exception
     * @throws IntegrationException
     * @throws \Exception
     * @throws \Fwok\Exceptions\InputException
     * @throws \PagSeguroServiceException
     */
    public function notificationPreApproval($preApprovalCode, $hook)
    {
        /** @var \PagSeguroPreApproval $preApproval */
        $preApproval = \PagSeguroNotificationService::checkPreApproval($this->getCredentials(), $preApprovalCode);

        $hook->setResponse($preApproval->toString())->saveOrExceptionNewTransaction();

        $pagseguro_status = Pagseguro::convertPreApprovalStatus($preApproval->getStatus()->getValue());
        /*
         * Não há ação para:
         * BusinessSubscriptions::STATUS_PENDING ou BusinessSubscriptions::STATUS_UNPAID
         */
        $business_subscription = BusinessSubscriptions::findFirstById($preApproval->getReference(), 1, 1);
        $business_subscription->setPagseguroCode($preApproval->getCode());
        $business_subscription->setStatus($pagseguro_status);
        $business = $business_subscription->getBusiness();
        switch ($pagseguro_status) {
            case (BusinessSubscriptions::STATUS_ACTIVE):
                $business_subscription->activate();
                break;
            case BusinessSubscriptions::STATUS_CANCELLED:
            case BusinessSubscriptions::STATUS_EXPIRED:
            case BusinessSubscriptions::STATUS_INACTIVE:
                $business_subscription->deactivate();
                $business->createSubscription(Subscriptions::findFirstFree(), true);
                break;
        }
        
        return $business_subscription->saveOrException();
    }

    /**
     * @return \PagSeguroAccountCredentials
     * @throws \Exception
     */
    public function getCredentials()
    {
        return \PagSeguroConfig::getAccountCredentials();
    }

    public static function convertTransactionStatus($status)
    {
        switch ($status) {
            case (1):
                return Payments::STATUS_AWAITING_PAYMENT;
                break;
            case (2):
                return Payments::STATUS_ANALYZED;
                break;
            case (3):
            case (4):
                return Payments::STATUS_PAID;
                break;
            case (5):
            case (6):
            case (8):
            case (9):
                return Payments::STATUS_OTHER;
                break;
            case (7):
                return Payments::STATUS_CANCELLED;
                break;
        }

        throw new IntegrationException(IntegrationException::INVALID_VALUE);
    }

    public function getUrl()
    {
        return $this->url;
    }

    public static function convertPreApprovalStatus($status)
    {
        /*
        'INITIATED' => 0,
        'PENDING' => 1,
        'ACTIVE' => 2,
        'CANCELLED' => 3,
        'CANCELLED_BY_RECEIVER' => 4,
        'CANCELLED_BY_SENDER' => 5,
        'EXPIRED' => 6
        'SUSPENDED' => 7
         */
        switch ($status) {
            case (0):
            case (1):
            case (7):
                return BusinessSubscriptions::STATUS_INACTIVE;
                break;
            case (2):
                return BusinessSubscriptions::STATUS_ACTIVE;
                break;
            case (3):
            case (4):
            case (5):
                return BusinessSubscriptions::STATUS_CANCELLED;
                break;
            case (6):
                return BusinessSubscriptions::STATUS_EXPIRED;
                break;
        }

        throw new IntegrationException(IntegrationException::INVALID_VALUE);
    }
}
