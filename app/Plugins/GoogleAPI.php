<?php

namespace Fwok\Plugins;

use Fwok\Exceptions\IntegrationException;
use Fwok\Models\Files;

class GoogleAPI extends PluginBase
{

    public function __construct()
    {
    }
    
    /**
     * Obtem um novo token de acesso
     *
     * @param \Google_Client $client
     * @return string
     * @throws \InvalidArgumentException
     * @throws \Fwok\Exceptions\IntegrationException
     */
    public static function getRefreshTokenCli(\Google_Client $client)
    {
        $client->setAccessType('offline');
        //$client->setApprovalPrompt("force");

        echo "URL para obter o codigo:\n\n" . $client->createAuthUrl() . "\n\n";
        echo "Colar o codigo aqui:\n";
        $authCode = trim(fgets(STDIN));

        $accessToken = $client->authenticate($authCode);
        $client->setAccessToken($accessToken);
        return $accessToken['refresh_token'];
    }


    /**
     * Inserindo um arquivo
     *
     * @param \Google_Service_Drive $service Instancia do objeto 'service Drive'.
     * @param string $filename Nome do arquivo no disco.
     * @param string $description Descrição do arquivo inserido.
     * @param string $parentId ID da pasta onde o arquivo será inserido.
     * @param string $mime Ex: "application/pdf"
     * @param string $title Título do arquivo inserido (deve incluir a extensão do arquivo).
     * @return \Google_Service_Drive_DriveFile O objeto "arquivo" que foi inserido. Se houver algum erro, então retorna NULL.
     * @throws IntegrationException
     */
    public static function driveInsertFile($service, $filename, $description = null, $parentId = null, $mime = null, $title = null)
    {
        if ($title === null) {
            $title = basename($filename);
        }

        $file = new \Google_Service_Drive_DriveFile([
            'name' => $title,
            'description' => $description,
            'parents' => [$parentId ?: null]
        ]);

        $metaData = [
            'data' => file_get_contents($filename),
            'mimeType' => $mime ?: Files::findMimeFromExt(pathinfo($filename, PATHINFO_EXTENSION)),
            'uploadType' => 'media'
        ];

        try {
            $createdFile = $service->files->create($file, $metaData);
        } catch (\Exception $e) {
            throw new IntegrationException(IntegrationException::INVALID_RESPONSE, null, $e->getMessage(), 500, [], $e);
//            print "Erro no upload do arquivo: " . $e->getMessage();
        }

        // A linha abaixo escreve o ID do arquivo
        // print 'File ID: %s' % $createdFile->getId();
        return $createdFile;
    }

    /**
     * Find parent folder
     *
     * driveFindParentFolder('Backup Server')
     *
     * @param \Google_Service_Drive $service
     * @param $folder
     * @return bool
     */
    public static function driveFindParentFolder(\Google_Service_Drive $service, $folder)
    {
        $folderId = false;
        $files = $service->files->listFiles();
        foreach ($files->getFiles() as $item) {
            if ($item['name'] === $folder) {
                $folderId = $item['id'];
                break;
            }
        }
        return $folderId;
    }

    /**
     * 
     * driveFindFile($service, 'Server Backup', 'application/vnd.google-apps.folder')
     * 
     * @param \Google_Service_Drive $service
     * @param $file_name
     * @param $mime
     * @return mixed
     * @internal param $file
     */
    public static function driveFindFile(\Google_Service_Drive $service, $file_name, $mime)
    {
        $response = $service->files->listFiles(array(
            'q' => sprintf("name = '%s' and mimeType='%s'",$file_name,$mime),
            'spaces' => 'drive',
            'fields' => 'files(id, name)',
        ));
        return $response->files;
//        foreach ($response->files as $drive_file) {
////            printf("Found file: %s (%s)\n", $file->name, $file->id);
//            $folderId = $drive_file->id;
//        }
    }
}
