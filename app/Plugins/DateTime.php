<?php

namespace Fwok\Plugins;
use Fwok\Application\App;

/**
 * Class Timezone
 * @package Fwok\Plugins
 *
 * Usage:
 *
 *
 */
class DateTime extends \DateTime
{
    public static $FORMAT = 'Y-m-d H:i:s';

    /**
     * DateTime constructor.
     * @param string $time
     * @param null|string $timezone
     */
    public function __construct($time, $timezone = null)
    {
        if($timezone === null){
            $timezone = App::getConfig('application')->timezone;
        }
        if($timezone === null) {
            $timezone = 'America/Sao_Paulo';
        }
        parent::__construct($time, new \DateTimeZone($timezone));
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->format(self::$FORMAT);
    }
}