<?php

namespace Fwok\Plugins\Financial;

use Fwok\Application\App;
use Fwok\Exceptions\Exception;
use Fwok\Exceptions\InputException;
use Fwok\Exceptions\IntegrationException;
use Fwok\Exceptions\SystemException;
use Fwok\Models\Users;
use Fwok\Models\UsersCardTokens;
use Fwok\Plugins\PluginBase;

class Iugu extends PluginBase
{
    const OBJECT_NAME = 'iugu';
    const ICON = 'fa fa-dollar fa-fw';

    const CONFIG_API_KEY = 'iugu_api_key';

    public $api_key;

    public function __construct($api_key = null)
    {
        if ($api_key) {
            $this->start($api_key);
        }
    }

    public function start($api_key = null)
    {
        $this->api_key = $api_key ?: App::getBusiness()->getConfig(self::CONFIG_API_KEY)->getValue();

        if (!$this->api_key) {
            $msg = 'Empty config->iugu->api_key';
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $msg);
        }

        \Iugu::setApiKey($this->api_key);

        return $this;
    }

    public static function createUser(Users $user)
    {
        $iugu_user = \Iugu_Customer::create(Array(
            'email' => $user->getEmail(),
            'name' => $user->getName(),
            'notes' => 'user_id=' . $user->getId()
        ));
        self::validate($iugu_user);
        $user->setIuguId($iugu_user->id);
        if ($user->save()) {
            return $iugu_user->id;
        } else {
            return false;
        }
    }


    public static function createCardToken($cc)
    {
        $card_token = \Iugu_PaymentToken::create([
            'method' => 'credit_card',
            'data' => [
                'number' => $cc->number,//'4111111111111111',
                'verification_value' => $cc->cvc,//'123',
                'first_name' => explode(' ', $cc->holderName)[0],//'Joao',
                'last_name' => array_pop(explode(' ', $cc->holderName)), //'Silva',
                'month' => $cc->month,//'12',
                'year' => $cc->year//date('Y'),
            ],
        ]);
        Iugu::validate($card_token);

        return $card_token;
    }

    /**
     * @param $card_token
     * @param $description
     * @param Users $user
     * @throws Exception
     * @throws IntegrationException
     * @throws InputException
     */
    public static function attachCardTokenToUser($card_token, $description, Users $user)
    {
        $iugu_user_id = $user->getIuguId() ?: App::getIugu()->createUser($user);
        $customer = self::fetchCustomer($iugu_user_id);

        /** @noinspection PhpUndefinedMethodInspection */
        $payment_method = $customer->payment_methods()->create([
            'description' => $description,
            //'item_type' => $card_token->method,
            'token' => $card_token->id
        ]);
        Iugu::validate($payment_method);

        $token = new UsersCardTokens();
        $token->setToken($payment_method->id);
        $token->setUserId($user->getId());
        $token->setDescription($payment_method->description);
        $token->setBrand($payment_method->data->brand);
        $token->setMonth($payment_method->data->month);
        $token->setYear($payment_method->data->year);
        $token->setHolderName($payment_method->data->holder_name);
        $token->setLastDigits(substr($payment_method->data->display_number, -4));
        $token->setGateway(UsersCardTokens::GATEWAY_IUGU);
        /** @noinspection DegradedSwitchInspection */
        switch ($card_token->method) {
            case 'credit_card':
                $token->setMethod(UsersCardTokens::METHOD_CC);
                break;
            default:
                $token->setMethod('');
        }

        $token->saveOrException();

        return $payment_method;
    }

    /**
     * @param Users $user
     * @param $id
     * @return \Iugu_SearchResult|null
     */
    public static function fetchPaymentMethod(Users $user, $id)
    {
        return \Iugu_PaymentMethod::fetch(Array(
            'customer_id' => $user->getIuguId(),
            'id' => $id
        ));
    }

    /**
     * @param $iugu_customer_id
     * @return \Iugu_SearchResult|null
     */
    public static function fetchCustomer($iugu_customer_id)
    {
        $customer = \Iugu_Customer::fetch($iugu_customer_id);
        self::validate($customer);

        return $customer;
    }

    /**
     * @param array $data
     * @param $token
     * @return \Iugu_SearchResult|null
     */
    public function chargeWithToken(array $data, $token)
    {
        return \Iugu_Charge::create(array_merge($data, ['token' => $token]));
    }

    /**
     * @param array $data
     * @param $payment_method
     * @return \Iugu_SearchResult|null
     */
    public function chargeWithPaymentMethod(array $data, $payment_method)
    {
        return \Iugu_Charge::create(array_merge($data, ['customer_payment_method_id' => $payment_method->id]));
    }

    public static function validate($response)
    {
        $msg = 'IUGU_COMMUNICATION_ERROR';
        if (is_string($response->errors)) {
            throw new IntegrationException(IntegrationException::INVALID_RESPONSE, $msg, $response->errors);
        } elseif (is_array($response->errors)) {
            $msg = [];
            foreach ($response->errors as $k => $v) {
                $msg[] = ucfirst($k . ' ' . implode(' | ', $v));
            }
            throw new IntegrationException(IntegrationException::INVALID_RESPONSE, implode(' ', $msg),
                $response->errors);
        }
        return true;
    }

}
