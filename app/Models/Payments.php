<?php

namespace Fwok\Models;

use Fwok\Application\App;
use Fwok\Exceptions\IntegrationException;
use Fwok\Exceptions\SystemException;
use Fwok\Models\Validators\DiscountValidator;

/**
 * @SWG\Definition(
 *      definition="Payment",
 *      type="object",
 *      @SWG\Property(property="discount",ref="#/definitions/Discount"),
 *      required={"method", "status"}
 *  )
 */
class Payments extends ModelBase
{
    const OBJECT_NAME = 'payments';
    const ICON = 'fa fa-dollar fa-fw';

    const METHOD_PAYPAL = 'paypal';
    const METHOD_PAGSEGURO = 'pagseguro';
    const METHOD_ANDROID_PAY = 'androidpay';
    const METHOD_CASH = 'cash';
    const METHOD_TEF = 'tef';
    const METHOD_IUGU_CC_DIRECT = 'cc-direct';
    const METHOD_IUGU_CC_TOKEN = 'cc-token';

    const STATUS_AWAITING_PAYMENT = 'AWAITING_PAYMENT';
    const STATUS_ANALYZED = 'ANALYZED';
    const STATUS_PAID = 'PAID';
    const STATUS_PAID_AND_DISABLED = 'STATUS_PAID_AND_DISABLED';
    const STATUS_CANCELLED = 'CANCELLED';
    const STATUS_OTHER = 'OTHER';
    const STATUS_ERROR = 'ERROR';

    const EXPORT_BUSINESS_SUBSCRIPTION = 'payments_export_business_subscription';
    const EXPORT_USER = 'payments_export_user';

    public function initialize()
    {
        $this->hasOne(
            'business_subscription_id', '\Fwok\Models\BusinessSubscriptions', 'id',
            ['alias' => 'BusinessSubscription', 'reusable' => true,]
        );
        $this->hasOne(
            'method_code', '\Fwok\Models\PaymentMethods', 'code',
            ['alias' => 'PaymentMethod', 'reusable' => true,]
        );
        $this->hasOne(
            'user_id', '\Fwok\Models\Users', 'id',
            ['alias' => 'User', 'reusable' => true,]
        );
        $this->hasOne(
            'responsible_id', '\Fwok\Models\Users', 'id',
            ['alias' => 'Responsible', 'reusable' => true,]
        );

        $this->hasOne(
            'discount_id', '\Fwok\Models\Discounts', 'id',
            ['alias' => 'Discount', 'reusable' => true,]
        );

        $this->setSource('fwok_payments');
    }

    public function delete()
    {
        $this->setStatus(self::STATUS_CANCELLED)->saveOrException();
        return parent::delete();
    }
    public function validation()
    {
        if ($this->getValue() === null) {
            $this->appendErrorMessageTranslated('INVALID_VALUE');
        }
        if (!in_array($this->getMethodCode(), PaymentMethods::findAllMethodCodes(),true)) {
            $this->appendErrorMessageTranslated('INVALID_METHOD');
        }
        if (!in_array($this->getStatus(), self::findAllStatus(),true)) {
            $this->appendErrorMessageTranslated('INVALID_STATUS');
        }
        if (!$this->getCurrencyCode()) {
            $defaultCurrencyCode = App::getConfig('application')->defaultCurrencyCode;
            if ($defaultCurrencyCode) {
                $this->setCurrencyCode($defaultCurrencyCode);
            } else {
                $this->appendErrorMessageTranslated('INVALID_CURRENCY_CODE');
            }
        }

        if ($this->getId()) {
            if ($this->getSnapshotData() and $this->getSnapshotData()['method'] !== $this->getMethod()) {
                $this->appendErrorMessageTranslated('CANNOT_CHANGE_PAYMENT_METHOD');
            }
        }
        if (!in_array($this->getMethod(), [
                self::METHOD_ANDROID_PAY,
                self::METHOD_PAYPAL,
                self::METHOD_PAGSEGURO,
                self::METHOD_IUGU_CC_DIRECT,
                self::METHOD_IUGU_CC_TOKEN
            ],true) and
            $this->getExternalToken()
        ) {
            $this->appendErrorMessageTranslated('EXTERNAL_PAYMENTS_MUST_HAVE_TOKEN');
        }

        $this->validate(new DiscountValidator([
            'id' => $this->getDiscountId(),
            'required' => 0,
            'acceptDeleted' => 1
        ]));
        $this->validate(new Validators\UserValidator([
            'id' => $this->getUserId(),
            'required' => 0,
            'acceptDeleted' => 1
        ]));
        $this->validate(new Validators\UserValidator([
            'id' => $this->getResponsibleId(),
            'required' => 0,
            'acceptDeleted' => 1
        ]));

        return !$this->validationHasFailed();
    }

    public static function findAllStatus()
    {
        return [
            self::STATUS_AWAITING_PAYMENT,
            self::STATUS_ANALYZED,
            self::STATUS_PAID,
            self::STATUS_PAID_AND_DISABLED,
            self::STATUS_CANCELLED,
            self::STATUS_OTHER,
        ];
    }

    public static function findAllMethods()
    {
        return [
            self::METHOD_PAYPAL,
            self::METHOD_PAGSEGURO,
            self::METHOD_ANDROID_PAY,
            self::METHOD_CASH,
            self::METHOD_TEF,
            self::METHOD_IUGU_CC_DIRECT,
            self::METHOD_IUGU_CC_TOKEN,
        ];
    }

    /**
     * Return the related "BusinessSubscription"
     *
     * @param array|string $parameters
     * @param int $exception_if_not_found
     * @throws \Fwok\Exceptions\Exception
     * @return \Fwok\Models\BusinessSubscriptions
     */
    public function getBusinessSubscription($parameters = null, $exception_if_not_found = 0)
    {
        return $this->getRelated('BusinessSubscription', $parameters, $exception_if_not_found);
    }
    /**
     * Return the related "PaymentMethod"
     *
     * @param array|string $parameters
     * @param int $exception_if_not_found
     * @throws \Fwok\Exceptions\Exception
     * @return \Fwok\Models\PaymentMethods
     */
    public function getPaymentMethod($parameters = null, $exception_if_not_found = 0)
    {
        return $this->getRelated('PaymentMethod', $parameters, $exception_if_not_found);
    }

    /**
     * Return the related "Discount"
     *
     * @param array|string $parameters
     * @param int $exception_if_not_found
     * @throws \Fwok\Exceptions\Exception
     * @return \Fwok\Models\Discounts
     */
    public function getDiscount($parameters = null, $exception_if_not_found = 0)
    {
        return $this->getRelated('Discount', $parameters, $exception_if_not_found);
    }

    public function export($opt = [])
    {
        $ret = [
            'object' => self::OBJECT_NAME,
            'user_id' => $this->getUserId(),
            'responsible_id' => $this->getResponsibleId(),
            'id' => $this->getId(),
            'deleted' => $this->getDeleted(),
            'value' => $this->getValue(),
            'currency_code' => $this->getCurrencyCode(),
            'method_code' => $this->getMethodCode(),
            'external_token' => $this->getExternalToken(),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt(),
            'status' => $this->getStatus(),
            'object_type' => $this->getObjectType(),
            'object_id' => $this->getObjectId(),
            'business_subscription_id' => $this->getBusinessSubscriptionId(),
            'description' => $this->getDescription()
        ];

        if($this->getPaymentMethod()) {
            $ret['method'] = $this->getPaymentMethod()->export();
        }

        if ($this->getDiscount()) {
            $ret['discount'] = $this->getDiscount()->export($opt);
        }

        if (@$opt[self::EXPORT_BUSINESS_SUBSCRIPTION]) {
            $ret['business_subscription'] = $this->getBusinessSubscription()->export();
        }
        if (@$opt[self::EXPORT_USER]) {
            $ret['user'] = $this->getUser() ? $this->getUser()->export() : null;
        }

        return $ret;
    }

    /**
     * Return the related "User"
     *
     * @param array|string $parameters
     * @param int $exception_if_not_found
     * @throws \Fwok\Exceptions\Exception
     * @return \Fwok\Models\Users
     */
    public function getUser($parameters = null, $exception_if_not_found = 0)
    {
        return $this->getRelated('User', $parameters, $exception_if_not_found);
    }

    /**
     * Return the related "Responsible"
     *
     * @param array|string $parameters
     * @param int $exception_if_not_found
     * @throws \Fwok\Exceptions\Exception
     * @return \Fwok\Models\Users
     */
    public function getResponsible($parameters = null, $exception_if_not_found = 0)
    {
        return $this->getRelated('Responsible', $parameters, $exception_if_not_found);
    }

    /**
     * @param null $parameters
     * @return $this
     */
    public static function findFirstByExternalToken($parameters = null)
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return parent::findFirstByExternalToken($parameters);
    }

    public function getPaypalTokenData()
    {
        if ($this->getMethod() !== self::METHOD_PAYPAL) {
            return 0;
        }

        return $this->getService('paypal')->getExpressCheckoutDetails($this->getExternalToken());

    }

    /**
     *
     * @var integer
     */
    protected $value;

    /**
     * Method to set the value of field value
     *
     * @param integer $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Returns the value of field value
     *
     * @return integer
     */
    public function getValue()
    {
        return (float)$this->value;
    }

    /**
     *
     * @var string
     */
    protected $external_token;

    /**
     * Method to set the value of field external_token
     *
     * @param string $external_token
     * @return $this
     */
    public function setExternalToken($external_token)
    {
        $this->external_token = $external_token;

        return $this;
    }

    /**
     * Returns the value of field external_token
     *
     * @return string
     */
    public function getExternalToken()
    {
        return $this->external_token;
    }

    /**
     *
     * @var string
     */
    protected $method;

    /**
     * Method to set the value of field method
     *
     * @param string $method
     * @return $this
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Returns the value of field method
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     *
     * @var integer
     */
    protected $discount_id;

    /**
     * Method to set the value of field discount_id
     *
     * @param integer $discount_id
     * @return $this
     */
    public function setDiscountId($discount_id)
    {
        $this->discount_id = $discount_id;

        return $this;
    }

    /**
     * Returns the value of field discount_id
     *
     * @return integer
     */
    public function getDiscountId()
    {
        return $this->discount_id;
    }

    /**
     *
     * @var string
     */
    protected $currency_code;

    /**
     * Method to set the value of field currency_code
     *
     * @param string $currency_code
     * @return $this
     */
    public function setCurrencyCode($currency_code)
    {
        $this->currency_code = $currency_code;

        return $this;
    }

    /**
     * Returns the value of field currency_code
     *
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currency_code;
    }

    /**
     *
     * @var integer
     * @SWG\Property(name="responsible_id",type="integer",description="")
     */
    protected $responsible_id;

    /**
     * Method to set the value of field responsible_id
     *
     * @param integer $responsible_id
     * @return $this
     */
    public function setResponsibleId($responsible_id)
    {
        $this->responsible_id = $responsible_id;

        return $this;
    }

    /**
     * Returns the value of field responsible_id
     *
     * @return integer
     */
    public function getResponsibleId()
    {
        return (int)$this->responsible_id;
    }

    /**
     *
     * @var integer
     */
    protected $business_subscription_id;

    /**
     * Method to set the value of field business_subscription_id
     *
     * @param integer $business_subscription_id
     * @return $this
     */
    public function setBusinessSubscriptionId($business_subscription_id)
    {
        $this->business_subscription_id = $business_subscription_id;

        return $this;
    }

    /**
     * Returns the value of field business_subscription_id
     *
     * @return integer
     */
    public function getBusinessSubscriptionId()
    {
        return (int)$this->business_subscription_id;
    }

    /**
     *
     * @var string
     */
    protected $status;

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function retrieveExternalStatus()
    {
        switch ($this->getMethod()) {
            case (Payments::METHOD_PAGSEGURO):
                return $this->retrieveExternalStatusPagseguro();
            case (Payments::METHOD_ANDROID_PAY):
                return $this->retrieveExternalStatusAndroidPay();
            case (Payments::METHOD_PAYPAL):
                throw new SystemException(SystemException::ACTION_INVALID, null, 'Not Implemented');
        }

        throw new SystemException(SystemException::ACTION_INVALID, null, 'Invalid payment method');
    }

    /**
     * @return \stdClass
     * @throws \Fwok\Exceptions\Exception
     * @throws IntegrationException
     */
    public function retrieveExternalStatusAndroidPay()
    {
        $subscription = App::getAndroidPay()->getSubscriptionPurchase(
            $this->getBusinessSubscription()->getSubscription()->getCode(),
            $this->getExternalToken()
        );

        return $subscription->toSimpleObject();
    }

    /**
     * @return bool
     * @throws \Fwok\Exceptions\Exception
     * @throws IntegrationException
     */
    public function retrieveExternalStatusAndroidPayIsActive()
    {
        $subscription = App::getAndroidPay()->getSubscriptionPurchase(
            $this->getBusinessSubscription()->getSubscription()->getCode(),
            $this->getExternalToken()
        );

        if ($subscription->paymentState === 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @return array
     * @throws \Fwok\Exceptions\Exception
     * @throws IntegrationException
     * @throws \Exception
     * @throws \PagSeguroServiceException
     */
    public function retrieveExternalStatusPagseguro()
    {
        $credentials = App::getPagSeguro()->getCredentials();
        if (!$this->getExternalToken()) {
            return ['Empty external token'];
        }
        $transaction = \PagSeguroTransactionSearchService::searchByCode($credentials, $this->getExternalToken());

        $return = array();
        $return['code'] = $transaction->getCode();
        $return['to_string'] = $transaction->toString();
        $return['email'] = $transaction->getSender() ? $transaction->getSender()->getEmail() : '';
        $return['date'] = $transaction->getDate();
        $return['reference'] = $transaction->getReference();
        $return['status_value'] = $transaction->getStatus()->getValue();
        $return['status_text'] = $transaction->getStatus()->getTypeFromValue();
        $return['status_fwok'] = App::getPagSeguro()->convertTransactionStatus(
            $transaction->getStatus()->getValue()
        );
        $return['itemsCount'] = $transaction->getItemCount();

        return $return;
    }

    /**
     *
     * @var integer
     * @SWG\Property(name="user_id",type="integer",description="")
     */
    protected $user_id;

    /**
     * Method to set the value of field user_id
     *
     * @param integer $user_id
     * @return $this
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Returns the value of field user_id
     *
     * @return integer
     */
    public function getUserId()
    {
        return (int)$this->user_id;
    }

    /**
     *
     * @var integer
     * @SWG\Property(name="business_id",type="integer",description="")
     */
    protected $business_id;

    /**
     * Method to set the value of field business_id
     *
     * @param integer $business_id
     * @return $this
     */
    public function setBusinessId($business_id)
    {
        $this->business_id = $business_id;

        return $this;
    }

    /**
     * Returns the value of field business_id
     *
     * @return integer
     */
    public function getBusinessId()
    {
        return (int)$this->business_id;
    }

    /**
     *
     * @var integer
     * @SWG\Property(name="object_id",type="integer",description="")
     */
    protected $object_id;

    /**
     * Method to set the value of field object_id
     *
     * @param integer $object_id
     * @return $this
     */
    public function setObjectId($object_id)
    {
        $this->object_id = $object_id;

        return $this;
    }

    /**
     * Returns the value of field object_id
     *
     * @return integer
     */
    public function getObjectId()
    {
        return (int)$this->object_id;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="object_type",type="string",description="")
     */
    protected $object_type;

    /**
     * Method to set the value of field object_type
     *
     * @param string $object_type
     * @return $this
     */
    public function setObjectType($object_type)
    {
        $this->object_type = $object_type;

        return $this;
    }

    /**
     * Returns the value of field object_type
     *
     * @return string
     */
    public function getObjectType()
    {
        return $this->object_type;
    }
    /**
     *
     * @var string
     * @SWG\Property(name="method_code",type="string",description="")
     */
    protected $method_code;

    /**
     * Method to set the value of field method_code
     *
     * @param string $method_code
     * @return $this
     */
    public function setMethodCode($method_code)
    {
        $this->method_code = $method_code;

        return $this;
    }

    /**
     * Returns the value of field method_code
     *
     * @return string
     */
    public function getMethodCode()
    {
        return $this->method_code;
    }
    /**
     *
     * @var string
     * @SWG\Property(name="description",type="string",description="")
     */
    protected $description;

    /**
     * Method to set the value of field description
     *
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Returns the value of field description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}
