<?php
namespace Fwok\Models\Criteria;

use Fwok\Models\ModelBase;
use Fwok\Plugins\RandomString;
use Phalcon\Filter;
use Phalcon\Mvc\Model\Resultset;

class CriteriaBase extends \Phalcon\Mvc\Model\Criteria
{
    /** * @return CriteriaBase */
    public function deleted($deleted = 0)
    {
        return $this->andWhere('deleted = :deleted:')->bind(array('deleted' => $deleted));
    }

    /**
     * Sets the conditions parameter in the criteria
     *
     * @param null $business_id
     * @param bool $andWhere
     * @return CriteriaBase
     * @throws \Fwok\Exceptions\SystemException
     */
    public function searchInBusiness($business_id = null, $andWhere = true)
    {
        if (!$business_id) {
            $business_id = \Fwok\Application\App::getBusiness()->getId();
        }

        if ($andWhere) {
            return $this->andWhere('business_id = :business_id:')->bind(array('business_id' => $business_id));
        } else {
            return $this->where('business_id = :business_id:')->bind(array('business_id' => $business_id));
        }
    }

    /**
     * Appends a condition to the current conditions using an AND operator
     *
     * @param string $conditions
     * @param mixed $bindParams
     * @param mixed $bindTypes
     * @return CriteriaBase
     */
    public function andWhere($conditions, $bindParams = null, $bindTypes = null)
    {
        return parent::andWhere($conditions, $bindParams = null, $bindTypes = null);
    }


    /**
     * Appends an IN condition to the current conditions
     * <code>
     * $criteria->inWhere('id', [1, 2, 3]);
     * </code>
     *
     * @param string $expr
     * @param array $values
     * @return CriteriaBase
     */
    public function inWhere($expr, array $values)
    {
        $filter = new Filter();
        $expr_filtered = $filter->sanitize($expr, 'alphanum');

        $tokens = [];
        foreach ($values as $key => $value) {
            $tokens[$expr_filtered . '_' . $key] = $value;
        }

        return $this->andWhere($expr . ' IN (:' . implode(':,:', array_keys($tokens)) . ':)')
            ->bind($tokens);

    }

    /**
     * Sets the conditions parameter in the criteria
     *
     * https://docs.phalconphp.com/uk/latest/reference/filter.html
     *
     * string,email,int,float,alphanum,striptags,trim,lower,upper
     *
     * @param $condition
     * @param mixed $value
     * @return CriteriaBase
     */
    public function addWhereBinded($condition, $value)
    {
        $filter = new Filter();
        $field = $filter->sanitize(explode(' ', $condition)[0], 'alphanum');

        $token = $this->generateToken($field);
        return $this->addWhere($condition . ':' . $token . ':')->bind(array($token => $value));
    }

    /**
     * Appends a BETWEEN condition to the current conditions
     * <code>
     * $criteria->betweenWhere('price', 100.25, 200.50);
     * </code>
     *
     * @param string $expr
     * @param mixed $minimum
     * @param mixed $maximum
     * @return CriteriaBase
     */
    public function betweenWhere($expr, $minimum, $maximum)
    {
        $filter = new Filter();
        $expr_filtered = $filter->sanitize($expr, 'alphanum');

        $token_min = $this->generateToken($expr_filtered) . '_min';
        $token_max = $this->generateToken($expr_filtered) . '_max';
        return $this->andWhere($expr . ' BETWEEN :' . $token_min . ': AND :' . $token_max . ':')
            ->bind([
                $token_min => $minimum,
                $token_max => $maximum
            ]);
    }

    /**
     * @param $lifetime
     * @param $key
     * @return $this
     */
    public function cached($lifetime, $key)
    {
        $this->cache([
            'lifetime' => $lifetime,
            'key' => $key
        ]);
        return $this;
    }

    /**
     * @param $condition
     * @param $value
     * @return CriteriaBase
     */
    public function andWhereBinded($condition, $value)
    {
        $filter = new Filter();
        $field = $filter->sanitize(explode(' ', $condition)[0], 'alphanum');

        $token = $this->generateToken($field);
        return $this->andWhere($condition . ':' . $token . ':')->bind(array($token => $value));
    }

    private function generateToken($prefix_or_field)
    {
        return $prefix_or_field . '_' . count($this->getParams()['bind']);
    }

    public function orWhereBinded($condition, $value)
    {
        $filter = new Filter();
        $field = $filter->sanitize(explode(' ', $condition)[0], 'alphanum');

        $token = $this->generateToken($field);
        return $this->orWhere($condition . ':' . $token . ':')->bind(array($token => $value));
    }

    /**
     * Sets the conditions parameter in the criteria
     *
     * https://docs.phalconphp.com/uk/latest/reference/filter.html
     *
     * string,email,int,float,alphanum,striptags,trim,lower,upper
     *
     * @param $field
     * @param mixed $value
     * @return CriteriaBase
     */
    public function addWhereLikeBinded($field, $value)
    {
        $token = $this->generateToken($field);
        return $this->addWhere($field . ' LIKE :' . $token . ':')->bind(array($token => '%' . $value . '%'));
    }

    /**
     * Sets the conditions parameter in the criteria
     *
     * https://docs.phalconphp.com/uk/latest/reference/filter.html
     *
     * string,email,int,float,alphanum,striptags,trim,lower,upper
     *
     * @param $field
     * @param mixed $value
     * @return CriteriaBase
     */
    public function orWhereLikeBinded($field, $value)
    {
        $token = $this->generateToken($field);
        return $this->orWhere($field . ' LIKE :' . $token . ':')->bind(array($token => '%' . $value . '%'));
    }

    /**
     * Created random column for avoid caching
     *
     * @param null $random
     * @return CriteriaBase
     */
    public function addRandomWhere($random = null)
    {
        if (!$random) {
            $random_obj = new RandomString('abcdefghijklmnopqrstuvwxyz');
            $random = $random_obj->generate(10);
        }
        return $this->addWhere(sprintf('"%s"="%s"', $random, $random));
    }

    /**
     * Sets the bound parameters in the criteria
     * This method adds all previously set bound parameters
     *
     * @param array $bindParams
     * @return CriteriaBase
     */
    public function bind(array $bindParams)
    {
        if (isset($this->_params["bind"])) {
            $bind = $this->_params["bind"];
        } else {
            $bind = null;
        }
        if (is_array($bind)) {
            $this->_params['bind'] = array_merge($bind, $bindParams);
        } else {
            $this->_params['bind'] = $bindParams;
        }

        return $this;
    }

    /**
     * Executes a find using the parameters built with the criteria
     *
     * @return array|Resultset|ModelBase[]|self[]
     */
    public function execute()
    {
        return parent::execute();
    }

    /**
     * @return array
     */
    public function executeToArray()
    {
        $results = $this->execute();
        $array = [];
        foreach ($results as $result) {
            $array[] = $results;
        }

        return $array;
    }

    /**
     * @return int
     */
    public function count()
    {
        $result = $this->columns('COUNT(*) AS count')->executeFirst();
        return (int)$result->count + 0;
    }

    /**
     * @param array|string $columns
     * @return \Phalcon\Mvc\Model\Criteria|self
     */
    public function columns($columns)
    {
        return parent::columns($columns); // TODO: Change the autogenerated stub
    }

    public function setParams($params)
    {
        $this->_params = $params;
        return $this;
    }

    /**
     * Executes a find using the parameters built with the criteria and return first result
     *
     * @param bool $excep_not_found
     * @param bool $excep_deleted
     * @param null $msg
     * @param null|ModelBase $class
     * @return callable|ModelBase
     * @throws \Fwok\Exceptions\InputException
     */
    public function executeFirst($excep_not_found = false, $excep_deleted = true, $msg = null, $class = null)
    {
        $model = $this->execute()->getFirst();

        if($class) {
            $class::throwModelCheck($model, $excep_not_found, $excep_deleted, $msg);
        }else{
            ModelBase::throwModelCheck($model, $excep_not_found, $excep_deleted, $msg);
        }

        return $model;
    }
}