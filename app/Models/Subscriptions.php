<?php

namespace Fwok\Models;

use Fwok\Exceptions\Exception;
use Fwok\Exceptions\SystemException;
use Fwok\Plugins\Logger;

class Subscriptions extends ModelBase
{

    const OBJECT_NAME = 'subscriptions';
    const ICON = 'fa fa-dolar fa-fw';

    const BILLING_PERIOD_MONTH = 'Month';
    const BILLING_PERIOD_DAY = 'Day';
    const BILLING_PERIOD_YEAR = 'Year';

    const TYPE_FREE = 'free';
    const TYPE_FULL = 'full';
    const TYPE_CUSTOM = 'custom';
    const TYPE_NORMAL = 'normal';

    public function initialize()
    {
        $this->hasOne(
            'id',
            'Fwok\Models\UserSubscriptions',
            'subscription_id',
            [
                'alias' => 'UserSubscriptions'
            ]
        );

        $this->hasMany(
            'id',
            'Fwok\Models\SubscriptionsFeatures',
            'subscription_id',
            [
                'alias' => 'Features'
            ]
        );
        $this->setSource('fwok_subscriptions');
    }

    /**
     * Return the related "Features"
     *
     * @param array|string $parameters
     * @param int $exception_if_not_found
     * @throws \Fwok\Exceptions\Exception
     * @return \Fwok\Models\SubscriptionsFeatures[]
     */
    public function getFeatures($parameters = null, $exception_if_not_found = 0)
    {
        if (!$parameters) {
            $parameters = [ "order" => "priority" ];
        }

        return $this->getRelated('Features', $parameters, $exception_if_not_found);
    }

    /**
     * Return first free subscription
     *
     * @throws Exception
     * @return $this
     */
    public static function findFirstFree()
    {
        return self::findFirstById(
            Config::findFirstByKey(Config::KEY_FREE_SUBSCRIPTION_ID, true)->getValue()
        );
    }

    /**
     * Return initial subscription
     *
     * @return $this
     * @throws SystemException
     */
    public static function findFirstInitial()
    {
        return self::findFirstById(
            Config::findFirstByKey(Config::KEY_INITIAL_SUBSCRIPTION_ID, true)->getValue()
        );
    }

    /**
     * Return first full subscription
     *
     * @throws Exception
     * @return $this
     */
    public static function findFirstFull()
    {
// TODO verificar pq existe esse método
        $subscriptions = self::find(
            [
                'type = :type: and active = :active:',
                'bind' => [
                    'type'   => \Fwok\Models\Subscriptions::TYPE_FULL,
                    'active' => 1
                ]
            ]
        );

        if ($subscriptions->count() > 1) {
            Logger::shoutInAlert(
                'There are more than one full plans. Selecting first one to attech to user',
                Logger::WARNING
            );
        } elseif (!$subscriptions) {
            throw new SystemException(SystemException::DATABASE_CONSISTENCY, null, 'No full subscription plan found');
        }

        return $subscriptions->getFirst();
    }

    /**
     * @param $code
     * @return self
     */
    public static function findFirstByCode($code)
    {
        return parent::findFirstByCode($code);
    }
    public function export($opt = [ ])
    {
        $ret = [
            'object'             => self::OBJECT_NAME,
            'id'                 => $this->getId(),
            'name'               => $this->getName(),
            'description'        => $this->getDescription(),
            'currency_code'      => $this->getCurrencyCode(),
            'code'               => $this->getCode(),
            'cost'               => $this->getCost(),
            'type'               => $this->getType(),
            'active'             => $this->getActive(),
            'billing_period'     => $this->getBillingPeriod(),
            'billing_frequency'  => $this->getBillingFrequency(),
            'highlight'          => $this->getHighlight(),
            'allow_legacy_renew' => $this->getAllowLegacyRenew(),
            'courtesy'           => $this->getCourtesy(),
            'created_at'         => $this->getCreatedAt(),
            'updated_at'         => $this->getUpdatedAt()
        ];

        foreach ($this->getFeatures() as $feature) {
            $ret['features'][] = $feature->export($opt);
        }

        return $ret;
    }

    /**
     *
     * @var string
     */
    protected $name;

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     *
     * @var integer
     */
    protected $cost;

    /**
     * Method to set the value of field cost
     *
     * @param integer $cost
     * @return $this
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Returns the value of field cost
     *
     * @return integer
     */
    public function getCost()
    {
        return $this->cost + 0;
    }

    /**
     *
     * @var bool
     */
    protected $active;

    /**
     * Method to set the value of field active
     *
     * @param bool $active
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Returns the value of field active
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     *
     * @var string
     */
    protected $description;

    /**
     * Method to set the value of field description
     *
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Returns the value of field description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     *
     * @var string
     */
    protected $currency_code;

    /**
     * Method to set the value of field currency_code
     *
     * @param string $currency_code
     * @return $this
     */
    public function setCurrencyCode($currency_code)
    {
        $this->currency_code = $currency_code;

        return $this;
    }

    /**
     * Returns the value of field currency_code
     *
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currency_code;
    }

    /**
     *
     * @var string
     */
    protected $billing_period;

    /**
     * Method to set the value of field billing_period
     *
     * @param string $billing_period
     * @return $this
     */
    public function setBillingPeriod($billing_period)
    {
        $this->billing_period = $billing_period;

        return $this;
    }

    /**
     * Returns the value of field billing_period
     *
     * @param string $frequentzation
     * @return string
     */
    public function getBillingPeriod($frequentzation = '')
    {
        if ($frequentzation) {
            $frequentzation = 'ly';
        }

        return $this->billing_period . $frequentzation;
    }

    /**
     *
     * @var integer
     */
    protected $billing_frequency;

    /**
     * Method to set the value of field billing_frequency
     *
     * @param integer $billing_frequency
     * @return $this
     */
    public function setBillingFrequency($billing_frequency)
    {
        $this->billing_frequency = $billing_frequency;

        return $this;
    }

    /**
     * Returns the value of field billing_frequency
     *
     * @return integer
     */
    public function getBillingFrequency()
    {
        return $this->billing_frequency;
    }

    public function getButtons()
    {
        ?>
        <div class="btn-group btn-group-xs">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                    aria-expanded="false">
                <i class="fa fa-cog fa-fw"></i> <span class="caret"></span>
            </button>
            <ul class="dropdown-menu pull-right" role="menu">
                <li><a href="/subscriptions/edit/<?php echo $this->getId() ?>">Edit</a></li>
            </ul>
        </div>
        <?php
    }

    /**
     *
     * @var integer
     */
    protected $highlight;

    /**
     * Method to set the value of field highlight
     *
     * @param integer $highlight
     * @return $this
     */
    public function setHighlight($highlight)
    {
        $this->highlight = $highlight;

        return $this;
    }

    /**
     * Returns the value of field highlight
     *
     * @return integer
     */
    public function getHighlight()
    {
        return $this->highlight;
    }

    /**
     *
     * @var string
     */
    protected $type;

    /**
     * Method to set the value of field type
     *
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Returns the value of field type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     *
     * @var integer
     */
    protected $courtesy;

    /**
     * Method to set the value of field courtesy
     *
     * @param integer $courtesy
     * @return $this
     */
    public function setCourtesy($courtesy)
    {
        $this->courtesy = $courtesy;

        return $this;
    }

    /**
     * Returns the value of field courtesy
     *
     * @return integer
     */
    public function getCourtesy()
    {
        return (int) $this->courtesy;
    }

    /**
     *
     * @var integer
     */
    protected $allow_legacy_renew;

    /**
     * Method to set the value of field allow_legacy_renew
     *
     * @param integer $allow_legacy_renew
     * @return $this
     */
    public function setAllowLegacyRenew($allow_legacy_renew)
    {
        $this->allow_legacy_renew = $allow_legacy_renew;

        return $this;
    }

    /**
     * Returns the value of field allow_legacy_renew
     *
     * @return integer
     */
    public function getAllowLegacyRenew()
    {
        return (int) $this->allow_legacy_renew;
    }

    /**
     *
     * @var string
     */
    protected $final_date_pagseguro;

    /**
     * Method to set the value of field final_date_pagseguro
     *
     * @param string $final_date_pagseguro
     * @return $this
     */
    public function setFinalDatePagseguro($final_date_pagseguro)
    {
        $this->final_date_pagseguro = $final_date_pagseguro;

        return $this;
    }

    /**
     * Returns the value of field final_date_pagseguro
     *
     * @return string
     */
    public function getFinalDatePagseguro()
    {
        return $this->final_date_pagseguro;
    }

    /**
     *
     * @var string
     */
    protected $code;

    /**
     * Method to set the value of field code
     *
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Returns the value of field code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
}