<?php

namespace Fwok\Models;

class RequestsCurl extends ModelBase
{
    const OBJECT_NAME = 'curl-requests';

    const SERVER_PAYPAL = 'paypal';
    const SERVER_PAGSEGURO = 'pagseguro';
    const SERVER_FACEBOOK = 'facebook';
    const SERVER_GOOGLE = 'google';
    const SERVER_GCM = 'gcm';
    const SERVER_PREMAILER_DIALECT = 'premailer dialect';

    public function initialize()
    {
        $this->setSource('fwok_requests_curl');

        $this->hasOne(
            'user_id',
            '\Fwok\Models\Users',
            'id',
            [
                'alias'    => 'User',
                'reusable' => true,
            ]
        );
    }

    public function export($opt = [ ])
    {
        $return = array_merge(
            parent::export(),
            [
                'object' => self::OBJECT_NAME,
                'user_id' => $this->getUserId(),
                'url' => $this->getUrl(),
                'post_data' => $this->getPostData(),
                'response' => $this->getResponse(),
                'server' => $this->getServer(),
            ]
        );

        return $return;
    }

    /**
     * Return the related "User"
     *
     * @param array|string $parameters
     * @return \Fwok\Models\Users
     */
    public function getUser($parameters = null)
    {
        return $this->getRelated('User', $parameters);
    }

    /**
     *
     * @var integer
     */
    protected $user_id;

    /**
     * Method to set the value of field user_id
     *
     * @param integer $user_id
     * @return $this
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Returns the value of field user_id
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     *
     * @var string
     */
    protected $url;

    /**
     * Method to set the value of field url
     *
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Returns the value of field url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     *
     * @var string
     */
    protected $post_data;

    /**
     * Method to set the value of field post_data
     *
     * @param string $post_data
     * @return $this
     */
    public function setPostData($post_data)
    {
        $this->post_data = $post_data;

        return $this;
    }

    /**
     * Returns the value of field post_data
     *
     * @return string
     */
    public function getPostData()
    {
        return $this->post_data;
    }

    /**
     *
     * @var string
     */
    protected $response;

    /**
     * Method to set the value of field response
     *
     * @param string $response
     * @return $this
     */
    public function setResponse($response)
    {
        if (is_object($response) or is_array($response)) {
            $this->response = json_encode($response);
        } elseif (json_decode($response)) {
            $this->response = $response;
        } else {
            $this->response = json_encode($response);
        }

        return $this;
    }

    /**
     * Returns the value of field response
     *
     * @return string
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     *
     * @var string
     */
    protected $server;

    /**
     * Method to set the value of field server
     *
     * @param string $server
     * @return $this
     */
    public function setServer($server)
    {
        $this->server = $server;

        return $this;
    }

    /**
     * Returns the value of field server
     *
     * @return string
     */
    public function getServer()
    {
        return $this->server;
    }
}