<?php

namespace Fwok\Models;

class Discounts extends ModelBase
{
    const OBJECT_NAME = 'discounts';

    public function initialize()
    {
        $this->hasMany(
            'id',
            '\Fwok\Models\AccountsSubscriptions',
            'discount_id',
            [
                'alias'    => 'AccountsSubscriptions',
                'reusable' => true
            ]
        );
        $this->hasMany(
            'id',
            '\Fwok\Models\Payments',
            'discount_id',
            [
                'alias'    => 'Payments',
                'reusable' => true
            ]
        );
        $this->setSource('fwok_discounts');
    }

    /**
     * Return the related "AccountsSubscriptions"
     *
     * @param array|string $parameters
     * @return \Fwok\Models\AccountsSubscriptions[]
     */
    public function getAccountsSubscriptions($parameters = null)
    {
        return $this->getRelated('AccountsSubscriptions', $parameters);
    }

    /**
     * Return the related "Payments"
     *
     * @param array|string $parameters
     * @return \Fwok\Models\Payments[]
     */
    public function getPayments($parameters = null)
    {
        return $this->getRelated('Payments', $parameters);
    }

    public function export($opt = [ ])
    {
        $ret = [
            'object'      => self::OBJECT_NAME,
            'id'          => $this->getId(),
            'code'        => $this->getCode(),
            'description' => $this->getDescription(),
            'deleted'     => $this->getDeleted(),
            'percentage'  => $this->getPercentage(),
            'active'      => $this->getActive(),
            'created_at'  => $this->getCreatedAt(),
            'updated_at'  => $this->getUpdatedAt()
        ];

        return $ret;
    }


    /**
     *
     * @var string
     */
    protected $description;

    /**
     * Method to set the value of field description
     *
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Returns the value of field description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     *
     * @var bool
     */
    protected $active;

    /**
     * Method to set the value of field active
     *
     * @param bool $active
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Returns the value of field active
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     *
     * @var integer
     */
    protected $percentage;

    /**
     * Method to set the value of field percentage
     *
     * @param integer $percentage
     * @return $this
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;

        return $this;
    }

    /**
     * Returns the value of field percentage
     *
     * @return integer
     */
    public function getPercentage()
    {
        return $this->percentage + 0;
    }

    /**
     *
     * @var string
     */
    protected $code;

    /**
     * Method to set the value of field code
     *
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Returns the value of field code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    public function getButtons()
    {
        ?>
        <div class="btn-group btn-group-xs">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                    aria-expanded="false">
                <i class="fa fa-cog fa-fw"></i> <span class="caret"></span>
            </button>
            <ul class="dropdown-menu pull-right" role="menu">
                <li><a href="/discounts/edit/<?php echo $this->getId() ?>">Edit</a></li>
            </ul>
        </div>
        <?php
    }
}