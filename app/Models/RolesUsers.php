<?php

namespace Fwok\Models;

use Fwok\Application\App;
use Fwok\Application\ApplicationBase;
use Fwok\Plugins\Logger;
use Phalcon\Mvc\Model\Validator\Uniqueness;
use Phalcon\Validation;

class RolesUsers extends ModelBase
{
    const OBJECT_NAME = 'rolesusers';

    public function initialize()
    {
        $this->setSource('fwok_roles_users');

        $this->belongsTo(
            'role_id',
            '\\Fwok\\Models\\Roles',
            'id',
            ['alias' => 'Role']
        );
        $this->belongsTo(
            'user_id',
            '\\Fwok\\Models\\Users',
            'id',
            ['alias' => 'User']
        );

        $this->keepSnapshots(true);
        $this->addBehavior(new \Fwok\Models\Behaviors\Auditable());
    }
    
    /**
     * Return the related "User"
     *
     * @param array|string $parameters
     * @param int $exception_if_not_found
     * @throws \Fwok\Exceptions\Exception
     * @return \Fwok\Models\Users
     */
    public function getUser($parameters = null, $exception_if_not_found = 0)
    {
        return $this->getRelated('User', $parameters, $exception_if_not_found);
    }
    
    public function validation()
    {

        $validator = new Validation();

        if (!$this->getRoleId()) {
            $validator->appendMessage(App::t('INVALID_ROLE'));
        }
        if (!$this->getUserId()) {
            $validator->appendMessage(App::t('INVALID_USER'));
        }
        
        $validator->add([ 'role_id', 'user_id', 'business_id', 'deleted' ], new Validation\Validator\Uniqueness([
                'message' => App::getTranslatedKey('USER_ALREADY_HAS_THIS_ROLE'),
            ])
        );
        
        return $this->validate($validator);
    }


    /**
     * @param Users $user
     * @param Roles $role
     * @param bool $search_in_business
     * @return $this
     * @throws \Fwok\Exceptions\SystemException
     */
    public static function findFirstByUserIdAndRoleId(Users $user, Roles $role)
    {
        $roleUser = false;
        
        if($user->getBusinessId()) {
            $roleUser = RolesUsers::query()
                ->searchInBusiness()
                ->addWhereBinded('user_id = ', $user->getId())
                ->addWhereBinded('role_id = ', $role->getId())
                ->executeFirst();
        }elseif($role->getIsPublic()){
            $roleUser = RolesUsers::query()
                ->addWhereBinded('user_id = ', $user->getId())
                ->addWhereBinded('role_id = ', $role->getId())
                ->executeFirst();
        }
        
        return $roleUser;
    }

    /**
     *
     * @var integer
     * @SWG\Property(name="role_id",type="integer",description="")
     */
    protected $role_id;

    /**
     * Method to set the value of field role_id
     *
     * @param integer $role_id
     * @return $this
     */
    public function setRoleId($role_id)
    {
        $this->role_id = $role_id;

        return $this;
    }

    /**
     * Returns the value of field role_id
     *
     * @return integer
     */
    public function getRoleId()
    {
        return (int)$this->role_id;
    }

    /**
     *
     * @var integer
     * @SWG\Property(name="user_id",type="integer",description="")
     */
    protected $user_id;

    /**
     * Method to set the value of field user_id
     *
     * @param integer $user_id
     * @return $this
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Returns the value of field user_id
     *
     * @return integer
     */
    public function getUserId()
    {
        return (int)$this->user_id;
    }

    /**
     *
     * @var integer
     * @SWG\Property(name="business_id",type="integer",description="")
     */
    protected $business_id;

    /**
     * Method to set the value of field business_id
     *
     * @param integer $business_id
     * @return $this
     */
    public function setBusinessId($business_id)
    {
        $this->business_id = $business_id;

        return $this;
    }

    /**
     * Returns the value of field business_id
     *
     * @return integer
     */
    public function getBusinessId()
    {
        return (int)$this->business_id;
    }
}