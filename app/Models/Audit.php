<?php

namespace Fwok\Models;

class Audit extends ModelBase
{

    public function initialize()
    {
        $this->hasMany('id', '\\Fwok\\Models\\AuditDetail', 'audit_id', [ 'alias' => 'Details' ]);
        $this->setSource('fwok_audit');
    }

    /**
     *
     * @var integer
     */
    protected $user_id;

    /**
     * Method to set the value of field user_id
     *
     * @param integer $user_id
     * @return $this
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Returns the value of field user_id
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     *
     * @var integer
     */
    protected $model_id;

    /**
     * Method to set the value of field model_id
     *
     * @param integer $model_id
     * @return $this
     */
    public function setModelId($model_id)
    {
        $this->model_id = $model_id;

        return $this;
    }

    /**
     * Returns the value of field model_id
     *
     * @return integer
     */
    public function getModelId()
    {
        return $this->model_id;
    }

    /**
     *
     * @var string
     */
    protected $model_name;

    /**
     * Method to set the value of field model_name
     *
     * @param string $model_name
     * @return $this
     */
    public function setModelName($model_name)
    {
        $this->model_name = $model_name;

        return $this;
    }

    /**
     * Returns the value of field model_name
     *
     * @return string
     */
    public function getModelName()
    {
        return $this->model_name;
    }

    /**
     *
     * @var string
     */
    protected $ipaddress;

    /**
     * Method to set the value of field ipaddress
     *
     * @param string $ipaddress
     * @return $this
     */
    public function setIpaddress($ipaddress)
    {
        $this->ipaddress = $ipaddress;

        return $this;
    }

    /**
     * Returns the value of field ipaddress
     *
     * @return string
     */
    public function getIpaddress()
    {
        return $this->ipaddress;
    }

    /**
     *
     * @var string
     */
    protected $type;

    /**
     * Method to set the value of field type
     *
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Returns the value of field type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

}