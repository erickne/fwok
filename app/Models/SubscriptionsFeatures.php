<?php

namespace Fwok\Models;

class SubscriptionsFeatures extends ModelBase
{

    const OBJECT_NAME = 'subscriptionsfeatures';
    const ICON = 'fa fa-dolar fa-fw';

    public function initialize()
    {
        $this->hasOne(
            'subscription_id',
            'Fwok\Models\Subscriptions',
            'id',
            [
                'alias' => 'Subscription'
            ]
        );
        $this->setSource('fwok_subscriptions_features');
    }

    public function export($opt = [ ])
    {
        $ret = [
            'object'      => self::OBJECT_NAME,
            'id'          => $this->getId(),
            'key'         => $this->getKey(),
            'value'       => $this->getValue(),
            'description' => $this->getDescription(),
            'icon'        => $this->getIcon(),
            'highlight'   => $this->getHighlight(),
            'show'        => $this->getShow(),
            'value_label' => $this->getValueLabel(),
            'priority'    => $this->getPriority(),
            'deleted'     => $this->getDeleted(),
            'created_at'  => $this->getCreatedAt(),
            'updated_at'  => $this->getUpdatedAt()
        ];

        return $ret;
    }

    /**
     *
     * @var integer
     */
    protected $subscription_id;

    /**
     * Method to set the value of field subscription_id
     *
     * @param integer $subscription_id
     * @return $this
     */
    public function setSubscriptionId($subscription_id)
    {
        $this->subscription_id = $subscription_id;

        return $this;
    }

    /**
     * Returns the value of field subscription_id
     *
     * @return integer
     */
    public function getSubscriptionId()
    {
        return $this->subscription_id;
    }

    /**
     *
     * @var string
     */
    protected $key;

    /**
     * Method to set the value of field key
     *
     * @param string $key
     * @return $this
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Returns the value of field key
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     *
     * @var string
     */
    protected $value;

    /**
     * Method to set the value of field value
     *
     * @param string $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Returns the value of field value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     *
     * @var string
     */
    protected $description;

    /**
     * Method to set the value of field description
     *
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Returns the value of field description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     *
     * @var string
     */
    protected $icon;

    /**
     * Method to set the value of field icon
     *
     * @param string $icon
     * @return $this
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Returns the value of field icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     *
     * @var string
     */
    protected $highlight;

    /**
     * Method to set the value of field highlight
     *
     * @param string $highlight
     * @return $this
     */
    public function setHighlight($highlight)
    {
        $this->highlight = $highlight;

        return $this;
    }

    /**
     * Returns the value of field highlight
     *
     * @return string
     */
    public function getHighlight()
    {
        return $this->highlight;
    }

    /**
     *
     * @var integer
     */
    protected $priority;

    /**
     * Method to set the value of field priority
     *
     * @param integer $priority
     * @return $this
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Returns the value of field priority
     *
     * @return integer
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     *
     * @var integer
     */
    protected $show;

    /**
     * Method to set the value of field show
     *
     * @param integer $show
     * @return $this
     */
    public function setShow($show)
    {
        $this->show = $show;

        return $this;
    }

    /**
     * Returns the value of field show
     *
     * @return integer
     */
    public function getShow()
    {
        return $this->show + 0;
    }

    /**
     *
     * @var string
     */
    protected $value_label;

    /**
     * Method to set the value of field value_label
     *
     * @param string $value_label
     * @return $this
     */
    public function setValueLabel($value_label)
    {
        $this->value_label = $value_label;

        return $this;
    }

    /**
     * Returns the value of field value_label
     *
     * @return string
     */
    public function getValueLabel()
    {
        if ($this->value_label) {
            return $this->value_label;
        }

        return $this->value;
    }
}