<?php
namespace Fwok\Models;

class Exception extends ModelBase
{
    const OBJECT_NAME = 'exception';

    public function export($o = [])
    {
        $return = [
            'id' => $this->getId(),
            'object' => self::OBJECT_NAME,
            'title' => $this->getTitle(),
            'dev_message' => $this->getDevMessage(),
            'code_message' => $this->getCodeMessage(),
            'message' => $this->getMessage(),
            'http_response_message' => $this->getHttpResponseMessage(),
            'validation_messages' => $this->getValidationMessages(),
            'previous_exception' => $this->getPreviousException(),
            'payload' => $this->getPayload(),
            'trace_id' => $this->getTraceId(),
            'http_response_code' => $this->getHttpResponseCode(),
            'code' => $this->getCode(),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt(),
        ];

        return $return;
    }

    public function initialize()
    {
        $this->setSource('fwok_exception');
    }

    /**
     *
     * @var string
     * @SWG\Property(name="title",type="string",description="")
     */
    protected $title;

    /**
     * Method to set the value of field title
     *
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Returns the value of field title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="dev_message",type="string",description="")
     */
    protected $dev_message;

    /**
     * Method to set the value of field dev_message
     *
     * @param string $dev_message
     * @return $this
     */
    public function setDevMessage($dev_message)
    {
        $this->dev_message = $dev_message;

        return $this;
    }

    /**
     * Returns the value of field dev_message
     *
     * @return string
     */
    public function getDevMessage()
    {
        return $this->dev_message;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="code_message",type="string",description="")
     */
    protected $code_message;

    /**
     * Method to set the value of field code_message
     *
     * @param string $code_message
     * @return $this
     */
    public function setCodeMessage($code_message)
    {
        $this->code_message = $code_message;

        return $this;
    }

    /**
     * Returns the value of field code_message
     *
     * @return string
     */
    public function getCodeMessage()
    {
        return $this->code_message;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="http_response_message",type="string",description="")
     */
    protected $http_response_message;

    /**
     * Method to set the value of field http_response_message
     *
     * @param string $http_response_message
     * @return $this
     */
    public function setHttpResponseMessage($http_response_message)
    {
        $this->http_response_message = $http_response_message;

        return $this;
    }

    /**
     * Returns the value of field http_response_message
     *
     * @return string
     */
    public function getHttpResponseMessage()
    {
        return $this->http_response_message;
    }

    /**
     *
     * @var \stdClass
     * @SWG\Property(name="validation_messages",type="array",description="")
     */
    protected $validation_messages;

    /**
     * Method to set the value of field validation_messages
     *
     * @param string $validation_messages
     * @return $this
     */
    public function setValidationMessages($validation_messages)
    {
        $this->validation_messages = ($validation_messages === null) ? null : json_encode($validation_messages);

        return $this;
    }

    /**
     * Returns the value of field validation_messages
     *
     * @return \stdClass
     */
    public function getValidationMessages()
    {
        return json_decode($this->validation_messages);
    }

    /**
     *
     * @var \stdClass
     * @SWG\Property(name="previous_exception",type="array",description="")
     */
    protected $previous_exception;

    /**
     * Method to set the value of field previous_exception
     *
     * @param string $previous_exception
     * @return $this
     */
    public function setPreviousException($previous_exception)
    {
        $this->previous_exception = ($previous_exception === null) ? null : json_encode($previous_exception);

        return $this;
    }

    /**
     * Returns the value of field previous_exception
     *
     * @return \stdClass
     */
    public function getPreviousException()
    {
        return json_decode($this->previous_exception);
    }

    /**
     *
     * @var \stdClass
     * @SWG\Property(name="payload",type="array",description="")
     */
    protected $payload;

    /**
     * Method to set the value of field payload
     *
     * @param string $payload
     * @return $this
     */
    public function setPayload($payload)
    {
        $this->payload = ($payload === null) ? null : json_encode($payload);

        return $this;
    }

    /**
     * Returns the value of field payload
     *
     * @return \stdClass
     */
    public function getPayload()
    {
        return json_decode($this->payload);
    }

    /**
     *
     * @var integer
     * @SWG\Property(name="trace_id",type="integer",description="")
     */
    protected $trace_id;

    /**
     * Method to set the value of field trace_id
     *
     * @param integer $trace_id
     * @return $this
     */
    public function setTraceId($trace_id)
    {
        $this->trace_id = $trace_id;

        return $this;
    }

    /**
     * Returns the value of field trace_id
     *
     * @return integer
     */
    public function getTraceId()
    {
        return (int)$this->trace_id;
    }

    /**
     *
     * @var integer
     * @SWG\Property(name="http_response_code",type="integer",description="")
     */
    protected $http_response_code;

    /**
     * Method to set the value of field http_response_code
     *
     * @param integer $http_response_code
     * @return $this
     */
    public function setHttpResponseCode($http_response_code)
    {
        $this->http_response_code = $http_response_code;

        return $this;
    }

    /**
     * Returns the value of field http_response_code
     *
     * @return integer
     */
    public function getHttpResponseCode()
    {
        return (int)$this->http_response_code;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="message",type="string",description="")
     */
    protected $message;

    /**
     * Method to set the value of field message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Returns the value of field message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="code",type="string",description="")
     */
    protected $code;

    /**
     * Method to set the value of field code
     *
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Returns the value of field code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
}
