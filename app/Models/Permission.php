<?php

namespace Fwok\Models;

use Fwok\Application\App;
use Fwok\Application\ApplicationBase;
use Fwok\Exceptions\Exception;
use Fwok\Exceptions\InputException;
use Fwok\Plugins\GetFeature;

class Permission extends ModelBase
{
    const OBJECT_NAME = 'permission';
    const MODE_READ = 'read';
    const MODE_WRITE = 'write';

    const TYPE_SINGLE_OBJECT = 'single_object';
    const TYPE_ALL_OBJECTS = 'all_objects';

    public function initialize()
    {
        $this->setSource('fwok_permission');

        $this->hasOne('user_id', '\\Fwok\\Models\\Users', 'id', [ 'alias' => 'User', 'reusable' => true, ]);
        $this->hasOne('team_id', '\Fwok\Models\Teams', 'id', [ 'alias' => 'Team', 'reusable' => true, ]);
        $this->hasOne('business_id', '\Fwok\Models\Business', 'id', [ 'alias' => 'Business', 'reusable' => true, ]);
    }


    public function validation()
    {

        # Verifica se a assinatura permite alteração e criação de permissões
        if (!GetFeature::checkValidityInt(GetFeature::KEY_PERMISSION_CONTROL, 1, 0)) {
            $this->appendErrorMessage(
                "Sua assinatura atual não permite alterar as permissões. " .
                "Faça já um upgrade na sua assiantura e aproveite diversas novas funcionalidades!"
            );
        }

        if ($this->getUserId() and $this->getTeamId()) {
            $this->appendErrorMessage("Não é possível vincular um usuário e um time a uma permissão");
        }

        if (!$this->getObjectType()) {
            $this->appendErrorMessage('Objeto selecionado não é válido');
        }

        if ($this->getUserId()) {
            $user = Users::findFirstValidById($this->getUserId());
        }
        if ($this->getTeamId()) {
            $team = Teams::findFirstValidById($this->getTeamId());
        }

        /*
         * Inicialmente, procura permissão total no sistema
         * Se não encontrar, procura permissão na base de permissões
         */
        
        $auth = App::getUser()->hasRole(Roles::ADMIN);;

        if (!$auth) {
            $permission = Permission::findFirstByObjectAndId($this->getObjectType(), $this->getObjectId());
            if ($permission) {
                $auth = $permission->getCanWrite();
            }
        }
        # Dispara exception se não achar nenhuma permissão
        if (!$auth) {
            throw new InputException(InputException::PERMISSION_INVALID);
        }

        return !$this->validationHasFailed();
    }


    public function export($opt = [ ])
    {
        $return = parent::export($opt);
        $return['object'] = self::OBJECT_NAME;
        $return['object_type'] = $this->getObjectType();
        $return['object_id'] = $this->getObjectId();
        $return['can_read'] = $this->getCanRead();
        $return['can_write'] = $this->getCanWrite();
        $return['type'] = $this->getType();

        if ($this->getUser()) {
            $return['user'] = $this->getUser()->export();
        }
        if ($this->getTeam()) {
            $return['team'] = $this->getTeam()->export();
        }

        return $return;
    }

    /**
     * Return the related "User"
     *
     * @param array|string $parameters
     * @return \Fwok\Models\Users
     */
    public function getUser($parameters = null)
    {
        return $this->getRelated('User', $parameters);
    }

    /**
     * Return the related "Team"
     *
     * @param array|string $parameters
     * @return \Fwok\Models\Teams
     */
    public function getTeam($parameters = null)
    {
        return $this->getRelated('Team', $parameters);
    }

    /**
     * Return the related "Business"
     *
     * @param array|string $parameters
     * @return \Fwok\Models\Business
     */
    public function getBusiness($parameters = null)
    {
        return $this->getRelated('Business', $parameters);
    }

    /**
     * @param $user_id
     * @param int $deleted
     * @return \Fwok\Models\Permission[]
     */
    public static function findAllByUserId($user_id, $deleted = 0)
    {
        $user = Users::findFirstById($user_id);
        $permissions = Permission::find(
            [
                'user_id = :user_id: and business_id = :business_id: and deleted = :deleted:',
                'bind' => [
                    'user_id'     => $user->getId(),
                    'deleted'     => $deleted,
                    'business_id' => ApplicationBase::getBusiness()->getId()
                ]
            ]
        );

        return $permissions;
    }

    /**
     * @param $team_id
     * @param int $deleted
     * @throws Exception
     * @internal param $user_id
     * @return \Fwok\Models\Permission[]
     */
    public static function findAllByTeamId($team_id, $deleted = 0)
    {
        $team = Teams::findFirstById($team_id);
        $permissions = Permission::find(
            [
                'team_id = :team_id: and business_id = :business_id: and deleted = :deleted:',
                'bind' => [
                    'team_id'     => $team->getId(),
                    'deleted'     => $deleted,
                    'business_id' => ApplicationBase::getBusiness()->getId()
                ]
            ]
        );

        return $permissions;
    }

    /**
     * @param null $object_type
     * @param null $object_id
     * @param int $deleted
     * @param null $user_id
     * @param null $team_id
     * @return $this|Permission
     * @throws \Fwok\Exceptions\SystemException
     */
    public static function findFirstByObjectAndId(
        $object_type = null,
        $object_id = null,
        $deleted = 0,
        $user_id = null,
        $team_id = null
    ) {
        $b_user = [ ];
        $b_object_type = [ ];
        $b_object_id = [ ];
        $q_user = '';
        $q_object_type = '';
        //$q_object_id = '';

        if ($user_id) {
            $q_user = ' and user_id = :user_id:';
            $b_user = [ 'user_id' => $user_id ];
        }
        if ($object_type) {
            $q_object_type = ' and object_type = :object_type:';
            $b_object_type = [ 'object_type' => $object_type ];
        }
        if ($object_id) {
            $q_object_id = ' and object_id = :object_id:';
            $b_object_id = [ 'object_id' => $object_id ];
        } else {
            $q_object_id = ' and object_id IS NULL or object_id = \'\'';
        }

        $bind = array_filter(array_merge($b_user, $b_object_type, $b_object_id));
        $bind['deleted'] = $deleted;

        $query = 'deleted = :deleted:' . $q_user . $q_object_type . $q_object_id;

        $permission = Permission::findFirst(
            [
                $query,
                'bind' => $bind
            ]
        );

        if (!$permission) {
            return new Permission();
        } else {
            return $permission;
        }
    }

    public static function findFirstByObjectAndIdAndUser($object_type, $object_id, $user_id, $deleted = 0)
    {
        $permission = Permission::findFirst(
            [
                'object_type = :object_type: ' .
                'and object_id = :object_id: ' .
                'and user_id = :user_id: ' .
                'and (deleted = 0 or deleted = :deleted:)',
                'bind' => [
                    'object_type' => $object_type,
                    'object_id'   => $object_id,
                    'user_id'     => $user_id,
                    'deleted'     => $deleted
                ]
            ]
        );

        return $permission;
    }

    /**
     *
     * @var integer
     */
    protected $user_id;

    /**
     * Method to set the value of field user_id
     *
     * @param integer $user_id
     * @return $this
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Returns the value of field user_id
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     *
     * @var integer
     */
    protected $team_id;

    /**
     * Method to set the value of field team_id
     *
     * @param integer $team_id
     * @return $this
     */
    public function setTeamId($team_id)
    {
        $this->team_id = $team_id;

        return $this;
    }

    /**
     * Returns the value of field team_id
     *
     * @return integer
     */
    public function getTeamId()
    {
        return $this->team_id;
    }

    /**
     *
     * @var integer
     */
    protected $can_read;

    /**
     * Method to set the value of field can_read
     *
     * @param integer $can_read
     * @return $this
     */
    public function setCanRead($can_read)
    {
        $this->can_read = $can_read;

        return $this;
    }

    /**
     * Returns the value of field can_read
     *
     * @return integer
     */
    public function getCanRead()
    {
        return $this->can_read + 0;
    }

    /**
     *
     * @var integer
     */
    protected $can_write;

    /**
     * Method to set the value of field can_write
     *
     * @param integer $can_write
     * @return $this
     */
    public function setCanWrite($can_write)
    {
        $this->can_write = $can_write;
        if ($can_write) {
            $this->can_read = $can_write;
        }

        return $this;
    }

    /**
     * Returns the value of field can_write
     *
     * @return integer
     */
    public function getCanWrite()
    {
        return $this->can_write + 0;
    }

    /**
     *
     * @var string
     */
    protected $object_type;

    /**
     * Method to set the value of field object_type
     *
     * @param string $object_type
     * @return $this
     */
    public function setObjectType($object_type)
    {
        $this->object_type = strtolower($object_type);

        return $this;
    }

    /**
     * Returns the value of field object_type
     *
     * @return string
     */
    public function getObjectType()
    {
        return $this->object_type;
    }

    /**
     *
     * @var integer
     */
    protected $object_id;

    /**
     * Method to set the value of field object_id
     *
     * @param integer $object_id
     * @return $this
     */
    public function setObjectId($object_id)
    {
        $this->object_id = $object_id;

        return $this;
    }

    /**
     * Returns the value of field object_id
     *
     * @return integer
     */
    public function getObjectId()
    {
        return (int) $this->object_id;
    }

    /**
     *
     * @var integer
     */
    protected $business_id;

    /**
     * Method to set the value of field business_id
     *
     * @param integer $business_id
     * @return $this
     */
    public function setBusinessId($business_id)
    {
        $this->business_id = $business_id;

        return $this;
    }

    /**
     * Returns the value of field business_id
     *
     * @return integer
     */
    public function getBusinessId()
    {
        return $this->business_id;
    }

    /**
     *
     * @var string
     */
    protected $type;

    /**
     * Method to set the value of field type
     *
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Returns the value of field type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
