<?php
namespace Fwok\Models;

use Fwok\Models\Validators\UserValidator;
use Phalcon\Mvc\Model;

class UsersCardTokens extends ModelBase
{
    const OBJECT_NAME = 'userscardtokens';

    const METHOD_CC = 'CREDIT_CARD';
    const GATEWAY_IUGU = 'IUGU';

    public function initialize()
    {
        $this->setSource('fwok_users_card_tokens');
        $this->hasOne('user_id', '\Fwok\Models\Users', 'id', ['alias' => 'User', 'reusable' => true]);
        $this->addBehavior(new Behaviors\SoftDelete(['field' => 'deleted', 'value' => 1]));
    }

    /**
     * Return the related "User"
     *
     * @param array|string $parameters
     * @param int $exception_if_not_found
     * @throws \Fwok\Exceptions\Exception
     * @return \Fwok\Models\Subscriptions
     */
    public function getUser($parameters = null, $exception_if_not_found = 0)
    {
        return $this->getRelated('User', $parameters, $exception_if_not_found);
    }

    public function validation()
    {
        if (!$this->getBrand()) {
            $this->appendErrorMessageTranslated('INVALID_CC_BRAND');
        }
        if (!$this->getGateway()) {
            $this->appendErrorMessageTranslated('INVALID_CC_GATEWAY');
        }
        if (!$this->getLastDigits()) {
            $this->appendErrorMessageTranslated('INVALID_CC_LAST_DIGITS');
        }
        if (!$this->getMethod()) {
            $this->appendErrorMessageTranslated('INVALID_CC_METHOD');
        }
        if (!$this->getToken()) {
            $this->appendErrorMessageTranslated('INVALID_CC_TOKEN');
        }
        $this->validate(new UserValidator([
            'id' => $this->getUserId(),
            'required' => 1,
            'acceptDeleted' => 1
        ]));

        return !$this->validationHasFailed();
    }

    public function export($o = [])
    {
        return [
            'object' => self::OBJECT_NAME,
            'id' => $this->getId(),
            'brand' => $this->getBrand(),
            'deleted' => $this->getDeleted(),
            'description' => $this->getDescription(),
            'gateway' => $this->getGateway(),
            'last_digits' => $this->getLastDigits(),
            'method' => $this->getMethod(),
            //'token' => $this->getToken(),
            'holder_name' => $this->getHolderName(),
            'year' => $this->getYear(),
            'month' => $this->getMonth(),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt(),
        ];
    }

    /**
     *
     * @var integer
     * @SWG\Property(name="user_id",type="integer",description="")
     */
    protected $user_id;

    /**
     * Method to set the value of field user_id
     *
     * @param integer $user_id
     * @return $this
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Returns the value of field user_id
     *
     * @return integer
     */
    public function getUserId()
    {
        return (int)$this->user_id;
    }

    /**
     *
     * @var integer
     * @SWG\Property(name="last_digits",type="integer",description="")
     */
    protected $last_digits;

    /**
     * Method to set the value of field last_digits
     *
     * @param integer $last_digits
     * @return $this
     */
    public function setLastDigits($last_digits)
    {
        $this->last_digits = $last_digits;

        return $this;
    }

    /**
     * Returns the value of field last_digits
     *
     * @return integer
     */
    public function getLastDigits()
    {
        return (int)$this->last_digits;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="token",type="string",description="")
     */
    protected $token;

    /**
     * Method to set the value of field token
     *
     * @param string $token
     * @return $this
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Returns the value of field token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="method",type="string",description="")
     */
    protected $method;

    /**
     * Method to set the value of field method
     *
     * @param string $method
     * @return $this
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Returns the value of field method
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="brand",type="string",description="")
     */
    protected $brand;

    /**
     * Method to set the value of field brand
     *
     * @param string $brand
     * @return $this
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Returns the value of field brand
     *
     * @return string
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="gateway",type="string",description="")
     */
    protected $gateway;

    /**
     * Method to set the value of field gateway
     *
     * @param string $gateway
     * @return $this
     */
    public function setGateway($gateway)
    {
        $this->gateway = $gateway;

        return $this;
    }

    /**
     * Returns the value of field gateway
     *
     * @return string
     */
    public function getGateway()
    {
        return $this->gateway;
    }
    /**
     *
     * @var integer
     * @SWG\Property(name="month",type="integer",description="")
     */
    protected $month;

    /**
     * Method to set the value of field month
     *
     * @param integer $month
     * @return $this
     */
    public function setMonth($month)
    {
        $this->month = $month;

        return $this;
    }

    /**
     * Returns the value of field month
     *
     * @return integer
     */
    public function getMonth()
    {
        return (int) $this->month;
    }
    /**
     *
     * @var integer
     * @SWG\Property(name="year",type="integer",description="")
     */
    protected $year;

    /**
     * Method to set the value of field year
     *
     * @param integer $year
     * @return $this
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Returns the value of field year
     *
     * @return integer
     */
    public function getYear()
    {
        return (int) $this->year;
    }
    
    /**
     *
     * @var string
     * @SWG\Property(name="description",type="string",description="")
     */
    protected $description;

    /**
     * Method to set the value of field description
     *
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Returns the value of field description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     *
     * @var string
     * @SWG\Property(name="holder_name",type="string",description="")
     */
    protected $holder_name;

    /**
     * Method to set the value of field holder_name
     *
     * @param string $holder_name
     * @return $this
     */
    public function setHolderName($holder_name)
    {
        $this->holder_name = $holder_name;

        return $this;
    }

    /**
     * Returns the value of field holder_name
     *
     * @return string
     */
    public function getHolderName()
    {
        return $this->holder_name;
    }
}
