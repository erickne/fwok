<?php
namespace Fwok\Models;

use Fwok\Exceptions\Exception;
use Fwok\Exceptions\SystemException;
use Fwok\Helpers\Format;

/**
 * Created by PhpStorm.
 * User: Erick
 * Date: 28/11/2014
 * Time: 11:27
 */
class Logs
{
    const ICON = "fa-newspaper-o";
    private $file;
    private $name;
    const OBJECT_NAME = 'log-files';

    static $LOG_DIR = '/app/Logs/';

    public function __construct($file)
    {
        $this->file = $file;
        $this->name = basename($file);
        if (!file_exists($file)) {
            throw new SystemException(SystemException::FILE_INVALID, 'File not found: ' . $this->name, 'File not found: ' . $file, 404);
        }
    }

    /**
     * @return Logs[]
     */
    public static function find()
    {
        $logs = [ ];
        $path = APPLICATION_PATH . str_replace('//', '/', self::$LOG_DIR . DIRECTORY_SEPARATOR . '*.log');
        //var_dump(glob($path));
        foreach (glob($path) as $file) {
            $logs[] = new Logs($file);
        }

        return $logs;
    }

    public function export($opt = [ ])
    {
        $return = [
            'object'     => self::OBJECT_NAME,
            'name'       => $this->getName(),
            'size'       => $this->getSize(),
            'updated_at' => $this->getUpdatedAt(),
        ];

        return $return;
    }


    public function delete()
    {
        return unlink($this->file);
    }

    public function getName()
    {
        return $this->name;
    }

    public function getContent($as_array = false)
    {
        $content = [ ];
        //return file_get_contents($this->file);
        $handle = fopen($this->file, "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $content[] = $line;
            }

            fclose($handle);
        } else {
            // error opening the file.
        }

        if($as_array){
            return $content;
        }else {
            return implode('<br />', $content);
        }
    }

    /**
     * @return string
     */
    public function getSize()
    {
        return Format::sizeUnits(filesize($this->file));
    }

    public function getUpdatedAt()
    {
        return date("Y-m-d H:i:s", filemtime($this->file));
    }

}
