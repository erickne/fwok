<?php

namespace Fwok\Models;

use Fwok\Application\App;
use Fwok\Models\Behaviors\Auditable;
use Fwok\Models\PaymentMethods;

class BusinessConfigs extends ModelBase
{
    const OBJECT_NAME = 'business-configs';

    const CODE_PAYMENT_METHODS = 'payment_methods';

    const TYPE_STRING = 'string';
    const TYPE_BOOL = 'bool';
    const TYPE_HTML = 'html';
    const TYPE_NUMBER = 'number';
    const TYPE_FLOAT = 'float';

    public function initialize()
    {
        $this->setSource('fwok_business_configs');
        $this->hasOne('business_id', '\Fwok\Models\Business', 'id', ['alias' => 'Business', 'reusable' => true,]);
        $this->keepSnapshots(true);
        $this->addBehavior(new Auditable());
    }

    /**
     * @param $code
     * @param null $business_id
     * @return $this|static
     * @throws \Fwok\Exceptions\InputException
     * @throws \Fwok\Exceptions\SystemException
     */
    public static function findByCode($code, $business_id = null)
    {
        $query = self::query()->addWhereBinded('code = ', $code);

        if ($business_id) {
            $query->addWhereBinded('business_id=', $business_id);
        } else {
            $query->searchInBusiness();
        }
        return $query->executeFirst();
    }

    /**
     * Return the related "Business"
     *
     * @param array|string $parameters
     * @return \Fwok\Models\Business
     * @throws \Fwok\Exceptions\Exception
     */
    public function getBusiness($parameters = null)
    {
        return $this->getRelated('Business', $parameters);
    }

    public function validation()
    {
        return !$this->validationHasFailed();
    }

    public function export($opt = null)
    {
        $ret = [
            'object' => self::OBJECT_NAME,
            'id' => $this->getId(),
            'business_id' => $this->getBusinessId(),
            'code' => $this->getCode(),
            'type' => $this->getType(),
            'group' => $this->getGroup(),
            'value' => $this->getValue(),
            'editable' => $this->getEditable(),
            'deleted' => $this->getDeleted(),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt(),
        ];

        if ($this->getCode() === self::CODE_PAYMENT_METHODS) {
            foreach (explode(';', $this->getValue()) as $method_code) {
                $method = PaymentMethods::findFirstByCode($method_code);
                if ($method) {
                    $ret['data'][] = $method->export();
                }
            }

        }

        return $ret;
    }

    /**
     * Method to set the value of field deleted
     *
     * @param integer $deleted
     * @return $this
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Returns the value of field deleted
     *
     * @return integer
     */
    public function getDeleted()
    {
        return (int)$this->deleted;
    }

    /**
     *
     * @var integer
     * @SWG\Property(name="business_id",type="integer",description="")
     */
    protected $business_id;

    /**
     * Method to set the value of field business_id
     *
     * @param integer $business_id
     * @return $this
     */
    public function setBusinessId($business_id)
    {
        $this->business_id = $business_id;

        return $this;
    }

    /**
     * Returns the value of field business_id
     *
     * @return integer
     */
    public function getBusinessId()
    {
        return (int)$this->business_id;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="code",type="string",description="")
     */
    protected $code;

    /**
     * Method to set the value of field code
     *
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Returns the value of field code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="value",type="string",description="")
     */
    protected $value;

    /**
     * Method to set the value of field value
     *
     * @param string $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Returns the value of field value
     *
     * @return string
     */
    public function getValue()
    {
        if($this->getType() === self::TYPE_NUMBER) {
            return (int) $this->value;
        }
        if($this->getType() === self::TYPE_FLOAT) {
            return (float) $this->value;
        }
        if($this->getType() === self::TYPE_BOOL) {
            if($this->value){
                return true;
            }else{
                return false;
            }
        }
        return $this->value;
    }

    /**
     *
     * @var integer
     * @SWG\Property(name="editable",type="integer",description="")
     */
    protected $editable;

    /**
     * Method to set the value of field editable
     *
     * @param integer $editable
     * @return $this
     */
    public function setEditable($editable)
    {
        $this->editable = $editable;

        return $this;
    }

    /**
     * Returns the value of field editable
     *
     * @return integer
     */
    public function getEditable()
    {
        return (int)$this->editable;
    }

    /**
     *
     * @var integer
     * @SWG\Property(name="public",type="integer",description="")
     */
    protected $public;

    /**
     * Method to set the value of field public
     *
     * @param integer $public
     * @return $this
     */
    public function setPublic($public)
    {
        $this->public = $public;

        return $this;
    }

    /**
     * Returns the value of field public
     *
     * @return integer
     */
    public function getPublic()
    {
        return (int)$this->public;
    }

    /**
     *
     * @var integer
     * @SWG\Property(name="restricted",type="integer",description="")
     */
    protected $restricted;

    /**
     * Method to set the value of field restricted
     *
     * @param integer $restricted
     * @return $this
     */
    public function setRestricted($restricted)
    {
        $this->restricted = $restricted;

        return $this;
    }

    /**
     * Returns the value of field restricted
     *
     * @return integer
     */
    public function getRestricted()
    {
        return (int) $this->restricted;
    }
    /**
     *
     * @var integer
     * @SWG\Property(name="ultra",type="integer",description="")
     */
    protected $ultra;

    /**
     * Method to set the value of field ultra
     *
     * @param integer $ultra
     * @return $this
     */
    public function setUltra($ultra)
    {
        $this->ultra = $ultra;

        return $this;
    }

    /**
     * Returns the value of field ultra
     *
     * @return integer
     */
    public function getUltra()
    {
        return (int) $this->ultra;
    }
    /**
     *
     * @var string
     * @SWG\Property(name="type",type="string",description="")
     */
    protected $type;

    /**
     * Method to set the value of field type
     *
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Returns the value of field type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     *
     * @var string
     * @SWG\Property(name="group",type="string",description="")
     */
    protected $group;

    /**
     * Method to set the value of field group
     *
     * @param string $group
     * @return $this
     */
    public function setGroup($group)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Returns the value of field group
     *
     * @return string
     */
    public function getGroup()
    {
        return $this->group;
    }
}