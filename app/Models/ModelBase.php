<?php

namespace Fwok\Models;

use Fwok\Application\App;
use Fwok\Application\ApplicationBase;
use Fwok\Exceptions\Exception;
use Fwok\Exceptions\InputException;
use Fwok\Exceptions\SystemException;
use Fwok\Models\Criteria\CriteriaBase;
use Fwok\Plugins\Debugger;
use Fwok\Plugins\FilterBag;
use Fwok\Plugins\FilterItem;
use Phalcon\Db;
use Phalcon\Di;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Message;
use Phalcon\Mvc\Model\Resultset\Simple;
use Phalcon\Mvc\ModelInterface;
use Phalcon\Tag;
use Phalcon\Translate\Adapter\NativeArray;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use Phalcon\Mvc\Model\TransactionInterface;

class ModelBase extends Model implements ModelBaseInterface
{
    const OBJECT_NAME = null;
    const ACTION_UPDATE = 'update';
    const ACTION_CREATE = 'create';
    const ACTION_DELETE = 'delete';
//    public static $MEMORY_CACHEABLE = true; // Saves all queries into memory and release after request finish
    public static $BACKEND_CACHEABLE = false; // Override MEMORY_CACHEABLE. This value is in SECONDS to keep cache stored
    public static $BACKEND_CACHE_LIFETIME = 500;

    protected static $cache = [];
    public $fields_names = [];
    protected $updated_at;
    protected $created_at;
    protected $deleted;
    protected $permission;
    protected $di;
    protected $created_by_id;
    protected $id;
    protected $_object_name;
    public $_runValidator = true;
    public $_runValidatorFeature = true;

    /**
     *
     * @var integer
     */
    protected $bid;

    /**
     * @usage getFilters(self::OBJECT_NAME)
     *
     * @param $object_name
     * @return FilterBag
     */
    public static function getFilters($object_name)
    {
        $bag = new FilterBag($object_name);

        $bag->addFilter(FilterItem::addDate('created_at', 'Data de Criação'));
        $bag->addFilter(FilterItem::addDate('updated_at', 'Data de Atualização'));

        return $bag;
    }

    /**
     * @param TransactionInterface|TransactionManager $txManager
     * @param array $data
     * @param array $whiteList
     * @return bool
     * @throws \Fwok\Exceptions\Exception
     * @throws \Fwok\Exceptions\InputException
     */
    public function saveOrExceptionNewTransaction(TransactionInterface $txManager = null, array $data = array(), array $whiteList = array())
    {
        if (!$txManager instanceof TransactionManager) {
            $txManager = new TransactionManager();
        }

        $transaction = $txManager->get();

        parent::setTransaction($transaction);

        $result = $this->saveOrException($data, $whiteList);

        $transaction->commit();

        return $result;
    }

    /**
     * Save an object to database or
     *  throw an exception with validation messages
     *  and rollback commit
     *
     * @param null $data
     * @param null $whiteList
     * @throws Exception
     * @return bool
     */
    public function createOrException($data = null, $whiteList = null)
    {
        $this->setId(null);
        $create = parent::create($data, $whiteList);

        if ($create) {
            self::findFirstById(1);
            return $save;
        }

        if ($this->getService('db')->isUnderTransaction()) {
            $this->getService('db')->rollback();
        }

        $user_msg = implode(' ', [App::t('ERROR_CREATING_MODEL'), App::t(static::getClassName(1))]);
        $dev_msg = sprintf('Validation error creating object [%s].', self::getClassName(1));
        throw new InputException(InputException::VALUE_INVALID, $user_msg, $dev_msg, 400, $this->getMessages(), null, $this->toArray());
    }

    /**
     * @param null|string|array $parameters
     * @param null $key
     * @param null $lifetime
     * @return $this[]|Model\Resultset|static[]
     */
    public static function findCacheBackend($parameters = null, $key = null, $lifetime = null)
    {
        $parameters = self::buildModelParameters($parameters, true, $key, $lifetime);

        return self::find($parameters);
    }

    /**
     * IN PHP
     *
     * $params = [
     *   'where' => [
     *     'dt_end' => [ 'date_gt' => '2016-01-01' ],
     *     'name' => [ 'l' => 'Empresa']
     *    ]
     *  ];
     *
     *  IN PHP CONTROLLER AFTER RECEIVING JS
     *
     *
     * $parameters = [
     *   'where' => json_decode($this->request->get('where')),
     *   'order' => json_decode($this->request->get('order'))
     * ];
     *
     *  IN JS
     *
     *   var params = {
     *      'where' : JSON.stringify({
     *         'dt_end': {'date_gt': moment().add(30, 'days').format('YYYY-MM-DD')},
     *         'name': {'l': 'Empresa'},
     *         'deleted': {'in': [0,1]}
     *      }),
     *      'order' : JSON.stringify(['-dt_end','+id'])
     *   }
     *   $scope.busy = BeneficiosService.getBeneficios(params).success(function (data) {
     *       $scope.beneficios = data.records;
     *   });
     *
     *  IN BENEFICIOS SERVICE
     *      this.getBeneficios = function (params) {
     *          return $http.get('/api/beneficios', {params: params});
     *      }
     *
     * @param array $params
     * @param bool $decode_values
     * @return static[]
     * @throws InputException
     */
    public static function buildExternalMainParameters($params = array(), $decode_values = false)
    {
        if ($decode_values) {
            $params['where'] = json_decode($params['where']);
            $params['order'] = json_decode($params['order']);
        }

        $bind = [];
        $query = '1=1';
        $find = ['conditions' => $query]; // Necessary to work with fetch methods!
        $class = get_called_class();
        $reflection_class = new $class;

        foreach ($params as $parameter => $fields) {

            if ($parameter === 'where' AND $fields) {
                foreach ($fields as $field => $condition) {
                    $field_bind = $field . mt_rand(1000, 9999);
                    if (!property_exists($reflection_class, $field)) {
                        throw new InputException(
                            InputException::PARAMETER_INVALID,
                            'INVALID_QUERY_FIELD',
                            sprintf('The field %s is not allowed to query', $field),
                            400, [], null, $fields
                        );
                    }
                    $comparators = [
                        'l' => 'LIKE', // Equal
                        'e' => '=', // Equal
                        'ne' => '!=', // Not equal
                        'lt' => '<', // Less
                        'let' => '<=', // Less or equal
                        'gt' => '>', // Grater
                        'get' => '>=', // Grater or equal
                        'date_e' => '==', // Equal
                        'date_ne' => '!=', // Not equal
                        'date_lt' => '<', // Less
                        'date_let' => '<=', // Less or equal
                        'date_gt' => '>', // Grater
                        'date_get' => '>=', // Grater or equal
                        'in' => 'IN'
                    ];

                    foreach ($condition as $key => $value) {

                        $date_parse = (strpos($key, 'date') !== false) ? 'date' : '';

                        if (array_key_exists($key, $comparators)) {

                            if ($key === 'in') {
                                $query .= sprintf(' and %s %s ({%s:array})', $field, $comparators[$key], $field_bind);
                                $bind[$field_bind] = $value;
                            } else {
                                $query .= sprintf(
                                    ' and %s %s %s({%s})',
                                    $field,
                                    $comparators[$key],
                                    $date_parse,
                                    $field_bind
                                );
                                if ($key === 'l') {
                                    $bind[$field_bind] = '%' . $value . '%';
                                } else {
                                    $bind[$field_bind] = $value;
                                }
                            }
                        }

                    }

                }
                $find['conditions'] = $query;
                $find['bind'] = $bind;
            }

            if ($parameter === 'order' AND $fields) {
                $order = [];
                foreach ($fields as $field_sorted) {
                    $sort = ' ASC';
                    if (strpos($field_sorted, '+') === 0) {
                        $sort = ' ASC';
                    } elseif (strpos($field_sorted, '-') === 0) {
                        $sort = ' DESC';
                    }
                    $field = substr($field_sorted, 1);
                    if (!property_exists($reflection_class, $field)) {
                        throw new InputException(
                            InputException::PARAMETER_INVALID,
                            'INVALID_QUERY_SORT_FIELD',
                            sprintf('The field %s is not allowed to sort query', $field),
                            400, [], null, $fields
                        );
                    }
                    $order[] = $field . $sort;

                }
                $find['order'] = implode(',', $order);
            }

            if ($parameter === 'count' AND $fields) {
                $find['limit'] = (int)$fields;
            }

            if ($parameter === 'page' AND $fields) {
                $find['offset'] = (int)$fields;
            }

        }

        if ($find['offset'] AND $find['limit']) {
            $find['offset'] = ($find['offset'] - 1) * $find['limit'];
        } else {
            unset($find['offset']);
        }

        return $find;
    }

    /**
     *
     *
     * $parameters = [
     *      'where' => json_decode($this->request->get('where')),
     *      'order' => json_decode($this->request->get('order'))
     * ];
     *
     * @param array $params
     * @param bool $decode_values
     * @return $this[]|Model\Resultset|static[]
     * @throws InputException
     */
    public static function findByMainParameters($params = array(), $decode_values = false)
    {
        return self::find(self::buildExternalMainParameters($params, $decode_values));
    }

    public static function countByMainParameters($params = array(), $decode_values = false)
    {
        // Removing not allowed params in count
        unset($params['page'], $params['count'], $params['order']);

        return self::count(self::buildExternalMainParameters($params, $decode_values));
    }

    public static function purgeCacheByKey($key)
    {
        return App::getModelsCache()->delete($key);
    }

    /**
     * @param null|string|array $parameters
     * @return bool
     */
    public static function purgeCacheByParameters($parameters = null)
    {
        $parameters = self::buildModelParameters($parameters, false);
        if (is_array($parameters)) {
            $key = $parameters['cache']['key'];
        } else {
            $key = self::createCacheKey($parameters);
        }

        return App::getModelsCache()->delete($key);
    }

    /**
     * @param array|string|null $parameters
     * @param bool $use_cache
     * @param null $cache_key
     * @param null $cache_lifetime
     * @return array|null|string
     */
    public static function buildModelParameters(
        $parameters,
        $use_cache = false,
        $cache_key = null,
        $cache_lifetime = null
    )
    {
        if ($use_cache OR static::$BACKEND_CACHEABLE) {
            $key = $cache_key ?: self::createCacheKey($parameters);
            $lifetime = $cache_lifetime ?: static::$BACKEND_CACHE_LIFETIME;
            $cache_array = [
                'key' => $key,
                'lifetime' => $lifetime
            ];

            if (is_array($parameters)) {
                if (!array_key_exists('cache', $parameters)) {
                    $parameters['cache'] = $cache_array;
                }
            } else {
                $parameters = [
                    $parameters,
                    'cache' => $cache_array
                ];
            }
        }

        return $parameters;
    }

    /**
     *
     * lifetime is expressed in seconds
     *
     * $feature = SubscriptionsFeatures::findFirstCacheBackend(
     *  [
     *      'key = :key: and subscription_id = :subscription_id:',
     *      'bind'  => [
     *          'key'             => $key,
     *          'subscription_id' => $subscription->getId()
     *      ],
     *      'cache' => [
     *          'lifetime' => 500,
     *          'key'      => 'user-cached-' . $key . '-' . $subscription->getId()
     *      ]
     *  ]
     *m  );
     *
     *
     * @param null|string|array $parameters
     * @param $key
     * @param $lifetime
     * @return Model|static
     * @throws \Fwok\Exceptions\SystemException
     */
    public static function findFirstCacheBackend($parameters = null, $key, $lifetime)
    {
        $parameters = self::buildModelParameters($parameters, true, $key, $lifetime);

        return self::findFirst($parameters);
    }

    /**
     * @inheritdoc
     *
     * @access public
     * @static
     * @param array|string $parameters Query parameters
     * @return mixed
     */
    public static function count($parameters = null)
    {
        $parameters = self::buildModelParameters($parameters);

        Debugger::modelParameters(self::getClassName(1), __METHOD__, $parameters);

        return parent::count($parameters);
    }

    /**
     * @param null|array|string $parameters
     * @param $key
     * @param $lifetime
     * @return mixed
     */
    public static function countCacheBackend($parameters = null, $key, $lifetime)
    {
        $parameters = self::buildModelParameters($parameters, true, $key, $lifetime);

        return self::count($parameters);
    }

    /**
     * Return called class name
     *
     * @return string
     */
    public static function modelName()
    {
        return lcfirst(strtolower(get_called_class()));
    }

    /**
     * @param array $ids
     * @return $this[]|Model\Resultset
     * @throws \Fwok\Exceptions\Exception
     */
    public static function findValidByIds($ids)
    {
        return self::findByIds($ids, 1);
    }

    /**
     * @param array $ids
     * @param int $check_permission
     * @throws Exception
     * @return $this[]|Model\Resultset
     */
    public static function findByIds($ids, $check_permission = 0)
    {
        if (!is_array($ids)) {
            $ids = [$ids];
        }

        $objects = self::find(
            [
                'id IN ({ids:array})',
                'bind' => [
                    'ids' => $ids
                ]
            ]
        );

        if ($check_permission) {
            foreach ($objects as $object) {
                $object->checkPermission();
            }
        }

        return $objects;

    }

    /**
     * Check object permission in database and throw exception if invalid
     *
     * @param string $mode
     * @throws Exception
     * @return bool
     */
    public function checkPermission($mode = Permission::MODE_READ)
    {
        $permission = $this->getPermission();

        if (!$permission) {
            $this->permissionException();
        }
        if ($mode === Permission::MODE_READ) {
            if ($permission->getCanRead() or $permission->getCanWrite()) {
                return true;
            } else {
                $this->permissionException(Permission::MODE_READ);
            }
        } elseif ($mode === Permission::MODE_WRITE) {
            if ($permission->getCanWrite()) {
                return true;
            } else {
                $this->permissionException(Permission::MODE_WRITE);
            }
        }

        $msg = sprintf('Invalid argument in check permission (%s)', $mode);
        throw new SystemException(SystemException::LOGIC_INVALID, null, $msg);
    }

    /**
     * Throw permission exception
     *
     * @param null|string $mode
     * @throws Exception
     */
    public function permissionException($mode = null)
    {
        if (!$mode) {
            $user_msg = ApplicationBase::getTranslatedKey('INVALID_PERMISSION');
        } elseif ($mode === Permission::MODE_READ) {
            $user_msg = ApplicationBase::getTranslatedKey('INVALID_READ_PERMISSION');
        } elseif ($mode === Permission::MODE_WRITE) {
            $user_msg = ApplicationBase::getTranslatedKey('INVALID_WRITE_PERMISSION');
        } else {
            throw new SystemException(SystemException::ACTION_INVALID);
        }

        $user_msg .= ApplicationBase::getTranslatedKey(static::getClassName(1));

        $dev_msg = sprintf(
            'Permission denied (%s) to user(%s) on %s(%s)',
            $mode,
            ApplicationBase::getUser()->getId(),
            static::getClassName(1),
            $this->getId()
        );
        throw new InputException(InputException::PERMISSION_INVALID, $user_msg, $dev_msg, 400);
    }

    /**
     * Verifica se um modelo existe. Se não existir, cria e retorna ele.
     *
     * @param \stdClass $data
     * @param int $dont_save
     * @throws Exception
     * @return bool|self|Model\Resultset|$this
     */
    public static function findFirstOrCreate($data, $dont_save = 0)
    {
        if (!$data) {
            return false;
        }

        if (is_int($data) and $data != 0) {
            $id = $data;
        } elseif ($data->id) {
            $id = $data->id;
        } else {
            $id = null;
        }

        if ($id) {
            $object = self::findFirstById($id);
            if ($object) {
                return $object;
            } else {
                return false;
            }
        } else {
            return self::createFromAutoPopulate($data, $dont_save);
        }
    }

    /**
     * Cria o objeto a partir das variáveis enviadas.
     *
     * WARNING: existe um loop para gravar as variáveis conforme elas chegam.
     * Não são utilizados métodos SetVARIÁVEL($valor); Podem ocorrer inconsistências
     * caso os dados não sejam enviados com o nome igual.
     *
     * @param $data
     * @param $dont_save
     * @return bool|self
     * @throws Exception
     *
     */
    public static function createFromAutoPopulate($data, $dont_save = false)
    {
        $class = get_called_class();
        /** @var self $object */
        $object = new $class();
        foreach ($data as $k => $v) {
            /*
             * All key come with CamelCase will be deCalmelCased
             * Eg: productType = product_type
             */
            $key_with_underscore = ltrim(strtolower(preg_replace('/[A-Z]/', '_$0', $k)), '_');
            $object->$key_with_underscore = $v;
        }
        if ((method_exists($object, 'setUserId'))) {
            $object->setUserId(ApplicationBase::getUser()->getId());
        }
        if ((method_exists($object, 'setDeleted'))) {
            $object->setDeleted(0);
        }

        if ($dont_save) {
            return $object;
        }

        $object->saveOrException();

        return $object;
    }

    /**
     * Verifica se um modelo existe. Se não existir, cria e retorna ele.
     *
     * @param \stdClass $data
     * @param int $dont_save
     * @throws Exception
     * @return bool|self
     */
    public static function findFirstValidOrCreate($data, $dont_save = 0)
    {
        if (!$data) {
            return false;
        }

        if (is_int($data) and $data != 0) {
            $id = $data;
        } elseif ($data->id) {
            $id = $data->id;
        } else {
            $id = null;
        }

        if ($id) {
            $object = self::findFirstValidById($id);
            if ($object) {
                return $object;
            } else {
                return false;
            }
        } else {
            return self::createFromAutoPopulate($data, $dont_save);
        }
    }

    public static function throwNotFound($id)
    {
        throw new InputException(
            InputException::NOT_FOUND,
            App::t(self::getClassName(1)) . " " .
            App::t("OBJECT_NOT_FOUND"),
            sprintf('Object not found in database. %s(%s)', get_called_class(), $id),
            404
        );
    }

    public static function throwDeleted($id = null)
    {
        throw new InputException(
            InputException::DELETED,
            App::t(self::getClassName(1)) . ' ' . App::t('IS_DELETED'),
            sprintf('Object deleted. %s(%s)', self::getClassName(1), $id),
            400
        );
    }

    /**
     * @param ModelBase|ModelInterface $model
     * @param bool $throwNotFound
     * @param bool $throwDeleted
     * @param string $info
     * @return bool
     * @throws InputException
     */
    public static function throwModelCheck($model, $throwNotFound = true, $throwDeleted = true, $info = '')
    {
        if ($throwNotFound AND !$model) {
            static::throwNotFound($info);
        }
        if ($throwDeleted AND method_exists($model, 'getDeleted') AND $model->getDeleted()) {
            static::throwDeleted($info);
        }
        return true;
    }

    /**
     * @param $id
     * @param int $exception_on_deleted
     * @param int $exception_on_not_found
     * @param int $check_permission
     * @throws Exception
     * @return $this
     */
    public static function findFirstValidById(
        $id,
        $exception_on_deleted = 0,
        $exception_on_not_found = 1,
        $check_permission = 1
    )
    {
        $object = self::findFirstById($id);

        if (!$object) {
            if ($exception_on_not_found) {
                self::throwNotFound($id);
            } else {
                return false;
            }
        }

        /*
         * Essa verificação serve para liberar o objeto UserDevice e outros
         */
        if (method_exists($object, 'getUserId')) {
            if ($object->getUserId() == ApplicationBase::getUser()->getId()) {
                return $object;
            }
        }

        if ($check_permission) {
            $object->checkPermission(Permission::MODE_READ);
        }

        if ($exception_on_deleted and $object->getDeleted()) {
            throw new InputException(
                InputException::DELETED,
                App::t(self::getClassName(1)) . " " . App::t('IS_DELETED'),
                sprintf('Object deleted. %s(%s)', self::getClassName(1), $object->getId()),
                400
            );
        }

        return $object;
    }

    /**
     * Returns the value of field deleted
     *
     * @return int
     */
    public function getDeleted()
    {
        return $this->deleted + 0;
    }

    /**
     * Method to set the value of field deleted
     *
     * @param int $deleted
     *
     * @return $this
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    public static function getStaticService($service)
    {
        return Di::getDefault()->get($service);
    }

    public static function getStaticDI()
    {
        return Di::getDefault();
    }

    /**
     * @param self $model
     * @param $query
     * @param null $parameters
     * @param null $cache
     * @param null $snapshots
     * @return array|Simple
     * @internal param null $params
     */
    public static function findByRawSql($model = null, $query, $parameters = null, $cache = null, $snapshots = null)
    {
        if ($model) {
            Debugger::modelParameters(self::getClassName(1), __METHOD__, $parameters, $query);
            /** @var Db\Result\Pdo|Db\AdapterInterface $connection */
            $connection = $model->getReadConnection();
            return new Simple(null, $model, $connection->query($query, $parameters), $cache, $snapshots);
        } else {
            Debugger::modelParameters(self::getClassName(1), __METHOD__, $parameters, $query);
            /** @var Db\Adapter\Pdo $connection2 */
            $connection2 = App::getDb();
            /** @var Db\Result\Pdo $result_set */
            $result_set = $connection2->query($query);
            $result_set->setFetchMode(Db::FETCH_ASSOC);
            $result_set = $result_set->fetchAll($result_set);

            return $result_set;
        }
    }

    /**
     * @return string
     */
    public static function generateCode()
    {
        return md5(microtime());
    }

    public function onConstruct()
    {
        $this->di = FactoryDefault::getDefault();

        $this->_object_name = self::OBJECT_NAME;
    }

    /**
     * Return related files
     *
     * @return array|ModelBase[]|Model\Resultset|static[]
     */
    public function getFiles()
    {
        return Files::query()
            ->addWhereBinded('object_type = ', static::getClassName())
            ->addWhereBinded('object_id = ', $this->getId())
            ->execute();
    }

    /**
     * Find models
     *
     * @param array|string $parameters Query parameters
     * @return self[]|Model\Resultset|$this[]
     * @access public
     * @static
     */
    public static function find($parameters = null)
    {
        $parameters = self::buildModelParameters($parameters);
        Debugger::modelParameters(self::getClassName(1), __METHOD__, $parameters);

        $find = parent::find($parameters);
        $find->_parameters = $parameters;

        return $find;
    }

    /**
     * Implement a method that returns a cache string key based
     * on the query parameters
     * @param array|string $parameters
     * @return string
     */
    protected static function createCacheKey($parameters)
    {
        if (is_string($parameters)) {
            $parameters = [$parameters];
        }
        $parameters['object'] = get_called_class();
        $parameters['query'] = $parameters[0];
        unset($parameters[0]);

        $parameters = json_encode($parameters);
        return static::OBJECT_NAME . '-' . md5($parameters);

    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id + 0;
    }

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function undelete()
    {
        $this->setDeleted(0);

        return $this->saveOrException();
    }

    /**
     * @param $uploadFiles
     * @return array|Files|Files[]
     * @throws \Fwok\Exceptions\Exception
     * @throws InputException
     * @throws SystemException
     */
    public function uploadFiles($uploadFiles)
    {
        if (!$this->getId()) {
            $msg = 'Model must be saved before uploading files';
            throw new SystemException(SystemException::LOGIC_INVALID, null, $msg);
        }

        if (is_array($uploadFiles)) {
            $return = [];
            foreach ($uploadFiles as $uploadFile) {
                $file = new Files();
                $file->writeBase64UrlToFolder($uploadFile);
                $file->setObjectType(static::OBJECT_NAME);
                $file->setObjectId($this->getId());
                $file->saveOrException();
                $return[] = $file;
            }
        } else {
            $file = new Files();
            $file->writeBase64UrlToFolder($uploadFiles);
            $file->setObjectType(static::OBJECT_NAME);
            $file->setObjectId($this->getId());
            $file->saveOrException();
            $return = $file;
        }

        return $return;
    }

    /**
     * Save an object to database or
     *  throw an exception with validation messages
     *  and rollback commit
     *
     * @param null $data
     * @param null $whiteList
     * @throws Exception
     * @return bool
     */
    public function saveOrException($data = null, $whiteList = null)
    {
        /*
         * Skip saving if there are no changes
         * Only applies if is updating the object
         */
        if ($this->getId() AND $this->hasSnapshotData()) {
            $changes_fields = $this->getChangedFields();
            if (count($changes_fields) === 0) {
                return true;
            }
        }
        $save = parent::save($data, $whiteList);

        if ($save) {
            self::findFirstById(1);
            return $save;
        }

        if ($this->getService('db')->isUnderTransaction()) {
            $this->getService('db')->rollback();
        }

        $user_msg = implode(' ', [App::t('ERROR_SAVING_MODEL'), App::t(static::getClassName(1))]);
        $dev_msg = sprintf('Validation error saving object [%s].', self::getClassName(1));
        throw new InputException(InputException::VALUE_INVALID, $user_msg, $dev_msg, 400, $this->getMessages(), null, $this->toArray());
    }

    /**
     * Get the service via static call
     *
     * @param $service
     * @return mixed
     */
    public function getService($service)
    {
        return $this->getDI()->get($service);
    }

    public static function getClassName($fix_backslash = 0)
    {
        if ($fix_backslash) {
            $class = str_replace("\\", "\\\\", get_called_class());
        } else {
            $class = get_called_class();
        }

        return $class;
    }

    public function getMessages($filter = null)
    {
        $messages = [];
        foreach (parent:: getMessages($filter) as $message) {
            /** @var Message $message */
            if (is_string($message)) {
                $messages[] = $message;
            } else {
                switch ($message->getType()) {
                    case 'InvalidCreateAttempt':
                        $messages[] = 'Error creating record.';
                        break;
                    case 'InvalidUpdateAttempt':
                        $messages[] = 'Error updating record.';
                        break;
                    case 'PresenceOf':
                        $messages[] = 'The field ' . $message->getField() . ' is required';
                        break;
                    case 'ConstraintViolation':
                        $messages[] = 'Foreign key violation in field ' . $message->getField();
                        break;
                    case 'InvalidValue':
                        $messages[] = 'Invalid value in field ' . $message->getField();
                        break;
                    case 'InclusionIn':
                        $messages[] = 'Invalid value in field ' . $message->getField();
                        break;
                    default:
                        $messages[] = $message->getMessage();
                        break;
                }
            }
        }

        return $messages;
    }

    /**
     * Export model data to array
     * @return array
     */
    public function export()
    {
        $return = [
            'id' => $this->getId(),
            'object' => self::OBJECT_NAME,
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt(),
            'deleted' => $this->getDeleted()
        ];

        return $return;
    }

    /**
     * Returns the value of field created_at
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Method to set the value of field created_at
     *
     * @param string $created_at
     *
     * @return $this
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Returns the value of field created_at
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Method to set the value of field updated_at
     *
     * @param string $updated_at
     *
     * @return $this
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Returns related records based on defined relations
     *
     * @param string $alias
     * @param array $parameters
     * @param int $exception_in_not_found
     * @throws Exception
     * @return \Phalcon\Mvc\Model\ResultsetInterface|self|self[]
     */
    public function getRelated($alias, $parameters = null, $exception_in_not_found = 0)
    {
        $result = parent::getRelated($alias, $parameters);

        if (!$result and $exception_in_not_found) {
            throw new SystemException(
                SystemException::DATABASE_CONSISTENCY,
                'INVALID_RELATED_MODEL',
                sprintf(
                    'Related %s could not be found with arguments (%s). Parent Object: %s(%s)',
                    $alias,
                    json_encode($parameters),
                    static::getClassName(1),
                    $this->getId()
                ),
                500
            );
        }

        return $result;
    }

    /**
     * Validate model before saving and appending messages
     *
     *  $this->validate(new \Phalcon\Mvc\Model\Validator\ExclusionIn([
     *    'field' => 'status',
     *    'domain' => ['A', 'I']
     *  ]));
     *
     *  if ($this->validationHasFailed() == true) {
     *    return false;
     *  }
     *
     */
    public function validation()
    {
    }


    /**
     * Create a criteria for a specific model
     *
     * @param mixed $dependencyInjector
     * @return CriteriaBase
     */
    public static function query(\Phalcon\DiInterface $dependencyInjector = null)
    {
        $criteria = new CriteriaBase();
        $criteria->setDI($dependencyInjector ? $dependencyInjector : Di::getDefault());
        $criteria->setModelName(get_called_class());
        return $criteria;
    }

    /**
     * Save an object to database or
     *  throw an exception with validation messages
     *  and rollback commit
     *
     * @throws Exception
     * @return bool
     */
    public function deleteOrException()
    {
        $delete = $this->delete();

        if ($delete) {
            return $delete;
        } else {
            $this->getService('db')->rollback();

            throw new InputException(
                InputException::VALUE_INVALID,
                'ERROR_DELETING_OBJECT',
                sprintf('Error deleting object class [%s]. Check validation messages.', self::getClassName(1)),
                500,
                $this->getMessages(),
                null,
                $this->export()
            );
        }
    }

    /**
     * Requery object from database. Usable because auditable need a fresh copy snapshot
     *
     * Usage:
     *
     * $model = $model->manualRefresh();
     *
     * @return $this
     * @throws Exception
     */
    public function manualRefresh()
    {
        return self::findFirstById($this->getId());
    }

    public static function checkReturnedModel($model)
    {

    }

    /**
     * @param $id
     * @param int $exception
     * @throws Exception
     * @return $this
     */
    public static function findFirstById($id, $exception = 0)
    {
        $param = ['id = :id:', 'bind' => ['id' => $id]];
        return self::findFirst($param, $exception);
    }

    /**
     * Allows to query the last record that match the specified conditions
     *
     * @access public
     * @static
     * @param array|string $parameters Query parameters
     * @param int $exception
     * @throws SystemException
     * @return $this
     */
    public static function findLast($parameters = null, $exception = 0)
    {
        $parameters = self::buildModelParameters($parameters);

        /*
         * Nesse caso sobrescreve a key ORDER pois não vai interessar
         * Vamos obter o ultimo id disponível
         */
        @$parameters['order'] = 'id DESC';

        return self::findFirst($parameters, $exception);
    }

    /**
     * @param string $field
     * @param string $value
     * @param bool $excep_not_found
     * @param bool $excep_deleted
     * @param null $msg
     * @return array|ModelBase|static|self
     */
    public static function queryFirstBy($field, $value, $excep_not_found = false, $excep_deleted = true, $msg = null)
    {
        (!$msg) ? $msg = $field . $value : null;
        return self::query()
            ->addWhereBinded($field, $value)
            ->executeFirst($excep_not_found, $excep_deleted, $msg, self::getClassName());
    }

    /**
     *
     * Allows to query the first record that match the specified conditions
     *
     * <code>
     *
     * //What's the first robot in robots table?
     * $robot = Robots::findFirst();
     * echo "The robot name is ", $robot->name;
     *
     * //What's the first mechanical robot in robots table?
     * $robot = Robots::findFirst("type='mechanical'");
     * echo "The first mechanical robot name is ", $robot->name;
     *
     * //Get first virtual robot ordered by name
     * $robot = Robots::findFirst(array("type='virtual'", "order" => "name"));
     * echo "The first virtual robot name is ", $robot->name;
     *
     * </code>
     *
     * @access public
     * @static
     * @param array|string $parameters Query parameters
     * @param int $exception
     * @throws SystemException
     * @return $this
     */
    public static function findFirst($parameters = null, $exception = 0)
    {
        $parameters = self::buildModelParameters($parameters);

        $model = parent::findFirst($parameters);

        Debugger::modelParameters(self::getClassName(1), __METHOD__, $parameters);

        if ($model) {
            $model->_parameters = $parameters;
            return $model;
        }

        if ($exception) {
            self::throwNotFound($parameters);
        }

        return false;
    }

    /**
     * Get permission information
     *
     * Ties to get permission in Permission Model. If cant find, tries to find from
     * Related ObjectType Parent (object_type and object_id)
     *
     *  # TODO Atualizar para esse padrão:
     * 0) Check if object is in the sabe active business
     * 1) Check master permission in system
     * 2.1) Check each user's groups (object_type x team_id)
     * 2.2) Check each user's groups (object_type,object_id x team_id)
     * 3.1) Check user (object_type x user_id)
     * 3.2) Check user (object_type,object_id x user_id)
     * 4) Check user parent object
     * SOMAR TODAS AS PERMISSION DE READ e DE WRITE!!!!
     *
     * @return Permission
     * @throws \Fwok\Exceptions\Exception
     */
    public function getPermission()
    {
        $sum_read = 0;
        $sum_write = 0;
        $user = App::getUser();

        /*
         * 1) Check master permission in system
         */
        if ($user->hasRole(Roles::ADMIN)) {
            $return_permission = new Permission();
            $return_permission->setCanRead(1);
            $return_permission->setCanWrite(1);
            return $return_permission;
        }

        /*
         * 2) Check each user's teams
         */
        foreach ($user->getTeamsUsers() as $team_user) {
            // 2.1 (object_type x team_id)
            $permission = Permission::findFirstByObjectAndId(static::OBJECT_NAME, null, 0, null, $team_user->getTeamId());
            if ($permission->getCanWrite()) {
                return $permission;
            } else {
                $sum_read += $permission->getCanRead();
                $sum_write += $permission->getCanWrite();
            }

            // 2.2 (object_type,object_id x team_id)
            $permission = Permission::findFirstByObjectAndId(static::OBJECT_NAME, $this->getId(), 0, null, $team_user->getId());
            if ($permission->getCanWrite()) {
                return $permission;
            } else {
                $sum_read += $permission->getCanRead();
                $sum_write += $permission->getCanWrite();
            }
        }

        /*
         * 3.1) Check user (object_type x user_id)
         */
        $permission = Permission::findFirstByObjectAndId(static::OBJECT_NAME, null, 0, $user->getId());
        if ($permission->getCanWrite()) {
            return $permission;
        } else {
            $sum_read += $permission->getCanRead();
            $sum_write += $permission->getCanWrite();
        }

        /*
         * 3.2) Check user (object_type,object_id x user_id)
         */
        $permission = Permission::findFirstByObjectAndId(static::OBJECT_NAME, $this->getId(), 0, $user->getId());
        if ($permission->getCanWrite()) {
            return $permission;
        } else {
            $sum_read += $permission->getCanRead();
            $sum_write += $permission->getCanWrite();
        }

        /*
         * 4) Check object parent object
         */

        # TODO Atualizar as possibilidades. Essa função é extendida em outra classe (Ex \GN\Models\ModelBase)! ATENÇÃO!
        /*
        $object_object_parent_permission = $this->getPermissionFromRelatedObjectTypeParent($force_refresh);
        if ($object_object_parent_permission) {
            if ($object_object_parent_permission->getCanWrite()) {
                return $object_object_parent_permission;
            } else {
                $sum_read += $object_object_parent_permission->getCanRead();
                $sum_write += $object_object_parent_permission->getCanWrite();
            }
        }
        */

        $return_permission = new Permission();
        $return_permission->setCanRead($sum_read);
        $return_permission->setCanWrite($sum_write);

        return $return_permission;
    }

    /**
     * Add an error message to validation process
     *
     * @param $message
     * @param null $field
     * @param null $type
     * @param bool $translated
     * @return $this
     */
    public function appendErrorMessage($message, $field = null, $type = null, $translated = true)
    {
        if ($translated) {
            $this->appendErrorMessageTranslated($message, $field, $type);
        } else {
            $this->appendMessage(new Message($message, $field, $type));
        }

        return $this;
    }

    /**
     * Add an error message to validation process
     *
     * @param $message_key
     * @param null $field
     * @param null $type
     * @return $this
     */
    public function appendErrorMessageTranslated($message_key, $field = null, $type = null)
    {
        /** @var NativeArray $t */
        $t = $this->getService('translation')->getMessages();

        $this->appendMessage(new Message($t->_($message_key), $field, $type));

        return $this;
    }

    /**
     * Insert values from array into object
     *
     * @param $data
     * @return $this
     */
    public function fillFromArray($data)
    {
        foreach ($data as $k => $v) {
            $this->$k = $v;
        }

        return $this;
    }

    /**
     * Get Property from model
     *
     * @param $property
     * @return bool|Model\Resultset
     */
    public function get($property)
    {
        $class = get_called_class();
        if (property_exists(new $class(), $property)) {
            return $this->$property;
        } else {
            return false;
        }
    }

    public function beforeCreate()
    {
        $this->setDeleted(0);
        $this->setUpdatedAt(date('Y-m-d H:i:s'));
        $this->setCreatedAt(date('Y-m-d H:i:s'));

        if (method_exists($this, 'setBusinessId') AND
            method_exists($this, 'getBusinessId') AND
            $this->getBusinessId() === null AND
            App::getBusiness(false)
        ) {
            $this->setBusinessId(App::getBusiness()->getId());
        }

        if (App::hasService('user')) {
            $this->setCreatedById(App::getUser()->getId());
        }
    }

    public function beforeSave()
    {
        $this->setUpdatedAt(date('Y-m-d H:i:s'));

        if (App::hasService('user')) {
            $this->setUpdatedById(App::getUser()->getId());
        }
        $this->setSnapshotData($this->toArray());
    }

    /**
     * Returns the value of field created_by_id
     *
     * @return integer
     */
    public function getCreatedById()
    {
        return (int)$this->created_by_id;
    }

    /**
     * Method to set the value of field created_by_id
     *
     * @param integer $created_by_id
     * @return $this
     */
    public function setCreatedById($created_by_id)
    {
        $this->created_by_id = $created_by_id;

        return $this;
    }

    /**
     *
     * @var integer
     * @SWG\Property(name="updated_by_id",type="integer",description="")
     */
    protected $updated_by_id;

    /**
     * Method to set the value of field updated_by_id
     *
     * @param integer $updated_by_id
     * @return $this
     */
    public function setUpdatedById($updated_by_id)
    {
        $this->updated_by_id = $updated_by_id;

        return $this;
    }

    /**
     * Returns the value of field updated_by_id
     *
     * @return integer
     */
    public function getUpdatedById()
    {
        return (int)$this->updated_by_id;
    }


    /**
     * Returns the value of field bid
     *
     * @param int $prefixed
     * @return integer|string
     */
    public function getBid($prefixed = 1)
    {
        if ($prefixed) {
            return implode('-', [App::t('PREFIX_BID_' . self::getClassName()), $this->bid]);
        }
        return (int)$this->bid;
    }

    /**
     * Method to set the value of field bid
     *
     * @param integer $bid
     * @return $this
     */
    public function setBid($bid)
    {
        $this->bid = $bid;

        return $this;
    }

    public function validatorDisable()
    {
        $this->_runValidator = false;
    }

    public function validatorEnable()
    {
        $this->_runValidator = true;
    }

    public function validatorFeatureDisable()
    {
        $this->_runValidatorFeature = false;
    }

    public function validatorFeatureEnable()
    {
        $this->_runValidatorFeature = true;
    }

    public function getRunValidatorFeature()
    {
        return $this->_runValidatorFeature;
    }

    public function getRunValidator()
    {
        return $this->_runValidator;
    }

}
