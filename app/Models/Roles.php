<?php

namespace Fwok\Models;

use Fwok\Application\App;
use Fwok\Exceptions\InputException;
use Phalcon\Mvc\Model\Message;
use Phalcon\Mvc\Model\Validator\Uniqueness;
use Phalcon\Validation;

class Roles extends ModelBase
{
    public static $SEARCH_DELETED = false;
    public static $BACKEND_CACHEABLE = 500000;

    const OBJECT_NAME = 'roles';

    const ADMIN = 'ADMIN';
    const USER = 'USER';
    const ULTRA = 'ULTRA';
    const ACTION_ADD = 'add';
    const ACTION_REVOKE = 'revoke';

    public function initialize()
    {
        $this->setSource('fwok_roles');
        $this->hasManyToMany(
            'id',
            '\\Fwok\\Models\\RolesUsers',
            'role_id',
            'user_id',
            '\\Fwok\\Models\\Users',
            'id',
            ['alias' => 'Users']
        );
        
        $this->keepSnapshots(true);
        $this->addBehavior(new \Fwok\Models\Behaviors\Auditable());
    }

    public function export($o = []) {
        $return = [
            'id' => $this->getId(),
            'code' => $this->getCode(),
            'name' => $this->getName(),
            'is_public' => $this->getIsPublic(),
        ];
        return $return;
    }

    /**
     * Find first Role by code with a VERY long cache
     * @param $code
     * @param bool $excep_not_found
     * @return Roles
     * @throws InputException
     */
    public static function findFirstByCode($code, $excep_not_found = false)
    {
        $role = self::query()
            ->addWhereBinded('code = ',$code)
            ->cached(3600,'roles-'.$code)
            ->executeFirst();
        
        if ($excep_not_found and !$role) {
            self::throwNotFound($code);
        }
        return $role;
    }

    public static function validateAction($action, $throw_exception)
    {
        if (in_array($action, [self::ACTION_ADD, self::ACTION_REVOKE])) {
            return true;
        }
        if ($throw_exception) {
            throw new InputException(InputException::VALUE_INVALID, 'INVALID_ACTION',
                'Invalid action. Use only revoke and add');
        }
        return false;
        
    }

    public function validation()
    {

        $validator = new Validation();

        $validator->add('code', new Validation\Validator\Uniqueness([
                'message' => App::getTranslatedKey('CODE_ALREADY_EXISTS'),
            ])
        );

        if (!$this->getCode()) {
            $validator->appendMessage(new Validation\Message(App::t('INVALID_ROLE_CODE')));
        }

        return $this->validate($validator);
    }

    /**
     * Return the related Users
     *
     * @param string|array $parameters
     * @return Users[]
     */
    public function getUsers($parameters = null)
    {
        return $this->getRelated('Users', $parameters);
    }


    /**
     *
     * @var string
     * @SWG\Property(name="name",type="string",description="")
     */
    protected $name;

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="code",type="string",description="")
     */
    protected $code;

    /**
     * Method to set the value of field code
     *
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = strtoupper($code);

        return $this;
    }

    /**
     * Returns the value of field code
     *
     * @return string
     */
    public function getCode()
    {
        return strtoupper($this->code);
    }

    /**
     *
     * @var integer
     * @SWG\Property(name="is_public",type="integer",description="")
     */
    protected $is_public;

    /**
     * Method to set the value of field is_public
     *
     * @param integer $is_public
     * @return $this
     */
    public function setIsPublic($is_public)
    {
        $this->is_public = $is_public;

        return $this;
    }

    /**
     * Returns the value of field is_public
     *
     * @return integer
     */
    public function getIsPublic()
    {
        return (int)$this->is_public;
    }
}