<?php

namespace Fwok\Models;

use Fwok\Application\App;
use Fwok\Exceptions\Exception;
use Fwok\Exceptions\InputException;
use Fwok\Exceptions\SystemException;
use Fwok\Helpers\Format;
use Fwok\Models\Criteria\CriteriaBase;
use Phalcon\Filter;
use Phalcon\Mvc\Model\Validator\Uniqueness;
use Phalcon\Validation;

class Users extends ModelBase
{
    const OBJECT_NAME = 'users';

    const EXPORT_ACCOUNT_INFO = 'users_show_account_info'; //show_account_info
    const EXPORT_PERMISSIONS = 'users_permissions'; //permissions
    const EXPORT_TEAMS = 'users_teams'; //teams

    public function initialize()
    {
        $this->setSource('fwok_users');

        $this->hasMany('id', '\Fwok\Models\Token', 'user_id', ['alias' => 'Token', 'reusable' => true]);
        $this->hasMany('id', '\Fwok\Models\TeamsUsers', 'user_id', ['alias' => 'TeamsUsers', 'reusable' => true]);
        $this->hasMany('id', '\Fwok\Models\UsersDevices', 'user_id', ['alias' => 'UsersDevices', 'reusable' => true]);
        $this->hasMany('id', '\Fwok\Models\UsersCardTokens', 'user_id', ['alias' => 'UsersCardTokens', 'reusable' => true]);
        $this->hasMany(
            'id',
            '\Fwok\Models\UsersSettings',
            'user_id',
            ['alias' => 'UsersSettings', 'reusable' => true]
        );
        $this->hasOne(
            'business_id',
            '\Fwok\Models\Business',
            'id',
            ['alias' => 'Business', 'reusable' => true]
        );

        $this->hasManyToMany(
            'id',
            '\\Fwok\\Models\\RolesUsers',
            'user_id',
            'role_id',
            '\\Fwok\\Models\\Roles',
            'id',
            [
                'alias' => 'Roles'
            ]
        );
    }

    public function beforeCreate()
    {
        parent::beforeCreate();
        $this->setSnapshotData($this->toArray());
        if (!$this->getLocale()) $this->setLocale(App::getConfig('application')->defaultLanguage);
    }

    /**
     * @param Subscriptions $subscription
     * @throws \Fwok\Exceptions\Exception
     */
    public function createUserInitialBusiness($subscription = null)
    {
        if (!$subscription) {
            $subscription = Subscriptions::findFirstFree();
        }

        $business = new \Fwok\Models\Business();
        $business->setContactUserId($this->getId());
        $business->saveOrException();

        $end_date = \Fwok\Helpers\Format::dateGetOffset(date('Y-m-d H:i:s'), '+1 month', 'Y-m-d H:i:s');

        $business->createSubscription($subscription, true, null, $end_date);

        $this->setBusinessId($business->getId());
        $this->saveOrException();
    }

    /**
     * Return the related Roles
     *
     * @param string|array $parameters
     * @return Roles[]
     */
    public function getRoles($parameters = null)
    {
        return $this->getRelated('Roles', $parameters);
    }

    /**
     * Return the related token
     *
     * @param string|array $parameters
     * @return \Phalcon\Mvc\Model\Resultset|Token[]
     */
    public function getToken($parameters = null)
    {
        return $this->getRelated('Token', $parameters);
    }

    /**
     * Return last token
     * @param null $parameters
     * @return Token|bool|\Phalcon\Mvc\ModelInterface
     */
    public function getLastToken($parameters = null)
    {
        return $this->getToken($parameters)->getLast();
    }

    /**
     * Return the related UsersDevices
     *
     * @param string|array $parameters
     * @return UsersDevices[]
     */
    public function getUsersDevices($parameters = null)
    {
        return $this->getRelated('UsersDevices', $parameters);
    }

    /**
     * Return the related UsersCardTokens
     *
     * @param string|array $parameters
     * @return UsersCardTokens[]
     * @throws \Fwok\Exceptions\Exception
     */
    public function getUsersCardTokens($parameters = null)
    {
        return $this->getRelated('UsersCardTokens', $parameters);
    }

    /**
     * Return the related UsersDevicesas Array
     *
     * @param string|array $parameters
     * @return Array
     * @throws \Fwok\Exceptions\Exception
     */
    public function getUsersDevicesAsArray($parameters = null)
    {
        $user_devices = $this->getRelated('UsersDevices', $parameters);
        $return = [];
        foreach ($user_devices as $user_device) {
            /** @var UsersDevices $user_device */
            $return[] = $user_device->getDeviceId();
        }
        return $return;
    }

    /**
     * Return the related UsersSettings
     *
     * @param string|array $parameters
     * @return UsersSettings[]
     */
    public function getUsersSettings($parameters = null)
    {
        return $this->getRelated('UsersSettings', $parameters);
    }

    /**
     * Return the related TeamsUsers
     *
     * @param string|array $parameters
     * @return TeamsUsers[]
     */
    public function getTeamsUsers($parameters = null)
    {
        return $this->getRelated('TeamsUsers', $parameters);
    }


    /**
     * Return the related Business
     *
     * @param string|array $parameters
     * @return Business
     */
    public function getBusiness($parameters = null)
    {
        return $this->getRelated('Business', $parameters);
    }

    public static function findFirstByAuthToken($auth_token)
    {
        $token = Token::findFirstByValue($auth_token);
        if (!$token) {
            return false;
        }

        $user = $token->getUser();
        if (!$user) {
            return false;
        }

        return $user;
    }

    public function afterCreate()
    {
        if (!$this->getReferralCode()) {
            $this->setReferralCode(Format::randomAlphaChar(8) . $this->getId());
            $this->saveOrException();
        }
    }

    /**
     * Add roleUser to user
     *
     * @param $role
     * @param $business_id
     * @return bool
     * @throws \Fwok\Exceptions\InputException
     */
    public function addRole($role, $business_id = null)
    {
        if (!$this->getId()) {
            throw new InputException(InputException::ACTION_INVALID, '', 'Cannot add role to not created user');
        }
        $role = Roles::findFirstByCode($role);
        $role_user = new RolesUsers();
        $role_user->setUserId($this->getId());
        $role_user->setRoleId($role->getId());
        $role_user->setBusinessId($business_id);

        return $role_user->saveOrException();
    }

    /**
     * Remove roleUser from user
     *
     * @param $role
     * @return bool
     * @throws \Fwok\Exceptions\InputException
     */
    public function removeRole($role)
    {
        if (!$this->getId()) {
            throw new InputException(InputException::ACTION_INVALID, '', 'Cannot add role to not created user');
        }

        $role = Roles::findFirstByCode($role);

        $role_user = RolesUsers::findFirstByUserIdAndRoleId($this, $role);
        if ($role_user) {
            return $role_user->deleteOrException();
        }

        return true;
    }

    /**
     * @param null|Validation $validator
     * @return bool
     */
    public function validation($validator = null)
    {
        if(!$validator) {
            $validator = new Validation();
        }

        $validator->add('email', new Validation\Validator\Uniqueness([
                'message' => App::getTranslatedKey('Este email já está registrado'),
            ])
        );

        $validator->add('password', new Validation\Validator\StringLength(
                [
                    'min' => 8,
                    'messageMinimum' => App::t('PASSWORD_MINIMUM_8_CHAR'),
                ]
            ));
        $validator->add(['name', 'email', 'password'], new Validation\Validator\PresenceOf([
                'message' => [
                    'name' => App::t('INVALID_NAME'),
                    'email' => App::t('INVALID_EMAIL'),
                    'password' => App::t('INVALID_PASSWORD'),
                ]]
        ));

        return $this->validate($validator);
    }


    /**
     * @param $email
     * @param $password
     * @return self|bool
     * @throws \Fwok\Exceptions\Exception
     * @throws SystemException
     */
    public static function loginEmail($email, $password)
    {
        /** @var Users $user */
        $user = Users::query()
            ->addWhereBinded('email = ', $email)
            ->executeFirst();

        if ($user) {
            if (App::getSecurity()->checkHash($password, $user->getPassword())) {
                return $user;
            }
        } else {
            // To protect against timing attacks. Regardless of whether a user exists or not, the script will take roughly the same amount as it will always be computing a hash.
            App::getSecurity()->hash(mt_rand());
        }

        return false;
    }

    public
    static function encodePassword($password)
    {
        if (!$password) {
            throw new InputException(InputException::VALUE_INVALID, 'INVALID_PASSWORD');
        }

        return App::getSecurity()->hash($password);
    }


    /**
     * Generates a strong password of N length containing at least one lower case letter,
     * one uppercase letter, one digit, and one special character. The remaining characters
     * in the password are chosen at random from those four sets.
     *
     * The available characters in each set are user friendly - there are no ambiguous
     * characters such as i, l, 1, o, 0, etc. This, coupled with the $add_dashes option,
     * makes it much easier for users to manually type or speak their passwords.
     *
     * Note: the $add_dashes option will increase the length of the password by
     * floor(sqrt(N)) characters.
     * @param int $length
     * @param bool $add_dashes
     * @param string $available_sets
     * @return string
     */
    public
    static function generatePassword($length = 9, $add_dashes = false, $available_sets = 'luds')
    {
        $sets = [];
        if (strpos($available_sets, 'l') !== false) {
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        }
        if (strpos($available_sets, 'u') !== false) {
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        }
        if (strpos($available_sets, 'd') !== false) {
            $sets[] = '23456789';
        }
        if (strpos($available_sets, 's') !== false) {
            $sets[] = '!@#$%&*?';
        }
        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++) {
            $password .= $all[array_rand($all)];
        }
        $password = str_shuffle($password);
        if (!$add_dashes) {
            return $password;
        }
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;

        return $dash_str;
    }


    public
    static function generateRandomCode()
    {
        return sha1(microtime());
    }

    public function export($opt = [])
    {
        $return = [
            'object' => self::OBJECT_NAME,
            'id' => $this->getId(),
            'business_id' => $this->getBusinessId(),
            'suspended' => $this->getSuspended(),
            'name' => $this->getName(),
            'email' => $this->getEmail(),
            'activated' => $this->getActivated(),
            'picture' => $this->getPicture(),
            'mobile' => $this->getMobile(),
            'locale' => $this->getLocale(),
            'timezone' => $this->getTimezone(),
            'code_ical' => $this->getCodeIcal(1),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt(),
            'referral_link' => $this->getReferralLink()
        ];

        if (isset($opt[self::EXPORT_ACCOUNT_INFO]) and $opt[self::EXPORT_ACCOUNT_INFO]) {
            $subscription = $this->getBusiness()->getActiveBusinessSubscription()->getSubscription();

            if ($subscription) {
                $return['subscription'] = $subscription->export($opt);
            }

            if ($this->hasRole(Roles::ADMIN)) {
                $return['account_info'] = $this->getBusiness()->export($opt);
            }
        }

        if (isset($opt[self::EXPORT_PERMISSIONS]) and $opt[self::EXPORT_PERMISSIONS]) {
            foreach (Permission::findAllByUserId($this->getId()) as $permission) {
                $return['permissions'][] = $permission->export($opt);
            }
        }

        foreach ($this->getToken() as $token) {
            $return['auth_token'] = $token->getValue();
        }

        foreach ($this->getUsersCardTokens() as $card) {
            $return['card_tokens'][] = $card->export();
        }

        foreach (Roles::find() as $role) {
            $return['roles'][strtolower($role->getCode())] = $role->export();
            $return['roles'][strtolower($role->getCode())]['userGotRole'] = $this->hasRole($role->getCode());
        }

        return $return;

    }


    /**
     * @param Roles|string $role
     * @param bool $throw_exception
     * @param bool $search_in_business
     * @return bool
     * @throws Exception
     * @throws InputException
     * @throws SystemException
     */
    public
    function hasRole($role, $throw_exception = false, $search_in_business = true)
    {
        $role = Roles::findFirstByCode($role);

        if (!$role) {
            throw new SystemException(SystemException::DATABASE_CONSISTENCY);
        }
        if ($search_in_business and App::getBusiness(false)) {
            $role_to_find = RolesUsers::query()
                ->searchInBusiness()
                ->addWhereBinded('user_id = ', $this->getId())
                ->addWhereBinded('role_id = ', $role->getId())
                ->executeFirst();
        } else {
            $role_to_find = RolesUsers::query()
                ->addWhereBinded('user_id = ', $this->getId())
                ->addWhereBinded('role_id = ', $role->getId())
                ->executeFirst();
        }

        if ($role_to_find) {
            return true;
        }

        if ($throw_exception) {
            $dev_msg = sprintf('User [%s] does not has [%s-%s] role', $this->getId(), $role->getId(), $role->getCode());
            throw new InputException(InputException::PERMISSION_INVALID, null, $dev_msg, 403);
        }
        return false;

    }

    public
    function getPicture()
    {
        if ($this->getGooglePicture()) {
            return $this->getGooglePicture();
        } elseif ($this->getFacebookPicture()) {
            return $this->getFacebookPicture();
        }

        return '';

    }

    public
    function createNewToken()
    {
        $token = new Token();
        $token->setValue(Token::generateToken());
        $token->setUserId($this->getId());
        $token->setDeleted(0);

        $token->saveOrException();

        return $token;
    }

    /**
     *
     * @var integer
     */
    protected
        $id;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public
    function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public
    function getId()
    {
        return $this->id;
    }

    /**
     *
     * @var string
     */
    protected
        $username;

    /**
     * Method to set the value of field username
     *
     * @param string $username
     * @return $this
     */
    public
    function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Returns the value of field username
     *
     * @return string
     */
    public
    function getUsername()
    {
        return $this->username;
    }

    /**
     *
     * @var string
     */
    protected
        $password;

    /**
     * Method to set the value of field password
     *
     * @param string $password
     * @return $this
     */
    public
    function setPassword($password)
    {
        $hashed_password = App::getConfig('application')->hashUserPassword;

        if ($hashed_password) {
            $this->password = App::getSecurity()->hash($password);
        } else {
            $this->password = self::encodePassword($password);
        }


        return $this;
    }

    /**
     * Returns the value of field password
     *
     * @return string
     */
    public
    function getPassword()
    {
        return $this->password;
    }

    /**
     *
     * @var string
     */
    protected
        $name;

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public
    function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public
    function getName()
    {
        return $this->name;
    }

    /**
     *
     * @var string
     */
    protected
        $email;

    /**
     * Method to set the value of field email
     *
     * @param string $email
     * @return $this
     */
    public
    function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Returns the value of field email
     *
     * @return string
     */
    public
    function getEmail()
    {
        return $this->email;
    }

    /**
     *
     * @var string
     */
    protected
        $facebook_id;

    /**
     * Method to set the value of field facebook_id
     *
     * @param string $facebook_id
     * @return $this
     */
    public
    function setFacebookId($facebook_id)
    {
        $this->facebook_id = $facebook_id;
        $this->setFacebookPicture(sprintf("http://graph.facebook.com/%s/picture", $facebook_id));

        return $this;
    }

    /**
     * Returns the value of field facebook_id
     *
     * @return string
     */
    public
    function getFacebookId()
    {
        return $this->facebook_id;
    }

    /**
     *
     * @var string
     */
    protected
        $google_id;

    /**
     * Method to set the value of field google_id
     *
     * @param string $google_id
     * @return $this
     */
    public
    function setGoogleId($google_id)
    {
        $this->google_id = $google_id;

        return $this;
    }

    /**
     * Returns the value of field google_id
     *
     * @return string
     */
    public
    function getGoogleId()
    {
        return $this->google_id;
    }

    /**
     *
     * @var string
     */
    protected
        $code_reset_password;

    /**
     * Method to set the value of field code_reset_password
     *
     * @param string $code_reset_password
     * @return $this
     */
    public
    function setCodeResetPassword($code_reset_password)
    {
        $this->code_reset_password = $code_reset_password;

        return $this;
    }

    /**
     * Returns the value of field code_reset_password
     *
     * @return string
     */
    public
    function getCodeResetPassword()
    {
        return $this->code_reset_password;
    }

    /**
     *
     * @var string
     */
    protected
        $code_activation;

    /**
     * Method to set the value of field code_activation
     *
     * @param string $code_activation
     * @return $this
     */
    public
    function setCodeActivation($code_activation)
    {
        $this->code_activation = $code_activation;

        return $this;
    }

    /**
     * Returns the value of field code_activation
     *
     * @return string
     */
    public
    function getCodeActivation()
    {
        return $this->code_activation;
    }

    /**
     *
     * @var string
     */
    protected
        $locale;

    /**
     * Method to set the value of field locale
     *
     * @param string $locale
     * @return $this
     */
    public
    function setLocale($locale)
    {
        $this->locale = strtolower(str_replace('-', '_', $locale));

        return $this;
    }

    /**
     * Returns the value of field locale
     *
     * @return string
     */
    public
    function getLocale()
    {
        return $this->locale;
    }

    /**
     *
     * @var string
     */
    protected
        $code_ical;

    /**
     * Method to set the value of field code_ical
     *
     * @param string $code_ical
     * @return $this
     */
    public
    function setCodeIcal($code_ical)
    {
        $this->code_ical = $code_ical;

        return $this;
    }

    /**
     * Returns the value of field code_ical
     *
     * @param int $link
     * @return string
     */
    public
    function getCodeIcal($link = 0)
    {
        if ($link) {
            return $this->getService('config')->application->baseHostApi . 'calendar/ical&code=' . $this->code_ical;
        } else {
            return $this->code_ical;
        }
    }

    /**
     *
     * @var int
     */
    protected
        $activated;

    /**
     * Method to set the value of field activated
     *
     * @param int $activated
     * @return $this
     */
    public
    function setActivated($activated)
    {
        $this->activated = $activated;

        return $this;
    }

    /**
     * Returns the value of field activated
     *
     * @return int
     */
    public
    function getActivated()
    {
        return $this->activated + 0;
    }

    /**
     *
     * @var string
     */
    protected
        $google_picture;

    /**
     * Method to set the value of field google_picture
     *
     * @param string $google_picture
     * @return $this
     */
    public
    function setGooglePicture($google_picture)
    {
        $this->google_picture = $google_picture;

        return $this;
    }

    /**
     * Returns the value of field google_picture
     *
     * @return string
     */
    public
    function getGooglePicture()
    {
        return $this->google_picture;
    }

    /**
     *
     * @var string
     */
    protected
        $facebook_picture;

    /**
     * Method to set the value of field facebook_picture
     *
     * @param string $facebook_picture
     * @return $this
     */
    public
    function setFacebookPicture($facebook_picture)
    {
        $this->facebook_picture = $facebook_picture;

        return $this;
    }

    /**
     * Returns the value of field facebook_picture
     *
     * @return string
     */
    public
    function getFacebookPicture()
    {
        return $this->facebook_picture;
    }

    /**
     *
     * @var string
     */
    protected
        $referral_code;

    /**
     * Method to set the value of field referral_code
     *
     * @param string $referral_code
     * @return $this
     */
    public
    function setReferralCode($referral_code)
    {
        $this->referral_code = $referral_code;

        return $this;
    }

    /**
     * Returns the value of field referral_code
     *
     * @return string
     */
    public
    function getReferralCode()
    {
        return $this->referral_code;
    }

    /**
     * Returns the value of field referral link
     *
     * @return string
     */
    public
    function getReferralLink()
    {
        return $this->getService('config')->application->baseHost . $this->referral_code;
    }

    /**
     *
     * @var integer
     */
    protected
        $referral_by;

    /**
     * Method to set the value of field referral_by
     *
     * @param integer $referral_by
     * @return $this
     */
    public
    function setReferralBy($referral_by)
    {
        $this->referral_by = $referral_by;

        return $this;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="timezone",type="string",description="")
     */
    protected
        $timezone;

    /**
     * Method to set the value of field timezone
     *
     * @param string $timezone
     * @return $this
     */
    public
    function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * Returns the value of field timezone
     *
     * @return string
     */
    public
    function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Returns the value of field referral_by
     *
     * @return integer
     */
    public
    function getReferralBy()
    {
        return (int)$this->referral_by;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="position",type="string",description="")
     */
    protected
        $position;

    /**
     * Method to set the value of field position
     *
     * @param string $position
     * @return $this
     */
    public
    function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Returns the value of field position
     *
     * @return string
     */
    public
    function getPosition()
    {
        return $this->position;
    }

    /**
     *
     * @var integer
     * @SWG\Property(name="business_id",type="integer",description="")
     */
    protected
        $business_id;

    /**
     * Method to set the value of field business_id
     *
     * @param integer $business_id
     * @return $this
     */
    public
    function setBusinessId($business_id)
    {
        $this->business_id = $business_id;

        return $this;
    }

    /**
     * Returns the value of field business_id
     *
     * @return integer
     */
    public
    function getBusinessId()
    {
        return (int)$this->business_id;
    }

    /**
     *
     * @var integer
     * @SWG\Property(name="suspended",type="integer",description="")
     */
    protected
        $suspended;

    /**
     * Method to set the value of field suspended
     *
     * @param integer $suspended
     * @return $this
     */
    public
    function setSuspended($suspended)
    {
        $this->suspended = $suspended;

        return $this;
    }

    /**
     * Returns the value of field suspended
     *
     * @return integer
     */
    public
    function getSuspended()
    {
        return (int)$this->suspended;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="personal_code",type="string",description="")
     */
    protected
        $personal_code;

    /**
     * Method to set the value of field personal_code
     *
     * @param string $personal_code
     * @return $this
     */
    public
    function setPersonalCode($personal_code)
    {
        $this->personal_code = $personal_code;

        return $this;
    }

    /**
     * Returns the value of field personal_code
     *
     * @return string
     */
    public
    function getPersonalCode()
    {
        return $this->personal_code;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="iugu_id",type="string",description="")
     */
    protected
        $iugu_id;

    /**
     * Method to set the value of field iugu_id
     *
     * @param string $iugu_id
     * @return $this
     */
    public
    function setIuguId($iugu_id)
    {
        $this->iugu_id = $iugu_id;

        return $this;
    }

    /**
     * Returns the value of field iugu_id
     *
     * @return string
     */
    public
    function getIuguId()
    {
        return $this->iugu_id;
    }

    /**
     *
     * @var integer
     * @SWG\Property(name="mobile",type="integer",description="")
     */
    protected
        $mobile;

    /**
     * Method to set the value of field mobile
     *
     * @param integer $mobile
     * @return $this
     */
    public
    function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Returns the value of field mobile
     *
     * @return integer
     */
    public
    function getMobile()
    {
        return $this->mobile;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="document",type="string",description="")
     */
    protected $document;

    /**
     * Method to set the value of field document
     *
     * @param string $document
     * @return $this
     */
    public function setDocument($document)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * Returns the value of field document
     *
     * @return string
     */
    public function getDocument()
    {
        return $this->document;
    }
}
