<?php

namespace Fwok\Models;

use Fwok\Models\Behaviors\Auditable;
use Phalcon\Mvc\Model\Behavior\SoftDelete;
use Phalcon\Mvc\Model\Resultset;

class Nfe extends ModelBase
{
    const OBJECT_NAME = 'nfes';
    const ICON = 'fa fa-file-text-o fa-fw';

    const VENDOR_WEBMANIA = 'webmania';

    public function initialize()
    {
        $this->setSource('fwok_nfe');
        $this->keepSnapshots(true);
        $this->addBehavior(new Auditable());
        $this->addBehavior(new SoftDelete(['field' => 'deleted', 'value' => 1]));
    }

    public function validation()
    {
        return !$this->validationHasFailed();
    }

    public function export()
    {
        $ret = [
            'object' => self::OBJECT_NAME,
            'id' => $this->getId(),
            'chave' => $this->getChave(),
            'danfe' => $this->getDanfe(),
            'statuss' => $this->getStatus(),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt(),
        ];

        return $ret;
    }

    /**
     *
     * @var integer
     * @SWG\Property(name="user_id",type="integer",description="")
     */
    protected $user_id;

    /**
     * Method to set the value of field user_id
     *
     * @param integer $user_id
     * @return $this
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Returns the value of field user_id
     *
     * @return integer
     */
    public function getUserId()
    {
        return (int)$this->user_id;
    }

    /**
     *
     * @var integer
     * @SWG\Property(name="business_id",type="integer",description="")
     */
    protected $business_id;

    /**
     * Method to set the value of field business_id
     *
     * @param integer $business_id
     * @return $this
     */
    public function setBusinessId($business_id)
    {
        $this->business_id = $business_id;

        return $this;
    }

    /**
     * Returns the value of field business_id
     *
     * @return integer
     */
    public function getBusinessId()
    {
        return (int)$this->business_id;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="object_type",type="string",description="")
     */
    protected $object_type;

    /**
     * Method to set the value of field object_type
     *
     * @param string $object_type
     * @return $this
     */
    public function setObjectType($object_type)
    {
        $this->object_type = $object_type;

        return $this;
    }

    /**
     * Returns the value of field object_type
     *
     * @return string
     */
    public function getObjectType()
    {
        return $this->object_type;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="object_id",type="string",description="")
     */
    protected $object_id;

    /**
     * Method to set the value of field object_id
     *
     * @param string $object_id
     * @return $this
     */
    public function setObjectId($object_id)
    {
        $this->object_id = $object_id;

        return $this;
    }

    /**
     * Returns the value of field object_id
     *
     * @return string
     */
    public function getObjectId()
    {
        return $this->object_id;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="status",type="string",description="")
     */
    protected $status;

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="vendor",type="string",description="")
     */
    protected $vendor;

    /**
     * Method to set the value of field vendor
     *
     * @param string $vendor
     * @return $this
     */
    public function setVendor($vendor)
    {
        $this->vendor = $vendor;

        return $this;
    }

    /**
     * Returns the value of field vendor
     *
     * @return string
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="nfe",type="string",description="")
     */
    protected $nfe;

    /**
     * Method to set the value of field nfe
     *
     * @param string $nfe
     * @return $this
     */
    public function setNfe($nfe)
    {
        $this->nfe = $nfe;

        return $this;
    }

    /**
     * Returns the value of field nfe
     *
     * @return string
     */
    public function getNfe()
    {
        return $this->nfe;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="serie",type="string",description="")
     */
    protected $serie;

    /**
     * Method to set the value of field serie
     *
     * @param string $serie
     * @return $this
     */
    public function setSerie($serie)
    {
        $this->serie = $serie;

        return $this;
    }

    /**
     * Returns the value of field serie
     *
     * @return string
     */
    public function getSerie()
    {
        return $this->serie;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="recibo",type="string",description="")
     */
    protected $recibo;

    /**
     * Method to set the value of field recibo
     *
     * @param string $recibo
     * @return $this
     */
    public function setRecibo($recibo)
    {
        $this->recibo = $recibo;

        return $this;
    }

    /**
     * Returns the value of field recibo
     *
     * @return string
     */
    public function getRecibo()
    {
        return $this->recibo;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="chave",type="string",description="")
     */
    protected $chave;

    /**
     * Method to set the value of field chave
     *
     * @param string $chave
     * @return $this
     */
    public function setChave($chave)
    {
        $this->chave = $chave;

        return $this;
    }

    /**
     * Returns the value of field chave
     *
     * @return string
     */
    public function getChave()
    {
        return $this->chave;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="xml",type="string",description="")
     */
    protected $xml;

    /**
     * Method to set the value of field xml
     *
     * @param string $xml
     * @return $this
     */
    public function setXml($xml)
    {
        $this->xml = $xml;

        return $this;
    }

    /**
     * Returns the value of field xml
     *
     * @return string
     */
    public function getXml()
    {
        return $this->xml;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="danfe",type="string",description="")
     */
    protected $danfe;

    /**
     * Method to set the value of field danfe
     *
     * @param string $danfe
     * @return $this
     */
    public function setDanfe($danfe)
    {
        $this->danfe = $danfe;

        return $this;
    }

    /**
     * Returns the value of field danfe
     *
     * @return string
     */
    public function getDanfe()
    {
        return $this->danfe;
    }
}
