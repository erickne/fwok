<?php

namespace Fwok\Models;

class AuditDetail extends ModelBase
{

    public function initialize()
    {
        $this->belongsTo('audit_id', '\\Fwok\\Models\\Audit', 'id', [ 'alias' => 'Audit' ]);
        $this->setSource('fwok_audit_detail');
    }

    /**
     *
     * @var integer
     */
    protected $audit_id;

    /**
     * Method to set the value of field audit_id
     *
     * @param integer $audit_id
     * @return $this
     */
    public function setAuditId($audit_id)
    {
        $this->audit_id = $audit_id;

        return $this;
    }

    /**
     * Returns the value of field audit_id
     *
     * @return integer
     */
    public function getAuditId()
    {
        return $this->audit_id;
    }

    /**
     *
     * @var string
     */
    protected $field_name;

    /**
     * Method to set the value of field field_name
     *
     * @param string $field_name
     * @return $this
     */
    public function setFieldName($field_name)
    {
        $this->field_name = $field_name;

        return $this;
    }

    /**
     * Returns the value of field field_name
     *
     * @return string
     */
    public function getFieldName()
    {
        return $this->field_name;
    }

    /**
     *
     * @var string
     */
    protected $old_value;

    /**
     * Method to set the value of field old_value
     *
     * @param string $old_value
     * @return $this
     */
    public function setOldValue($old_value)
    {
        $this->old_value = $old_value;

        return $this;
    }

    /**
     * Returns the value of field old_value
     *
     * @return string
     */
    public function getOldValue()
    {
        return $this->old_value;
    }

    /**
     *
     * @var string
     */
    protected $new_value;

    /**
     * Method to set the value of field new_value
     *
     * @param string $new_value
     * @return $this
     */
    public function setNewValue($new_value)
    {
        $this->new_value = $new_value;

        return $this;
    }

    /**
     * Returns the value of field new_value
     *
     * @return string
     */
    public function getNewValue()
    {
        return $this->new_value;
    }

}