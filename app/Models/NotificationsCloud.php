<?php
namespace Fwok\Models;

use Phalcon\Mvc\Model;

class NotificationsCloud extends ModelBase
{
    const OBJECT_NAME = 'cloud-notifications';
    const SEARCH_DELETED = 0;

    public function initialize()
    {
        $this->setSource('fwok_notifications_cloud');

        $this->hasOne(
            'users_device_id',
            '\Fwok\Models\UsersDevices',
            'id',
            [
                'alias'    => 'UsersDevices',
                'reusable' => true,
            ]
        );
        $this->hasOne(
            'request_curl_id',
            '\Fwok\Models\RequestsCurl',
            'id',
            [
                'alias'    => 'RequestCurl',
                'reusable' => true,
            ]
        );

        $this->addBehavior(new \Fwok\Models\Behaviors\SoftDelete([ 'field' => 'deleted', 'value' => 1 ]));
    }

    /**
     * Return the related "UsersDevices"
     *
     * @param array|string $parameters
     * @param int $exception_if_not_found
     * @throws \Fwok\Exceptions\Exception
     * @return \Fwok\Models\UsersDevices
     */
    public function getUsersDevices($parameters = null, $exception_if_not_found = 0)
    {
        return $this->getRelated('UsersDevices', $parameters, $exception_if_not_found);
    }

    /**
     * Return the related "RequestsCurl"
     *
     * @param array|string $parameters
     * @param int $exception_if_not_found
     * @throws \Fwok\Exceptions\Exception
     * @return \Fwok\Models\RequestsCurl
     */
    public function getRequestCurl($parameters = null, $exception_if_not_found = 0)
    {
        return $this->getRelated('RequestCurl', $parameters, $exception_if_not_found);
    }

    public function export($opt)
    {
        $return = array_merge(
            parent::export(),
            [
                'object'          => self::OBJECT_NAME,
                'request_curl_id' => $this->getRequestCurlId(),
                'request_curl'    => $this->getRequestCurl()->export(),
            ]
        );

        if ($this->getUsersDevices()) {
            $return['users_device'] = $this->getUsersDevices()->export();
        }

        return $return;
    }

    /**
     *
     * @var integer
     */
    protected $users_device_id;

    /**
     * Method to set the value of field users_device_id
     *
     * @param integer $users_device_id
     * @return $this
     */
    public function setUsersDeviceId($users_device_id)
    {
        $this->users_device_id = $users_device_id;

        return $this;
    }

    /**
     * Returns the value of field users_device_id
     *
     * @return integer
     */
    public function getUsersDeviceId()
    {
        return (int) $this->users_device_id;
    }

    /**
     *
     * @var integer
     */
    protected $request_curl_id;

    /**
     * Method to set the value of field request_curl_id
     *
     * @param integer $request_curl_id
     * @return $this
     */
    public function setRequestCurlId($request_curl_id)
    {
        $this->request_curl_id = $request_curl_id;

        return $this;
    }

    /**
     * Returns the value of field request_curl_id
     *
     * @return integer
     */
    public function getRequestCurlId()
    {
        return (int) $this->request_curl_id;
    }

    /**
     *
     * @var string
     */
    protected $message;

    /**
     * Method to set the value of field message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Returns the value of field message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     *
     * @var string
     */
    protected $returned_message;

    /**
     * Method to set the value of field returned_message
     *
     * @param string $returned_message
     * @return $this
     */
    public function setReturnedMessage($returned_message)
    {
        $this->returned_message = $returned_message;

        return $this;
    }

    /**
     * Returns the value of field returned_message
     *
     * @return string
     */
    public function getReturnedMessage()
    {
        return $this->returned_message;
    }

    /**
     *
     * @var string
     */
    protected $returned_error;

    /**
     * Method to set the value of field returned_error
     *
     * @param string $returned_error
     * @return $this
     */
    public function setReturnedError($returned_error)
    {
        $this->returned_error = $returned_error;

        return $this;
    }

    /**
     * Returns the value of field returned_error
     *
     * @return string
     */
    public function getReturnedError()
    {
        return $this->returned_error;
    }
}
