<?php

namespace Fwok\Models;

use Fwok\Models\Behaviors\Auditable;
use Phalcon\Mvc\Model\Behavior\SoftDelete;
use Phalcon\Mvc\Model\Resultset;


class UsersSettings extends ModelBase
{
    const OBJECT_NAME = 'users-settings';
    const ICON = 'fa fa-tag fa-fw';

    public function initialize()
    {
        $this->setSource('fwok_users_settings');
        $this->keepSnapshots(true);
        $this->addBehavior(new Auditable());
        $this->addBehavior(new SoftDelete(['field' => 'deleted', 'value' => 1]));
    }


    public function export()
    {
        $return = [
            'object' => UsersSettings::OBJECT_NAME,
            'id' => $this->getId(),
            'user_id' => $this->getUserId(),
            'type' => $this->getType(),
            'key' => $this->getKey(),
            'value' => $this->getValue(),
            'name' => $this->getName(),
            'deleted' => $this->getDeleted(),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt(),

        ];

//        if ($this->getColorSet()) {
//            $ret['color_set'] = $this->getColorSet()->export();
//        }

        return $return;

    }


    public function validation()
    {
//        if (!$this->getName()) {
//            $this->appendErrorMessageTranslated('INVALID_NAME');
//        }

        return !$this->validationHasFailed();
    }


    /**
     *
     * @var integer
     * @SWG\Property(name="user_id",type="integer",description="")
     */
    protected $user_id;

    /**
     * Method to set the value of field user_id
     *
     * @param integer $user_id
     * @return $this
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Returns the value of field user_id
     *
     * @return integer
     */
    public function getUserId()
    {
        return (int)$this->user_id;
    }

    /**
     *
     * @var array
     * @SWG\Property(name="value",type="array",description="")
     */
    protected $value;

    /**
     * Method to set the value of field value
     *
     * @param array $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = json_encode($value);

        return $this;
    }

    /**
     * Returns the value of field value
     *
     * @return array
     */
    public function getValue()
    {
        return json_decode($this->value);
    }

    /**
     *
     * @var string
     * @SWG\Property(name="key",type="string",description="")
     */
    protected $key;

    /**
     * Method to set the value of field key
     *
     * @param string $key
     * @return $this
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Returns the value of field key
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="name",type="string",description="")
     */
    protected $name;

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     *
     * @var string
     * @SWG\Property(name="type",type="string",description="")
     */
    protected $type;

    /**
     * Method to set the value of field type
     *
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Returns the value of field type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}

