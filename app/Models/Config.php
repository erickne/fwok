<?php

namespace Fwok\Models;

use Fwok\Exceptions\SystemException;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Config extends ModelBase
{
    const OBJECT_NAME = 'config';

    const GROUP_FRONTEND = 'frontend';
    const GROUP_BACKEND = 'backend';
    const GROUP_INTEGRATION = 'integration';
    const GROUP_ANDROID = 'android';
    const GROUP_IOS = 'ios';

    const KEY_FREE_SUBSCRIPTION_ID = 'FREE_SUBSCRIPTION_ID';
    const KEY_INITIAL_SUBSCRIPTION_ID = 'INITIAL_SUBSCRIPTION_ID';

    /**
     * @param $key
     * @param bool $exception_if_not_found
     * @return $this
     * @throws \Fwok\Exceptions\SystemException
     */
    public static function findFirstByKey($key, $exception_if_not_found = true)
    {
        $config = self::findFirst([ 'key = :key:', 'bind' => [ 'key' => $key ] ]);
        if ($config) {
            return $config;
        }
        if ($exception_if_not_found) {
            $msg = sprintf('Could not find Config key [%s]', $key);
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, $msg);
        }

        return false;
    }

    public function export($opt)
    {
        $return = [
            'id'         => $this->getId(),
            'object'     => self::OBJECT_NAME,
            'key'        => $this->getKey(),
            'value'      => $this->getValue(),
            'group'      => $this->getGroup(),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt(),
        ];

        return $return;
    }

    public function initialize()
    {
        $this->setSource('fwok_config');

        $this->addBehavior(new \Fwok\Models\Behaviors\Auditable());
    }

    public function validation()
    {
        if (!$this->getKey()) {
            $this->appendErrorMessageTranslated('INVALID_KEY');
        }

        $this->validate(
            new Uniqueness(
                [
                    'field'   => 'key',
                    'message' => 'CONFIG_KEY_ALREADY_EXISTS'
                ]
            )
        );

        return !$this->validationHasFailed();
    }

    /**
     *
     * @var string
     * @SWG\Property(name="key",type="string",description="")
     */
    protected $key;

    /**
     * Method to set the value of field key
     *
     * @param string $key
     * @return $this
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Returns the value of field key
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="value",type="string",description="")
     */
    protected $value;

    /**
     * Method to set the value of field value
     *
     * @param string $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Returns the value of field value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     *
     * @var integer
     * @SWG\Property(name="business_id",type="integer",description="")
     */
    protected $business_id;

    /**
     * Method to set the value of field business_id
     *
     * @param integer $business_id
     * @return $this
     */
    public function setBusinessId($business_id)
    {
        $this->business_id = $business_id;

        return $this;
    }

    /**
     * Returns the value of field business_id
     *
     * @return integer
     */
    public function getBusinessId()
    {
        return (int) $this->business_id;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="group",type="string",description="")
     */
    protected $group;

    /**
     * Method to set the value of field group
     *
     * @param string $group
     * @return $this
     */
    public function setGroup($group)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Returns the value of field group
     *
     * @return string
     */
    public function getGroup()
    {
        return $this->group;
    }
}
