<?php
namespace Fwok\Models;

class Trace extends ModelBase
{
    const OBJECT_NAME = 'traces';

    public function export($o = [])
    {
        $return = [
            'id'            => $this->getId(),
            'object'        => self::OBJECT_NAME,
            'agent'         => $this->getAgent(),
            'client_ip'     => $this->getClientIp(),
            'proxy'         => $this->getProxyIp(),
            'endpoint'      => $this->getEndpoint(),
            'has_exception' => !!$this->getException(),
            'has_response'  => !!$this->getResponse(),
            'exception'     => $this->getException(),
            'method'        => $this->getMethod(),
            'response'      => json_decode($this->getResponse()),
            'envelopeMeta'      => json_decode($this->getEnvelopeMeta()),
            'var_get'       => json_decode($this->getVarGet()),
            'var_post'      => json_decode($this->getVarPost()),
            'var_body'      => json_decode($this->getVarBody()),
            'response_code' => $this->getResponseCode(),
            'token_id'      => $this->getTokenId(),
            'version'       => $this->getVersion(),
            'user_id'       => $this->getUserId(),
            'created_at'    => $this->getCreatedAt(),
            'updated_at'    => $this->getUpdatedAt(),
        ];

        return $return;
    }

    public function initialize()
    {
        $this->setSource('fwok_trace');
    }

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @var string
     */
    protected $endpoint;

    /**
     * Method to set the value of field endpoint
     *
     * @param string $endpoint
     * @return $this
     */
    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;

        return $this;
    }

    /**
     * Returns the value of field endpoint
     *
     * @return string
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     *
     * @var string
     */
    protected $method;

    /**
     * Method to set the value of field method
     *
     * @param string $method
     * @return $this
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Returns the value of field method
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     *
     * @var string
     */
    protected $response;

    /**
     * Method to set the value of field response
     *
     * @param string $response
     * @return $this
     */
    public function setResponse($response)
    {
        $this->response = json_encode($response);

        return $this;
    }

    /**
     * Returns the value of field response
     *
     * @return string
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     *
     * @var string
     */
    protected $var_body;

    /**
     * Method to set the value of field var_body
     *
     * @param string $var_body
     * @return $this
     */
    public function setVarBody($var_body)
    {
        $this->var_body = $var_body;

        return $this;
    }

    /**
     * Returns the value of field var_body
     *
     * @return string
     */
    public function getVarBody()
    {
        return $this->var_body;
    }

    /**
     *
     * @var string
     */
    protected $var_get;

    /**
     * Method to set the value of field var_get
     *
     * @param string $var_get
     * @return $this
     */
    public function setVarGet($var_get)
    {
        $this->var_get = json_encode($var_get);

        return $this;
    }

    /**
     * Returns the value of field var_get
     *
     * @return string
     */
    public function getVarGet()
    {
        return $this->var_get;
    }

    /**
     *
     * @var string
     */
    protected $var_post;

    /**
     * Method to set the value of field var_post
     *
     * @param string $var_post
     * @return $this
     */
    public function setVarPost($var_post)
    {
        $this->var_post = json_encode($var_post);

        return $this;
    }

    /**
     * Returns the value of field var_post
     *
     * @return string
     */
    public function getVarPost()
    {
        return $this->var_post;
    }

    /**
     *
     * @var string
     */
    protected $exception;

    /**
     * Method to set the value of field exception
     *
     * @param string $exception
     * @return $this
     */
    public function setException($exception)
    {
        $this->exception = json_encode($exception);

        return $this;
    }

    /**
     * Returns the value of field exception
     *
     * @return string
     */
    public function getException()
    {
        return $this->exception;
    }

    /**
     *
     * @var string
     */
    protected $proxy_ip;

    /**
     * Method to set the value of field proxy_ip
     *
     * @param string $proxy_ip
     * @return $this
     */
    public function setProxyIp($proxy_ip)
    {
        $this->proxy_ip = $proxy_ip;

        return $this;
    }

    /**
     * Returns the value of field proxy_ip
     *
     * @return string
     */
    public function getProxyIp()
    {
        return $this->proxy_ip;
    }

    /**
     *
     * @var string
     */
    protected $client_ip;

    /**
     * Method to set the value of field client_ip
     *
     * @param string $client_ip
     * @return $this
     */
    public function setClientIp($client_ip)
    {
        $this->client_ip = $client_ip;

        return $this;
    }

    /**
     * Returns the value of field client_ip
     *
     * @return string
     */
    public function getClientIp()
    {
        return $this->client_ip;
    }

    /**
     *
     * @var string
     */
    protected $agent;

    /**
     * Method to set the value of field agent
     *
     * @param string $agent
     * @return $this
     */
    public function setAgent($agent)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Returns the value of field agent
     *
     * @return string
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     *
     * @var integer
     */
    protected $response_code;

    /**
     * Method to set the value of field response_code
     *
     * @param integer|string $response_code
     * @return $this
     */
    public function setResponseCode($response_code)
    {
        $this->response_code = (int) $response_code;

        return $this;
    }

    /**
     * Returns the value of field response_code
     *
     * @return integer
     */
    public function getResponseCode()
    {
        return (int) $this->response_code;
    }

    /**
     *
     * @var integer
     */
    protected $user_id;

    /**
     * Method to set the value of field user_id
     *
     * @param integer $user_id
     * @return $this
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Returns the value of field user_id
     *
     * @return integer
     */
    public function getUserId()
    {
        return (int) $this->user_id;
    }

    /**
     *
     * @var string
     */
    protected $version;

    /**
     * Method to set the value of field version
     *
     * @param string $version
     * @return $this
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Returns the value of field version
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     *
     * @var integer
     */
    protected $token_id;

    /**
     * Method to set the value of field token_id
     *
     * @param integer $token_id
     * @return $this
     */
    public function setTokenId($token_id)
    {
        $this->token_id = $token_id;

        return $this;
    }

    /**
     * Returns the value of field token_id
     *
     * @return integer
     */
    public function getTokenId()
    {
        return (int) $this->token_id;
    }
    /**
     *
     * @var string
     * @SWG\Property(name="envelope_meta",type="string",description="")
     */
    protected $envelope_meta;

    /**
     * Method to set the value of field envelope_meta
     *
     * @param string $envelope_meta
     * @return $this
     */
    public function setEnvelopeMeta($envelope_meta)
    {
        $this->envelope_meta = json_encode($envelope_meta);

        return $this;
    }

    /**
     * Returns the value of field envelope_meta
     *
     * @return string
     */
    public function getEnvelopeMeta()
    {
        return  $this->envelope_meta;
    }
}
