<?php
namespace Fwok\Models;

use Fwok\Application\App;
use Fwok\Application\ApplicationBase;
use Phalcon\Mvc\Model;
use Phalcon\Validation;

class UsersDevices extends ModelBase
{
    const OBJECT_NAME = 'usersdevices';
    const SEARCH_DELETED = 0;

    public function initialize()
    {
        $this->setSource('fwok_users_devices');

        $this->hasOne(
            'user_id',
            '\Fwok\Models\Users',
            'id',
            [
                'alias'    => 'User',
                'reusable' => true,
            ]
        );

        $this->addBehavior(new \Fwok\Models\Behaviors\SoftDelete([ 'field' => 'deleted', 'value' => 1 ]));
    }

    /**
     * Return the related "User"
     *
     * @param array|string $parameters
     * @param int $exception_if_not_found
     * @throws \Fwok\Exceptions\Exception
     * @return \Fwok\Models\Subscriptions
     */
    public function getUser($parameters = null, $exception_if_not_found = 0)
    {
        return $this->getRelated('User', $parameters, $exception_if_not_found);
    }

    /**
     * @param int $active
     * @return $this[]
     * @throws \Fwok\Exceptions\Exception
     */
    public static function findUsersDevices($active = 1)
    {
        return UsersDevices::find(
            [
                'user_id = :user_id: AND deleted = 0 and active = 1',
                'bind' => [
                    'user_id' => ApplicationBase::getUser()->getId(),
                ]
            ]
        );
    }

    /**
     * @param null $parameter
     * @return $this
     */
    public static function findFirstByKey($parameter = null)
    {
        return parent::findFirstByKey($parameter);
    }

    public function validation()
    {
        $validation = new Validation();

        $validation->add(['user_id', 'device_id'], new Validation\Validator\PresenceOf([
                'message' => [
                    'user_id' => App::t('Usuário inválido'),
                    'device_id' => App::t('Device ID inválido'),
                ]]
        ));

        $validation->add('device_id', new Validation\Validator\Uniqueness([
                'message' => App::getTranslatedKey('Este dispositivo já está registrado'),
            ])
        );

        return $this->validate($validation);
    }

    public function export()
    {
        return [
            'object'     => self::OBJECT_NAME,
            'id'         => $this->getId(),
            'device_id'  => $this->getDeviceId(),
            'deleted'    => $this->getDeleted(),
            'vendor'     => $this->getVendor(),
            'name'       => $this->getName(),
            'active'     => $this->getActive(),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt(),
        ];
    }

    /**
     *
     * @var string
     */
    protected $vendor;

    /**
     * Method to set the value of field vendor
     *
     * @param string $vendor
     * @return $this
     */
    public function setVendor($vendor)
    {
        $this->vendor = $vendor;

        return $this;
    }

    /**
     * Returns the value of field vendor
     *
     * @return string
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     *
     * @var string
     */
    protected $name;

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     *
     * @var string
     */
    protected $device_id;

    /**
     * Method to set the value of field device_id
     *
     * @param string $device_id
     * @return $this
     */
    public function setDeviceId($device_id)
    {
        $this->device_id = $device_id;

        return $this;
    }

    /**
     * Returns the value of field device_id
     *
     * @return string
     */
    public function getDeviceId()
    {
        return $this->device_id;
    }

    /**
     *
     * @var integer
     */
    protected $user_id;

    /**
     * Method to set the value of field user_id
     *
     * @param integer $user_id
     * @return $this
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Returns the value of field user_id
     *
     * @return integer
     */
    public function getUserId()
    {
        return (int) $this->user_id;
    }

    /**
     *
     * @var integer
     */
    protected $active;

    /**
     * Method to set the value of field active
     *
     * @param integer $active
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Returns the value of field active
     *
     * @return integer
     */
    public function getActive()
    {
        return (int) $this->active;
    }
    
}
