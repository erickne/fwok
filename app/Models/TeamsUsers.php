<?php

namespace Fwok\Models;

use Phalcon\Mvc\Model\Validator\Uniqueness;

class TeamsUsers extends ModelBase
{
    const OBJECT_NAME = 'teamsusers';

    const EXPORT_TEAM = 'export_team';
    const EXPORT_USER = 'export_user';

    public function initialize()
    {
        $this->setSource('fwok_teams_users');
        $this->hasOne('team_id', '\Fwok\Models\Teams', 'id', [ 'alias' => 'Team', 'reusable' => true, ]);
        $this->hasOne('user_id', '\Fwok\Models\Users', 'id', [ 'alias' => 'User', 'reusable' => true, ]);
        $this->addBehavior(new \Fwok\Models\Behaviors\SoftDelete([ 'field' => 'deleted', 'value' => 1 ]));
    }

    public function export($opt = [ ])
    {
        $ret = [
            'object'      => self::OBJECT_NAME,
            'id'          => $this->getId(),
            'leader'      => $this->getLeader(),
            'team_id'     => $this->getTeamId(),
            'user_id'     => $this->getUserId(),
            'description' => $this->getDescription(),
            'created_at'  => $this->getCreatedAt(),
            'updated_at'  => $this->getUpdatedAt(),
        ];


        if ($opt[self::EXPORT_USER]) {
            $ret['user'] = $this->getUser()->export($opt);
        }

        if ($opt[self::EXPORT_TEAM]) {
            $ret['team'] = $this->getTeam()->export($opt);
        }

        return $ret;
    }

    /**
     * Return the related "teams"
     *
     * @param null|string|array $parameters
     * @param int $exception_if_not_found
     * @throws \Fwok\Exceptions\Exception
     * @return \Fwok\Models\Teams
     */
    public function getTeam($parameters = null, $exception_if_not_found = 0)
    {
        return $this->getRelated('Team', $parameters, $exception_if_not_found);
    }

    /**
     * Return the related "users"
     *
     * @param null|string|array $parameters
     * @param int $exception_if_not_found
     * @throws \Fwok\Exceptions\Exception
     * @return \Fwok\Models\Users
     */
    public function getUser($parameters = null, $exception_if_not_found = 0)
    {
        return $this->getRelated('User', $parameters, $exception_if_not_found);
    }

    public function validation()
    {
        // Verifica se o usuário já existe na base
        // caso não ele não possua a propriedade deleted (onde ele está sendo excluído)
        // Lembrando: o softDelete faz uma operação de update e não delete
        if (!$this->getDeleted()) {
            $this->validate(
                new Uniqueness(
                    [
                        'field'   => [ 'user_id', 'team_id', 'deleted' ],
                        'message' => 'Este funcionário já pertence a este time'
                    ]
                )
            );
        }

        return !$this->validationHasFailed();
    }

    /**
     *
     * @var integer
     */
    protected $team_id;

    /**
     * Method to set the value of field team_id
     *
     * @param integer $team_id
     * @return $this
     */
    public function setTeamId($team_id)
    {
        $this->team_id = $team_id;

        return $this;
    }

    /**
     * Returns the value of field team_id
     *
     * @return integer
     */
    public function getTeamId()
    {
        return (int) $this->team_id;
    }

    /**
     *
     * @var integer
     */
    protected $user_id;

    /**
     * Method to set the value of field user_id
     *
     * @param integer $user_id
     * @return $this
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Returns the value of field user_id
     *
     * @return integer
     */
    public function getUserId()
    {
        return (int) $this->user_id;
    }

    /**
     *
     * @var integer
     */
    protected $leader;

    /**
     * Method to set the value of field leader
     *
     * @param integer $leader
     * @return $this
     */
    public function setLeader($leader)
    {
        $this->leader = $leader;

        return $this;
    }

    /**
     * Returns the value of field leader
     *
     * @return integer
     */
    public function getLeader()
    {
        return (int) $this->leader;
    }

    /**
     *
     * @var string
     */
    protected $description;

    /**
     * Method to set the value of field description
     *
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Returns the value of field description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}
