<?php

namespace Fwok\Models;

class ColorSet extends ModelBase
{
    const OBJECT_NAME = 'colorset';


    public static $color_sets = null;
    /**
     *
     * @var string
     */
    protected $name;
    /**
     *
     * @var string
     */
    protected $front_color;
    /**
     *
     * @var string
     */
    protected $back_color;
    /**
     *
     * @var integer
     */
    protected $system;

    public static function findFirst($params = null)
    {
        $id = $params['bind']['APR0'];
        if (!isset(self::$color_sets[$id])) {
            $id = 47;
        }
        $color = new ColorSet();
        $color->setId(self::$color_sets[$id][0]);
        $color->setName(self::$color_sets[$id][1]);
        $color->setFrontColor(self::$color_sets[$id][2]);
        $color->setBackColor(self::$color_sets[$id][3]);
        $color->setSystem(self::$color_sets[$id][4]);
        $color->setDeleted(self::$color_sets[$id][5]);

        return $color;
    }

    /**
     * @param null $params
     * @return self[]
     */
    public static function find($params = null)
    {
        $return = [ ];
        foreach (self::$color_sets as $color_set) {
            $color = new ColorSet();
            $color->setId($color_set[0]);
            $color->setName($color_set[1]);
            $color->setFrontColor($color_set[2]);
            $color->setBackColor($color_set[3]);
            $color->setSystem($color_set[4]);
            $color->setDeleted($color_set[5]);
            $return[] = $color;
        }

        unset($return[0]);

        return $return;

    }

    public function initialize()
    {

    }

    public function export($o = [])
    {
        $ret = [
            'object'      => ColorSet::OBJECT_NAME,
            'id'          => $this->getId(),
            'name'        => $this->getName(),
            'front_color' => $this->getFrontColor(),
            'back_color'  => $this->getBackColor(),
            'system'      => $this->getSystem(),
            'created_at'  => $this->getCreatedAt(),
            'updated_at'  => $this->getUpdatedAt(),
        ];

        return $ret;

    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Returns the value of field front_color
     *
     * @return string
     */
    public function getFrontColor()
    {
        return $this->front_color;
    }

    /**
     * Method to set the value of field front_color
     *
     * @param string $front_color
     * @return $this
     */
    public function setFrontColor($front_color)
    {
        $this->front_color = $front_color;

        return $this;
    }

    /**
     * Returns the value of field back_color
     *
     * @return string
     */
    public function getBackColor()
    {
        return $this->back_color;
    }

    /**
     * Method to set the value of field back_color
     *
     * @param string $back_color
     * @return $this
     */
    public function setBackColor($back_color)
    {
        $this->back_color = $back_color;

        return $this;
    }

    /**
     * Returns the value of field system
     *
     * @return integer
     */
    public function getSystem()
    {
        return $this->system;
    }

    /**
     * Method to set the value of field system
     *
     * @param integer $system
     * @return $this
     */
    public function setSystem($system)
    {
        $this->system = $system;

        return $this;
    }

    public function validation()
    {
        return true;
    }
}
