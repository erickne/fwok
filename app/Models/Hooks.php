<?php

namespace Fwok\Models;

class Hooks extends ModelBase
{
    const OBJECT_NAME = 'hooks';

    const METHOD_PAYPAL = 'paypal';
    const METHOD_PAGSEGURO = 'pagseguro';
    const METHOD_ANDROIDPAY = 'androidpay';

    public function initialize()
    {
        $this->setSource('fwok_hooks');
    }

    public function export()
    {
        $return = [
            'id'         => $this->getId(),
            'object'     => self::OBJECT_NAME,
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt(),
            'method'     => $this->getMethod(),
            'type'       => $this->getType(),
            'code'       => $this->getCode(),
            'validated'  => $this->getValidated(),
            'processed'  => $this->getProcessed(),
            'response'   => $this->getResponse(),
            'get_data'   => $this->getGetData(),
            'post_data'  => $this->getPostData(),
        ];

        return $return;
    }

    /**
     * Verify if received data is real
     *
     * https://developer.paypal.com/docs/classic/ipn/integration-guide/IPNIntro/
     */
    public function verifyPaypal()
    {
        $data = json_decode($this->getPostData(), 1);

        $request = file_get_contents(
            'https://www.paypal.com/cgi-bin/webscr?cmd=_notify-validate&' . implode('&', $data)
        );
        if ($request == 'VERIFIED') {
            $this->setValidated(1);
        } elseif ($request == 'INVALID') {
            $this->setValidated(0);
        } else {
            $this->setValidated(0);
        }
        $this->saveOrException();

        return $request;
    }

    /**
     *
     * @var integer
     */
    protected $processed;

    /**
     * Method to set the value of field processed
     *
     * @param integer $processed
     * @return $this
     */
    public function setProcessed($processed)
    {
        $this->processed = $processed;

        return $this;
    }

    /**
     * Returns the value of field processed
     *
     * @return integer
     */
    public function getProcessed()
    {
        return $this->processed;
    }

    /**
     *
     * @var integer
     */
    protected $validated;

    /**
     * Method to set the value of field validated
     *
     * @param integer $validated
     * @return $this
     */
    public function setValidated($validated)
    {
        $this->validated = $validated;

        return $this;
    }

    /**
     * Returns the value of field validated
     *
     * @return integer
     */
    public function getValidated()
    {
        return $this->validated;
    }

    /**
     *
     * @var string
     */
    protected $post_data;

    /**
     * Method to set the value of field post_data
     *
     * @param string $post_data
     * @return $this
     */
    public function setPostData($post_data)
    {
        $this->post_data = json_encode($post_data);

        return $this;
    }

    /**
     * Returns the value of field post_data
     *
     * @return string
     */
    public function getPostData()
    {
        return json_decode($this->post_data);
    }

    /**
     *
     * @var string
     */
    protected $type;

    /**
     * Method to set the value of field type
     *
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Returns the value of field type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     *
     * @var string
     */
    protected $method;

    /**
     * Method to set the value of field method
     *
     * @param string $method
     * @return $this
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Returns the value of field method
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     *
     * @var string
     */
    protected $code;

    /**
     * Method to set the value of field code
     *
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Returns the value of field code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     *
     * @var string
     */
    protected $get_data;

    /**
     * Method to set the value of field get_data
     *
     * @param string $get_data
     * @return $this
     */
    public function setGetData($get_data)
    {
        $this->get_data = json_encode($get_data);

        return $this;
    }

    /**
     * Returns the value of field get_data
     *
     * @return string
     */
    public function getGetData()
    {
        return json_decode($this->get_data);
    }

    /**
     *
     * @var string
     */
    protected $response;

    /**
     * Method to set the value of field response
     *
     * @param string $response
     * @return $this
     */
    public function setResponse($response)
    {
        $this->response = json_encode($response);

        return $this;
    }

    /**
     * Returns the value of field response
     *
     * @return string
     */
    public function getResponse()
    {
        return json_decode($this->response);
    }
}