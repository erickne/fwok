<?php

namespace Fwok\Models;

class PaymentMethods extends ModelBase
{
    const OBJECT_NAME = 'payment-methods';

    public static $data = [
        'cash' => [
            'id' => 1,
            'code' => 'cash',
            'name' => 'Dinheiro',
            'icon' => 'fa fa-usd fa-fw'
        ],
        self::METHOD_MASTER => [
            'id' => 2,
            'code' => self::METHOD_MASTER,
            'name' => 'MasterCard',
            'icon' => 'fa fa-cc-mastercard fa-fw'
        ],
        self::METHOD_VISA => [
            'id' => 3,
            'code' => self::METHOD_VISA,
            'name' => 'Visa',
            'icon' => 'fa fa-cc-visa fa-fw'
        ],
        self::METHOD_AMEX => [
            'id' => 6,
            'code' => self::METHOD_AMEX,
            'name' => 'American Express',
            'icon' => 'fa fa-cc-amex fa-fw'
        ],
        self::METHOD_IUGU_CC_TOKEN => [
            'id' => 4,
            'code' => self::METHOD_IUGU_CC_TOKEN,
            'name' => 'Cartão de Crédito Token',
            'icon' => 'fa fa-credit-card fa-fw'
        ],
        self::METHOD_IUGU_CC_DIRECT => [
            'id' => 5,
            'code' => self::METHOD_IUGU_CC_DIRECT,
            'name' => 'Cartão de Crédito Direto',
            'icon' => 'fa fa-credit-card-alt fa-fw'
        ],
        self::METHOD_PAYPAL => [
            'id' => 6,
            'code' => self::METHOD_PAYPAL,
            'name' => 'Paypal',
            'icon' => 'fa fa-paypal fa-fw'
        ],
        self::METHOD_PAGSEGURO => [
            'id' => 7,
            'code' => self::METHOD_PAGSEGURO,
            'name' => 'PagSeguro',
            'icon' => 'fa fa-usd fa-fw'
        ],
        self::METHOD_ANDROID_PAY => [
            'id' => 7,
            'code' => self::METHOD_ANDROID_PAY,
            'name' => 'Android Pay',
            'icon' => 'fa fa-android fa-fw'
        ]

    ];

    const METHOD_PAYPAL = 'paypal';
    const METHOD_PAGSEGURO = 'pagseguro';
    const METHOD_ANDROID_PAY = 'androidpay';
    const METHOD_CASH = 'cash';
    const METHOD_AMEX = 'amex';
    const METHOD_VISA = 'visa';
    const METHOD_MASTER = 'master';
    const METHOD_IUGU_CC_DIRECT = 'cc-direct';
    const METHOD_IUGU_CC_TOKEN = 'cc-token';

    public static function findAllMethodCodes()
    {
        $ret = [];
        foreach (self::$data as $method_code => $method) {
            $ret[] = $method_code;
        }
        return $ret;
    }
    /**
     *
     * @var string
     */
    protected $name;

    public static function findFirstByCode($code)
    {
        if (!array_key_exists($code, self::$data)) {
            return false;
        };
        $method = new PaymentMethods();
        $method->setId(self::$data[$code]['id']);
        $method->setCode(self::$data[$code]['code']);
        $method->setName(self::$data[$code]['name']);
        $method->setIcon(self::$data[$code]['icon']);

        return $method;
    }

    public static function findFirst($params = null)
    {
        $code = $params['bind']['APR0'];
        if (!array_key_exists($code, self::$data)) {
            return null;
        }

        $method = new PaymentMethods();
        $method->setId(self::$data[$code]['id']);
        $method->setCode(self::$data[$code]['code']);
        $method->setName(self::$data[$code]['name']);
        $method->setIcon(self::$data[$code]['icon']);

        return $method;
    }

    /**
     * @param null $params
     * @return self[]
     */
    public static function find($params = null)
    {
        $return = [];
        foreach (self::$data as $method_item) {

            $method = new PaymentMethods();
            $method->setId($method_item['id']);
            $method->setCode($method_item['code']);
            $method->setName($method_item['name']);
            $method->setIcon($method_item['icon']);

            $return[] = $method;
        }

        return $return;

    }

    public function initialize()
    {

    }

    public function export()
    {
        $ret = [
            'object' => self::OBJECT_NAME,
            'id' => $this->getId(),
            'code' => $this->getCode(),
            'icon' => $this->getIcon(),
            'name' => $this->getName(),
        ];

        return $ret;

    }


    public function validation()
    {
        return true;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="code",type="string",description="")
     */
    protected $code;

    /**
     * Method to set the value of field code
     *
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Returns the value of field code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="icon",type="string",description="")
     */
    protected $icon;

    /**
     * Method to set the value of field icon
     *
     * @param string $icon
     * @return $this
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Returns the value of field icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }
}
