<?php

namespace Fwok\Models;

use Fwok\Application\App;
use Fwok\Application\ApplicationBase;
use Fwok\Exceptions\Exception;
use Fwok\Exceptions\IntegrationException;
use Fwok\Exceptions\SystemException;
use Fwok\Plugins\DateTime;
use Fwok\Plugins\GCMMessage;
use Fwok\Plugins\Logger;
use Fwok\Services\EmailsService;

class BusinessSubscriptions extends ModelBase
{
    const OBJECT_NAME = 'business-subscriptions';

    const STATUS_ACTIVE = 'ACTIVE';
    const STATUS_INACTIVE = 'INACTIVE';
    const STATUS_UNPAID = 'UNPAID';
    const STATUS_PENDING = 'PENDING';
    const STATUS_CANCELLED = 'CANCELLED';
    const STATUS_EXPIRED = 'EXPIRED';
    const STATUS_UNKNOWN = 'UNKNOWN';

    const PAYMENT_METHOD_PAYPAL = 'paypal';
    const PAYMENT_METHOD_PAGSEGURO = 'pagseguro';
    const PAYMENT_METHOD_ANDROID_PAY = 'androidpay';

    const EXPORT_PAYMENTS = 'business_subscriptions_export_payments';


    public function initialize()
    {
        $this->setSource('fwok_business_subscriptions');

        $this->hasOne(
            'business_id',
            '\Fwok\Models\Business',
            'id',
            [
                'alias' => 'Business',
                'reusable' => true,
            ]
        );

        $this->hasOne(
            'subscription_id',
            '\Fwok\Models\Subscriptions',
            'id',
            [
                'alias' => 'Subscription',
                'reusable' => true,
            ]
        );
        $this->hasOne(
            'discount_id',
            '\Fwok\Models\Discounts',
            'id',
            [
                'alias' => 'Discount',
                'reusable' => true,
            ]
        );
        $this->hasMany(
            'id',
            '\Fwok\Models\Payments',
            'business_subscription_id',
            [
                'alias' => 'Payments',
                'reusable' => true,
            ]
        );

        $this->keepSnapshots(true);
        //$this->addBehavior(new Auditable());
    }

    public function beforeCreate()
    {
        parent::beforeCreate();

        if (!$this->getStartDate()) {
            $this->setStartDate(date('Y-m-d H:i:s'));
        }
    }

    public function beforeSave()
    {
    }

    /**
     * @param $business Business
     * @throws \Fwok\Exceptions\Exception
     */
    public static function checkForInactiveAndCreateAFreeSubscription($business)
    {
        // TODO apagar essa porra
        # If cannot find any active, create another free one
        $business_subscriptions = BusinessSubscriptions::find(
            [
                'status = :status: and business_id = :business_id:',
                'bind' => [
                    'status' => BusinessSubscriptions::STATUS_ACTIVE,
                    'business_id' => $business->getId()
                ]
            ]
        );

        if (!count($business_subscriptions)) {
            $free_subscription = Subscriptions::findFirstFree();
            $business->createSubscription($free_subscription, true);
        }
    }

    /**
     * Set the business subscription active or inactive
     * @return $this
     * @throws \Fwok\Exceptions\Exception
     * @throws IntegrationException
     * @throws \Fwok\Exceptions\InputException
     */
    public function activate()
    {
        $this->getBusiness()->deactivateAllSubscription();

        $this->setStatus(self::STATUS_ACTIVE);
        if (!$this->getStartDate()) {
            $this->setStartDate(date('Y-m-d H:i:s'));
        }
        $this->saveOrException();

        $email = new EmailsService();
        $email->templateSubscriptionActivated($this);

        /*
        ApplicationBase::getGCM()->send(
            GCMMessage::export(
                0,
                Subscriptions::OBJECT_NAME,
                ApplicationBase::getTranslatedKey('GN_SUBSCRIPTION_CHANGED_TITLE'),
                ApplicationBase::getTranslatedKey('GN_SUBSCRIPTION_CHANGED_TEXT'),
                ''
            ),
            $this->getBusiness()->getBusinessUsersDevices(1, 1, 0)
        );
        */

        return $this;
    }

    public function deactivate()
    {
        # Se estiver ativo
        if ($this->getStatus() === self::STATUS_ACTIVE) {
            /*
             * Se tiver perfil no paypal, vai lá cancelar
             */
            if ($this->getPaymentMethod() === self::PAYMENT_METHOD_PAYPAL) {
                $this->getService('paypal')->cancelRecurringPaymentProfile(
                    $this->getPaypalProfileId()
                );
                /*
                 * if (!$cancel_paypal) throw new Exception(Exception::INVALID_RESPONSE,'Error disabling PayPal
                 * subscription',null,sprintf('Invalid response from PayPal
                 * [profile_id=%s] [business_subscription=%s].
                 * ',$this->getPaypalProfileId(),$this->getId()));
                 */
            }
            /*
             * Se tiver perfil no pagseguro, vai lá cancelar
             */
            if ($this->getPaymentMethod() === self::PAYMENT_METHOD_PAGSEGURO) {
                $cred = App::getPagSeguro()->getCredentials();
                $preapproval = \PagSeguroPreApprovalSearchService::searchByCode($cred, $this->getPagseguroCode());
                $status = App::getPagSeguro()->convertTransactionStatus($preapproval->getStatus());
                if ($status === BusinessSubscriptions::STATUS_ACTIVE) {
                    \PagSeguroPreApprovalService::cancelPreApproval($cred, $this->getPagseguroCode());
                } else {
                    Logger::shoutInAlert(sprintf(
                        'Tried to disable BusSubs id [%s] linked to PagSeguro code [%s]' .
                        ' but has status [%s]. Should be status [%s]',
                        $this->getId(),
                        $this->getPagseguroCode(),
                        $this->getStatus(),
                        BusinessSubscriptions::STATUS_ACTIVE
                    ));
                }

            }
            /*
             * Se tiver perfil no android pay, vai lá cancelar
             */
            if ($this->getPaymentMethod() === self::PAYMENT_METHOD_ANDROID_PAY) {
                /** @var Payments $payment */
                $payment = $this->getPayments()->getFirst();

                App::getAndroidPay()->cancelSubscriptionPurchase(
                    $this->getSubscription()->getCode(),
                    $payment->getExternalToken()
                );
                $payment->setStatus(Payments::STATUS_CANCELLED);
                $payment->saveOrException();
            }

        }
        // TODO Enviar email com sorry to see you go
        # Inativa a subscrição
        $this->setStatus(self::STATUS_INACTIVE);
        if (!$this->getEndDate()) {
            $this->setEndDate(date('Y-m-d H:i:s'));
        }

        $this->saveOrException();

        return $this;
    }

    /**
     * Return the related "Business"
     *
     * @param array|string $parameters
     * @return \Fwok\Models\Business
     * @throws \Fwok\Exceptions\Exception
     */
    public function getBusiness($parameters = null)
    {
        return $this->getRelated('Business', $parameters);
    }

    /**
     * Return the related "Subscription"
     *
     * @param array|string $parameters
     * @param int $exception_if_not_found
     * @throws Exception
     * @return \Fwok\Models\Subscriptions
     */
    public function getSubscription($parameters = null, $exception_if_not_found = 0)
    {
        return $this->getRelated('Subscription', $parameters, $exception_if_not_found);
    }

    /**
     * Return the related "Discount"
     *
     * @param array|string $parameters
     * @return \Fwok\Models\Discounts
     * @throws \Fwok\Exceptions\Exception
     */
    public function getDiscount($parameters = null)
    {
        return $this->getRelated('Discount', $parameters);
    }

    /**
     * Return the related "Payments"
     *
     * @param array|string $parameters
     * @return \Phalcon\Mvc\Model\Resultset|\Fwok\Models\Payments[]
     * @throws \Fwok\Exceptions\Exception
     */
    public function getPayments($parameters = null)
    {
        return $this->getRelated('Payments', $parameters);
    }


    /**
     * Return subscription price with discount applied
     *
     * @throws Exception
     * @return float
     */
    public function getCalculatedPrice()
    {
        return $this->getSubscription(null, 1)->getCost() * ($this->getDiscount() ? $this->getDiscount()
            ->getPercentage() : 1);
    }

    public function validation()
    {
        $subscription = $this->getSubscription();
        if (!$subscription) {
            $this->appendErrorMessageTranslated('SUBSCRIPTION_IS_MANDATORY');
        } else {
            if (!$subscription->getActive()) {
                if (!$subscription->getAllowLegacyRenew()) {
                    $this->appendErrorMessageTranslated('SUBSCRIPTION_INACTIVE');
                }
            }
        }

        $discount = $this->getDiscount();
        if ($discount) {
            if (!$discount->getActive()) {
                $this->appendErrorMessageTranslated('DISCOUNT_INACTIVE');
            }
        }
        // TODO verificar se o usuário que está gravando can_write no business
        return !$this->validationHasFailed();
    }

    public function export($opt = [])
    {
        $ret = [
            'object' => self::OBJECT_NAME,
            'id' => $this->getId(),
            'start_date' => $this->getStartDate(),
            'end_date' => $this->getEndDate(),
            'status' => $this->getStatus(),
            'success' => $this->getSuccess(),
            'cost' => $this->getCost(),
            'paypal_profile_id' => $this->getPaypalProfileId(),
            'pagseguro_code' => $this->getPagseguroCode(),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt(),
            'payment_method' => $this->getPaymentMethod(),
            'payment_url' => $this->getPaymentUrl()
        ];

        if ($this->getBusiness()) {
            $ret['business'] = $this->getBusiness()->export([Business::EXPORT_SUBSCRIPTIONS => 0]);
        }
        if ($this->getSubscription()) {
            $ret['subscription'] = $this->getSubscription()->export($opt);
        }
        if ($this->getDiscount()) {
            $ret['discount'] = $this->getDiscount()->export($opt);
        }
        if ($opt[self::EXPORT_PAYMENTS]) {
            foreach ($this->getPayments() as $payment) {
                $ret['payments'][] = $payment->export($opt);
            }
        }

        return $ret;
    }

    /**
     *
     * @var integer
     */
    protected $business_id;

    /**
     * Method to set the value of field business_id
     *
     * @param integer $business_id
     * @return $this
     */
    public function setBusinessId($business_id)
    {
        $this->business_id = $business_id;

        return $this;
    }

    /**
     * Returns the value of field business_id
     *
     * @return integer
     */
    public function getBusinessId()
    {
        return $this->business_id;
    }

    /**
     *
     * @var integer
     */
    protected $subscription_id;

    /**
     * Method to set the value of field subscription_id
     *
     * @param integer $subscription_id
     * @return $this
     */
    public function setSubscriptionId($subscription_id)
    {
        $this->subscription_id = $subscription_id;

        return $this;
    }

    /**
     * Returns the value of field subscription_id
     *
     * @return integer
     */
    public function getSubscriptionId()
    {
        return $this->subscription_id;
    }

    /**
     *
     * @var integer
     */
    protected $discount_id;

    /**
     * Method to set the value of field discount_id
     *
     * @param integer $discount_id
     * @return $this
     */
    public function setDiscountId($discount_id)
    {
        $this->discount_id = $discount_id;

        return $this;
    }

    /**
     * Returns the value of field discount_id
     *
     * @return integer
     */
    public function getDiscountId()
    {
        return $this->discount_id;
    }

    /**
     *
     * @var integer
     */
    protected $payment_id;

    /**
     * Method to set the value of field payment_id
     *
     * @param integer $payment_id
     * @return $this
     */
    public function setPaymentId($payment_id)
    {
        $this->payment_id = $payment_id;

        return $this;
    }

    /**
     * Returns the value of field payment_id
     *
     * @return integer
     */
    public function getPaymentId()
    {
        return $this->payment_id;
    }

    /**
     *
     * @var integer
     */
    protected $success;

    /**
     * Method to set the value of field success
     *
     * @param integer $success
     * @return $this
     */
    public function setSuccess($success)
    {
        $this->success = $success;

        return $this;
    }

    /**
     * Returns the value of field success
     *
     * @return integer
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     *
     * @var string
     */
    protected $status;

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     *
     * @var string
     */
    protected $paypal_profile_id;

    /**
     * Method to set the value of field paypal_profile_id
     *
     * @param string $paypal_profile_id
     * @return $this
     */
    public function setPaypalProfileId($paypal_profile_id)
    {
        $this->paypal_profile_id = $paypal_profile_id;

        return $this;
    }

    /**
     * Returns the value of field paypal_profile_id
     *
     * @return string
     */
    public function getPaypalProfileId()
    {
        return $this->paypal_profile_id;
    }

    /**
     *
     * @var string
     */
    protected $start_date;

    /**
     * Method to set the value of field start_date
     *
     * @param string $start_date
     * @return $this
     */
    public function setStartDate($start_date)
    {
        $this->start_date = $start_date;

        return $this;
    }

    /**
     * Returns the value of field start_date
     *
     * @return string
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     *
     * @var string
     */
    protected $end_date;

    /**
     * Method to set the value of field end_date
     *
     * @param string $end_date
     * @return $this
     */
    public function setEndDate($end_date)
    {
        $this->end_date = $end_date;

        return $this;
    }

    /**
     * Returns the value of field end_date
     *
     * @return string
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     *
     * @var string
     */
    protected $pagseguro_code;

    /**
     * Method to set the value of field pagseguro_code
     *
     * @param string $pagseguro_code
     * @return $this
     */
    public function setPagseguroCode($pagseguro_code)
    {
        $this->pagseguro_code = $pagseguro_code;

        return $this;
    }

    /**
     * Returns the value of field pagseguro_code
     *
     * @return string
     */
    public function getPagseguroCode()
    {
        return $this->pagseguro_code;
    }

    /**
     *
     * @var integer
     */
    protected $cost;

    /**
     * Method to set the value of field cost
     *
     * @param integer $cost
     * @return $this
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Returns the value of field cost
     *
     * @return integer
     */
    public function getCost()
    {
        return (int)$this->cost;
    }

    /**
     *
     * @var string
     */
    protected $payment_method;

    /**
     * Method to set the value of field payment_method
     *
     * @param string $payment_method
     * @return $this
     */
    public function setPaymentMethod($payment_method)
    {
        $this->payment_method = $payment_method;

        return $this;
    }

    /**
     * Returns the value of field payment_method
     *
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->payment_method;
    }

    /**
     *
     * @var string
     */
    protected $payment_url;

    /**
     * Method to set the value of field payment_url
     *
     * @param string $payment_url
     * @return $this
     */
    public function setPaymentUrl($payment_url)
    {
        $this->payment_url = $payment_url;

        return $this;
    }

    /**
     * Returns the value of field payment_url
     *
     * @return string
     */
    public function getPaymentUrl()
    {
        return $this->payment_url;
    }

    public function getDaysLeft()
    {
        if (\Fwok\Helpers\Format::isFuture($this->getEndDate())) {
            return \Fwok\Helpers\Format::dateDiff($this->getEndDate(), new DateTime())->days;
        } else {
            return 0;
        }
    }

    /**
     * @return array|\stdClass
     * @throws \Fwok\Exceptions\IntegrationException
     * @throws \Fwok\Exceptions\SystemException
     * @throws \Fwok\Exceptions\Exception
     * @throws \Exception
     * @throws \PagSeguroServiceException
     */
    public function retrieveExternalStatus()
    {
        switch ($this->getPaymentMethod()) {
            case (self::PAYMENT_METHOD_ANDROID_PAY):
                return $this->retrieveExternalStatusAndroidPay();
                break;
            case (self::PAYMENT_METHOD_PAGSEGURO):
                return $this->retrieveExternalStatusPagseguro();
                break;
            case (self::PAYMENT_METHOD_PAYPAL):
                throw new SystemException(SystemException::ACTION_INVALID, null, 'Not Implemented');
                break;
        }
        throw new SystemException(SystemException::ACTION_INVALID, null, 'Invalid payment method');
    }

    /**
     * @return \stdClass
     * @throws \Fwok\Exceptions\Exception
     */
    public function retrieveExternalStatusAndroidPay()
    {
        /** @var Payments $payment */
        $payment = $this->getPayments()->getFirst();
        return $payment->retrieveExternalStatusAndroidPay();
    }

    /**
     * @return bool
     * @throws \Fwok\Exceptions\Exception
     */
    public function retrieveExternalStatusAndroidPayIsActive()
    {
        /** @var Payments $payment */
        $payment = $this->getPayments()->getFirst();
        return $payment->retrieveExternalStatusAndroidPayIsActive();
    }

    /**
     * @return array
     * @throws \Fwok\Exceptions\Exception
     * @throws IntegrationException
     * @throws \Exception
     * @throws \PagSeguroServiceException
     */
    public function retrieveExternalStatusPagseguro()
    {
        $credentials = App::getPagSeguro()->getCredentials();
        if (!$this->getPagseguroCode()) {
            return ['Empty PagSeguro Code'];
        }
        /** @var \PagSeguroPreApproval $preapproval */
        $preapproval = \PagSeguroPreApprovalSearchService::searchByCode($credentials, $this->getPagseguroCode());

        $return = array();
        $return['code'] = $preapproval->getCode();
        $return['to_string'] = $preapproval->toString();
        $return['email'] = $preapproval->getSender() ? $preapproval->getSender()->getEmail() : '';
        $return['date'] = $preapproval->getDate();
        $return['last_event'] = $preapproval->getTracker();
        $return['charge'] = $preapproval->getCharge();
        $return['reference'] = $preapproval->getReference();
        $return['status_value'] = $preapproval->getStatus()->getValue();
        $return['status_text'] = $preapproval->getStatus()->getTypeFromValue();
        $return['status_fwok'] = App::getPagSeguro()->convertPreApprovalStatus(
            $preapproval->getStatus()->getValue()
        );

        return $return;
    }
}
