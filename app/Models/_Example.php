<?php

namespace Fwok\Models;

use Phalcon\Mvc\Model\Message;
use Phalcon\Validation\Validator\ExclusionIn;

class _Example extends ModelBase
{
    const OBJECT_NAME = 'example';

    public function initialize()
    {
        $this->setSource('fwok_discounts');

        $this->hasMany(
            'id',
            '\Fwok\Models\RobotsParts',
            'discount_id',
            [
                'alias'    => 'RobotsParts',
                'reusable' => true,
            ]
        );

        $this->keepSnapshots(true);
        $this->addBehavior(new \Fwok\Models\Behaviors\Auditable());
        $this->addBehavior(
            new \Fwok\Models\Behaviors\Imageable(
                [
                    'beforeValidation' => [
                        'field'  => 'image_cover',
                        'object' => 'sitetags',
                    ],
                ]
            )
        );
    }

    /**
     * Return the related "robots parts"
     *
     * @param null|string|array $parameters
     * @param int $exception_if_not_found
     * @throws \Fwok\Exceptions\Exception
     * @return \Fwok\Models\Users[]
     */
    public function getRobotsParts($parameters = null, $exception_if_not_found = 0)
    {
        return $this->getRelated('RobotsParts', $parameters, $exception_if_not_found);
    }

    public function validation()
    {
        $this->validate(
            new ExclusionIn(
                [
                    'field'  => 'status',
                    'domain' => [ 'A', 'I' ]
                ]
            )
        );

        if (!$this->getAnyRelatedObject()->checkPermission()) {
            $this->appendMessage(new Message('Invalid Description'));
        }

        if ($this->validationHasFailed() == true) {
            return false;
        } else {
            return true;
        }
    }

    /**
     *
     * @var integer
     */
    protected $special_file_id;

    /**
     * Method to set the value of field special_file_id
     *
     * @param integer $special_file_id
     * @return $this
     */
    public function setSpecialFileId($special_file_id)
    {
        $this->special_file_id = $special_file_id;

        return $this;
    }

    /**
     * Returns the value of field special_file_id
     *
     * @return integer
     */
    public function getSpecialFileId()
    {
        return $this->special_file_id;
    }

    /**
     *
     * @var string
     */
    protected $name;

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    protected $description;

    public function setDescription($description)
    {
        if ($description == 'invalid') {
            $this->appendMessage(new Message('Invalid Description'));
        }

        $this->description = $description;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getButtons()
    {
        ?>
        <div class="btn-group btn-group-xs">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                    aria-expanded="false">
                <i class="fa fa-cog fa-fw"></i> <span class="caret"></span>
            </button>
            <ul class="dropdown-menu pull-right" role="menu">
                <li><a href="/discounts/edit/<?php echo $this->getId() ?>">Edit</a></li>
            </ul>
        </div>
    <?php
    }
}