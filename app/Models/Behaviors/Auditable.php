<?php

namespace Fwok\Models\Behaviors;

use Fwok\Application\ApplicationBase;
use Fwok\Bootstrap\BootstrapBase;
use Fwok\Models\Audit;
use Fwok\Models\AuditDetail;
use Fwok\Models\ModelBase;
use Phalcon\Application;
use Phalcon\Di;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\Model\BehaviorInterface;
use Phalcon\Mvc\ModelInterface;

class Auditable extends Behavior implements BehaviorInterface
{
    const TYPE_UPDATE = 'UPDATE';
    const TYPE_CREATE = 'CREATE';
    const TYPE_DELETE = 'DELETE';

    /** @noinspection PhpHierarchyChecksInspection
     *
     * Receives notifications from the Models Manager
     *
     * ATENÇÃO: o ModelInterface precisa ser mantido como está e o notify ficará grifado com erro!
     *
     * @param string $eventType
     * @param ModelInterface $model
     * @return bool|void
     */
    public function notify($eventType, ModelInterface $model)
    {
        if ($eventType == 'afterCreate') {
            return $this->auditAfterCreate($model);
        }
        if ($eventType == 'afterUpdate') {
            return $this->auditAfterUpdate($model);
        }
        /**
         * O afterDelete não funciona em conjunto com o softDelete
         * pois não é realmente excluído. Nesse caso, usa-se a propriedade
         * delete para confirmar a exclusão disparada pelo afterUpdate.
         */
        if ($eventType == 'afterDelete') {
            return $this->auditAfterDelete($model);
        }

        return true;
    }

    /**
     * Creates an Audit instance based on the current environment
     *
     * @param  string $type
     * @param  ModelBase $model
     * @return Audit
     */
    public function createAudit($type, ModelBase $model)
    {
        if(ApplicationBase::hasStaticService('user')) {
            $audit = new Audit();
            $audit->setUserId(ApplicationBase::getUser()->getId());
            $audit->setModelName($model->getClassName());
            $audit->setModelId($model->getId());
            $audit->setIpaddress(BootstrapBase::getRealClientIp());
            $audit->setType($type);

            return $audit;
        }
        return true;
    }


    /**
     * Audits an CREATE operation
     *
     * @param  ModelBase|ModelInterface $model
     * @return boolean
     */
    public function auditAfterCreate(ModelBase $model)
    {

        if(ApplicationBase::hasStaticService('user')) {
            //Create a new audit
            $audit = $this->createAudit(Auditable::TYPE_CREATE, $model);
            $metaData = $model->getModelsMetaData();
            $fields = $metaData->getAttributes($model);
            $details = [];
            foreach ($fields as $field) {
                $skip_fields = ['created_at', 'updated_at', 'id'];
                if (!in_array($field, $skip_fields)) {
                    $auditDetail = new AuditDetail();
                    $auditDetail->setFieldName($field);
                    $auditDetail->setOldValue(null);
                    $auditDetail->setNewValue($model->get($field));
                    $details[] = $auditDetail;
                }
            }
            $audit->details = $details;

            return $audit->saveOrException();
        }
        return true;
    }

    /**
     * Audits an DELETE operation
     *
     * @param  ModelBase|ModelInterface $model
     * @return boolean
     */
    public function auditAfterDelete(ModelBase $model)
    {
        if(ApplicationBase::hasStaticService('user')) {
            $audit = $this->createAudit(Auditable::TYPE_DELETE, $model);

            return $audit->saveOrException();
        }
        return true;
    }

    /**
     * Audits an UPDATE operation
     *
     * @param  ModelBase|ModelInterface $model
     * @return boolean
     */
    public function auditAfterUpdate(ModelBase $model)
    {
        $changedFields = $model->getChangedFields();

        if (count($changedFields)) {
            $audit = $this->createAudit(Auditable::TYPE_UPDATE, $model);

            $originalData = $model->getSnapshotData();

            $details = [ ];
            foreach ($changedFields as $field) {
                $skip_fields = [ 'created_at', 'updated_at', 'id' ];
                if (!in_array($field, $skip_fields)) {
                    $auditDetail = new AuditDetail();
                    $auditDetail->setFieldName($field);
                    $auditDetail->setOldValue($originalData[$field]);
                    $auditDetail->setNewValue($model->get($field));

                    $details[] = $auditDetail;
                }

            }
            $audit->details = $details;

            return $audit->saveOrException();
        }

        return null;
    }
}
