<?php

namespace Fwok\Models\Behaviors;

use Fwok\Exceptions\Exception;
use Fwok\Exceptions\SystemException;
use Fwok\Helpers\Format;
use Fwok\Models\Files;
use Fwok\Plugins\Logger;
use Phalcon\DI;
use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\Model\BehaviorInterface;
use Phalcon\Mvc\ModelInterface;

class Imageable extends Behavior implements BehaviorInterface
{
    protected $errorMessages = [ ];
    protected $object = null;
    protected $object_id = null;

    /**
     * Upload image path
     * @var string
     */
    protected $uploadPath = null;

    /**
     * Model field
     * @var null
     */
    protected $imageField = null;

    /**
     * Old model image
     * @var string
     */
    protected $oldFileId = null;

    /**
     * Allowed types
     * @var array
     */
    protected $allowedFormats = [
        'image/jpeg',
        'image/png',
        'image/gif',
        'image/x-icon'
    ];

    protected $sizeLimit = 10240000;

    /**
     * @param string $eventType
     * @param ModelInterface $model
     * @throws Exception
     * @return $this|bool|void
     */
    public function notify($eventType, ModelInterface $model)
    {
        $options = $this->getOptions($eventType);

        if (is_string($eventType) and is_array($options)) {

            return $this->setImageField($options, $model)
                        ->setAllowedFormats($options)
                        ->setSizeLimit($options)
                        ->setObjectInfo($options, $model)
                        ->setUploadPath($options)
                        ->processUpload($model);

        }

        return true;
    }

    protected function setImageField(array $options, ModelInterface $model)
    {
        if (!isset($options['field']) || !is_string($options['field'])) {
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, 'Invalid Imageable field');
        }

        $this->imageField = $options['field'];

        return $this;
    }

    protected function setObjectInfo(array $options, $model)
    {
        if (!isset($options['object']) or !$model->getId()) {
            //throw new Exception("The option 'object' and 'model->id' must be defined to upload file.");
        }
        $this->object = $options['object'];
        $this->object_id = $model->getId();

        return $this;
    }

    protected function setAllowedFormats(array $options)
    {
        if (isset($options['allowedFormats']) && is_array($options['allowedFormats'])) {
            $this->allowedFormats = $options['allowedFormats'];
        }

        return $this;
    }

    protected function setSizeLimit(array $options)
    {
        if (isset($options['sizeLimit']) && is_array($options['sizeLimit'])) {
            $this->sizeLimit = $options['sizeLimit'];
        }

        return $this;
    }

    protected function setUploadPath(array $options)
    {
        $storage_path = Di::getDefault()->get('config')->files->storagePath;

        if (!$storage_path) {
            throw new SystemException(SystemException::CONFIGURATION_INVALID, null, 'Invalid configuration public dir');
        };


        if (!file_exists($storage_path)) {
            mkdir($storage_path);
        }

        $this->uploadPath = $storage_path;

        return $this;
    }

    protected function setErrorMessage($message)
    {
        $this->errorMessages[] = $message;
        Logger::shoutInDebug($message);

        return true;
    }

    protected function validate($file)
    {
        $msg = '';
        if (!$file->getSize()) {
            $msg = $this->setErrorMessage(sprintf('Zero size file %s', $file->getName()));
        }
        if (!in_array($file->getType(), $this->allowedFormats)) {
            $msg = $this->setErrorMessage(
                sprintf(
                    'Invalid file format [%s]. Allowed files: [%s].',
                    $file->getName(),
                    implode(", ", $this->allowedFormats)
                )
            );
        }
        if ($file->getSize() > $this->sizeLimit) {
            $msg = $this->setErrorMessage(
                sprintf(
                    'Invalid file limit [%s]. Maximum allowed: [%s].',
                    Format::sizeUnits($file->getSize()),
                    Format::sizeUnits($this->sizeLimit)
                )
            );
        }

        if ($msg) {
            return false;
        } else {
            return true;
        }

    }

    /**
     * @param ModelInterface $model
     * @throws Exception
     * @return $this
     */
    protected function processUpload(ModelInterface $model)
    {
        /** @var \Phalcon\Http\Request $request */
        $request = $model->getDI()->getRequest();

        if (true == $request->hasFiles(true)) {
            foreach ($request->getUploadedFiles() as $file) {

                # if there are more than one files being uploaded, return true to skip uploading this one.
                if ($file->getKey() != $this->imageField) {
                    return true;
                }
                if (!$this->validate($file)) {
                    foreach ($this->errorMessages as $msg) {
                        $model->appendErrorMessage($msg);
                    }

                    return false;
                }
            };

            $uniqueFileName = Files::generateUniqueName(
                $file->getName()
            ); #TODO o setNameUnique do Files está gerando automaticamente
            $fullPath = rtrim($this->uploadPath, '/\\') . DIRECTORY_SEPARATOR . $uniqueFileName;

            if ($file->moveTo($fullPath)) {
                Logger::shoutInDebug(sprintf('Success upload file %s into %s', $uniqueFileName, $fullPath));
                $file_object = new Files();
                $file_object->setName($file->getName());
                $file_object->setSize($file->getSize());
                $file_object->setObject($model->getClassName());
                $file_object->setObjectId($model->getId());
                $file_object->setDeleted(0);
                $file_object->setNameUnique($uniqueFileName);
                $file_object->save();

                # Save new file_id in model
                $field = ($this->imageField);
                $this->oldFileId = $model->$field;
                $model->writeAttribute($this->imageField, $file_object->getId());
                if (!$model->save()) {
                    return false;
                }

                # Eventually the object still dont have id, because it was not created it. So we save it after model saving.
                $file_object->setObjectId($model->getId());
                if (!$file_object->save()) {
                    return false;
                }
                if (!$this->deleteOldFile()) {
                    return false;
                }

                return true;

            }
            /*else{
              Logger::shoutInAlert(sprintf('Could not move file %s into %s' , $uniqueFileName , $fullPath));
              return false;
            }
            */
        }

        return true;
    }

    protected function deleteOldFile()
    {
        if ($this->oldFileId) {
            $old_file = Files::findFirstById($this->oldFileId, 1);

            return $old_file->delete();
        }

        return true;
    }
}