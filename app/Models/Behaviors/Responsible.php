<?php

namespace Fwok\Models\Behaviors;

use Fwok\Application\ApplicationBase;
use Fwok\Bootstrap\BootstrapBase;
use Fwok\Exceptions\Exception;
use Fwok\Models\Audit;
use Fwok\Models\AuditDetail;
use Fwok\Models\Users;
use Phalcon\Di;
use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\Model\BehaviorInterface;
use Phalcon\Mvc\ModelInterface;

class Responsible extends Behavior implements BehaviorInterface
{

    /**
     * Receives notifications from the Models Manager
     *
     * ATENÇÃO: o ModelInterface precisa ser mantido como está e o notify ficará grifado com erro!
     *
     * @param string $eventType
     * @param ModelInterface $model
     * @return null|void
     */
    public function notify($eventType, ModelInterface $model)
    {
        if ($eventType == 'beforeCreate') {
            return $this->actBeforeCreate($model);
        }
    }

    /**
     * Before Create Operation
     *
     * @param  \Phalcon\Mvc\ModelInterface $model
     * @throws Exception
     * @return boolean
     */
    public function actBeforeCreate(ModelInterface $model)
    {
        $user = ApplicationBase::getUser();

        /** @var Users $user */
        $model->setCreatedById($user->getId());
        if (!$model->getResponsibleById()) {
            $model->setResponsibleById($user->getId());
        }
        //return true;
    }

}