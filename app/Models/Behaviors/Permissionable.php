<?php

namespace Fwok\Models\Behaviors;

use Fwok\Exceptions\Exception;
use Fwok\Models\Permission;
use Phalcon\DI;
use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\Model\BehaviorInterface;
use Phalcon\Mvc\ModelInterface;

class Permissionable extends Behavior implements BehaviorInterface
{

    /**
     * Este erro está propositalmente aqui no docblock para o code autocomplete funcionar
     *
     * @param string $eventType
     * @param \Fwok\Models\ModelBase|ModelInterface $model
     * @throws Exception
     * @return $this|bool|void
     */
    public function notify($eventType, ModelInterface $model)
    {
        /*
         * Esse método está bugado no phalcon. O afterFetch não funciona dentro de behavior
         *
         * Redirecionei o método para dentro da classe ModelBase
         * https://github.com/phalcon/cphalcon/issues/1950
         */
        if ($eventType === 'afterFetch') {
            return $model->checkPermission(Permission::MODE_READ);
        }

        if ($eventType === 'beforeValidation') {
            return $model->checkPermission(Permission::MODE_WRITE);
        }
        return true;
    }

}