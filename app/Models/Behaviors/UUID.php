<?php

namespace Fwok\Models\Behaviors;

use Fwok\Models\ModelBase;
use Fwok\Plugins\UUID as UUID_Generator;
use Phalcon\Di;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\Model\BehaviorInterface;
use Phalcon\Mvc\ModelInterface;

class UUID extends Behavior implements BehaviorInterface
{
    /** @noinspection PhpHierarchyChecksInspection
     *
     * Receives notifications from the Models Manager
     *
     * ATENÇÃO: o ModelInterface precisa ser mantido como está e o notify ficará grifado com erro!
     *
     * @param string $eventType
     * @param ModelInterface $model
     * @return bool|void
     * @throws \Fwok\Exceptions\SystemException
     */
    public function notify($eventType, ModelInterface $model)
    {
        if ($eventType === 'beforeCreate') {
            return $this->uniqueBeforeCreate($model);
        }

        return true;
    }

    /**
     *
     * @param  ModelBase|ModelInterface $model
     * @return boolean
     * @throws \Fwok\Exceptions\SystemException
     */
    public function uniqueBeforeCreate(ModelBase $model)
    {
        $model->uuid = UUID_Generator::v4();

        return true;
    }
}
