<?php

namespace Fwok\Models\Behaviors;

use Fwok\Application\App;
use Phalcon\Di;
use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\ModelInterface;

class SetBusinessId extends Behavior
{

    /**
     * Receives notifications from the Models Manager
     *
     * ATENÇÃO: o ModelInterface precisa ser mantido como está e o notify ficará grifado com erro!
     *
     * @param string $eventType
     * @param ModelInterface $model
     * @return bool|void
     * @throws \Fwok\Exceptions\Exception
     */
    public function notify($eventType, ModelInterface $model)
    {
        if ($eventType === 'beforeSave') {
            return $this->actBeforeSave($model);
        }
        return true;
    }

    /**
     * Before Create Operation
     *
     * @param  \Phalcon\Mvc\ModelInterface $model
     * @return boolean
     * @throws \Fwok\Exceptions\Exception
     * @throws \Fwok\Exceptions\SystemException
     */
    public function actBeforeSave(ModelInterface $model)
    {
        if (method_exists($this, 'setBusinessId') AND !$this->business_id AND App::getBusiness(false)) {
            $this->setBusinessId(App::getBusiness()->getId());
        }
    }

}