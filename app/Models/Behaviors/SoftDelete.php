<?php

namespace Fwok\Models\Behaviors;

use Fwok\Application\ApplicationBase;
use Fwok\Bootstrap\BootstrapBase;
use Fwok\Exceptions\Exception;
use Fwok\Models\Audit;
use Fwok\Models\AuditDetail;
use Fwok\Models\Users;
use Phalcon\Di;
use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\Model\BehaviorInterface;
use Phalcon\Mvc\ModelInterface;
use Phalcon\Mvc\Model\Message;
use Phalcon\Mvc\Model\Relation;

/**
 * Fwok\Model\Behavior\SoftDelete
 */
class SoftDelete extends Behavior implements BehaviorInterface
{
    /**
     *  {@inheritdoc}
     *
     * @param string                      $eventType
     * @param \Phalcon\Mvc\ModelInterface $model
     */
    public function notify($eventType, ModelInterface $model)
    {
        if ($eventType == 'beforeDelete') {
            $options = $this->getOptions();
            $field = $options['field'];
            $value = $options['value'];
            $model->skipOperation(true);
            if ($model->readAttribute($field) === $value) {
                //$model->appendMessage(new Message('Model was already deleted'));
                //return false;
            }
            $this->fireEvent($model, 'beforeSoftDelete');
            $this->fireEvent($model, 'beforeDelete');
            $updateModel = clone $model;
            $updateModel->writeAttribute($field, $value);
            if (!$updateModel->save()) {
                foreach ($updateModel->getMessages() as $message) {
                    $model->appendMessage($message);
                }
                return false;
            }
            $model->writeAttribute($field, $value);
            if (isset($options['cascade']) && $options['cascade'] === true) {
                $this->cascadeDelete($model);
            }
            $this->fireEvent($model, 'afterSoftDelete');
            $this->fireEvent($model, 'afterDelete');
        }
    }
    private function cascadeDelete(ModelInterface $model)
    {
        $modelsManager = $model->getModelsManager();
        $hasManyRelations = $modelsManager->getHasMany($model);
        foreach ($hasManyRelations as $relation) {
            $relOptions = $relation->getOptions();
            $foreignKey = $relOptions['foreignKey'];
            if (isset($foreignKey['action']) && $foreignKey['action'] === Relation::ACTION_CASCADE) {
                $alias = $relOptions['alias'];
                $relatedModels = $model->{"get{$alias}"}();
                foreach ($relatedModels as $relModel) {
                    $relModel->delete();
                }
            }
        }
    }
    private function fireEvent(ModelInterface $model, $eventName)
    {
        $options = $this->getOptions();
        // Force the event to fire since the delete operation is being skipped over
        if (isset($options[$eventName])) {
            $options->$eventName();
        } else if (method_exists($model, $eventName)) {
            $model->{$eventName}();
        }
    }
}