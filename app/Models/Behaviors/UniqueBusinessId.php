<?php

namespace Fwok\Models\Behaviors;

use Fwok\Models\ModelBase;
use Phalcon\Di;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\Model\BehaviorInterface;
use Phalcon\Mvc\ModelInterface;

class UniqueBusinessId extends Behavior implements BehaviorInterface
{
    const TYPE_UPDATE = 'UPDATE';
    const TYPE_CREATE = 'CREATE';
    const TYPE_DELETE = 'DELETE';

    /** @noinspection PhpHierarchyChecksInspection
     *
     * Receives notifications from the Models Manager
     *
     * ATENÇÃO: o ModelInterface precisa ser mantido como está e o notify ficará grifado com erro!
     *
     * @param string $eventType
     * @param ModelInterface $model
     * @return bool|void
     * @throws \Fwok\Exceptions\SystemException
     */
    public function notify($eventType, ModelInterface $model)
    {
        if ($eventType === 'beforeCreate') {
            return $this->uniqueBeforeCreate($model);
        }

        return true;
    }

    /**
     *
     * @param  ModelBase|ModelInterface $model
     * @return boolean
     * @throws \Fwok\Exceptions\SystemException
     */
    public function uniqueBeforeCreate(ModelBase $model)
    {
        // Avoid cache to fuck model counting :)
        $count = $model::query()
            ->searchInBusiness($model->business_id)
            ->addRandomWhere()
            ->count();
        $model->setBid($count + 1);

        return true;
    }
}
