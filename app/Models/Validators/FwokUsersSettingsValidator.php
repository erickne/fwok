<?php

namespace Fwok\Models\Validators;

use Fwok\Application\App;
use Phalcon\Mvc\ModelInterface;
use Fwok\Models\UsersSettings;
use Phalcon\Di;
use Phalcon\Validation\Message;
use Phalcon\Mvc\Model\Validator;

class UsersSettingsValidator extends Validator
{
    /**
     * Executes the validation
     *
     * @param \Phalcon\Mvc\ModelInterface $object
     * @throws \Fwok\Exceptions\Exception
     * @internal param \Phalcon\Validation $validator
     * @return boolean
     */
    public function validate(ModelInterface $object)
    {
        $this->di = Di\FactoryDefault::getDefault();
        $id = $this->getOption('id');
        $required = $this->getOption('required');
        $accept_deleted = $this->getOption('acceptDeleted');

        if (!$id and $required) {
            $this->appendMessage(App::getTranslatedKey('VALIDATION_FWOKUSERSSETTINGS_IS_MANDATORY'));
        }

        if ($id) {
            /** @var UsersSettings $fwok_users_settings */
            $fwok_users_settings = UsersSettings::query()->searchInBusiness()->addWhereBinded('id = ',$id)->executeFirst();

            if (!$fwok_users_settings) {
                $this->appendMessage(App::getTranslatedKey('VALIDATION_FWOKUSERSSETTINGS_NOT_FOUND'));
            } else {
                if ($fwok_users_settings->getDeleted() and !$accept_deleted) {
                    $this->appendMessage(App::getTranslatedKey('VALIDATION_FWOKUSERSSETTINGS_DELETED'));
                }
            }
        }

        if ($this->getMessages()) {
            return false;
        }

        return true;
    }
}