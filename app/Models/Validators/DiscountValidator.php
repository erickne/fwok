<?php

namespace Fwok\Models\Validators;

use Fwok\Models\Discounts;
use Phalcon\Mvc\ModelInterface;
use Phalcon\Di;
use Phalcon\Validation\Message;
use Phalcon\Mvc\Model\Validator;

class DiscountValidator extends Validator
{
    /**
     * Executes the validation
     *
     * @param ModelInterface $object
     * @throws \Fwok\Exceptions\Exception
     * @internal param \Phalcon\Validation $validator
     * @return boolean
     */
    public function validate(ModelInterface $object)
    {
        $this->di = Di\FactoryDefault::getDefault();
        $this->translation = $this->di->getShared('translation')->getMessages();
        $id = $this->getOption('id');
        $required = $this->getOption('required');
        $accept_deleted = $this->getOption('acceptDeleted');

        if (!$id and $required) {
            $this->appendMessage($this->translation->_('VALIDATION_DISCOUNT_IS_MANDATORY'));
        }

        if ($id) {
            $discount = Discounts::findFirstValidById($id, 0, 0, 1, 1);
            if (!$discount) {
                $this->appendMessage($this->translation->_('VALIDATION_DISCOUNT_NOT_FOUND'));
            } else {
                if ($discount->getDeleted() and !$accept_deleted) {
                    $this->appendMessage($this->translation->_('VALIDATION_DISCOUNT_DELETED'));
                }
            }
        }
        if ($this->getMessages()) {
            return false;
        }

        return true;
    }
}
