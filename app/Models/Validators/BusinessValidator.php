<?php

namespace Fwok\Models\Validators;

use Phalcon\Mvc\ModelInterface;
use Fwok\Models\Business;
use Phalcon\Di;
use Phalcon\Validation\Message;
use Phalcon\Mvc\Model\Validator;

class BusinessValidator extends Validator
{
    /**
     * Executes the validation
     *
     * @param ModelInterface $object
     * @throws \Fwok\Exceptions\Exception
     * @internal param \Phalcon\Validation $validator
     * @return boolean
     */
    public function validate(ModelInterface $object)
    {
        $this->di = Di\FactoryDefault::getDefault();
        $this->translation = $this->di->getShared('translation')->getMessages();
        $id = $this->getOption('id');
        $required = $this->getOption('required');

        if (!$id and $required) {
            $this->appendMessage($this->translation->_('VALIDATION_BUSINESS_IS_MANDATORY'));
        }

        if ($id) {
            $business = Business::findFirstById($id, true);
            if (!$business) {
                $this->appendMessage($this->translation->_('VALIDATION_BUSINESS_NOT_FOUND'));
            }
        }
        if ($this->getMessages()) {
            return false;
        }

        return true;
    }
}
