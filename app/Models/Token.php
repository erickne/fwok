<?php
namespace Fwok\Models;

use Fwok\Application\ApplicationBase;
use Phalcon\Mvc\Model;

class Token extends ModelBase
{
    const OBJECT_NAME = 'user';

    public function initialize()
    {
        $this->setSource('fwok_token');

        $this->hasOne(
            'user_id',
            '\Fwok\Models\Users',
            'id',
            [
                'alias'    => 'User',
                'reusable' => true,
            ]
        );

    }

    /**
     * Return the related "User"
     *
     * @param array|string $parameters
     * @param int $exception_if_not_found
     * @throws \Fwok\Exceptions\Exception
     * @return \Fwok\Models\Subscriptions
     */
    public function getUser($parameters = null, $exception_if_not_found = 0)
    {
        return $this->getRelated('User', $parameters, $exception_if_not_found);
    }


    /**
     * @param null $value
     * @param bool $use_cache
     * @return $this
     * @throws \Fwok\Exceptions\SystemException
     */
    public static function findFirstByValue($value = null, $use_cache = false)
    {
        if ($use_cache) {
            return parent::findFirstCacheBackend(
                [
                    "value = :value:",
                    "bind"  => [
                        "value" => $value
                    ],
                    'cache' => [
                        'lifetime' => 1000
                    ]
                ],
                false,
                1000
            );
        } else {
            return parent::findFirst(
                [
                    "value = :value:",
                    "bind" => [
                        "value" => $value
                    ]
                ]
            );
        }
    }

    public function validation()
    {
        if (!$this->getValue()) {
            $this->appendMessage(new Model\Message('Invalid Token Value'));
        }
        if (!$this->getUserId()) {
            $this->appendMessage(new Model\Message('Invalid User'));
        }
        if (is_null($this->getRequests())) {
            $this->setRequests(0);
        }

        return !$this->validationHasFailed();
    }

    public static function generateToken()
    {
        return md5(microtime());
    }

    public function export($o = [])
    {
        return [
            'value'      => $this->getValue(),
            'updated_at' => $this->getUpdatedAt(),
            'created_at' => $this->getCreatedAt(),
        ];
    }

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @var string
     */
    protected $value;

    /**
     * Method to set the value of field value
     *
     * @param string $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Returns the value of field token
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     *
     * @var integer
     */
    protected $user_id;

    /**
     * Method to set the value of field user_id
     *
     * @param integer $user_id
     * @return $this
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Returns the value of field user_id
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     *
     * @var integer
     */
    protected $deleted;

    /**
     * Method to set the value of field deleted
     *
     * @param integer $deleted
     * @return $this
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Returns the value of field deleted
     *
     * @return integer
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     *
     * @var integer
     */
    protected $requests;

    /**
     * Method to set the value of field requests
     *
     * @param integer $requests
     * @return $this
     */
    public function setRequests($requests)
    {
        $this->requests = $requests;

        return $this;
    }

    /**
     * Returns the value of field requests
     *
     * @return integer
     */
    public function getRequests()
    {
        return (int) $this->requests;
    }
}

