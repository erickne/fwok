<?php

namespace Fwok\Models;

use Fwok\Helpers\Format;
use Fwok\Plugins\GCMMessageAction;
use Phalcon\Di;

class GCMMessage extends ModelBase
{
    const OBJECT_NAME = 'gcm-message';

    public function initialize()
    {
        $this->setSource('fwok_gcm_message');
    }

    public function export()
    {
        $ret = [
            'id' => $this->getId(),
            'object_id' => $this->getObjectId(),
            'object_type' => $this->getObjectType(),
            'title' => $this->getTitle(),
            'message' => $this->getMessage(),
            'icon' => $this->getIcon(),
            'actions' => $this->getActions(),
            'created_at' => $this->getCreatedAt()
        ];

        return Format::camelCaseKeys($ret);
    }
    
    public function exportToSend()
    {
        return json_encode($this->export());
    }

    public function addAction(GCMMessageAction $action)
    {
        $actions = $this->getActions();
        $actions[] = Format::camelCaseKeys($action->export());
        $this->setActions($actions);

        return $this;
    }

    /**
     *
     * @var GCMMessageAction[]
     * @SWG\Property(name="actions",type="array",description="")
     */
    protected $actions;

    /**
     * Method to set the value of field actions
     *
     * @param array $actions
     * @return $this
     */
    public function setActions($actions)
    {
        $this->actions = json_encode($actions);

        return $this;
    }

    /**
     * Returns the value of field actions
     *
     * @return array
     */
    public function getActions()
    {
        return json_decode($this->actions);
    }

    /**
     *
     * @var integer
     * @SWG\Property(name="object_id",type="integer",description="")
     */
    protected $object_id;

    /**
     * Method to set the value of field object_id
     *
     * @param integer $object_id
     * @return $this
     */
    public function setObjectId($object_id)
    {
        $this->object_id = $object_id;

        return $this;
    }

    /**
     * Returns the value of field object_id
     *
     * @return integer
     */
    public function getObjectId()
    {
        return (int)$this->object_id;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="object_type",type="string",description="")
     */
    protected $object_type;

    /**
     * Method to set the value of field object_type
     *
     * @param string $object_type
     * @return $this
     */
    public function setObjectType($object_type)
    {
        $this->object_type = $object_type;

        return $this;
    }

    /**
     * Returns the value of field object_type
     *
     * @return string
     */
    public function getObjectType()
    {
        return $this->object_type;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="icon",type="string",description="")
     */
    protected $icon;

    /**
     * Method to set the value of field icon
     *
     * @param string $icon
     * @return $this
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Returns the value of field icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="title",type="string",description="")
     */
    protected $title;

    /**
     * Method to set the value of field title
     *
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Returns the value of field title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="message",type="string",description="")
     */
    protected $message;

    /**
     * Method to set the value of field message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Returns the value of field message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

}
