<?php

namespace Fwok\Models;

use Fwok\Application\App;
use Fwok\Application\ApplicationBase;
use Fwok\Exceptions\SystemException;
use Fwok\Plugins\Logger;
use Phalcon\Di;

class Business extends ModelBase
{
    const OBJECT_NAME = 'businesses';

    const EXPORT_CONTACT_USER = 'business_contact_user';
    const EXPORT_SUBSCRIPTIONS = 'business_subscriptions';

    public function initialize()
    {
        $this->hasMany('id', '\Fwok\Models\BusinessConfigs', 'business_id', [
            'alias' => 'Configs',
            'reusable' => true,
        ]);
        $this->hasMany(
            'id',
            '\Fwok\Models\Users',
            'business_id',
            [
                'alias' => 'Users',
                'reusable' => true,
            ]
        );
        $this->hasMany(
            'id',
            '\Fwok\Models\BusinessSubscriptions',
            'business_id',
            [
                'alias' => 'BusinessSubscriptions',
                'reusable' => true,
            ]
        );
        $this->hasOne(
            'contact_user_id',
            '\Fwok\Models\Users',
            'id',
            [
                'alias' => 'ContactUser',
                'reusable' => true,
            ]
        );
        $this->setSource('fwok_business');

        $this->keepSnapshots(true);
        //$this->addBehavior(new Auditable());
    }

    /**
     * @param Subscriptions $subscription
     * @param bool $activate
     * @param null $start_date
     * @param null $end_date
     * @return bool
     * @throws \Fwok\Exceptions\IntegrationException
     * @throws \Fwok\Exceptions\Exception
     * @throws \Fwok\Exceptions\InputException
     */
    public function createSubscription(
        Subscriptions $subscription,
        $activate = true,
        $start_date = null,
        $end_date = null
    )
    {
        $new_subscription = new BusinessSubscriptions();
        $new_subscription->setSubscriptionId($subscription->getId());
        $new_subscription->setBusinessId($this->getId());
        $new_subscription->setDiscountId(null);
        $new_subscription->setStatus(BusinessSubscriptions::STATUS_ACTIVE);
        $new_subscription->setSuccess(1);
        if ($end_date) {
            $new_subscription->setEndDate($end_date);
        }
        if ($start_date) {
            $new_subscription->setStartDate($start_date);
        } else {
            $new_subscription->setStartDate(date('Y-m-d H:i:s'));
        }
        if ($activate) {
            $new_subscription->activate();
        }

        return $new_subscription->saveOrException();
    }


    /**
     * Deactivate all subscriptions
     * @throws \Fwok\Exceptions\InputException
     * @throws \Fwok\Exceptions\IntegrationException
     * @throws \Fwok\Exceptions\Exception
     */
    public function deactivateAllSubscription()
    {
        foreach ($this->getBusinessSubscriptions() as $inactive_subscription) {
            $inactive_subscription->deactivate();
            $inactive_subscription->saveOrException();
        }
    }

    /**
     * @param int $return_devices_id Return device id not the object
     * @param int $remove_own_devices
     * @throws \Fwok\Exceptions\Exception
     * @return $this[]
     */
    public function getAllBusinessDevices($return_devices_id = 1, $remove_own_devices = 1)
    {
        $devices_obj = [];
        $devices_id = [];

        foreach ($this->getUsers() as $user) {
            // Do not include own devices
            if ($remove_own_devices and App::getUser()->getId() === $user->getId()) {
                continue;
            }

            foreach ($user->getUsersDevices() as $device) {
                $devices_obj[] = $device;
                $devices_id[] = $device->getDeviceId();
            }
        }
        if ($return_devices_id) {
            return $devices_id;
        } else {
            return $devices_obj;
        }
    }

    /**
     * Return the related "BusinessConfigs"
     *
     * @param array|string $parameters
     * @return \Fwok\Models\BusinessConfigs[]
     * @throws \Fwok\Exceptions\Exception
     */
    public function getConfigs($parameters = null)
    {
        return $this->getRelated('Configs', $parameters);
    }

    /**
     * Return the related "Users"
     *
     * @param array|string $parameters
     * @return \Fwok\Models\Users[]
     * @throws \Fwok\Exceptions\Exception
     */
    public function getUsers($parameters = null)
    {
        return $this->getRelated('Users', $parameters);
    }

    /**
     * Return the related "BusinessConfig"
     *
     * @param array|string $code
     * @param null $business_id
     * @param bool $excep_not_found
     * @return BusinessConfigs
     * @throws \Fwok\Exceptions\InputException
     * @throws SystemException
     */
    public function getConfig($code, $business_id = null, $excep_not_found = true)
    {
        $config = BusinessConfigs::findByCode($code, $business_id);
        if(!$config AND $excep_not_found) {
            $msg = sprintf('Business Config Code (%s) not set for vendor (%s)',$code,$business_id);
            throw new SystemException(SystemException::CONFIGURATION_INVALID,$msg);
        }
        return $config;
    }

    /**
     * Return the related "BusinessSubscriptions"
     *
     * @param array|string $parameters
     * @return \Phalcon\Mvc\Model\Resultset|\Fwok\Models\BusinessSubscriptions[]
     * @throws \Fwok\Exceptions\Exception
     */
    public function getBusinessSubscriptions($parameters = null)
    {
        return $this->getRelated('BusinessSubscriptions', $parameters);
    }

    /**
     * Return the related "BusinessSubscriptions"
     * @return BusinessSubscriptions
     * @throws SystemException
     */
    public function getActiveBusinessSubscription()
    {

        $active_subscriptions = BusinessSubscriptions::find([
            'status = :status: and business_id = :business_id:',
            'bind' => [
                'status' => BusinessSubscriptions::STATUS_ACTIVE,
                'business_id' => $this->getId()
            ]

        ]);

        if ($active_subscriptions->count() > 1) {
            $msg = sprintf('User [%s] has %s active subscriptions', $this->getId(), $active_subscriptions->count());
            Logger::shoutInAlert($msg);
            //throw new SystemException(SystemException::DATABASE_CONSISTENCY, null, $msg);
        } elseif ($active_subscriptions->count() === 0) {
            $msg = sprintf('User [%s] does not have active subscription', $this->getId());
            Logger::shoutInAlert($msg);
            throw new SystemException(SystemException::DATABASE_CONSISTENCY, null, $msg);
        }
        return $active_subscriptions->getFirst();
    }

    /**
     * Return the related "ContactUser"
     *
     * @param null|string|array $parameters
     * @return \Fwok\Models\Users
     * @throws \Fwok\Exceptions\Exception
     */
    public function getContactUser($parameters = null)
    {
        return $this->getRelated('ContactUser', $parameters);
    }

    public function validation()
    {
        if (App::hasStaticService('user') and Di::getDefault()->user and App::getUser()->getId() !== $this->getContactUser()) {
            $this->appendErrorMessageTranslated('INVALID_PERMISSION');
        }

        return !$this->validationHasFailed();
    }

    public function export($opt = [])
    {
        $ret = [
            'object' => self::OBJECT_NAME,
            'id' => $this->getId(),
            'city' => $this->getCity(),
            'company_name' => $this->getCompanyName(),
            'complement' => $this->getComplement(),
            'country' => $this->getCountry(),
            'number' => $this->getNumber(),
            'registration_code' => $this->getRegistrationCode(),
            'state' => $this->getState(),
            'street' => $this->getStreet(),
            'zip_code' => $this->getZipCode(),
            'code' => $this->getCode(),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt(),
        ];


        if ($this->getIndustryMain()) {
            $ret['industry_main'] = [
                'id' => $this->getIndustryMain(),
                'name' => ApplicationBase::getTranslatedKey($this->getIndustryMain())
            ];
        }
        if ($this->getIndustrySector()) {
            $ret['industry_sector'] = [
                'id' => $this->getIndustrySector(),
                'name' => ApplicationBase::getTranslatedKey($this->getIndustrySector())
            ];
        }
        if ($this->getFteSize()) {
            $ret['fte_size'] = [
                'id' => $this->getFteSize(),
                'name' => ApplicationBase::getTranslatedKey($this->getFteSize())
            ];
        }

        if ($opt[self::EXPORT_CONTACT_USER] and $this->getContactUser()) {
            $ret['contact_user'] = $this->getContactUser()->export($opt);
        }

        if ($opt[self::EXPORT_SUBSCRIPTIONS]) {
            foreach ($this->getBusinessSubscriptions() as $business_subscription) {
                $ret['business_subscriptions'][] = $business_subscription->export($opt);
            }
        }

        foreach ($this->getConfigs('public=1') as $config) {
            $ret['configs'][$config->getCode()] = $config->getValue();
        }

        return $ret;
    }

    /**
     *
     * @var integer
     */
    protected
        $contact_user_id;

    /**
     * Method to set the value of field contact_user_id
     *
     * @param integer $contact_user_id
     * @return $this
     */
    public
    function setContactUserId(
        $contact_user_id
    )
    {
        $this->contact_user_id = $contact_user_id;

        return $this;
    }

    /**
     * Returns the value of field contact_user_id
     *
     * @return integer
     */
    public
    function getContactUserId()
    {
        return $this->contact_user_id;
    }

    /**
     *
     * @var string
     */
    protected
        $company_name;

    /**
     * Method to set the value of field company_name
     *
     * @param string $company_name
     * @return $this
     */
    public
    function setCompanyName(
        $company_name
    )
    {
        $this->company_name = $company_name;

        return $this;
    }

    /**
     * Returns the value of field company_name
     *
     * @return string
     */
    public
    function getCompanyName()
    {
        return $this->company_name;
    }

    /**
     *
     * @var string
     */
    protected
        $registration_code;

    /**
     * Method to set the value of field registration_code
     *
     * @param string $registration_code
     * @return $this
     */
    public
    function setRegistrationCode(
        $registration_code
    )
    {
        $this->registration_code = $registration_code;

        return $this;
    }

    /**
     * Returns the value of field registration_code
     *
     * @return string
     */
    public
    function getRegistrationCode()
    {
        return $this->registration_code;
    }

    /**
     *
     * @var string
     */
    protected
        $zip_code;

    /**
     * Method to set the value of field zip_code
     *
     * @param string $zip_code
     * @return $this
     */
    public
    function setZipCode(
        $zip_code
    )
    {
        $this->zip_code = $zip_code;

        return $this;
    }

    /**
     * Returns the value of field zip_code
     *
     * @return string
     */
    public
    function getZipCode()
    {
        return $this->zip_code;
    }

    /**
     *
     * @var string
     */
    protected
        $country;

    /**
     * Method to set the value of field country
     *
     * @param string $country
     * @return $this
     */
    public
    function setCountry(
        $country
    )
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Returns the value of field country
     *
     * @return string
     */
    public
    function getCountry()
    {
        return $this->country;
    }

    /**
     *
     * @var string
     */
    protected
        $state;

    /**
     * Method to set the value of field state
     *
     * @param string $state
     * @return $this
     */
    public
    function setState(
        $state
    )
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Returns the value of field state
     *
     * @return string
     */
    public
    function getState()
    {
        return $this->state;
    }

    /**
     *
     * @var string
     */
    protected
        $city;

    /**
     * Method to set the value of field city
     *
     * @param string $city
     * @return $this
     */
    public
    function setCity(
        $city
    )
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Returns the value of field city
     *
     * @return string
     */
    public
    function getCity()
    {
        return $this->city;
    }

    /**
     *
     * @var string
     */
    protected
        $street;

    /**
     * Method to set the value of field street
     *
     * @param string $street
     * @return $this
     */
    public
    function setStreet(
        $street
    )
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Returns the value of field street
     *
     * @return string
     */
    public
    function getStreet()
    {
        return $this->street;
    }

    /**
     *
     * @var string
     */
    protected
        $number;

    /**
     * Method to set the value of field number
     *
     * @param string $number
     * @return $this
     */
    public
    function setNumber(
        $number
    )
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Returns the value of field number
     *
     * @return string
     */
    public
    function getNumber()
    {
        return $this->number;
    }

    /**
     *
     * @var string
     */
    protected
        $complement;

    /**
     * Method to set the value of field complement
     *
     * @param string $complement
     * @return $this
     */
    public
    function setComplement(
        $complement
    )
    {
        $this->complement = $complement;

        return $this;
    }

    /**
     * Returns the value of field complement
     *
     * @return string
     */
    public
    function getComplement()
    {
        return $this->complement;
    }

    /**
     *
     * @var string
     */
    protected
        $industry_main;

    /**
     * Method to set the value of field industry_main
     *
     * @param string $industry_main
     * @return $this
     */
    public
    function setIndustryMain(
        $industry_main
    )
    {
        $this->industry_main = strtoupper($industry_main);

        return $this;
    }

    /**
     * Returns the value of field industry_main
     *
     * @return string
     */
    public
    function getIndustryMain()
    {
        return $this->industry_main;
    }

    /**
     *
     * @var string
     */
    protected
        $industry_sector;

    /**
     * Method to set the value of field industry_sector
     *
     * @param string $industry_sector
     * @return $this
     */
    public
    function setIndustrySector(
        $industry_sector
    )
    {
        $this->industry_sector = strtoupper($industry_sector);

        return $this;
    }

    /**
     * Returns the value of field industry_sector
     *
     * @return string
     */
    public
    function getIndustrySector()
    {
        return $this->industry_sector;
    }

    /**
     *
     * @var string
     */
    protected
        $fte_size;

    /**
     * Method to set the value of field fte_size
     *
     * @param string $fte_size
     * @return $this
     */
    public
    function setFteSize(
        $fte_size
    )
    {
        $this->fte_size = strtoupper($fte_size);

        return $this;
    }

    /**
     * Returns the value of field fte_size
     *
     * @return string
     */
    public
    function getFteSize()
    {
        return $this->fte_size;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="code",type="string",description="")
     */
    protected
        $code;

    /**
     * Method to set the value of field code
     *
     * @param string $code
     * @return $this
     */
    public
    function setCode(
        $code
    )
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Returns the value of field code
     *
     * @return string
     */
    public
    function getCode()
    {
        return $this->code;
    }
}
