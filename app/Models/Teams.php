<?php

namespace Fwok\Models;

use Fwok\Plugins\GetFeature;
use Phalcon\Mvc\Model\Message;

class Teams extends ModelBase
{
    const OBJECT_NAME = 'teams';
    const ICON = 'fa fa-users fa-fw';

    const EXPORT_PERMISSIONS = 'teams_permissions'; //permissions

    public function initialize()
    {
        $this->setSource('fwok_teams');

        $this->hasMany(
            'id',
            '\Fwok\Models\TeamsUsers',
            'team_id',
            [
                'alias'    => 'TeamsUsers',
                'reusable' => true,
            ]
        );

    }

    public function export($opt = [ ])
    {

        $return = [
            'object'     => Teams::OBJECT_NAME,
            'icon'       => Teams::ICON,
            'id'         => $this->getId(),
            'name'       => $this->getName(),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt(),
        ];

        foreach ($this->getTeamsUsers() as $teamuser) {
            if (!$teamuser->getDeleted()) {
                $return['users'][] = $teamuser->export($opt);
            }
        }

        if ($opt[self::EXPORT_PERMISSIONS]) {
            $return['permissions'] = [ ];
            foreach (Permission::findAllByTeamId($this->getId()) as $permission) {
                $return['permissions'][] = $permission->export($opt);
            }
        }

        return $return;
    }

    /**
     * Return the related "teams users"
     *
     * @param null|string|array $parameters
     * @param int $exception_if_not_found
     * @throws \Fwok\Exceptions\Exception
     * @return \Fwok\Models\TeamsUsers[]
     */
    public function getTeamsUsers($parameters = null, $exception_if_not_found = 0)
    {
        if (!$parameters) {
            $parameters .= 'deleted = 0';
        } else {
            $parameters .= ' AND deleted = 0';
        }

        return $this->getRelated('TeamsUsers', $parameters, $exception_if_not_found);
    }

    public function validation()
    {
        # Verifica se a assinatura permite alteração e criação de salestage
        if (!GetFeature::checkValidityInt(GetFeature::KEY_TEAMS_CONTROL, 1, 0)) {
            $this->appendErrorMessage(
                "Sua assinatura atual não permite alterar os times. " .
                "Faça já um upgrade na sua assinatura e aproveite diversas novas funcionalidades!"
            );
        }

        if (!$this->getName()) {
            $this->appendMessage(new Message('Invalid Team Name'));
        }

        return !$this->validationHasFailed();

    }

    /**
     *
     * @var string
     */
    protected $name;

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     *
     * @var integer
     */
    protected $business_id;

    /**
     * Method to set the value of field business_id
     *
     * @param integer $business_id
     * @return $this
     */
    public function setBusinessId($business_id)
    {
        $this->business_id = $business_id;

        return $this;
    }

    /**
     * Returns the value of field business_id
     *
     * @return integer
     */
    public function getBusinessId()
    {
        return (int) $this->business_id;
    }
}
