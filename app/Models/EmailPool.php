<?php

namespace Fwok\Models;

use Fwok\Models\Behaviors\Auditable;
use Phalcon\Mvc\Model\Behavior\SoftDelete;
use Phalcon\Validation;

class EmailPool extends ModelBase
{
    const OBJECT_NAME = 'email-pool';
    const ICON = 'fa fa-tag fa-fw';

    public function initialize()
    {
        $this->setSource('fwok_email_pool');
        $this->keepSnapshots(true);
        $this->addBehavior(new Auditable());
        $this->addBehavior(new SoftDelete(['field' => 'deleted', 'value' => 1]));
    }


    public function export()
    {
        $return = [
            'object' => EmailPool::OBJECT_NAME,
            'id' => $this->getId(),
            'sent' => $this->getSent(),
            'content_raw' => $this->getContentRaw(),
            'content' => $this->getContent(),
            'subject' => $this->getSubject(),
            'to' => $this->getTo(),
            'debug' => $this->getDebug(),
            'debug_return' => $this->getDebugReturn(),
            'object_type' => $this->getObjectType(),
            'object_id' => $this->getObjectId(),
            'deleted' => $this->getDeleted(),
            'business_id' => $this->getBusinessId(),
            'sent_at' => $this->getSentAt(),
            'created_at' => $this->getCreatedAt(),

        ];

//        if ($this->getColorSet()) {
//            $ret['color_set'] = $this->getColorSet()->export();
//        }

        return $return;

    }


    public function validation()
    {
        $validation = new Validation();

        $validation->add([
            'subject',
            'content_raw',
            'to',
            'business_id'
        ], new Validation\Validator\PresenceOf([
            'message' => [
                'subject' => 'O assunto é obrigatório',
                'content_raw' => 'O conteúdo é obrigatório',
                'to' => 'O destinatário é obrigatório',
                'business_id' => 'O business_id é obrigatório',
            ]
        ]));

        return !$this->validationHasFailed();
    }


    /**
     *
     * @var integer
     * @SWG\Property(name="sent",type="integer",description="")
     */
    protected $sent;

    /**
     * Method to set the value of field sent
     *
     * @param integer $sent
     * @return $this
     */
    public function setSent($sent)
    {
        $this->sent = $sent;

        return $this;
    }

    /**
     * Returns the value of field sent
     *
     * @return integer
     */
    public function getSent()
    {
        return (int)$this->sent;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="content_raw",type="string",description="")
     */
    protected $content_raw;

    /**
     * Method to set the value of field content_raw
     *
     * @param string $content_raw
     * @return $this
     */
    public function setContentRaw($content_raw)
    {
        $this->content_raw = $content_raw;

        return $this;
    }

    /**
     * Returns the value of field content_raw
     *
     * @return string
     */
    public function getContentRaw()
    {
        return $this->content_raw;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="content",type="string",description="")
     */
    protected $content;

    /**
     * Method to set the value of field content
     *
     * @param string $content
     * @return $this
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Returns the value of field content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="subject",type="string",description="")
     */
    protected $subject;

    /**
     * Method to set the value of field subject
     *
     * @param string $subject
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Returns the value of field subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="to",type="string",description="")
     */
    protected $to;

    /**
     * Method to set the value of field to
     *
     * @param string $to
     * @return $this
     */
    public function setTo($to)
    {
        $this->to = json_encode($to);

        return $this;
    }

    /**
     * Returns the value of field to
     *
     * @return array
     */
    public function getTo()
    {
        return json_decode($this->to);
    }

    /**
     *
     * @var string
     * @SWG\Property(name="debug",type="string",description="")
     */
    protected $debug;

    /**
     * Method to set the value of field debug
     *
     * @param string $debug
     * @return $this
     */
    public function setDebug($debug)
    {
        $this->debug = $debug;

        return $this;
    }

    /**
     * Returns the value of field debug
     *
     * @return string
     */
    public function getDebug()
    {
        return $this->debug;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="debug_return",type="string",description="")
     */
    protected $debug_return;

    /**
     * Method to set the value of field debug_return
     *
     * @param string $debug_return
     * @return $this
     */
    public function setDebugReturn($debug_return)
    {
        $this->debug_return = $debug_return;

        return $this;
    }

    /**
     * Returns the value of field debug_return
     *
     * @return string
     */
    public function getDebugReturn()
    {
        return $this->debug_return;
    }

    /**
     *
     * @var string
     * @SWG\Property(name="object_type",type="string",description="")
     */
    protected $object_type;

    /**
     * Method to set the value of field object_type
     *
     * @param string $object_type
     * @return $this
     */
    public function setObjectType($object_type)
    {
        $this->object_type = $object_type;

        return $this;
    }

    /**
     * Returns the value of field object_type
     *
     * @return string
     */
    public function getObjectType()
    {
        return $this->object_type;
    }

    /**
     *
     * @var integer
     * @SWG\Property(name="object_id",type="integer",description="")
     */
    protected $object_id;

    /**
     * Method to set the value of field object_id
     *
     * @param integer $object_id
     * @return $this
     */
    public function setObjectId($object_id)
    {
        $this->object_id = $object_id;

        return $this;
    }

    /**
     * Returns the value of field object_id
     *
     * @return integer
     */
    public function getObjectId()
    {
        return (int)$this->object_id;
    }

    /**
     *
     * @var integer
     * @SWG\Property(name="business_id",type="integer",description="")
     */
    protected $business_id;

    /**
     * Method to set the value of field business_id
     *
     * @param integer $business_id
     * @return $this
     */
    public function setBusinessId($business_id)
    {
        $this->business_id = $business_id;

        return $this;
    }

    /**
     * Returns the value of field business_id
     *
     * @return integer
     */
    public function getBusinessId()
    {
        return (int)$this->business_id;
    }

    /**
     *
     * @var
     * @SWG\Property(name="sent_at",type="",description="")
     */
    protected $sent_at;

    /**
     * Method to set the value of field sent_at
     *
     * @param  $sent_at
     * @return $this
     */
    public function setSentAt($sent_at)
    {
        $this->sent_at = $sent_at;

        return $this;
    }

    /**
     * Returns the value of field sent_at
     *
     * @return $sent_at
     */
    public function getSentAt()
    {
        return $this->sent_at;
    }
}

