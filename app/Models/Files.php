<?php
namespace Fwok\Models;

use Fwok\Application\ApplicationBase;
use Fwok\Exceptions\Exception;
use Fwok\Exceptions\InputException;
use Fwok\Exceptions\SystemException;
use Fwok\Helpers\Format;
use Fwok\Plugins\Logger;
use Phalcon\Mvc\Model\Resultset;
use Phalcon\Tag;

class Files extends ModelBase
{
    const OBJECT_NAME = 'file';

    public function initialize()
    {
        $this->setSource('fwok_files');

        $this->keepSnapshots(true);
        $this->addBehavior(new \Fwok\Models\Behaviors\Auditable());
        $this->addBehavior(new \Fwok\Models\Behaviors\UniqueBusinessId());
        if (!$this->getNameUnique()) {
            $this->setNameUnique(Files::generateUniqueName());
        }
    }

    /**
     * @param array $o
     * @return self[]|Resultset|Files[]
     */
    public static function findByMainParameters($o = [])
    {

        $query = ' business_id = :business_id: and deleted = 0 ';
        $bind = [
            'business_id' => ApplicationBase::getBusiness()->getId(),
        ];

        if ($o['id']) {
            $query .= ' and id = :id: ';
            $bind['id'] = $o['id'];
        }

        if ($o['object_type']) {
            $query .= ' and object_type = :object_type: ';
            $bind['object_type'] = $o['object_type'];
        }

        if ($o['object_id']) {
            $query .= ' and object_id  = :object_id: ';
            $bind['object_id'] = $o['object_id'];
        }

        if ($o['q']) {
            $query .= " and (name LIKE :name: or description LIKE :description: or caption LIKE :caption:) ";
            $bind['name'] = '%' . $o['q'] . '%';
            $bind['description'] = '%' . $o['q'] . '%';
            $bind['caption'] = '%' . $o['q'] . '%';
        }

        return self::find([$query, 'bind' => $bind]);
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getStoragePath()
    {
        $storage_path = $this->getService('config')->files->storagePath;

        return $storage_path . DIRECTORY_SEPARATOR;
    }

    public function export()
    {
        $ret = [
            'object' => Files::OBJECT_NAME,
            'id' => $this->getId(),
            'name' => $this->getName(),
            'caption' => $this->getCaption(),
            'is_renderable' => $this->isRenderable(),
            'is_streamable' => $this->isStreamable(),
            'description' => $this->getDescription(),
            'name_unique' => $this->getNameUnique(),
            'size' => $this->getSize(),
            'object_type' => $this->getObjectType(),
            'object_id' => $this->getObjectId(),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt(),
            //'path'          => $this->getExternalPath(),
            'path_view' => $this->getPathView(),
            'path_download' => $this->getPathDownload(),
            'path_stream' => $this->getPathStream(),
            'mime' => $this->getMime(),
        ];

        return $ret;
    }

    public function validation()
    {
        if (!$this->getName()) {
            $this->setName($this->getNameUnique() . Files::fileExt($this->getMime()));
        }

        return !$this->validationHasFailed();
    }


    /**
     *
     * $file = \Fwok\Plugins\Backup::dumpAppFiles('../../public',null,'public');
     * \Fwok\Plugins\Backup::download($file, true);
     *
     * @param $file
     * @param bool $delete_after
     */
    public
    static function download($file, $delete_after = false, $type = null)
    {
        if (!file_exists($file)) {
            header("HTTP/1.0 404 Not Found", true, 404);
            echo sprintf('File %s not found!', basename($file));
            die();
        }
        header('Content-Description: File Transfer');
        if ($type) {
            header(sprintf('Content-Type: %s', $type));
        }
        //header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . basename($file) . '"');
        header('Content-Transfer-Encoding: binary');
        header('Connection: Keep-Alive');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . sprintf("%u", filesize($file)));
        readfile($file);
        // cleanup
        if ($delete_after) {
            unlink($file);
        }
        die();

//        header("Cache-Control: public");
//        header("Content-Transfer-Encoding: binary\n");
//        header("Content-Type: $contentType");

    }

    public function isRenderable()
    {
        if ($this->imageType($this->getPathSystem())) {
            return 1;
        } else {
            return 0;
        }
    }

    private function imageType($image)
    {
        if (function_exists('exif_imagetype')) {
            return exif_imagetype($image);
        }

        $r = getimagesize($image);
        return $r[2];
    }

    /**
     *
     * O parâmetro data precisa possuir a propriedade base64
     *
     *
     * @param $data
     *
     * @throws Exception
     * @return Files
     */
    public static function createFromBase64($data)
    {
        if (!$data->base64) {
            $dev = 'Empty file base64 property';
            throw new InputException(InputException::VALUE_INVALID, 'INVALID_FILE_BASE64_DATA', $dev);
        }

        $file = new Files();
        $file->setMime($data->mime);
        /** Workaround to angular plugin that sends only 'filename' */
        if ($data->filename) {
            $file->setName($data->filename);
        } elseif ($data->name) {
            $file->setName($data->name);
        } else {
            $file->setName(md5(microtime()) . Files::fileExt($data->mime));
        }
        $file->setObjectType($data->object_type);
        $file->setObjectId($data->object_id);
        $file->writeBase64ToFolder($data->base64);

        $file->saveOrException();

        return $file;

    }

    /**
     * @param $file_string
     * @return mixed
     */
    public static function decodeBase64File($file_string)
    {
        if (!$file_string) {
            return false;
        }
        if (!explode(',', $file_string)[1]) {
            return false;
        }
        return base64_decode(explode(',', $file_string)[1]);
    }

    /**
     * @param $data_encoded_b64
     * @return int
     * @throws Exception
     */
    public function writeBase64ToFolder($data_encoded_b64 = null)
    {
        if (!$data_encoded_b64) {
            $this->appendErrorMessageTranslated('EMPTY_FILE');

            return false;
        }
        $data_decoded_b64 = base64_decode($data_encoded_b64);

        if (!$this->getMime()) {
            $f = \finfo_open();
            $mime_type = finfo_buffer($f, $data_decoded_b64, FILEINFO_MIME_TYPE);
            $this->setMime($mime_type);
        }

        $file = $this->getPathSystem();

        # File is saved first to get final filesize
        $result = file_put_contents($file, $data_decoded_b64);
        if (!$result) {
            throw new SystemException(
                SystemException::FILE_INVALID,
                'FAILED_TO_SAVE_FILE',
                sprintf('Impossible to put file contents in directory: %s', $file),
                500
            );
        }

        if ($result > self::getStaticService('config')->files->maximumSize) {
            throw new SystemException(
                SystemException::FILE_INVALID,
                'FILE_IS_TOO_LARGE',
                null,
                sprintf(
                    'Filesize: %s | maxSize: %s',
                    Format::sizeUnits($result),
                    Format::sizeUnits(self::getStaticService('config')->files->maximumSize)
                )
            );
        }

        
        if (!in_array(
            self::fileExt($this->getMime()),
            explode(',', self::getStaticService('config')->files->allowedExtensions)
        )
        ) {
            throw new SystemException(
                SystemException::FILE_INVALID,
                'FILE_EXTENSION_IS_INVALID',
                'Filetype: ' . self::fileExt($this->getMime()) . '| Allowed: ' . self::getStaticService(
                    'config'
                )->files->allowedExtensions
            );
        }

        $this->setSize($result);

        return $result;
    }

    /**
     * @param null $url_data_encoded_b64
     * @return int
     * @throws \Fwok\Exceptions\Exception
     * @throws SystemException
     */
    public function writeBase64UrlToFolder($url_data_encoded_b64 = null)
    {
        $data_decoded_b64 = explode(',', $url_data_encoded_b64)[1];
        return $this->writeBase64ToFolder($data_decoded_b64);
    }

    /**
     *
     * @var string
     */
    protected $mime;

    /**
     * Method to set the value of field mime
     *
     * @param string $mime
     * @return $this
     */
    public function setMime($mime)
    {
        $this->mime = $mime;

        return $this;
    }

    /**
     * Returns the value of field mime
     *
     * @return string
     */
    public function getMime()
    {
        return $this->mime;
    }

    /**
     *
     * @var string
     */
    protected $name_unique;

    /**
     * Method to set the value of field name_unique
     *
     * @param string $name_unique
     * @return $this
     */
    public function setNameUnique($name_unique = null)
    {
        if (!$name_unique) {
            $name_unique = self::generateUniqueName($this->getName());
        }
        $this->name_unique = $name_unique;

        return $this;
    }

    /**
     * Returns the value of field name_unique
     *
     * @return string
     */
    public function getNameUnique()
    {
        return $this->name_unique;
    }

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $description;

    /**
     *
     * @var string
     */
    protected $caption;

    /**
     *
     * @var string
     */
    protected $size;

    /**
     *
     * @var string
     */
    protected $object_type;

    /**
     *
     * @var integer
     */
    protected $object_id;


    /**
     * Return Files objects
     *
     * @param $object
     * @param $id
     * @return Files
     */
    public static function findAllByObject($object, $id)
    {
        $files = Files::find(
            [
                'object = :object: and object_id = :object_id:',
                'bind' => [
                    'object' => $object,
                    'object_id' => $id,
                ]
            ]
        );

        return $files;
    }

    /**
     * Returns the value of field description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Method to set the value of field description
     *
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Returns the value of field size
     *
     * @return string
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Method to set the value of field size
     *
     * @param string $size
     *
     * @return $this
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    private function getPathBuilder($mode)
    {
        $str = implode('/', [
            $this->getService('config')->application->baseHostCdn,
            'file',
            $mode,
            $this->getNameUnique(),
            rawurlencode($this->getName())
        ]);
        $str = str_replace('//', '/', $str);
        $str = str_replace('http:/', 'http://', $str); // IMPORTANT!
        $str = str_replace('https:/', 'https://', $str); // BOTH! :)
        return $str;
    }

    public function getPathView()
    {
        return $this->getPathBuilder('view');
    }

    public function getPathStream()
    {
        return $this->getPathBuilder('stream');
    }

    public function getPathDownload()
    {
        return $this->getPathBuilder('download');
    }

    public function getPathSystem()
    {
        return rtrim($this->getStoragePath(), '/\\') . DIRECTORY_SEPARATOR . $this->getNameUnique();
    }

    public function getExternalPath()
    {
        return $this->getService('config')->application->baseHost . 'file/showbyname/' . $this->getNameUnique();
    }

    public function fileExists()
    {
        if (!file_exists($this->getPath())) {
            $msg = 'Internal file not found Files::fileExists id=%s path=%s';
            Logger::shoutInAlert(sprintf($msg, $this->getId(), $this->getPath()));
            return false;
        } else {
            return true;
        }
    }

    /**
     * Returns the value of field path using Imageable Behavior
     *
     * @return string
     * @throws \Fwok\Exceptions\Exception
     */
    public function getPath()
    {
        $path = implode(DIRECTORY_SEPARATOR, [$this->getStoragePath(), $this->getNameUnique()]);
        $path = str_replace(DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, $path);

        return $path;
    }

    public static function generateUniqueName()
    {
        return time() . '-' . uniqid('file', true);
    }

    public function readContents()
    {
        if ($this->fileExists()) {
            return file_get_contents($this->getPathSystem());
        }
        $msg = sprintf('Could not render Files::readContents id=%s in path=%s', $this->getId(), $this->getPathSystem());
        Logger::shoutInAlert($msg);
        return false;
    }

    public function delete()
    {
        if (!parent::delete()) {
            return false;
        }

        if (unlink($this->getPath())) {
            return true;
        }

        Logger::shoutInAlert(sprintf('An error occurred deleting file %s', $this->getPath()));
        return false;
    }


    /**
     * Returns the value of field object_type
     *
     * @return string
     */
    public function getObjectType()
    {
        return $this->object_type;
    }

    /**
     * Method to set the value of field object_type
     *
     * @param string $object_type
     *
     * @return $this
     */
    public function setObjectType($object_type)
    {
        $this->object_type = $object_type;

        return $this;
    }

    /**
     * Returns the value of field object_id
     *
     * @return integer
     */
    public function getObjectId()
    {
        return $this->object_id;
    }

    /**
     * Method to set the value of field object_id
     *
     * @param integer $object_id
     *
     * @return $this
     */
    public function setObjectId($object_id)
    {
        $this->object_id = $object_id;

        return $this;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }


    /**
     * Returns the value of field caption
     *
     * @return string
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * Method to set the value of field caption
     *
     * @param string $caption
     *
     * @return $this
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;

        return $this;
    }

    /**
     * Get pathinfo from file
     * Available parts: dirname, basename, extension, filename
     *
     * @param string $part
     *
     * @return array
     */
    public function getPathinfo($part)
    {
        return pathinfo($this->getPath())[$part];
    }

    public function getExtension()
    {
        return pathinfo($this->getName(), PATHINFO_EXTENSION);
    }


    public function isStreamable()
    {
        $array_stream = [
            'ra',
            'ram',
            'ogg',
            'wav',
            'wmv',
            'avi',
            'asf',
            'divx',
            'mp3',
            'mp4',
            'mpeg',
            'mpg',
            'mpe',
            'mov',
            'swf',
            '3gp',
            'm4a',
            'aac',
            'm3u'
        ];

        return in_array($this->getExtension(), $array_stream, true);
    }

    public static function fileExt($mime)
    {
        $map = [
            'application/x-authorware-bin' => '.aab',
            'application/x-authorware-map' => '.aam',
            'application/x-authorware-seg' => '.aas',
            'text/vnd.abc' => '.abc',
            'video/animaflex' => '.afl',
            'application/x-aim' => '.aim',
            'text/x-audiosoft-intra' => '.aip',
            'application/x-navi-animation' => '.ani',
            'application/x-nokia-9000-communicator-add-on-software' => '.aos',
            'application/mime' => '.aps',
            'application/arj' => '.arj',
            'image/x-jg' => '.art',
            'text/asp' => '.asp',
            'application/x-mplayer2' => '.asx',
            'video/x-ms-asf-plugin' => '.asx',
            'audio/x-au' => '.au',
            'application/x-troff-msvideo' => '.avi',
            'video/avi' => '.avi',
            'video/msvideo' => '.avi',
            'video/x-msvideo' => '.avi',
            'video/avs-video' => '.avs',
            'application/x-bcpio' => '.bcpio',
            'application/mac-binary' => '.bin',
            'application/macbinary' => '.bin',
            'application/x-binary' => '.bin',
            'application/x-macbinary' => '.bin',
            'image/x-windows-bmp' => '.bmp',
            'application/x-bzip' => '.bz',
            'application/vnd.ms-pki.seccat' => '.cat',
            'application/clariscad' => '.ccad',
            'application/x-cocoa' => '.cco',
            'application/cdf' => '.cdf',
            'application/x-cdf' => '.cdf',
            'application/java' => '.class',
            'application/java-byte-code' => '.class',
            'application/x-java-class' => '.class',
            'application/x-cpio' => '.cpio',
            'application/mac-compactpro' => '.cpt',
            'application/x-compactpro' => '.cpt',
            'application/x-cpt' => '.cpt',
            'application/pkcs-crl' => '.crl',
            'application/pkix-crl' => '.crl',
            'application/x-x509-user-cert' => '.crt',
            'application/x-csh' => '.csh',
            'text/x-script.csh' => '.csh',
            'application/x-pointplus' => '.css',
            'text/css' => '.css',
            'application/x-deepv' => '.deepv',
            'video/dl' => '.dl',
            'video/x-dl' => '.dl',
            'application/commonground' => '.dp',
            'application/drafting' => '.drw',
            'application/x-dvi' => '.dvi',
            'drawing/x-dwf (old)' => '.dwf',
            'model/vnd.dwf' => '.dwf',
            'application/acad' => '.dwg',
            'application/dxf' => '.dxf',
            'text/x-script.elisp' => '.el',
            'application/x-bytecode.elisp (compiled elisp)' => '.elc',
            'application/x-elc' => '.elc',
            'application/x-esrehber' => '.es',
            'text/x-setext' => '.etx',
            'application/envoy' => '.evy',
            'application/vnd.fdf' => '.fdf',
            'application/fractals' => '.fif',
            'image/fif' => '.fif',
            'video/fli' => '.fli',
            'video/x-fli' => '.fli',
            'text/vnd.fmi.flexstor' => '.flx',
            'video/x-atomic3d-feature' => '.fmf',
            'image/vnd.fpx' => '.fpx',
            'image/vnd.net-fpx' => '.fpx',
            'application/freeloader' => '.frl',
            'image/g3fax' => '.g3',
            'image/gif' => '.gif',
            'video/gl' => '.gl',
            'video/x-gl' => '.gl',
            'application/x-gsp' => '.gsp',
            'application/x-gss' => '.gss',
            'application/x-gtar' => '.gtar',
            'multipart/x-gzip' => '.gzip',
            'application/x-hdf' => '.hdf',
            'text/x-script' => '.hlb',
            'application/hlp' => '.hlp',
            'application/x-winhelp' => '.hlp',
            'application/binhex' => '.hqx',
            'application/binhex4' => '.hqx',
            'application/mac-binhex' => '.hqx',
            'application/mac-binhex40' => '.hqx',
            'application/x-binhex40' => '.hqx',
            'application/x-mac-binhex40' => '.hqx',
            'application/hta' => '.hta',
            'text/x-component' => '.htc',
            'text/webviewhtml' => '.htt',
            'x-conference/x-cooltalk' => '.ice ',
            'image/x-icon' => '.ico',
            'application/x-ima' => '.ima',
            'application/x-httpd-imap' => '.imap',
            'application/inf' => '.inf ',
            'application/x-internett-signup' => '.ins',
            'application/x-ip2' => '.ip ',
            'video/x-isvideo' => '.isu',
            'audio/it' => '.it',
            'application/x-inventor' => '.iv',
            'i-world/i-vrml' => '.ivr',
            'application/x-livescreen' => '.ivy',
            'audio/x-jam' => '.jam ',
            'application/x-java-commerce' => '.jcm ',
            'image/x-jps' => '.jps',
            'application/x-javascript' => '.js ',
            'image/jutvision' => '.jut',
            'music/x-karaoke' => '.kar',
            'application/x-ksh' => '.ksh',
            'text/x-script.ksh' => '.ksh',
            'audio/x-liveaudio' => '.lam',
            'application/lha' => '.lha',
            'application/x-lha' => '.lha',
            'application/x-lisp' => '.lsp ',
            'text/x-script.lisp' => '.lsp ',
            'text/x-la-asf' => '.lsx',
            'application/x-lzh' => '.lzh',
            'application/lzx' => '.lzx',
            'application/x-lzx' => '.lzx',
            'text/x-m' => '.m',
            'audio/x-mpequrl' => '.m3u ',
            'application/x-troff-man' => '.man',
            'application/x-navimap' => '.map',
            'application/mbedlet' => '.mbd',
            'application/x-magic-cap-package-1.0' => '.mc$',
            'application/mcad' => '.mcd',
            'application/x-mathcad' => '.mcd',
            'image/vasa' => '.mcf',
            'text/mcf' => '.mcf',
            'application/netmc' => '.mcp',
            'application/x-troff-me' => '.me ',
            'application/x-frame' => '.mif',
            'application/x-mif' => '.mif',
            'www/mime' => '.mime ',
            'audio/x-vnd.audioexplosion.mjuicemediafile' => '.mjf',
            'video/x-motion-jpeg' => '.mjpg ',
            'application/x-meme' => '.mm',
            'audio/mod' => '.mod',
            'audio/x-mod' => '.mod',
            'audio/x-mpeg' => '.mp2',
            'video/x-mpeq2a' => '.mp2',
            'audio/mpeg3' => '.mp3',
            'audio/x-mpeg-3' => '.mp3',
            'application/vnd.ms-project' => '.mpp',
            'application/marc' => '.mrc',
            'application/x-troff-ms' => '.ms',
            'application/x-vnd.audioexplosion.mzz' => '.mzz',
            'application/vnd.nokia.configuration-message' => '.ncm',
            'application/x-mix-transfer' => '.nix',
            'application/x-conference' => '.nsc',
            'application/x-navidoc' => '.nvd',
            'application/oda' => '.oda',
            'application/x-omc' => '.omc',
            'application/x-omcdatamaker' => '.omcd',
            'application/x-omcregerator' => '.omcr',
            'text/x-pascal' => '.p',
            'application/pkcs10' => '.p10',
            'application/x-pkcs10' => '.p10',
            'application/pkcs-12' => '.p12',
            'application/x-pkcs12' => '.p12',
            'application/x-pkcs7-signature' => '.p7a',
            'application/x-pkcs7-certreqresp' => '.p7r',
            'application/pkcs7-signature' => '.p7s',
            'text/pascal' => '.pas',
            'image/x-portable-bitmap' => '.pbm ',
            'application/vnd.hp-pcl' => '.pcl',
            'application/x-pcl' => '.pcl',
            'image/x-pict' => '.pct',
            'image/x-pcx' => '.pcx',
            'application/pdf' => '.pdf',
            'audio/make.my.funk' => '.pfunk',
            'image/x-portable-graymap' => '.pgm',
            'image/x-portable-greymap' => '.pgm',
            'application/x-newton-compatible-pkg' => '.pkg',
            'application/vnd.ms-pki.pko' => '.pko',
            'text/x-script.perl' => '.pl',
            'application/x-pixclscript' => '.plx',
            'text/x-script.perl-module' => '.pm',
            'application/x-portable-anymap' => '.pnm',
            'image/x-portable-anymap' => '.pnm',
            'model/x-pov' => '.pov',
            'image/x-portable-pixmap' => '.ppm',
            'application/powerpoint' => '.ppt',
            'application/x-mspowerpoint' => '.ppt',
            'application/x-freelance' => '.pre',
            'paleovu/x-pv' => '.pvu',
            'text/x-script.phyton' => '.py ',
            'applicaiton/x-bytecode.python' => '.pyc ',
            'audio/vnd.qcelp' => '.qcp ',
            'video/x-qtc' => '.qtc',
            'audio/x-realaudio' => '.ra',
            'application/x-cmu-raster' => '.ras',
            'image/x-cmu-raster' => '.ras',
            'text/x-script.rexx' => '.rexx ',
            'image/vnd.rn-realflash' => '.rf',
            'image/x-rgb' => '.rgb ',
            'application/vnd.rn-realmedia' => '.rm',
            'audio/mid' => '.rmi',
            'application/ringing-tones' => '.rng',
            'application/vnd.nokia.ringing-tone' => '.rng',
            'application/vnd.rn-realplayer' => '.rnx ',
            'image/vnd.rn-realpix' => '.rp ',
            'text/vnd.rn-realtext' => '.rt',
            'application/x-rtf' => '.rtf',
            'video/vnd.rn-realvideo' => '.rv',
            'audio/s3m' => '.s3m ',
            'application/x-lotusscreencam' => '.scm',
            'text/x-script.guile' => '.scm',
            'text/x-script.scheme' => '.scm',
            'video/x-scm' => '.scm',
            'application/sdp' => '.sdp ',
            'application/x-sdp' => '.sdp ',
            'application/sounder' => '.sdr',
            'application/sea' => '.sea',
            'application/x-sea' => '.sea',
            'application/set' => '.set',
            'application/x-sh' => '.sh',
            'text/x-script.sh' => '.sh',
            'audio/x-psid' => '.sid',
            'application/x-sit' => '.sit',
            'application/x-stuffit' => '.sit',
            'application/x-seelogo' => '.sl ',
            'audio/x-adpcm' => '.snd',
            'application/solids' => '.sol',
            'application/x-pkcs7-certificates' => '.spc ',
            'application/futuresplash' => '.spl',
            'application/streamingmedia' => '.ssm ',
            'application/vnd.ms-pki.certstore' => '.sst',
            'application/sla' => '.stl',
            'application/vnd.ms-pki.stl' => '.stl',
            'application/x-navistyle' => '.stl',
            'application/x-sv4cpio' => '.sv4cpio',
            'application/x-sv4crc' => '.sv4crc',
            'x-world/x-svr' => '.svr',
            'application/x-shockwave-flash' => '.swf',
            'application/x-tar' => '.tar',
            'application/toolbook' => '.tbk',
            'application/x-tcl' => '.tcl',
            'text/x-script.tcl' => '.tcl',
            'text/x-script.tcsh' => '.tcsh',
            'application/x-tex' => '.tex',
            'application/plain' => '.text',
            'application/gnutar' => '.tgz',
            'audio/tsp-audio' => '.tsi',
            'application/dsptype' => '.tsp',
            'audio/tsplayer' => '.tsp',
            'text/tab-separated-values' => '.tsv',
            'text/x-uil' => '.uil',
            'application/i-deas' => '.unv',
            'application/x-ustar' => '.ustar',
            'multipart/x-ustar' => '.ustar',
            'application/x-cdlink' => '.vcd',
            'text/x-vcalendar' => '.vcs',
            'application/vda' => '.vda',
            'video/vdo' => '.vdo',
            'application/groupwise' => '.vew ',
            'application/vocaltec-media-desc' => '.vmd ',
            'application/vocaltec-media-file' => '.vmf',
            'audio/voc' => '.voc',
            'audio/x-voc' => '.voc',
            'video/vosaic' => '.vos',
            'audio/voxware' => '.vox',
            'audio/x-twinvq' => '.vqf',
            'application/x-vrml' => '.vrml',
            'x-world/x-vrt' => '.vrt',
            'application/wordperfect6.1' => '.w61',
            'audio/wav' => '.wav',
            'audio/x-wav' => '.wav',
            'application/x-qpro' => '.wb1',
            'image/vnd.wap.wbmp' => '.wbmp',
            'application/vnd.xara' => '.web',
            'application/x-123' => '.wk1',
            'windows/metafile' => '.wmf',
            'text/vnd.wap.wml' => '.wml',
            'application/vnd.wap.wmlc' => '.wmlc ',
            'text/vnd.wap.wmlscript' => '.wmls',
            'application/vnd.wap.wmlscriptc' => '.wmlsc ',
            'application/x-wpwin' => '.wpd',
            'application/x-lotus' => '.wq1',
            'application/mswrite' => '.wri',
            'application/x-wri' => '.wri',
            'text/scriplet' => '.wsc',
            'application/x-wintalk' => '.wtk ',
            'image/x-xbitmap' => '.xbm',
            'image/x-xbm' => '.xbm',
            'image/xbm' => '.xbm',
            'video/x-amt-demorun' => '.xdr',
            'xgl/drawing' => '.xgz',
            'image/vnd.xiff' => '.xif',
            'audio/xm' => '.xm',
            'application/xml' => '.xml',
            'text/xml' => '.xml',
            'xgl/movie' => '.xmz',
            'application/x-vnd.ls-xpix' => '.xpix',
            'image/xpm' => '.xpm',
            'video/x-amt-showrun' => '.xsr',
            'image/x-xwd' => '.xwd',
            'image/x-xwindowdump' => '.xwd',
            'application/x-compress' => '.z',
            'application/x-zip-compressed' => '.zip',
            'application/zip' => '.zip',
            'multipart/x-zip' => '.zip',
            'text/x-script.zsh' => '.zsh',
        ];
        if (isset($map[$mime])) {
            return $map[$mime];
        }

        // HACKISH CATCH ALL (WHICH IN MY CASE IS
        // PREFERRED OVER THROWING AN EXCEPTION)
        $pieces = explode('/', $mime);

        return '.' . array_pop($pieces);
    }

    public static function findMimeFromExt($extension)
    {
        $extension = str_replace('.', '', $extension);
        $extension = '.' . $extension;
        $map = [
            'application/x-authorware-bin' => '.aab',
            'application/x-authorware-map' => '.aam',
            'application/x-authorware-seg' => '.aas',
            'text/vnd.abc' => '.abc',
            'video/animaflex' => '.afl',
            'application/x-aim' => '.aim',
            'text/x-audiosoft-intra' => '.aip',
            'application/x-navi-animation' => '.ani',
            'application/x-nokia-9000-communicator-add-on-software' => '.aos',
            'application/mime' => '.aps',
            'application/arj' => '.arj',
            'image/x-jg' => '.art',
            'text/asp' => '.asp',
            'application/x-mplayer2' => '.asx',
            'video/x-ms-asf-plugin' => '.asx',
            'audio/x-au' => '.au',
            'application/x-troff-msvideo' => '.avi',
            'video/avi' => '.avi',
            'video/msvideo' => '.avi',
            'video/x-msvideo' => '.avi',
            'video/avs-video' => '.avs',
            'application/x-bcpio' => '.bcpio',
            'application/mac-binary' => '.bin',
            'application/macbinary' => '.bin',
            'application/x-binary' => '.bin',
            'application/x-macbinary' => '.bin',
            'image/x-windows-bmp' => '.bmp',
            'application/x-bzip' => '.bz',
            'application/vnd.ms-pki.seccat' => '.cat',
            'application/clariscad' => '.ccad',
            'application/x-cocoa' => '.cco',
            'application/cdf' => '.cdf',
            'application/x-cdf' => '.cdf',
            'application/java' => '.class',
            'application/java-byte-code' => '.class',
            'application/x-java-class' => '.class',
            'application/x-cpio' => '.cpio',
            'application/mac-compactpro' => '.cpt',
            'application/x-compactpro' => '.cpt',
            'application/x-cpt' => '.cpt',
            'application/pkcs-crl' => '.crl',
            'application/pkix-crl' => '.crl',
            'application/x-x509-user-cert' => '.crt',
            'application/x-csh' => '.csh',
            'text/x-script.csh' => '.csh',
            'application/x-pointplus' => '.css',
            'text/css' => '.css',
            'application/x-deepv' => '.deepv',
            'video/dl' => '.dl',
            'video/x-dl' => '.dl',
            'application/commonground' => '.dp',
            'application/drafting' => '.drw',
            'application/x-dvi' => '.dvi',
            'drawing/x-dwf (old)' => '.dwf',
            'model/vnd.dwf' => '.dwf',
            'application/acad' => '.dwg',
            'application/dxf' => '.dxf',
            'text/x-script.elisp' => '.el',
            'application/x-bytecode.elisp (compiled elisp)' => '.elc',
            'application/x-elc' => '.elc',
            'application/x-esrehber' => '.es',
            'text/x-setext' => '.etx',
            'application/envoy' => '.evy',
            'application/vnd.fdf' => '.fdf',
            'application/fractals' => '.fif',
            'image/fif' => '.fif',
            'video/fli' => '.fli',
            'video/x-fli' => '.fli',
            'text/vnd.fmi.flexstor' => '.flx',
            'video/x-atomic3d-feature' => '.fmf',
            'image/vnd.fpx' => '.fpx',
            'image/vnd.net-fpx' => '.fpx',
            'application/freeloader' => '.frl',
            'image/g3fax' => '.g3',
            'image/gif' => '.gif',
            'video/gl' => '.gl',
            'video/x-gl' => '.gl',
            'application/x-gsp' => '.gsp',
            'application/x-gss' => '.gss',
            'application/x-gtar' => '.gtar',
            'multipart/x-gzip' => '.gzip',
            'application/x-hdf' => '.hdf',
            'text/x-script' => '.hlb',
            'application/hlp' => '.hlp',
            'application/x-winhelp' => '.hlp',
            'application/binhex' => '.hqx',
            'application/binhex4' => '.hqx',
            'application/mac-binhex' => '.hqx',
            'application/mac-binhex40' => '.hqx',
            'application/x-binhex40' => '.hqx',
            'application/x-mac-binhex40' => '.hqx',
            'application/hta' => '.hta',
            'text/x-component' => '.htc',
            'text/webviewhtml' => '.htt',
            'x-conference/x-cooltalk' => '.ice ',
            'image/x-icon' => '.ico',
            'application/x-ima' => '.ima',
            'application/x-httpd-imap' => '.imap',
            'application/inf' => '.inf ',
            'application/x-internett-signup' => '.ins',
            'application/x-ip2' => '.ip ',
            'video/x-isvideo' => '.isu',
            'audio/it' => '.it',
            'application/x-inventor' => '.iv',
            'i-world/i-vrml' => '.ivr',
            'application/x-livescreen' => '.ivy',
            'audio/x-jam' => '.jam ',
            'application/x-java-commerce' => '.jcm ',
            'image/x-jps' => '.jps',
            'application/x-javascript' => '.js ',
            'image/jutvision' => '.jut',
            'music/x-karaoke' => '.kar',
            'application/x-ksh' => '.ksh',
            'text/x-script.ksh' => '.ksh',
            'audio/x-liveaudio' => '.lam',
            'application/lha' => '.lha',
            'application/x-lha' => '.lha',
            'application/x-lisp' => '.lsp ',
            'text/x-script.lisp' => '.lsp ',
            'text/x-la-asf' => '.lsx',
            'application/x-lzh' => '.lzh',
            'application/lzx' => '.lzx',
            'application/x-lzx' => '.lzx',
            'text/x-m' => '.m',
            'audio/x-mpequrl' => '.m3u ',
            'application/x-troff-man' => '.man',
            'application/x-navimap' => '.map',
            'application/mbedlet' => '.mbd',
            'application/x-magic-cap-package-1.0' => '.mc$',
            'application/mcad' => '.mcd',
            'application/x-mathcad' => '.mcd',
            'image/vasa' => '.mcf',
            'text/mcf' => '.mcf',
            'application/netmc' => '.mcp',
            'text/markdown' => '.md',
            'application/x-troff-me' => '.me ',
            'application/x-frame' => '.mif',
            'application/x-mif' => '.mif',
            'www/mime' => '.mime ',
            'audio/x-vnd.audioexplosion.mjuicemediafile' => '.mjf',
            'video/x-motion-jpeg' => '.mjpg ',
            'application/x-meme' => '.mm',
            'audio/mod' => '.mod',
            'audio/x-mod' => '.mod',
            'audio/x-mpeg' => '.mp2',
            'video/x-mpeq2a' => '.mp2',
            'audio/mpeg3' => '.mp3',
            'audio/x-mpeg-3' => '.mp3',
            'application/vnd.ms-project' => '.mpp',
            'application/marc' => '.mrc',
            'application/x-troff-ms' => '.ms',
            'application/x-vnd.audioexplosion.mzz' => '.mzz',
            'application/vnd.nokia.configuration-message' => '.ncm',
            'application/x-mix-transfer' => '.nix',
            'application/x-conference' => '.nsc',
            'application/x-navidoc' => '.nvd',
            'application/oda' => '.oda',
            'application/x-omc' => '.omc',
            'application/x-omcdatamaker' => '.omcd',
            'application/x-omcregerator' => '.omcr',
            'text/x-pascal' => '.p',
            'application/pkcs10' => '.p10',
            'application/x-pkcs10' => '.p10',
            'application/pkcs-12' => '.p12',
            'application/x-pkcs12' => '.p12',
            'application/x-pkcs7-signature' => '.p7a',
            'application/x-pkcs7-certreqresp' => '.p7r',
            'application/pkcs7-signature' => '.p7s',
            'text/pascal' => '.pas',
            'image/x-portable-bitmap' => '.pbm ',
            'application/vnd.hp-pcl' => '.pcl',
            'application/x-pcl' => '.pcl',
            'image/x-pict' => '.pct',
            'image/x-pcx' => '.pcx',
            'application/pdf' => '.pdf',
            'audio/make.my.funk' => '.pfunk',
            'image/x-portable-graymap' => '.pgm',
            'image/x-portable-greymap' => '.pgm',
            'application/x-newton-compatible-pkg' => '.pkg',
            'application/vnd.ms-pki.pko' => '.pko',
            'text/x-script.perl' => '.pl',
            'application/x-pixclscript' => '.plx',
            'text/x-script.perl-module' => '.pm',
            'application/x-portable-anymap' => '.pnm',
            'image/x-portable-anymap' => '.pnm',
            'model/x-pov' => '.pov',
            'image/x-portable-pixmap' => '.ppm',
            'application/powerpoint' => '.ppt',
            'application/x-mspowerpoint' => '.ppt',
            'application/x-freelance' => '.pre',
            'paleovu/x-pv' => '.pvu',
            'text/x-script.phyton' => '.py ',
            'applicaiton/x-bytecode.python' => '.pyc ',
            'audio/vnd.qcelp' => '.qcp ',
            'video/x-qtc' => '.qtc',
            'audio/x-realaudio' => '.ra',
            'application/x-cmu-raster' => '.ras',
            'image/x-cmu-raster' => '.ras',
            'text/x-script.rexx' => '.rexx ',
            'image/vnd.rn-realflash' => '.rf',
            'image/x-rgb' => '.rgb ',
            'application/vnd.rn-realmedia' => '.rm',
            'audio/mid' => '.rmi',
            'application/ringing-tones' => '.rng',
            'application/vnd.nokia.ringing-tone' => '.rng',
            'application/vnd.rn-realplayer' => '.rnx ',
            'image/vnd.rn-realpix' => '.rp ',
            'text/vnd.rn-realtext' => '.rt',
            'application/x-rtf' => '.rtf',
            'video/vnd.rn-realvideo' => '.rv',
            'audio/s3m' => '.s3m ',
            'application/x-lotusscreencam' => '.scm',
            'text/x-script.guile' => '.scm',
            'text/x-script.scheme' => '.scm',
            'video/x-scm' => '.scm',
            'application/sdp' => '.sdp ',
            'application/x-sdp' => '.sdp ',
            'application/sounder' => '.sdr',
            'application/sea' => '.sea',
            'application/x-sea' => '.sea',
            'application/set' => '.set',
            'application/x-sh' => '.sh',
            'text/x-script.sh' => '.sh',
            'audio/x-psid' => '.sid',
            'application/x-sit' => '.sit',
            'application/x-stuffit' => '.sit',
            'application/x-seelogo' => '.sl ',
            'audio/x-adpcm' => '.snd',
            'application/solids' => '.sol',
            'application/x-pkcs7-certificates' => '.spc ',
            'application/futuresplash' => '.spl',
            'application/streamingmedia' => '.ssm ',
            'application/vnd.ms-pki.certstore' => '.sst',
            'application/sla' => '.stl',
            'application/vnd.ms-pki.stl' => '.stl',
            'application/x-navistyle' => '.stl',
            'application/x-sv4cpio' => '.sv4cpio',
            'application/x-sv4crc' => '.sv4crc',
            'x-world/x-svr' => '.svr',
            'application/x-shockwave-flash' => '.swf',
            'application/x-tar' => '.tar',
            'application/toolbook' => '.tbk',
            'application/x-tcl' => '.tcl',
            'text/x-script.tcl' => '.tcl',
            'text/x-script.tcsh' => '.tcsh',
            'application/x-tex' => '.tex',
            'application/plain' => '.text',
            'application/gnutar' => '.tgz',
            'audio/tsp-audio' => '.tsi',
            'application/dsptype' => '.tsp',
            'audio/tsplayer' => '.tsp',
            'text/tab-separated-values' => '.tsv',
            'text/x-uil' => '.uil',
            'application/i-deas' => '.unv',
            'application/x-ustar' => '.ustar',
            'multipart/x-ustar' => '.ustar',
            'application/x-cdlink' => '.vcd',
            'text/x-vcalendar' => '.vcs',
            'application/vda' => '.vda',
            'video/vdo' => '.vdo',
            'application/groupwise' => '.vew ',
            'application/vocaltec-media-desc' => '.vmd ',
            'application/vocaltec-media-file' => '.vmf',
            'audio/voc' => '.voc',
            'audio/x-voc' => '.voc',
            'video/vosaic' => '.vos',
            'audio/voxware' => '.vox',
            'audio/x-twinvq' => '.vqf',
            'application/x-vrml' => '.vrml',
            'x-world/x-vrt' => '.vrt',
            'application/wordperfect6.1' => '.w61',
            'audio/wav' => '.wav',
            'audio/x-wav' => '.wav',
            'application/x-qpro' => '.wb1',
            'image/vnd.wap.wbmp' => '.wbmp',
            'application/vnd.xara' => '.web',
            'application/x-123' => '.wk1',
            'windows/metafile' => '.wmf',
            'text/vnd.wap.wml' => '.wml',
            'application/vnd.wap.wmlc' => '.wmlc ',
            'text/vnd.wap.wmlscript' => '.wmls',
            'application/vnd.wap.wmlscriptc' => '.wmlsc ',
            'application/x-wpwin' => '.wpd',
            'application/x-lotus' => '.wq1',
            'application/mswrite' => '.wri',
            'application/x-wri' => '.wri',
            'text/scriplet' => '.wsc',
            'application/x-wintalk' => '.wtk ',
            'image/x-xbitmap' => '.xbm',
            'image/x-xbm' => '.xbm',
            'image/xbm' => '.xbm',
            'video/x-amt-demorun' => '.xdr',
            'xgl/drawing' => '.xgz',
            'image/vnd.xiff' => '.xif',
            'audio/xm' => '.xm',
            'application/xml' => '.xml',
            'text/xml' => '.xml',
            'xgl/movie' => '.xmz',
            'application/x-vnd.ls-xpix' => '.xpix',
            'image/xpm' => '.xpm',
            'video/x-amt-showrun' => '.xsr',
            'image/x-xwd' => '.xwd',
            'image/x-xwindowdump' => '.xwd',
            'application/x-compress' => '.z',
            'application/x-zip-compressed' => '.zip',
            'application/zip' => '.zip',
            'multipart/x-zip' => '.zip',
            'text/x-script.zsh' => '.zsh',
        ];
        $map = array_flip($map);


        if (isset($map[$extension])) {
            return $map[$extension];
        }
        return null;
    }

    /**
     *
     * @var integer
     */
    protected $business_id;

    /**
     * Method to set the value of field business_id
     *
     * @param integer $business_id
     * @return $this
     */
    public function setBusinessId($business_id)
    {
        $this->business_id = $business_id;

        return $this;
    }

    /**
     * Returns the value of field business_id
     *
     * @return integer
     */
    public function getBusinessId()
    {
        return (int)$this->business_id;
    }
}
