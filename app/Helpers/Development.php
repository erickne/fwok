<?php



namespace Fwok\Helpers;

use Phalcon\DI as DI;
use Phalcon\Mvc\User\Component;

class Development extends Component
{
    const FIELD_TYPE_INT = 1;
    const FIELD_TYPE_FLOAT = 2;
    const FIELD_TYPE_STRING = 3;

    /**
     * Get the service via static call
     * @param $service
     * @return object
     */
    function getService($service)
    {
        return $this->getDI()->get($service);
    }

    static function getStaticService($service)
    {
        return DI::getDefault()->get($service);
    }

    static function getStaticDI()
    {
        return DI::getDefault();
    }

    static function creatorGetSet($fields, $type = Development::FIELD_TYPE_INT)
    {

    }
}