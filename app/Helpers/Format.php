<?php
namespace Fwok\Helpers;

use Fwok\Plugins\Logger;

class Format extends \Phalcon\Mvc\User\Component
{
    const OS_WINDOWS = 'win';
    const OS_UNIX = 'unix';

    public static function randomAlphaChar($string_length)
    {
        $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $string = '';
        for ($i = 0; $i < $string_length; $i++) {
            $string .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $string;
    }

    /**
     * Only Linux
     * @return float
     */
    public static function getServerMemoryUsage()
    {
        if (Format::detectOs() === Format::OS_UNIX) {
            $free = shell_exec('free');
            $free = (string)trim($free);
            $free_arr = explode("\n", $free);
            $mem = explode(" ", $free_arr[1]);
            $mem = array_filter($mem);
            $mem = array_merge($mem);
            return $mem[2] / $mem[1] * 100;
        } else {
            return false;
        }
    }


    /** função para remover null */
    public static function arrayRemoveNull($array)
    {
        foreach ($array as $key => $value) {
            if (is_null($value)) {
                unset($array[$key]);
            }
            if (is_array($value)) {
                $array[$key] = self::arrayRemoveNull($value);
            }
        }

        return $array;
    }

    /**
     * Only Linux
     * @return mixed
     */
    public static function getServerCpuUsage()
    {
        if (Format::detectOs() == Format::OS_UNIX) {
            $load = sys_getloadavg();

            return $load[0];
        } else {
            return false;
        }

    }

    /**
     * @return int
     */
    public static function detectOs()
    {
        if (DIRECTORY_SEPARATOR == '/') {
            return Format::OS_UNIX;
        }
        if (DIRECTORY_SEPARATOR == '\\') {
            return Format::OS_WINDOWS;
        }
    }

    static function seoUrl($string)
    {
        //Lower case everything
        $string = strtolower($string);
        //Make alphanumeric (removes all other characters)
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
        //Clean up multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);
        //Convert whitespaces and underscore to dash
        $string = preg_replace("/[\s_]/", "-", $string);

        return $string;
    }

    static function arrayToObject($array)
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $array[$key] = arrayToObject($value);
            }
        }

        return (object)$array;
    }

    /**
     * Convert a multidimensional array into string
     *
     * @param $array
     * @return string
     */
    static function convertMultiArrayIntoString($array)
    {
        $str = "";
        foreach ($array as $k => $i) {
            if (is_array($i)) {
                $str .= self::convertMultiArrayIntoString($i);
            } else {
                $str .= $k . "=>" . $i;
            }
        }

        return $str;
    }

    static function timeElapsedString($datetime, $full = false)
    {
        $now = new \DateTime;
        $ago = new \DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = [
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        ];
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) {
            $string = array_slice($string, 0, 1);
        }

        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    /**
     * Coloca em maiúsculo as primeiras letras de um nome, removendo as preposições. Ainda não está 100%.
     *
     * @param string $nome Nome a ser capitalizado
     *
     * @return string
     */
    public static function capitalizeNomeProprio($nome)
    {

        $partesNome = explode(" ", $nome);
        $exclude = ['da', 'das', 'de', 'do', 'dos', 'e'];
        $nomeCapitalizado = '';
        foreach ($partesNome as $parte) {
            if (in_array(strtolower($parte), $exclude)) {
                $parte = strtolower($parte);
            } else {
                $parte = ucfirst($parte);
            }
            $nomeCapitalizado .= $parte . " ";
        }

        return trim($nomeCapitalizado);
    }

    public static function sizeUnits($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    /**
     * Parse a number into fixed decimals and bring the number styles
     *
     * @param     $number   Number to be parsed
     * @param int $decimal Decimal numbers
     * @param int $color styleNumber parameter
     *
     * @return float
     */
    public static function toFixed($number, $decimal = 2, $currency = 0, $color = 0)
    {
        $number_format = number_format($number, $decimal, ',', '.');
        if ($currency) {
            $number_format = "R$ " . $number_format;
        }

        return Format::styleNumber($number, $number_format, $color);
    }

    /**
     * Style a number
     *
     * @param float $number
     * @param string $number_formatted
     * @param int $theme
     *
     * @return string
     */
    public static function styleNumber($number, $number_formatted, $theme = 0)
    {
        if ($theme == 1) {
            $c['positive'] = 'green_value';
            $c['negative'] = 'red_value';
            $c['zero'] = 'zero_value';
        } elseif ($theme == 2) {
            $c['positive'] = 'red_value';
            $c['negative'] = 'green_value';
            $c['zero'] = 'zero_value';
        }

        if ($number == 0) {
            $style = $c['zero'];
        } elseif ($number > 0) {
            $style = $c['positive'];
        } elseif ($number < 0) {
            $style = $c['negative'];
        }

        if ($theme) {
            return "<span class=\"$style\">" . $number_formatted . "</span>";
        } else {
            return $number_formatted;
        }
    }

    /**
     * Get time offset
     * $offset = +1 day, +1 week, +2 week, +1 month, +30 days, +1 year
     * $date = date('Y-m-d H:i:s')
     *
     * @param $date
     * @param $offset
     * @param string $output_format
     * @return bool|string
     */
    public static function dateGetOffset($date, $offset, $output_format = 'Y-m-d H:i:s')
    {
        $date = strtotime($date . ' ' . $offset);

        return date($output_format, $date);
    }

    /**
     * Get date difference
     *
     * return->days;
     *
     * @param string $date_start
     * @param string $date_end
     * @return \DateTime
     */
    public static function dateDiff($date_start, $date_end)
    {
        $dStart = new \DateTime($date_start);
        $dEnd = new \DateTime($date_end);
        $dDiff = $dStart->diff($dEnd);
        //echo $dDiff->format('%R'); // use for point out relation: smaller/greater
        //echo $dDiff->days;
        return $dDiff;
    }

    /**
     * Date with default format Y-m-d H:i:s
     *
     * @param $date_start
     * @param $date_end
     * @return false|int
     */
    public static function dateDiffInSeconds($date_start, $date_end = null)
    {
        if(!$date_end) {
            $date_end = date('Y-m-d H:i:s');
        }
        $timeFirst  = strtotime($date_start);
        $timeSecond = strtotime($date_end);
        $differenceInSeconds = $timeSecond - $timeFirst;
        return $differenceInSeconds;
    }

    public static function setDateTimeToDayEnd($date)
    {
        if (!$date) {
            return false;
        }
        //\DateTime::createFromFormat('Y-m-d H:i:s',$date);
        return substr($date, 0, 10) . ' 23:59:59';
    }

    public static function setDateTimeToDayStart($date)
    {
        if (!$date) {
            return false;
        }
        //\DateTime::createFromFormat('Y-m-d H:i:s',$date);
        return substr($date, 0, 10) . ' 00:00:00';
    }

    public static function dateFormat(
        $date,
        $output_format = 'd/m/Y H:i',
        $input_format = 'Y-m-d H:i:s',
        $return_date = 0
    ) {
        if ($date == "") {
            return;
        } else {
            $o_date = \DateTime::createFromFormat($input_format, $date);
            if (!$o_date instanceof \DateTime) {
                var_dump($o_date);
                Logger::shoutInAlert(sprintf('Tried to parse date [%s] with format [%s]', $date, $input_format));

                return false;
            }
            if ($return_date) {
                return $o_date;
            } else {
                return $o_date->format($output_format);
            }
        }
    }

    /**
     * @param $date1
     * @param $date2
     * @return int
     */
    public static function dateDiffDays($date1, $date2)
    {
        return round(abs(strtotime($date1) - strtotime($date2)) / (60 * 60 * 24));
    }

    /**
     * Verify if given date is in the future
     * Param format: Y-m-d H:i:s
     *
     * @param str
     *
     * @return boolean
     */
    public static function isFuture($date)
    {
        $dnow = \DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));
        $dreq = \DateTime::createFromFormat('Y-m-d H:i:s', $date);
        if ($dnow < $dreq) {
            return true;
        } else {
            return false;
        }
    }

    public static function surroundWith($string, $char)
    {
        return $char . $string . $char;
    }

    /**
     * Translates a camel case string into a string with underscores (e.g. firstName -&gt; first_name)
     * @param    string $str String in camel case format
     * @return    string            $str Translated into underscore format
     */
    public static function fromCamelCase($str)
    {
        $str[0] = strtolower($str[0]);
        $func = create_function('$c', 'return "_" . strtolower($c[1]);');

        return preg_replace_callback('/([A-Z])/', $func, $str);
    }

    /**
     * Translates a string with underscores into camel case (e.g. first_name -&gt; firstName)
     * @param    string $str String in underscore format
     * @param    bool $capitalise_first_char If true, capitalise the first char in $str
     * @return   string                              $str translated into camel caps
     */
    public static function toCamelCase($str, $capitalise_first_char = false)
    {
        if ($capitalise_first_char) {
            $str[0] = strtoupper($str[0]);
        }
        $func = create_function('$c', 'return strtoupper($c[1]);');

        return preg_replace_callback('/_([a-z])/', $func, $str);
    }


    /**
     * Convert under_score type array's keys to camelCase type array's keys
     * @param   array   $array          array to convert
     * @param   array   $arrayHolder    parent array holder for recursive array
     * @return  array   camelCase array
     */
    public static function camelCaseKeys($array, $arrayHolder = array()) {
        $camelCaseArray = !empty($arrayHolder) ? $arrayHolder : array();
        foreach ($array as $key => $val) {
            $newKey = @explode('_', $key);
            array_walk($newKey, create_function('&$v', '$v = ucwords($v);'));
            $newKey = @implode('', $newKey);
            $newKey{0} = strtolower($newKey{0});
            if (!is_array($val)) {
                $camelCaseArray[$newKey] = $val;
            } else {
                $camelCaseArray[$newKey] = self::camelCaseKeys($val, $camelCaseArray[$newKey]);
            }
        }
        return $camelCaseArray;
    }
}
