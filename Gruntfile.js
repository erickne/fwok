module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
                mangle: false
            },
            build: {
                src: 'src/<%= pkg.name %>.js',
                dest: 'build/<%= pkg.name %>.min.js'
            },

            third: {
                files: {
                    'public/admin/assets/js/third.min.js': [
                        'public/bower_components/jquery/dist/jquery.min.js',
                        'public/bower_components/bootstrap/dist/js/bootstrap.min.js',
                        'public/bower_components/angular/angular.js',
                        'public/bower_components/moment/moment.js',
                        'public/bower_components/angular-animate/angular-animate.min.js',
                        'public/bower_components/angular-cookies/angular-cookies.min.js',
                        'public/bower_components/angular-csv-import/dist/angular-csv-import.min.js',
                        'public/bower_components/angular-route/angular-route.js',
                        'public/bower_components/angular-sanitize/angular-sanitize.min.js',
                        'public/bower_components/angular-mocks/angular-mocks.js',
                        'public/bower_components/angular-ui-bootstrap/ui-bootstrap.min.js',
                        'public/bower_components/angular-ui-bootstrap/ui-bootstrap-tpls.min.js',
                        'public/bower_components/angularjs-toaster/toaster.min.js',
                        'public/bower_components/angular-strap/dist/angular-strap.min.js',
                        'public/bower_components/angular-strap/dist/angular-strap.tpl.min.js',
                        'public/bower_components/angular-xeditable/dist/js/xeditable.min.js',
                        'public/bower_components/angular-base64-upload/dist/angular-base64-upload.min.js',
                        'public/bower_components/angular-loading-bar/build/loading-bar.min.js',
                        'public/bower_components/ng-table/dist/ng-table.min.js',
                        'public/bower_components/angular-auto-validate/dist/jcs-auto-validate.min.js',
                        'public/bower_components/angular-busy/dist/angular-busy.min.js',
                        'public/bower_components/angular-moment/angular-moment.min.js',
                        'public/bower_components/angular-ui-select/dist/select.min.js',
                        'public/bower_components/angular-ui-calendar/src/calendar.js',
                        'public/bower_components/fullcalendar/dist/fullcalendar.min.js',
                        'public/bower_components/fullcalendar/dist/gcal.js',
                        'public/bower_components/fullcalendar/dist/lang/pt-br.js',
                        'public/bower_components/angulartics/dist/angulartics.min.js',
                        'public/bower_components/angulartics-google-analytics/dist/angulartics-ga.min.js',
                        'public/bower_components/angulartics-google-tag-manager/dist/angulartics-google-tag-manager.min.js',
                        'public/bower_components/gsap/src/minified/TweenMax.min.js',
                        'public/bower_components/ng-fx/dist/ngFx.min.js',
                        'public/bower_components/tv4/tv4.js',
                        'public/bower_components/objectpath/lib/ObjectPath.js',
                        'public/bower_components/api-check/dist/api-check.min.js',
                        'public/bower_components/angular-i18n/angular-locale_pt-br.js',
                        'public/bower_components/angular-input-masks/angular-input-masks-standalone.js',
                        'public/bower_components/angular-input-masks/angular-input-masks-dependencies.js'
                    ]
                }
            }
        }, // uglify
        sass: {
            dist: {
                options: {style: 'expanded'},
                files: {
                    'public/admin/assets/src/style.css': 'public/admin/assets/_sass/style.sass',
                    'public/admin/assets/src/panels.css': 'public/admin/assets/_sass/panels.sass',
                    'public/admin/assets/src/third.extended.css': 'public/admin/assets/_sass/third.extended.sass',
                    'public/admin/assets/src/third/text-angular.css': 'public/admin/assets/_sass/third/text-angular.sass',
                    'public/admin/assets/src/third/angular-busy.css': 'public/admin/assets/_sass/third/angular-busy.sass'
                }
            }
        }, // sass
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'public/admin/assets/css/main.min.css': [
                        'public/admin/assets/src/style.css',
                        'public/admin/assets/src/panels.css',
                        'public/admin/assets/src/third.extended.css',
                        'public/admin/assets/src/third/text-angular.css',
                        'public/admin/assets/src/third/angular-busy.css'
                    ],
                    'public/admin/assets/css/third.min.css': [
                        'public/bower_components/html5-boilerplate/dist/css/normalize.css',
                        'public/bower_components/html5-boilerplate/dist/css/main.css',
                        'public/bower_components/bootstrap/dist/css/bootstrap.min.css',
                        'public/bower_components/bootstrap/dist/css/bootstrap-theme.min.css',
                        'public/bower_components/angular-ui-bootstrap/ui-bootstrap-csp.css',
                        'public/bower_components/angular-xeditable/dist/css/xeditable.min.css',
                        'public/bower_components/angular-loading-bar/build/loading-bar.min.css',
                        'public/bower_components/ng-table/dist/ng-table.min.css',
                        'public/bower_components/angular-busy/dist/angular-busy.min.css',
                        'public/bower_components/angular-ui-select/dist/select.min.css',
                        'public/bower_components/components-font-awesome/css/font-awesome.min.css',
                        'public/bower_components/fullcalendar/dist/fullcalendar.min.css',
                        'public/bower_components/angularjs-toaster/toaster.min.css',
                        'public/bower_components/nvd3/build/nv.d3.css',
                        'public/bower_components/textAngular/dist/textAngular.css'
                    ]
                }
            }
        },//cssmin
        concat: {
            options: {
                separator: ';'
            },
            nvd3: {
                src: [
                    'public/bower_components/d3/d3.js',
                    'public/bower_components/nvd3/build/nv.d3.js',
                    'public/bower_components/angular-nvd3/dist/angular-nvd3.js',
                    'public/bower_components/textAngular/dist/textAngular-rangy.min.js',
                    'public/bower_components/textAngular/dist/textAngular-sanitize.min.js',
                    'public/bower_components/textAngular/dist/textAngular.js',
                    'public/bower_components/textAngular/dist/textAngularSetup.js'
                ],
                dest: 'public/admin/assets/js/third.nvd3.js'
            },
            textAngular: {
                src: [
                    'public/bower_components/textAngular/dist/textAngular-rangy.min.js',
                    'public/bower_components/textAngular/dist/textAngular-sanitize.min.js',
                    'public/bower_components/textAngular/dist/textAngular.js',
                    'public/bower_components/textAngular/dist/textAngularSetup.js'
                ],
                dest: 'public/admin/assets/js/third.textAngular.js'
            }
        } //concat
    });

    // Plugins do Grunt
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    // Tarefas que serão executadas
    //grunt.registerTask( 'default', [ 'uglify' ] );
    grunt.registerTask('default', ['uglify', 'sass', 'cssmin']);

};