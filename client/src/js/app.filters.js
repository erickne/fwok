(function () {
    'use strict';

    angular
        .module(APP_MODULE)
        .filter('toHtml', ['$sce', function ($sce) {
            return function (text) {
                return $sce.trustAsHtml(text);
            };
        }])
        /**
         * <p ng-hide="items | isEmpty">Some Content</p>
         */
        .filter('isEmpty', function () {
            var bar;
            return function (obj) {
                for (bar in obj) {
                    if (obj.hasOwnProperty(bar)) {
                        return false;
                    }
                }
                return true;
            };
        });
});