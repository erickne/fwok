/*!
 * 
 * Naut - Bootstrap Admin Theme + AngularJS
 * 
 * Author: @geedmo
 * Website: http://geedmo.com
 * License: https://wrapbootstrap.com/help/licenses
 * 
 */
APP_MODULE = 'ultra';

angular
    .module('ultra', [
        'toaster',
        'ngRoute',
        'ngAnimate',
        'ngStorage',
        'ngCookies',
        'ngSanitize',
        'ngResource',
        'ui.bootstrap',
        'ui.router',
        'ui.utils',
        'oc.lazyLoad',
        'cfp.loadingBar',
        'pascalprecht.translate',
        'cgBusy',
        'ngTable'
    ])

    
    .config(['settingsConfigProvider', function (settingsConfigProvider) {

        //noinspection JSAnnotator
        settingsConfigProvider.setSettings({
            name: 'Ultra Admin Fwok',
            description: 'Administration Interface',
            year: new Date().getFullYear(),
            views: {
                animation: 'ng-fadeInLeft2'
            },
            router : {
                otherwise : '/app/dashboard'
            },
            layout: {
                isFixed: false,
                isBoxed: false,
                isDocked: false,
                isMaterial: true
            },
            sidebar: {
                isOffscreen: false,
                isMini: false,
                isRight: false
            },
            footer: {
                hidden: false
            },
            themeId: 1
        });

    }])
    .value('ngTableDefaults', {
        page: 1,            // show first page
        count: 20           // count per page
    })
    .value('cgBusyDefaults', {
        message: 'Loading',
        delay: 100
    })
;