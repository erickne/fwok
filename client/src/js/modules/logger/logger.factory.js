(function() {
    'use strict';

    angular
        .module(APP_MODULE)
        .factory('logger', logger);

    logger.$inject = ['$log', 'toaster'];

    /* @ngInject */
    function logger($log, toaster) {
        return {
            showToasts: true,

            error   : error,
            info    : info,
            success : success,
            warning : warning,

            // straight to console; bypass toaster
            log     : log//$log.log
        };
        /////////////////////


        function log(message, data) {
            $log.log('Log: ' + message, data);
        }

        function error(message, data, title) {
            toaster.pop({
                type: 'error',
                title: title,
                body: message,
                timeout: 3000
            });
            $log.error('Error: ' + message, data);
        }

        function info(message, data, title) {
            toaster.pop({
                type: 'info',
                title: title,
                body: message,
                timeout: 3000
            });
            $log.info('Info: ' + message, data);
        }

        function success(message, data, title) {
            toaster.pop({
                type: 'success',
                title: title,
                body: message,
                timeout: 3000
            });
            $log.info('Success: ' + message, data);
        }

        function warning(message, data, title) {
            toaster.pop({
                type: 'warning',
                title: title,
                body: message,
                timeout: 3000
            });
            $log.warn('Warning: ' + message, data);
        }
    }
}());
