(function() {
    'use strict';

    angular
        .module(APP_MODULE)
        .config(httpHandlersConfig);
    /* @ngInject */
    function httpHandlersConfig($httpProvider) {
        $httpProvider.interceptors.push('httpRequestInterceptor');
    }
    httpHandlersConfig.$inject = ['$httpProvider'];

})();