/**
 * Factory FWOK to control HTTP requests
 */
(function () {
    'use strict';

    angular.module(APP_MODULE)
        .factory('httpHandler', function (toaster, $location) {
            return {
                response: responseHandler,
                responseError: responseErrorHandler
            };

            function responseHandler(result) { // (result, request)

                // Evita que quando for salvar alguma preferência fique disparando pop de confirmação
                // TODO Substituir com um variável na request, como noToaster = 1
                if (result.config.url == "/api/settings/" && (result.status == 200 || result.status == 201)) {
                    return result;
                }

                var toasterCfg = {
                    msg: '',
                    title: '',
                    type: ''
                };

                if (result.config.method == 'POST' && result.status == 201) {
                    toasterCfg.type = 'success';
                    toasterCfg.title = 'Salvo!';
                }
                if (result.config.method == 'PUT' && result.status == 200) {
                    toasterCfg.type = 'success';
                    toasterCfg.title = 'Atualizado!';
                }
                if (result.config.method == 'DELETE' && result.status == 200) {
                    toasterCfg.type = 'success';
                    toasterCfg.title = 'Excluído!';
                }

                if (toasterCfg.type) {
                    toaster.pop({
                        type: toasterCfg.type,
                        title: toasterCfg.title,
                        body: toasterCfg.msg,
                        bodyOutputType: 'trustedHtml'
                    });
                }

                if (result.data.meta) {
                    angular.forEach(result.data.meta.messages, function (messageObj) {

                        switch (messageObj.mode) {
                            case 'console':
                                console.log(messageObj.message);
                                break;
                            case 'toaster':
                                toaster.pop({
                                    type: messageObj.type,
                                    title: messageObj.title,
                                    body: messageObj.message,
                                    bodyOutputType: 'trustedHtml'
                                });
                                break;
                            default:
                                console.log('Invalid message object mode:', messageObj.mode);
                                console.log('Nessage object:', messageObj);
                        }
                    });
                }
                return result;
            }

            function responseErrorHandler(rejection, request) {
                switch (rejection.status) {
                    case 400:
                    case 403:
                    case 404:
                        if (rejection.data.meta.errors.code == 'INP-VALU-001') {

                            var msg = '';
                            if (request.method == 'POST' || request.method == 'PUT' || request.method == 'DELETE') {
                                if (rejection.data) {
                                    if (rejection.data.meta) {
                                        if (rejection.data.meta.errors) {
                                            if (rejection.data.meta.errors.validationMessages) {
                                                angular.forEach(rejection.data.meta.errors.validationMessages, function (validationMessage) {
                                                    msg = msg + '<li>' + validationMessage + '</li>';
                                                });
                                            }
                                            if (msg) msg = '<ul>' + msg + '</ul>';
                                        }
                                    }
                                }
                            }

                            toaster.pop({
                                type: 'error',
                                title: rejection.data.meta.errors.message,
                                body: msg,
                                bodyOutputType: 'trustedHtml'
                            });
                        } else {
                            toaster.pop('error', 'Erro!', rejection.data.meta.errors.message);
                        }
                        break;
                    case 401:
                        toaster.pop('info', 'Usuário não autenticado!', 'Por favor realize seu login');
                        $location.path('/auth/login');
                        break;
                    case 500:
                    case 503:
                        console.log('Erro ' + rejection.status, rejection.data);
                        if (angular.isUndefined(rejection.data.meta)) {
                            toaster.pop('error', 'Erro de sistema!', 'Uma notificação foi enviada aos administradores');
                        } else {
                            toaster.pop('error', 'Erro', rejection.data.meta.errors.message);
                        }
                        break;
                    default:
                        //throw Error('Invalid error response code: ' + rejection.status);
                        //console.log('Invalid error response code: ' + rejection.status);
                        break;
                }

            }
        })
        .factory('httpRequestInterceptor', function ($q, $rootScope, $location, $injector, httpHandler, $cookieStore) {
            var request = null;
            return {
                request: function (reqConf) {
                    reqConf.headers['Content-Type'] = 'application/json';
                    //reqConf.headers['fw-version'] = config.version;
                    if (angular.isDefined($cookieStore.get('token'))) {
                        reqConf.headers['Authorization'] = 'Bearer ' + $cookieStore.get('token');
                    }

                    if (reqConf.url.indexOf('.html') == -1 && reqConf.url.indexOf('.json') == -1) {
                        reqConf.url = '/api/' + reqConf.url;
                    } else {

                    }

                    request = reqConf;
                    return reqConf;
                },
                // On request failure
                // Return the promise rejection.
                requestError: function (rejection) {
                    return $q.reject(rejection);
                },
                response: function (result) {
                    httpHandler.response(result, request);
                    return result;
                },
                responseError: function (rejection) {
                    httpHandler.responseError(rejection, request);
                    return $q.reject(rejection);
                }
            };
        })
}());
