'use strict';

angular.module(APP_MODULE)
    .service('User', function ($http) {

        this.getMe = function () {
            return $http.get('fwok/users/me');
        }
    })
;