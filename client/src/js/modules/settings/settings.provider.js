/**=========================================================
 * Module: SettingsService
 =========================================================*/

(function () {
    'use strict';

    angular
        .module(APP_MODULE)
        .provider('settingsConfig', settingsConfig);

    function settingsConfig() {
        this.settings = {};

        this.$get = function () {
            var settings = this.settings;
            return {
                getSettings: function () {
                    return settings;
                }
            }
        };

        this.setSettings = function (settings) {
            this.settings = settings;
            return this.settings;
        };

    }

})();
