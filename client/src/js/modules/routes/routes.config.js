/**=========================================================
 * Module: RoutesConfig.js
 =========================================================*/

(function () {
    'use strict';

    angular
        .module(APP_MODULE)
        .config(routesConfig);

    routesConfig.$inject = ['$locationProvider', '$stateProvider', '$urlRouterProvider', 'RouteProvider', 'settingsConfigProvider'];
    function routesConfig($locationProvider, $stateProvider, $urlRouterProvider, Route, settingsConfigProvider) {

        var settings = settingsConfigProvider.settings;

        if(!angular.isDefined(settings.router.otherwise)){
            throw Error('Configuration error: Settings->router->otherwise');
        }

        // use the HTML5 History API
        $locationProvider.html5Mode(false);

        // Default route
        $urlRouterProvider.otherwise(settings.router.otherwise);

        // Application Routes States
        $stateProvider
            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: Route.base('app.html'),
                resolve: {
                    _assets: Route.require('icons', 'toaster', 'animate')
                }
            })
            .state('page', {
                url: '/page',
                abstract: true,
                templateUrl: Route.base('page.html'),
                resolve: {
                    _assets: Route.require('icons', 'toaster', 'animate')
                }
            })
            .state('auth', {
                url: '/auth',
                abstract: true,
                templateUrl: Route.base('auth.html'),
                resolve: {
                    _assets: Route.require('icons', 'toaster', 'animate')
                }
            })
            // Layout dock
            .state('app-dock', {
                url: '/dock',
                abstract: true,
                templateUrl: Route.base('app-dock.html'),
                resolve: {
                    assets: Route.require('icons', 'toaster', 'animate')
                }
            })
            .state('app-dock.dashboard', {
                url: '/dashboard',
                templateUrl: Route.base('dashboard.html'),
                resolve: {}
            })
            // Layout full height
            .state('app-fh', {
                url: '/fh',
                abstract: true,
                templateUrl: Route.base('app-fh.html'),
                resolve: {
                    assets: Route.require('icons', 'toaster', 'animate')
                }

            })
            .state('app-fh.columns', {
                url: '/columns',
                templateUrl: Route.base('layout.columns.html')
            })
    }

})();

