/**=========================================================
 * Module: CoreController.js
 =========================================================*/

(function () {
    'use strict';

    angular
        .module(APP_MODULE)
        .controller('CoreController', CoreController);

    /* @ngInject */
    function CoreController($scope, $rootScope, $location, objectsName) {
        // Get title for each page
        $rootScope.pageTitle = function () {
            return $rootScope.app.name + ' - ' + $rootScope.app.description;
        };

        // Cancel events from templates
        // ----------------------------------- 
        $rootScope.cancel = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
        };

        $scope.go = function (object,event) {
            //console.log(object);
            $location.path('/app/' + object.object + '/' + object.id);
            $rootScope.cancel(event);
        };

        $scope.objectsName = {
            users : 'users'
        };
        
        $scope.userPreferences = {
            tables: {
                showFilter: true
            }
        };

    }

    CoreController.$inject = ['$scope', '$rootScope', '$location'];

})();
