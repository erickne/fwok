/**=========================================================
 * Module: DashboardController.js
 =========================================================*/

(function() {
    'use strict';

    angular
        .module(APP_MODULE)
        .controller('DashboardController', function ($scope) {
            $scope.headerTile = 'Dashboard';
            $scope.headerSubTile = 'Dashboard subtitle';
            $scope.description = 'This project is an application skeleton for a typical AngularJS web app. You can use it to quickly bootstrap your angular webapp projects and dev environment for these projects.' +
                'The seed app doesnt do much, just shows how to wire some controllers and views together.';

        })
      

})();
