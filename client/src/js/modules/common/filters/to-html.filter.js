/**=========================================================
 * Module: TitleCaseFilter.js
 * Convert any case to title
 =========================================================*/

(function() {
    'use strict';

    angular
        .module(APP_MODULE)
        .filter('toHtml', ['$sce', function ($sce) {
            return function (text) {
                return $sce.trustAsHtml(text);
            };
        }])

})();
