/**=========================================================
 * Module: LinkObjectDirective.js
 * Builds up a link from FWOK object
 =========================================================*/

(function() {
    'use strict';

    angular
        .module(APP_MODULE)
        .directive('linkObject', linkObject);

    linkObject.$inject = ['$location'];
    function linkObject($location) {
        return {
            restrict: 'EA',
            scope: {
                object: '=?',
                objectName: '=?',
                objectId: '=?',
                caption: '=?'
            },
            transclude: true,
            template: '<a href="{{link}}">{{caption}}' +
            '<ng-transclude></ng-transclude>' +
            '</a>',

            controller: controller,
            link: link
        };

        function controller($scope) {
        }

        function link(scope, element, attributes) {
            scope.$watch('object', function (object, oldVal) {
                if (angular.isDefined(object)) {
                    scope.link = '/#/app/' + object.object + '/' + object.id
                } else {
                    scope.link = '/#/app/' + scope.objectName + '/' + scope.objectId
                }
            }, true);

            element.on('click touchstart', function (event) {
                $location.path(scope.link);
                event.stopPropagation();
            })
        }
    }
    
})();

