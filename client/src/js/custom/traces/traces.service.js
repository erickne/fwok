'use strict';

angular.module(APP_MODULE)
    .service('Trace', function ($http) {

        this.headerTitle = "Traces";
        this.headerSubTitle = "View system traces";
        
        this.getAll = function (params) {
            return $http.get('fwok/traces', {params: params});
        };
        this.getId = function (id, params) {
            return $http.get('fwok/traces/'+id, {params: params});
        };
        
    })
;