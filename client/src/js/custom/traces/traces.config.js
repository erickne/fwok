'use strict';

angular.module(APP_MODULE)
    .config(function ($stateProvider) {
        $stateProvider
            .state('app.logs-traces', {
                url: '/traces',
                templateUrl: 'app/views/traces/list.html',
                controller: 'TracesListCtrl'
            })
            .state('app.logs-traces-view', {
                url: '/traces/:id',
                templateUrl: 'app/views/traces/view.html',
                controller: 'TracesViewCtrl'
            })
    });