'use strict';

angular.module(APP_MODULE)
    .controller('TracesListCtrl', function ($scope, utils, Trace, NgTableParams, ngTableDefaults) {
        $scope.headerTile = Trace.headerTitle;
        $scope.headerSubTile = Trace.headerSubTitle;
        $scope.traces = [];
        
        $scope.tableParams = new NgTableParams(ngTableDefaults, {
            getData: function (params) {
                return $scope.busy = Trace.getAll(utils.convertNgTableToFwokParams(params)).success(function (data) {
                    $scope.traces = data.records;
                }).then(function(response){
                    params.total(response.data.meta.count_max);
                    $scope.headerSubTile = "View system traces. Total ["+response.data.meta.count_max+"] rows";
                    return $scope.traces;
                })
            }
        });

    })
    .controller('TracesViewCtrl', function ($scope, $stateParams, Trace) {
        $scope.trace = {};
        $scope.headerTile = Trace.headerTitle;
        $scope.headerSubTile = "";

        $scope.busy = Trace.getId($stateParams.id, {}).success(function (data) {
            $scope.trace = data.records;
        })

    });