'use strict';

angular.module(APP_MODULE)
    .service('Businesses', function ($http) {

        this.headerTitle = "Businesses";
        this.headerSubTitle = "Manage system businesses";

        this.getAll = function (params) {
            return $http.get('fwok/businesses', {params: params});
        };
        this.getId = function (id, params) {
            return $http.get('fwok/businesses/'+id, {params: params});
        };
        
    })
;