'use strict';

angular.module(APP_MODULE)
    .controller('BusinessesListCtrl', function ($scope, utils, Businesses, NgTableParams, ngTableDefaults) {
        $scope.headerTile = Businesses.headerTitle;
        $scope.headerSubTile = Businesses.headerSubTitle;
        $scope.businesses = [];

        $scope.tableParams = new NgTableParams(ngTableDefaults, {
            getData: function (params) {
                return $scope.busy = Businesses.getAll(utils.convertNgTableToFwokParams(params)).success(function (data) {
                    $scope.businesses = data.records;
                }).then(function(response){
                    params.total(response.data.meta.count_max);
                    $scope.headerSubTile = Businesses.headerSubTitle+" Total ["+response.data.meta.count_max+"] rows";
                    return $scope.businesses;
                })
            }
        });

    })
    .controller('BusinessesViewCtrl', function ($scope, $stateParams, Businesses) {
        $scope.headerTile = Businesses.headerTitle;
        $scope.headerSubTile = "";
        $scope.business = {};

        var params = {};
        $scope.busy = Businesses.getId($stateParams.id, params).success(function (data) {
            $scope.business = data.records;
        })

    });
