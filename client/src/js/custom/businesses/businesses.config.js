'use strict';

angular.module(APP_MODULE)
    .config(function ($stateProvider) {
        $stateProvider
            .state('app.users-businesses', {
                url: '/businesses',
                templateUrl: 'app/views/businesses/list.html',
                controller: 'BusinessesListCtrl'
            })
            .state('app.users-businesses-view', {
                url: '/businesses/:id',
                templateUrl: 'app/views/businesses/view.html',
                controller: 'BusinessesViewCtrl'
            })
    });