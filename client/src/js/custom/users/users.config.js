'use strict';

angular.module(APP_MODULE)
    .config(function ($stateProvider) {
        $stateProvider
            .state('app.users-users', {
                url: '/users',
                templateUrl: 'app/views/users/list.html',
                controller: 'UsersListCtrl'
            })
            .state('app.users-users-view', {
                url: '/users/:id',
                templateUrl: 'app/views/users/view.html',
                controller: 'UsersViewCtrl'
            })
    });