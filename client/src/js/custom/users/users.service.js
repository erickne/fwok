'use strict';

angular.module(APP_MODULE)
    .service('Users', function ($http) {

        this.headerTitle = "Users";
        this.headerSubTitle = "Manage users";

        this.getAll = function (params) {
            return $http.get('fwok/users', {params: params});
        };
        this.getId = function (id, params) {
            return $http.get('fwok/users/'+id, {params: params});
        };
        
    })
;