'use strict';

angular.module(APP_MODULE)
    .controller('UsersListCtrl', function ($scope, utils, Users, NgTableParams, ngTableDefaults) {
        $scope.headerTile = Users.headerTitle;
        $scope.headerSubTile = Users.headerSubTitle;
        $scope.users = [];

        $scope.tableParams = new NgTableParams(ngTableDefaults, {
            getData: function (params) {
                return $scope.busy = Users.getAll(utils.convertNgTableToFwokParams(params)).success(function (data) {
                    $scope.users = data.records;
                }).then(function(response){
                    params.total(response.data.meta.count_max);
                    $scope.headerSubTile = "View system users. Total ["+response.data.meta.count_max+"] rows";
                    return $scope.users;
                })
            }
        });

    })
    .controller('UsersViewCtrl', function ($scope, $stateParams, Users) {
        $scope.user = {
            subscription : {}
        };
        $scope.headerTile = Users.headerTitle;
        $scope.headerSubTile = "";

        $scope.busy = Users.getId($stateParams.id, {}).success(function (data) {
            $scope.user = data.records;
        })

    });