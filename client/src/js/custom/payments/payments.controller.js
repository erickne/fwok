'use strict';

angular.module(APP_MODULE)
    .controller('PaymentsListCtrl', function ($scope, utils, Payments, NgTableParams, ngTableDefaults) {
        $scope.headerTile = Payments.headerTitle;
        $scope.headerSubTile = Payments.headerSubTitle;
        $scope.payments = [];

        $scope.tableParams = new NgTableParams(ngTableDefaults, {
            getData: function (params) {
                return $scope.busy = Payments.getAll(utils.convertNgTableToFwokParams(params)).success(function (data) {
                    $scope.payments = data.records;
                }).then(function(response){
                    params.total(response.data.meta.count_max);
                    $scope.headerSubTile = "View system payments. Total ["+response.data.meta.count_max+"] rows";
                    return $scope.payments;
                })
            }
        });

    })
    .controller('PaymentsViewCtrl', function ($scope, $stateParams, Payments) {
        $scope.payment = {
            subscription : {}
        };
        $scope.headerTile = Payments.headerTitle;
        $scope.headerSubTile = "";

        $scope.busy = Payments.getId($stateParams.id, {}).success(function (data) {
            $scope.payment = data.records;
        });

        $scope.refreshStatus = function() {
            $scope.busy = Payments.refreshStatus($scope.payment.id).success(function(data){
                $scope.statusResponse = data.records;
            })
        };

        $scope.statusResponse = {}

    });