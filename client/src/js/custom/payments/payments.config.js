'use strict';

angular.module(APP_MODULE)
    .config(function ($stateProvider) {
        $stateProvider
            .state('app.financial-payments', {
                url: '/payments',
                templateUrl: 'app/views/payments/list.html',
                controller: 'PaymentsListCtrl'
            })
            .state('app.financial-payments-view', {
                url: '/payments/:id',
                templateUrl: 'app/views/payments/view.html',
                controller: 'PaymentsViewCtrl'
            })
    });