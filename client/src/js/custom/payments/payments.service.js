'use strict';

angular.module(APP_MODULE)
    .service('Payments', function ($http) {

        this.headerTitle = "Payment";
        this.headerSubTitle = "Manage system payments";

        this.getAll = function (params) {
            return $http.get('fwok/payments', {params: params});
        };
        this.getId = function (id, params) {
            return $http.get('fwok/payments/'+id, {params: params});
        };
        this.refreshStatus = function (id, params) {
            return $http.get('fwok/payments/refresh/'+id, {params: params});
        };
        
    })
;