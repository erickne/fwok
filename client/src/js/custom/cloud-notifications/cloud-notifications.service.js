'use strict';

angular.module(APP_MODULE)
    .service('CloudNotifications', function ($http) {

        this.headerTitle = "Cloud Notifications";
        this.headerSubTitle = "Google Cloud Messages sent to users devices";

        this.getAll = function (params) {
            return $http.get('fwok/cloud-notifications', {params: params});
        };
        this.getId = function (id, params) {
            return $http.get('fwok/cloud-notifications/'+id, {params: params});
        };
        
    })
;