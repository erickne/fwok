'use strict';

angular.module(APP_MODULE)
    .controller('CloudNotificationsListCtrl', function ($scope, utils, CloudNotifications, NgTableParams, ngTableDefaults) {
        $scope.headerTile = CloudNotifications.headerTitle;
        $scope.headerSubTile = CloudNotifications.headerSubTitle;
        $scope.cloudNotifications = [];

        $scope.tableParams = new NgTableParams(ngTableDefaults, {
            total: 10,
            getData: function (params) {
                return $scope.busy = CloudNotifications.getAll(utils.convertNgTableToFwokParams(params)).success(function (data) {
                    $scope.cloudNotifications = data.records;
                }).then(function (response) {
                    params.total(response.data.meta.count_max);
                    $scope.headerSubTile = CloudNotifications.headerSubTitle+". Total [" + response.data.meta.count_max + "] rows";
                    return $scope.cloudNotifications;
                })
            }
        });

    })
    .controller('CloudNotificationsViewCtrl', function ($scope, $stateParams, CloudNotifications) {
        $scope.headerTile = CloudNotifications.headerTitle;
        $scope.headerSubTile = "";
        $scope.cloudNotification = {};

        $scope.busy = CloudNotifications.getId($stateParams.id, {}).success(function (data) {
            $scope.cloudNotification = data.records;
        })

    });