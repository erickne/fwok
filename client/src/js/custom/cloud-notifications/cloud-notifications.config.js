'use strict';

angular.module(APP_MODULE)
    .config(function ($stateProvider) {
        $stateProvider
            .state('app.integrations-cloudnotifications', {
                url: '/cloud-notifications',
                templateUrl: 'app/views/cloud-notifications/list.html',
                controller: 'CloudNotificationsListCtrl'
            })
            .state('app.integrations-cloudnotifications-view', {
                url: '/cloud-notifications/:id',
                templateUrl: 'app/views/cloud-notifications/view.html',
                controller: 'CloudNotificationsViewCtrl'
            })
    });