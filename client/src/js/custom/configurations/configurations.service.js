'use strict';

angular.module(APP_MODULE)
    .service('Configuration', function ($http) {

        this.getAll = function (params) {
            return $http.get('fwok/configurations', {params: params});
        };
        
    })
;