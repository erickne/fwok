'use strict';

angular.module(APP_MODULE)
  .config(function ($stateProvider) {
    $stateProvider
      .state('app.system-configuration', {
        url: '/configurations',
        templateUrl: 'app/views/configurations/list.html',
        controller: 'ConfigurationsListCtrl'
      })
  });