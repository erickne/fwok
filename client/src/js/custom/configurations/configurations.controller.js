'use strict';

angular.module(APP_MODULE)
  .controller('ConfigurationsListCtrl', function ($scope, Configuration, NgTableParams) {
    $scope.headerTile = "Configuration";
    $scope.headerSubTile = "Manage system configuration parameters";
    $scope.configurations = [];

    $scope.tableParams = new NgTableParams({}, {
      getData: function ($defer, params) {
        $scope.busy = Configuration.getAll().success(function (data) {
          $defer.resolve(data.records);
          $scope.configurations = data.records;
        })
      }
    });
  });
