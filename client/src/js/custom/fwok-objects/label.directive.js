(function() {
    'use strict';

    angular.module('ultra', [])
        .directive('labelBase', function () {
            return {
                restrict: 'EA',
                scope: {
                    class: '=',
                    colorSet: '=',
                    tooltipText: '=',
                    block: '=',
                    hideTooltip: '='
                },
                transclude: true,
                template: '<span data-title="{{tooltipText}}" bs-tooltip>' +
                '<i ng-show="block" style="color: {{coloset.backColor}}; " class="fa fa-circle icon-block"></i>' +
                '<span ng-hide="block" class="label" style="color: {{coloset.frontColor}}; background-color: {{coloset.backColor}}">' +
                '<ng-transclude></ng-transclude>' +
                '</span>' +
                '</span>',
                controller: function ($scope) {
                    if (angular.isUndefined($scope.colorSet)) {
                        $scope.colorSet = {};
                        $scope.colorSet.frontColor = '#ffffff';
                        $scope.colorSet.backColor = '#000000';
                    }

                }
            };
        });
})