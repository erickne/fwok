'use strict';

angular.module(APP_MODULE)
  .config(function ($stateProvider) {
    $stateProvider
      .state('app.logs-log-files', {
        url: '/log-files',
        templateUrl: 'app/views/log-files/list.html',
        controller: 'LogFilesListCtrl'
      })
      .state('app.logs-log-files-view', {
        url: '/log-files/:logFileName',
        templateUrl: 'app/views/log-files/view.html',
        controller: 'LogFilesViewCtrl'
      })
  });