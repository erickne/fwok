'use strict';

angular.module(APP_MODULE)
    .controller('LogFilesListCtrl', function ($scope, LogFiles, NgTableParams, $location, $rootScope) {
        $scope.headerTile = "Log Files";
        $scope.headerSubTile = "View system logs";
        $scope.logFiles = [];

        $scope.tableParams = new NgTableParams({}, {
            getData: function ($defer, params) {
                $scope.busy = LogFiles.getAll().success(function (data) {
                    $defer.resolve(data.records);
                    $scope.logFiles = data.records;
                })
            }
        });

        $scope.go = function (object) {
            $location.path('/app/' + object.object + '/' + object.name);
        };

        $scope.delete = function (idx,event) {
            var logFile = $scope.logFiles[idx];

            LogFiles.deleteFile(logFile.name).success(function (data) {
                $scope.logFiles.splice(idx, 1);
            });
            $rootScope.cancel(event);
        }
    })
    .controller('LogFilesViewCtrl', function ($scope, $stateParams, LogFiles) {
        $scope.logFileName = $stateParams.logFileName;

        $scope.headerTile = "Log Files";
        $scope.headerSubTile = $scope.logFileName;

        var params = {};
        $scope.busy = LogFiles.getFile($scope.logFileName, params).success(function (data) {
            $scope.logContent = data.records;
        })
            
    });
