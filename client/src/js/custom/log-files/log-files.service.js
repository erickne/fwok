'use strict';

angular.module(APP_MODULE)
    .service('LogFiles', function ($http) {

        this.getAll = function (params) {
            return $http.get('fwok/log-files', {params: params});
        };
        this.getFile = function(filename,params) {
            return $http.get('fwok/log-files/'+filename, {params: params});
        };
        this.deleteFile = function(filename,params) {
            return $http.delete('fwok/log-files/'+filename, {params: params});
        };
        
    })
;