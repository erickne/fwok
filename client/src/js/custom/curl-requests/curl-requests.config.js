'use strict';

angular.module(APP_MODULE)
  .config(function ($stateProvider) {
    $stateProvider
      .state('app.integrations-curlrequests', {
        url: '/curl-requests',
        templateUrl: 'app/views/curl-requests/list.html',
        controller: 'CurlRequestsListCtrl'
      })
        .state('app.integrations-curlrequests-view', {
            url: '/curl-requests/:id',
            templateUrl: 'app/views/curl-requests/view.html',
            controller: 'CurlRequestsViewCtrl'
        })
  });