'use strict';

angular.module(APP_MODULE)
    .controller('CurlRequestsListCtrl', function ($scope, utils, CurlRequests, NgTableParams, ngTableDefaults) {
        $scope.headerTile = "cURL Requests";
        $scope.headerSubTile = "View cUrl requests made by system";
        $scope.curlRequests = [];

        $scope.tableParams = new NgTableParams(ngTableDefaults, {
            total: 10,
            getData: function (params) {
                return $scope.busy = CurlRequests.getAll(utils.convertNgTableToFwokParams(params)).success(function (data) {
                    $scope.curlRequests = data.records;
                }).then(function (response) {
                    params.total(response.data.meta.count_max);
                    $scope.headerSubTile = "View system cUrl Requests. Total [" + response.data.meta.count_max + "] rows";
                    return $scope.curlRequests;
                })
            }
        });
    })
    .controller('CurlRequestsViewCtrl', function ($scope, $stateParams, CurlRequests) {
        $scope.curlRequest = {};
        $scope.headerTile = "cURL Requests";
        $scope.headerSubTile = "";

        $scope.busy = CurlRequests.getId($stateParams.id, {}).success(function (data) {
            $scope.curlRequest = data.records;
        })

    });
