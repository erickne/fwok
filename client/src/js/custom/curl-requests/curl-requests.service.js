'use strict';

angular.module(APP_MODULE)
    .service('CurlRequests', function ($http) {

        this.getAll = function (params) {
            return $http.get('fwok/curlrequests', {params: params});
        };
        this.getId = function (id, params) {
            return $http.get('fwok/curlrequests/'+id, {params: params});
        };
        
    })
;