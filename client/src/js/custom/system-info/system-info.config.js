'use strict';

angular.module(APP_MODULE)
  .config(function ($stateProvider) {
    $stateProvider
      .state('app.system-info', {
        url: '/system-info',
        templateUrl: 'app/views/system-info/list.html',
        controller: 'SystemInfoListCtrl'
      })
  });