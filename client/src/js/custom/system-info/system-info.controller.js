'use strict';

angular.module(APP_MODULE)
  .controller('SystemInfoListCtrl', function ($scope, SystemInfo, NgTableParams) {
    $scope.headerTile = "System Information";
    $scope.headerSubTile = "View all system and server information";
    $scope.systemInfo = [];

    $scope.tableParams = new NgTableParams({}, {
      getData: function ($defer, params) {
        $scope.busy = SystemInfo.getAll().success(function (data) {
          $defer.resolve(data.records);
          $scope.systemInfo = data.records;
        })
      }
    });
  });
