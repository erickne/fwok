'use strict';

angular.module(APP_MODULE)
    .service('SystemInfo', function ($http) {

        this.getAll = function (params) {
            return $http.get('fwok/system-info', {params: params});
        };
        
    })
;