'use strict';

angular.module(APP_MODULE)
    .controller('HooksListCtrl', function ($scope, utils, Hooks, NgTableParams, ngTableDefaults) {
        $scope.headerTile = Hooks.headerTitle;
        $scope.headerSubTile = Hooks.headerSubTitle;
        $scope.hooks = [];

        $scope.tableParams = new NgTableParams(ngTableDefaults, {
            getData: function (params) {
                return $scope.busy = Hooks.getAll(utils.convertNgTableToFwokParams(params)).success(function (data) {
                    $scope.hooks = data.records;
                }).then(function(response){
                    params.total(response.data.meta.count_max);
                    $scope.headerSubTile = Hooks.headerSubTitle+" Total ["+response.data.meta.count_max+"] rows";
                    return $scope.hooks;
                })
            }
        });

    })
    .controller('HooksViewCtrl', function ($scope, $stateParams, Hooks) {
        $scope.headerTile = Hooks.headerTitle;
        $scope.headerSubTile = "";
        $scope.hook = {};

        var params = {};
        $scope.busy = Hooks.getId($stateParams.id, params).success(function (data) {
            $scope.hook = data.records;
        })

    });
