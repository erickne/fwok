'use strict';

angular.module(APP_MODULE)
  .config(function ($stateProvider) {
    $stateProvider
      .state('app.integrations-hooks', {
        url: '/hooks',
        templateUrl: 'app/views/hooks/list.html',
        controller: 'HooksListCtrl'
      })
        .state('app.integrations-hooks-view', {
            url: '/hooks/:id',
            templateUrl: 'app/views/hooks/view.html',
            controller: 'HooksViewCtrl'
        })
  });