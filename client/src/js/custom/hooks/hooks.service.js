'use strict';

angular.module(APP_MODULE)
    .service('Hooks', function ($http) {

        this.headerTitle = "WebHooks";
        this.headerSubTitle = "Received notification hooks from other servers";
        
        this.getAll = function (params) {
            return $http.get('fwok/hooks', {params: params});
        };
        this.getId = function (id, params) {
            return $http.get('fwok/hooks/'+id, {params: params});
        };
        
    })
;