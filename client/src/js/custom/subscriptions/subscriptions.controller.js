'use strict';

angular.module(APP_MODULE)
    .controller('SubscriptionsListCtrl', function ($scope, Subscriptions, NgTableParams, ngTableDefaults) {
        $scope.headerTile = Subscriptions.headerTitle;
        $scope.headerSubTile = Subscriptions.headerSubTitle;
        $scope.subscriptions = [];

        $scope.busy = Subscriptions.getAll().success(function (data) {
            $scope.subscriptions = data.records;
            $scope.tableParams = new NgTableParams(ngTableDefaults, {
                data: $scope.subscriptions
            });
        });

    })
    .controller('SubscriptionsViewCtrl', function ($scope, $stateParams, Subscriptions) {
        $scope.headerTile = Subscriptions.headerTitle;
        $scope.headerSubTile = "";
        $scope.subscription = {};

        var params = {};
        $scope.busy = Subscriptions.getId($stateParams.id, params).success(function (data) {
            $scope.subscription = data.records;
        })

    });
