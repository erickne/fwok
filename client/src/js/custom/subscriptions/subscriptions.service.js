'use strict';

angular.module(APP_MODULE)
    .service('Subscriptions', function ($http) {

        this.headerTitle = "Subscriptions";
        this.headerSubTitle = "Manage system subscriptions";

        this.getAll = function (params) {
            return $http.get('fwok/subscriptions', {params: params});
        };
        this.getId = function (id, params) {
            return $http.get('fwok/subscriptions/'+id, {params: params});
        };
        
    })
;