'use strict';

angular.module(APP_MODULE)
    .config(function ($stateProvider) {
        $stateProvider
            .state('app.financial-subscriptions', {
                url: '/subscriptions',
                templateUrl: 'app/views/subscriptions/list.html',
                controller: 'SubscriptionsListCtrl'
            })
            .state('app.financial-subscriptions-view', {
                url: '/subscriptions/:id',
                templateUrl: 'app/views/subscriptions/view.html',
                controller: 'SubscriptionsViewCtrl'
            })
    });