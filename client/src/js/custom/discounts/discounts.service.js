'use strict';

angular.module(APP_MODULE)
    .service('Discount', function ($http) {

        this.getAll = function (params) {
            return $http.get('fwok/discounts', {params: params});
        };
        
    })
;