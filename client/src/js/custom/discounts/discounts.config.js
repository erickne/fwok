'use strict';

angular.module(APP_MODULE)
  .config(function ($stateProvider) {
    $stateProvider
      .state('app.financial-discounts', {
        url: '/discounts',
        templateUrl: 'app/views/discounts/list.html',
        controller: 'DiscountsListCtrl'
      })
  });