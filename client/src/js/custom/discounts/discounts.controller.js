'use strict';

angular.module(APP_MODULE)
  .controller('DiscountsListCtrl', function ($scope, Discount, NgTableParams) {
    $scope.headerTile = "Discounts";
    $scope.headerSubTile = "Manage payment and subscriptions discounts";
    $scope.discounts = [];

    $scope.tableParams = new NgTableParams({}, {
      getData: function ($defer, params) {
        $scope.busy = Discount.getAll().success(function (data) {
          $defer.resolve(data.records);
          $scope.discounts = data.records;
        })
      }
    });
  });
