'use strict';

angular.module(APP_MODULE)
    .config(function ($stateProvider) {
        $stateProvider

            .state('app.dashboard', {
                url: '/dashboard',
                templateUrl: 'app/views/dashboards/dashboard.html',
                controller: 'DashboardController'
            })
    });