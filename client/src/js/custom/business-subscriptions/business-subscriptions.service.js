'use strict';

angular.module(APP_MODULE)
    .service('BusinessSubscriptions', function ($http) {

        this.headerTitle = "Business Subscriptions";
        this.headerSubTitle = "Manage Business Subscriptions";
        
        this.getAll = function (params) {
            return $http.get('fwok/business-subscriptions', {params: params});
        };
        this.getId = function (id, params) {
            return $http.get('fwok/business-subscriptions/'+id, {params: params});
        };
        this.refreshStatus = function (id, params) {
            return $http.get('fwok/business-subscriptions/refresh/'+id, {params: params});
        };
        
    })
;