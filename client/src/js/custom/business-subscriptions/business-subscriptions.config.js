'use strict';

angular.module(APP_MODULE)
    .config(function ($stateProvider) {
        $stateProvider
            .state('app.users-business-subscriptions', {
                url: '/business-subscriptions',
                templateUrl: 'app/views/business-subscriptions/list.html',
                controller: 'BusinessSubscriptionsListCtrl'
            })
            .state('app.users-business-subscriptions-view', {
                url: '/business-subscriptions/:id',
                templateUrl: 'app/views/business-subscriptions/view.html',
                controller: 'BusinessSubscriptionsViewCtrl'
            })
    });