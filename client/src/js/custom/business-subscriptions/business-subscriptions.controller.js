'use strict';

angular.module(APP_MODULE)
    .controller('BusinessSubscriptionsListCtrl', function ($scope, utils, BusinessSubscriptions, NgTableParams, ngTableDefaults) {
        $scope.headerTile = BusinessSubscriptions.headerTitle;
        $scope.headerSubTile = BusinessSubscriptions.headerSubTitle;
        $scope.businessSubscriptions = [];

        $scope.tableParams = new NgTableParams(ngTableDefaults, {
            getData: function (params) {
                return $scope.busy = BusinessSubscriptions.getAll(utils.convertNgTableToFwokParams(params)).success(function (data) {
                    $scope.businessSubscriptions = data.records;
                }).then(function(response){
                    params.total(response.data.meta.count_max);
                    $scope.headerSubTile = BusinessSubscriptions.headerSubTitle+" Total ["+response.data.meta.count_max+"] rows";
                    return $scope.businessSubscriptions;
                })
            }
        });

    })
    .controller('BusinessSubscriptionsViewCtrl', function ($scope, $stateParams, BusinessSubscriptions) {
        $scope.headerTile = BusinessSubscriptions.headerTitle;
        $scope.headerSubTile = "";
        $scope.businessSubscription = {};

        var params = {};
        $scope.busy = BusinessSubscriptions.getId($stateParams.id, params).success(function (data) {
            $scope.businessSubscription = data.records;
        })

        $scope.refreshStatus = function() {
            $scope.busy = BusinessSubscriptions.refreshStatus($scope.businessSubscription.id).success(function(data){
                $scope.statusResponse = data.records;
            })
        };

        $scope.statusResponse = {}

    });
