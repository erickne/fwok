'use strict';

angular.module(APP_MODULE)
    .controller('LoginCtrl', function ($scope, Auth, $location, $window, $rootScope) {
        $scope.user = {};
        $scope.errors = {};

        function init() {
            if(Auth.isLoggedIn()) {
                $location.path('/');
            }else{
                // Certifies that user is logged out and all variables cleared
                Auth.logout();
            }
        }
        init();

        $scope.login = function (form) {
            $scope.submitted = true;

            if (form.$valid) {
                Auth.login({
                    email: $scope.user.email,
                    password: $scope.user.password
                })
                    .then(function () {
                        // Logged in, redirect to home
                        $location.path('/');
                    })
                    .catch(function (err) {
                        $scope.errors.other = err.message;
                    });
            }
        };

        $scope.loginOauth = function (provider) {
            $window.location.href = '/auth/' + provider;
        };
    });
