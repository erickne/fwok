'use strict';

angular.module(APP_MODULE)
    .controller('LogoutCtrl', function ($scope, Auth, $location, $window, $rootScope) {

        function init() {
            Auth.logout();
            $location.path('/auth/login')
        }
        init();
    });
