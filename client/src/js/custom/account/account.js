'use strict';

angular.module(APP_MODULE)
    .config(function ($stateProvider) {
        $stateProvider
            .state('auth.login', {url: '/login', templateUrl: 'app/views/account/login.html', controller: 'LoginCtrl'})
            .state('auth.logout', {url: '/logout', template: 'logout...', controller: 'LogoutCtrl'})
            .state('auth.signup', {
                url: '/signup',
                templateUrl: 'app/views/account/signup.html',
                controller: 'SignupCtrl'
            })
    });