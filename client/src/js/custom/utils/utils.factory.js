'use strict';

angular.module(APP_MODULE)
    .factory('utils', function () {
        return {
            convertNgTableToFwokParams: convertNgTableToFwokParams,
            convertStringFromCamelCaseToLowUnderscore:convertStringFromCamelCaseToLowUnderscore
        };
        function convertNgTableToFwokParams(ngTableParams) {
            /*
             * var params = {
             *      'where' : JSON.stringify({
             *         'dt_end': {'date_gt': moment().add(30, 'days').format('YYYY-MM-DD')},
             *         'name': {'l': 'Empresa'},
             *         'deleted': {'in': [0,1]}
             *      }),
             *      'order' : JSON.stringify(['-dt_end','+id'])
             *   }
             */

            var filter = ngTableParams.filter();
            var sorting = ngTableParams.sorting();
            var count = ngTableParams.count();
            var page = ngTableParams.page();
            //console.log('filter: ', filter, 'sorting: ', sorting, 'count: ', count, 'page: ', page);
            var params = {};

            Object.prototype.isEmpty = function() {
                for(var key in this) {
                    if(this.hasOwnProperty(key))
                        return false;
                }
                return true;
            }

            if (!filter.isEmpty()) {
                params.where = {};
                angular.forEach(ngTableParams.filter(), function (value, field) {
                    if(!!value) {
                        params.where[convertStringFromCamelCaseToLowUnderscore(field)] = {'l': value}
                    }
                });
                params.where = JSON.stringify(params.where);
            }
            if (!sorting.isEmpty()) {
                params.order = [];
                angular.forEach(ngTableParams.sorting(), function (direction, field) {
                    field = convertStringFromCamelCaseToLowUnderscore(field);
                    if(direction == 'asc'){
                        params.order.push('+'+field)
                    }else{
                        params.order.push('-'+field)
                    }
                });
                params.order = JSON.stringify(params.order);
            }
            if (page) {
                params.page = page;
            }
            if (count) {
                params.count = count;
            }

            //console.log('Final Params:',params)

            return params;
        }

        /**
         * Converts camelCase strings to low_underscore
         *
         * Alternativa?
         * https://github.com/domchristie/humps
         *
         * @param string
         * @returns {string}
         */
        function convertStringFromCamelCaseToLowUnderscore(string)
        {
            return string.replace(/([a-zA-Z])(?=[A-Z])/g, '$1_').toLowerCase();
        }
    })


